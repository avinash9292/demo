package com.socialide.AsyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.socialide.Activities.MainActivity;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.Mypreferences;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by Akram on 11/8/2017.
 */

public class AsyncRequestSearch extends AsyncTask<String, Integer, String> {
    OnAsyncRequestComplete caller;
    Activity context;
    String method = "GET";
    Fragment f;
    AVLoadingIndicatorView view = null;
    List<NameValuePair> parameters = null;
    ProgressDialog pDialog = null;
    int checkProgress = 0;
    ProgressBar viewProgress;

    // Three Constructors
    public AsyncRequestSearch(OnAsyncRequestComplete c, Activity a, String m, List<NameValuePair> p) {
        caller = (OnAsyncRequestComplete) c;
        context = a;

        method = m;
        parameters = p;
    }

    public AsyncRequestSearch(OnAsyncRequestComplete c, Activity a, String m, List<NameValuePair> p, AVLoadingIndicatorView v) {
        caller = (OnAsyncRequestComplete) c;
        context = a;
        view = v;

        method = m;
        parameters = p;
    }

    int checkSetting = 0;

    public AsyncRequestSearch(OnAsyncRequestComplete c, Activity a, String m, List<NameValuePair> p, AVLoadingIndicatorView v, int checkSetting) {
        caller = (OnAsyncRequestComplete) c;
        context = a;
        view = v;

        method = m;
        parameters = p;
        this.checkSetting = checkSetting;
    }


    public AsyncRequestSearch(OnAsyncRequestComplete c, Activity a, String m, List<NameValuePair> p, AVLoadingIndicatorView v, int checkProgress, ProgressBar viewProgress) {
        caller = (OnAsyncRequestComplete) c;
        context = a;
        view = v;
        this.checkProgress = checkProgress;
        this.viewProgress = viewProgress;

        method = m;
        parameters = p;
    }

    public AsyncRequestSearch(Activity a, String m, List<NameValuePair> p) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
        parameters = p;
    }

    public AsyncRequestSearch(Activity a, String m, List<NameValuePair> p, AVLoadingIndicatorView v) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
        view = v;

        parameters = p;
    }

    public AsyncRequestSearch(Activity a, String m, List<NameValuePair> p, AVLoadingIndicatorView v,ProgressBar viewProgress) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
        view = v;
        this.checkProgress = checkProgress;
        parameters = p;
    }

    public AsyncRequestSearch(Activity a, String m) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
    }

    public AsyncRequestSearch(Activity a) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
    }


    // Interface to be implemented by calling activity
    public interface OnAsyncRequestComplete {
        public void asyncResponseSearch(String response);
    }

    public String doInBackground(String... urls) {
        // get url pointing to entry point of API
        String address = urls[0].toString();
        if (method == "POST") {
            return post(address);
        }

        if (method == "GET") {
            return get(address);
        }

        return null;
    }

    public void onPreExecute() {
        pDialog = new ProgressDialog(context);

        if (view != null) {


            view.setVisibility(View.VISIBLE);
            view.show();
        }
        MainActivity.checkClick = 1;
        if (checkSetting == 1) {
            MainActivity.img_setting.setVisibility(View.GONE);
        }
        // pDialog.setContentView(v);
//        LineSpinFadeLoaderIndicator a= new LineSpinFadeLoaderIndicator();
//        a.setG
        //  pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.

        CheckInternet mCheckInternet = new CheckInternet();
        if (!mCheckInternet.isConnectingToInternet(context)) {

            if (view != null) {


                view.setVisibility(View.GONE);
                view.hide();
            }
            if (checkSetting == 1) {
                MainActivity.img_setting.setVisibility(View.VISIBLE);
            }
            MainActivity.checkClick = 0;

            CustomSnackBar.toast(context, "Please connect to internet!");

        } else {
            //  pDialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);

            //pDialog.show();
        }
    }

    public void onProgressUpdate(Integer... progress) {
        // you can implement some progressBar and update it in this record
        // setProgressPercent(progress[0]);
    }

    public void onPostExecute(String response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if (view != null) {

            view.setVisibility(View.GONE);
            view.hide();
        }
        MainActivity.checkClick = 0;
        if (checkSetting == 1&&MainActivity.tabPosition==4) {
            MainActivity.img_setting.setVisibility(View.VISIBLE);
        } else {
            if (MainActivity.img_setting != null)
                MainActivity.img_setting.setVisibility(View.GONE);
        }
        CheckInternet mCheckInternet = new CheckInternet();
        if (!mCheckInternet.isConnectingToInternet(context)) {

            CustomSnackBar.toast(context, "Please connect to internet!");

        } else {
            if (response!=null)
                caller.asyncResponseSearch(response);
            else
                CustomSnackBar.toast(context, "Please check your internet connection");
        }
    }

    protected void onCancelled(String response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if (view != null) {


            view.setVisibility(View.GONE);
            view.hide();
        }
        caller.asyncResponseSearch(response);
    }

    @SuppressWarnings("deprecation")
    private String get(String address) {
        try {

            if (parameters != null) {

                String query = "";
                String EQ = "=";
                String AMP = "&";
                for (NameValuePair param : parameters) {
                    query += param.getName() + EQ + URLEncoder.encode(param.getValue()) + AMP;
                }

                if (query != "") {
                    address += "?" + query;
                }
            }
            CheckInternet mCheckInternet = new CheckInternet();
            if (!mCheckInternet.isConnectingToInternet(context)) {


                CustomSnackBar.toast(context, "Please connect to internet!");

            } else {
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(address);

                HttpResponse response = client.execute(get);
                return stringifyResponse(response);
            }
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }

        return null;
    }

    private String post(String address) {
        try {

            Log.v("akram", "url = " + address);

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(address);
            post.addHeader("access_token", Mypreferences.Access_token);
            JSONObject jsonObject = new JSONObject();
            for (int i = 0; i < parameters.size(); i++) {

                jsonObject.accumulate(parameters.get(i).getName(), parameters.get(i).getValue());
            }


            if (parameters != null) {
                Log.e("Request", jsonObject.toString());
                Log.v("akram", jsonObject.toString());
            }

            if (parameters != null) {
                post.setEntity(new UrlEncodedFormEntity(parameters));
            }

            HttpResponse response = client.execute(post);
            return stringifyResponse(response);

        } catch (ClientProtocolException e) {

            // TODO Auto-generated catch block
        } catch (IOException e) {

            // TODO Auto-generated catch block
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return null;
    }

    private String stringifyResponse(HttpResponse response) {
        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();

            return sb.toString();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }
}

