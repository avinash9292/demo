package com.socialide.AsyncTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by info1010 on 9/28/2017.
 */

public class ImageConvertAsync extends AsyncTask<Void, Void, Void> {
    Uri uri_new = null;
    ProgressDialog dialog;

    ArrayList<String> strings;
    Context context;
    String text;

    ArrayList<Uri> filesUri = new ArrayList<>();

    public ImageConvertAsync(ArrayList<String> strings, String text, Context context) {
        this.strings = strings;
        this.context = context;
        this.text = text;
    }

    @Override
    protected void onPreExecute() {

        dialog = new ProgressDialog(context);

        String s = "Photos Sharing may take a while,\nplease be patient.";
        SpannableString ss2 = new SpannableString(s);
        ss2.setSpan(new RelativeSizeSpan(1.1f), 0, ss2.length(), 0);
        dialog.setMessage(ss2);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                cancel(true);

            }
        });

        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Bitmap image;

        for (int i = 0; i < strings.size(); i++) {

            try {
                URL url = new URL(strings.get(i));
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                uri_new = getLocalBitmapUri(image);
                filesUri.add(uri_new);

            } catch (IOException e) {

                System.out.println(e);
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

        dialog.dismiss();
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, text);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Socialide");
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, filesUri);
        intent.setType("image/*");
        context.startActivity(Intent.createChooser(intent, "Share Image"));

        // Intent i = new Intent(Intent.ACTION_SEND_MULTIPLE);
        // i.setType("image/jpeg");
        // i.putExtra(Intent.EXTRA_STREAM, uri);
        super.onPostExecute(aVoid);
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
