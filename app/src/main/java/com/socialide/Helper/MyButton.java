package com.socialide.Helper;

/**
 * Created by DELL on 28-Jun-16.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.util.Hashtable;


public class MyButton extends Button {

    Context context;
    String ttfName;

    String TAG = getClass().getName();

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        for (int i = 0; i < attrs.getAttributeCount(); i++) {
           // Log.i(TAG, attrs.getAttributeName(i));

            init();
        }

    }

    private void init() {
      //  Typeface font = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
        Controller application = (Controller) getContext().getApplicationContext();
        setTypeface(application.getArialFont1());
    }

    @Override
    public void setTypeface(Typeface tf) {

        // TODO Auto-generated method stub
        super.setTypeface(tf);
    }
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();


    public static Typeface getFont(Context c, String assetPath) {
        synchronized (cache) {

            if (!cache.containsKey(assetPath)) {

                try {
                    Typeface t = (Typeface.createFromAsset(c.getAssets(),
                            "OpenSans-Regular.ttf"));

                    cache.put(assetPath, t);

                } catch (Exception e) {
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}