package com.socialide.Helper;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.socialide.R;

import java.net.URL;

/**
 * Created by Avinash on 5/22/2017.
 */
public class ImageLoader {

static DisplayImageOptions options;
    public static void image(String path, ImageView imgVIew, final DonutProgress spinner, Activity _activity)
    {

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profilepic)
                .showImageOnFail(R.drawable.profilepic)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .resetViewBeforeLoading(true)

                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(0))
                .build();

        com.nostra13.universalimageloader.core.ImageLoader imageLoader= com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(_activity));



        imageLoader.displayImage(path, imgVIew, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                int count;
                URL url = null;

                spinner.setVisibility(View.VISIBLE);

                spinner.setMax(100);


                spinner.setProgress(0);

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }
              //  Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();

                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                spinner.setVisibility(View.GONE);
            }


        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {


                spinner.setProgress(Math.round(100.0f * current / total));
            }
        });


    }



    public static void image(String path, ImageView imgVIew, final DonutProgress spinner, final Activity _activity, final ImageView rl)
    {

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profilepic)
                .showImageOnFail(R.drawable.profilepic)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .resetViewBeforeLoading(true)

                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(0))
                .build();

        com.nostra13.universalimageloader.core.ImageLoader imageLoader= com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(_activity));



        imageLoader.displayImage(path, imgVIew, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                int count;
                URL url = null;

                spinner.setVisibility(View.VISIBLE);

                spinner.setMax(100);


                spinner.setProgress(0);

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }
                //  Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();

                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view,final Bitmap loadedImage) {
                spinner.setVisibility(View.GONE);

                rl.setImageBitmap(loadedImage);

            }


        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

                Log.e("current", "aa ;   " + current);
                Log.e("total","total ;   "+total);
                spinner.setProgress(Math.round(100.0f * current / total));
            }
        });


    }
}
