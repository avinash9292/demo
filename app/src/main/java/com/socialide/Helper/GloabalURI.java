package com.socialide.Helper;

/**
 * Created by Avinash on 5/5/2017.
 */

public class GloabalURI {
    /*global server base url*/

    public static String baseURI = "http://socialideapi.infoenum.com/";
    //   public static String baseURI = "http://socialide.infoenum.com/";
    public static String ImageBaseUri = "http://socialide.infoenum.com/uploads/";
    public static String SIGNUP = "signUp.php";
    public static String LOGIN = "UserLogin.php";
    public static String LOGOUT = "logout.php";
    public static String uploadEventPhotos = "uploadEventPhotos.php";
    public static String FORGOT_PASSWORD = "ForgotPassword.php";
    public static String UPDATE_ORGANIZATION = "updateOrganizationlProfile.php";
    public static String UPDATE_PROFESSIONAL = "updateProfessionalProfile.php";
    public static String GET_ACTIVITIES_DETAILS = "getActivityDetails.php";
    public static String GET_SCHEDULED_EVENTS = "getScheduledEventsByUserId.php";
    public static String GET_PENDING_EVENTS = "getPendingEventsByUserId.php";
    public static String GET_DECLINE_EVENTS = "getDeclinedEventsByUserId.php";
    public static String LIKE_DISLIKE = "likeDislikeActivity.php";
    public static String DECLINE_AN_EVENT = "declinePendingEvent.php";
    public static String ACCEPT_AN_EVENT = "acceptPendingEvent.php";
    public static String REMOVE_AN_EVENT = "removeDeclinedEvent.php";
    public static String MY_PROFILE_DETAIL = "getMyProfileDetails.php";
    public static String DELETE_ARTICLE = "deleteActivity.php";
    public static String reportActivity = "reportActivity.php";
    public static String DELETE_EVENT_PHOTO = "deleteEventPhoto.php";
    public static String TOP10WEEKLYUSER = "getWeeklyUsersByUserId.php";
    public static String TOP10USER = "getOverallUsersByUserId.php";
    public static String MYACTIVITY = "getMyPosts.php";
    // public static String MYACTIVITY = "getActivityDetails.php";
    public static String MYPASTEVENT = "getPastEventsByUserId.php";
    public static String GETPROFILEDETAIL = "getProfileDetailsByUserId.php";
    public static String getEvent = "getPastEventsByUserId.php";
    public static String getSetting = "getuserSettingDetailsByUserId.php";
    public static String uploadSetting = "updateSettings.php";
    public static String verifyEmail = "verifyEmail.php";
    public static String getMobileVerificationCode = "getMobileVerificationCode.php";
    public static String getPublicProfilByUserId = "getPublicProfilByUserId.php";
    public static String favoriteUnfavoriteUser = "favoriteUnfavoriteUser.php";
    public static String getFavoriteUser = "getFavoriteUser.php";
    public static String searchProfessional = "searchProfessional.php";
    public static String requestForEvent = "requestForEvent.php";
    public static String addArticle = "addArticle.php";
    public static String readAllNotificationMessage = "readAllNotificationMessage.php";
    public static String deleteAllNotificationMessage = "deleteAllNotificationMessage.php";
    public static String getNotificationMessage = "getNotificationMessage.php";
    public static String readNotificationMessage = "readNotificationMessage.php";
    public static String deleteNotificationMessage = "deleteNotificationMessage.php";
    public static String updateShareContact = "updateShareContact.php";
}
