package com.socialide.Helper;

/**
 * Created by Avinash on 4/15/2017.
 */
public interface DrawableClickListener {
    public static enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
    public void onClick(DrawablePosition target);
}
