package com.socialide.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by Avinash on 5/19/2017.
 */
public class DeclinePopup {

    static PopupWindow mPopupWindow;

    static String s = "";

    public static void popup(final Activity c, final AsyncRequest.OnAsyncRequestComplete d, RelativeLayout mRelativeLayout, final String eventid, final AVLoadingIndicatorView viw) {
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        View popupView = LayoutInflater.from(c).inflate(R.layout.decline_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );

        mPopupWindow.setFocusable(true);
        //    mPopupWindows.update();
        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        MyButton submit = (MyButton) popupView.findViewById(R.id.btn_submit);
        ImageView cross = (ImageView) popupView.findViewById(R.id.img_cross);
        final MyEditTextView editText = (MyEditTextView) popupView.findViewById(R.id.edt_reason);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        // Set a click listener for the popup window close button
        submit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {

                if (!("" + editText.getText().toString()).equals("")) {
                    // Dismiss the popup window

                    params.add(new BasicNameValuePair("eventId", "" + eventid));
                    params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                    params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                    params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
                    params.add(new BasicNameValuePair("message", "" + editText.getText().toString()));
                    mPopupWindow.dismiss();
                    AsyncRequest getPosts = new AsyncRequest(d, c, "POST", params, viw);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.DECLINE_AN_EVENT);


                    s = editText.getText().toString();
                } else {
                    CustomSnackBar.toast(c, "Please insert to decline this event");
                }

            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();

            }
        });

        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
    }


}
