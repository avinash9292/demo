package com.socialide.Helper;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.socialide.Model.BeanForProfessionalCategory;
import com.socialide.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Avinash on 10/12/2017.
 */

public class getDropdownData {


    public static ArrayList<BeanForProfessionalCategory> getStateList(Context c)
    {
        ArrayList<BeanForProfessionalCategory> cat_list=new ArrayList<>();
        int i=0;
        try {
            JSONArray obj = new JSONArray(readJSONFromAsset(c));
            for (int i1 = 0; i1 < obj.length(); i1++) {
                JSONObject j=obj.getJSONObject(i1);
                ArrayList<String> al= new ArrayList<>();
               // BeanForProfessionalCategory bean=new BeanForProfessionalCategory();

                JSONArray ja= j.getJSONArray("SubCategories");

                for (int i2 = 0; i2 < ja.length(); i2++) {

                    al.add(""+ja.getJSONObject(i2).getString("Name"));
                }

                cat_list.add(new BeanForProfessionalCategory(""+j.getString("Name"),al));

            }






        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(""+cat_list.size(),""+cat_list.size());
        return  cat_list;

    }


    public static String readJSONFromAsset(Context c) {
        String json = null;
        try {
            InputStream is = c.getAssets().open("job_caregory.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
