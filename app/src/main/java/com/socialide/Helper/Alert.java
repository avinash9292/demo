package com.socialide.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Avinash on 4/19/2017.
 */
public class Alert {


    public static void alert(Activity c,String msg)
    {

        AlertDialog alertDialog = new AlertDialog.Builder(c).create();
        alertDialog.setTitle("Warning");
        alertDialog.setMessage(""+msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
