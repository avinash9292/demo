package com.socialide.Helper;
import android.app.Application;
import android.graphics.Typeface;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;



public class Controller extends Application{
	

    Typeface 	arialFont,arialFont1;
    

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        // Initialize the SDK before executing any other operations,

        arialFont = Typeface.createFromAsset(getAssets(),  "museosanscyrl.ttf");

        arialFont1 = Typeface.createFromAsset(getAssets(),  "OpenSans-Regular.ttf");

    }
    
    

    public Typeface getArialFont() {
        return arialFont;
    }
    public Typeface getArialFont1() {
        return arialFont1;
    }

   
}
