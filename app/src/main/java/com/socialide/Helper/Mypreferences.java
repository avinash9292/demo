package com.socialide.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;

/**
 * Created by Avinash on 3/30/2017.
 */
public class Mypreferences {

    public static String Email = "email";
    public static String Pass = "Pass";
    public static String User_Type = "user_type";
    public static Bitmap bitmap = null;
    public static String User_id = "User_id";
    public static String Access_token = "";

    public static String STABLISHMENT_YEAR = "STABLISHMENT_YEAR";
    public static String DOB = "DOB";
    public static String FIRST_NAME = "FIRST_NAME";
    public static String LAST_NAME = "LAST_NAME";
    public static String ORGANISATION_NAME = "ORGANISATION_NAME";
    public static String REGISTRATION_NUMBER = "REGISTRATION_NUMBER";
    public static String MOBILE_NUMBER = "MOBILE_NUMBER";
    public static String LANDLINE_NUMBER = "LANDLINE_NUMBER";
    public static String COUNTRY = "COUNTRY";
    public static String STATE = "STATE";
    public static String CITY = "CITY";
    public static String ABOUT_YOU = "ABOUT_YOU";
    public static String DESIGNATION = "DESIGNATION";
    public static String GENDER = "GENDER";
    public static String CATEGORY = "CATEGORY";
    public static String SUB_CATEGORY = "SUB_CATEGORY";
    public static String CURRENT_COMPANY = "CURRENT_COMPANY";
    public static String EXPERIENCE = "EXPERIENCE";
    public static String Maximum_Event_Request = "";
    public static String Social_Score = "";
    public static String Join_Flow_Completed = "";
    public static String Picture = "";
    public static String Address = "";
    public static String Availability = "";
    public static String Profession = "";
    public static String notificationCount = "notificationCount";
    public static int NotificationCount = 0;
    public static String isAvailable = "0";

    public static String Contact_position = "";


    public static String Request_an_event_from = "";

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;

    public static final int USE_ADDRESS_NAME = 1;
    public static final int USE_ADDRESS_LOCATION = 2;
    public static Double Lat = 0.0;
    public static Double Lang = 0.0;


    //  {"success":"true","message":"User login successful.","result":{"userid":"1","first_name":"Avinash","last_name":"Tiwari","profession":"","dob":"23\/5\/1992","experience":"","password":"123","email":"aa@aa.aa","mobile_number":"7509097959","state":"MP","city":"Indore","profile_pic":"","gender":"Male","country":"India","current_company":"","category":"","subcategory":"","about_me":"","maximum_event_request":"","availability":"","social_score":"0","join_flow_completed":"0"},"type":"Professional"}


    public static void setString(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setInt(String key, int value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static String getString(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }

    public static int getInt(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(key, 0);
    }

    public static void clear(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }
}
