package com.socialide.Helper;

/**
 * Created by DELL on 28-Jun-16.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import java.util.Hashtable;


public class MyAutoCompleteTextView extends AutoCompleteTextView {

    Context context;
    String ttfName;

    String TAG = getClass().getName();

    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        for (int i = 0; i < attrs.getAttributeCount(); i++) {
            Log.i(TAG, attrs.getAttributeName(i));

            init();
        }

    }

    private void init() {
      //  Typeface font = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
        Controller application = (Controller) getContext().getApplicationContext();
        setTypeface(application.getArialFont());
    }

    @Override
    public void setTypeface(Typeface tf) {

        // TODO Auto-generated method stub
        super.setTypeface(tf);
    }
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();


    public static Typeface getFont(Context c, String assetPath) {
        synchronized (cache) {

            if (!cache.containsKey(assetPath)) {

                try {
                    Typeface t = (Typeface.createFromAsset(c.getAssets(),
                            "museosanscyrl.ttf"));

                    cache.put(assetPath, t);

                } catch (Exception e) {
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}