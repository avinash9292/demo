package com.socialide.Helper;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.socialide.R;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Avinash on 4/3/2017.
 */
public class SpinnerData {

    public static ArrayList<String> genderList = new ArrayList<>();
    public static ArrayList<String> yearlist = new ArrayList<>();
    public static ArrayList<String> expreince_list = new ArrayList<>();

    public static ArrayList<String> getGenderAdapter(Context c) {
        genderList.clear();
        genderList.add("Male");
        genderList.add("Female");
        ArrayAdapter a = new ArrayAdapter(c, R.layout.spinner_item, R.id.txt, genderList);
        return genderList;
    }

    public static ArrayList<String> getStablishYearsList(Context c) {
        Calendar calendar;
        calendar = Calendar.getInstance();
        yearlist.clear();

        int Year = calendar.get(Calendar.YEAR);

        for (int i = 1901; i <= Year; i++) {
            yearlist.add(String.valueOf(i));
        }
        ArrayAdapter a = new ArrayAdapter(c, R.layout.spinner_item, R.id.txt, yearlist);
        return yearlist;
    }

    public static ArrayList<String> getexpYearsList(Context c) {

        expreince_list.clear();
        for (int i = 0; i <= 49; i++) {
            expreince_list.add(String.valueOf(i));
        }
        expreince_list.add("50+");
        ArrayAdapter a = new ArrayAdapter(c, R.layout.spinner_item, R.id.txt, expreince_list);
        return expreince_list;
    }
}
