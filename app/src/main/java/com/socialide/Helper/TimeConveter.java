package com.socialide.Helper;

import android.util.Log;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by Avinash on 5/26/2017.
 */
public class TimeConveter {
    public static String getdatetime(String dater,String time){
        DateFormat sdf = new SimpleDateFormat("dd MMM yyyy'T'h:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = sdf.parse(dater + "T" + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat pstFormat = new SimpleDateFormat("dd MMM yyyy'T'h:mm a");
        pstFormat.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        //    sdf.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getDisplayName()));
//               System.out.println(sdf.format(date));
        if (date==null){
            return "";
        }
        return ""+pstFormat.format(date);

    }

    public static String convertToUTC(String dater,String time){
        String lv_dateFormateInUTC=""; //Will hold the final converted date
        SimpleDateFormat lv_formatter = new SimpleDateFormat("dd MMM yyyy'T'h:mm a");
        Date date = null;
        try {
            date = lv_formatter.parse(dater + "T" + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        lv_formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        lv_dateFormateInUTC = lv_formatter.format(date);

        return lv_dateFormateInUTC;
    }


    public static String convertToUTC1(String dater,String time){
        String lv_dateFormateInUTC=""; //Will hold the final converted date
        SimpleDateFormat lv_formatter = new SimpleDateFormat("dd-MM-yyyy'T'h:mm a");
        Date date = null;
        try {

            date = lv_formatter.parse(dater + "T" + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        lv_formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        lv_dateFormateInUTC = lv_formatter.format(date);

        return lv_dateFormateInUTC;
    }



    public static String agotime(String dater,String time)
    {
//
//        DateFormat sdf = new SimpleDateFormat("dd MMM yyyy'T'h:mm a");
////        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//        Date date = null;
//        try {
//
//            date = sdf.parse(dater + "T" + time);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        PrettyTime p = new PrettyTime(new Date(System.currentTimeMillis()));
//
//
//
//       long i= System.currentTimeMillis()-getDateInMillis(dater+"T"+time);
//        i=i/1000;
//        if((i/60)>=1)
//
//        //System.out.println(p.format(new Date()));
//
//
//        Log.e("p.format",""+p.format(date));
//        return ""+p.format(date);

        return mail(getdatetime(dater,time));

    }



    public static long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "dd MMM yyyy'T'h:mm a");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {
         //   Log.d("Exception while parsing date. " + e.getMessage());
            e.printStackTrace();
        }

        return 0;
    }



    public static String mail(String srcDate){
String msg="";

            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy'T'h:mm a");
        Date past = null;
        try {
            past = format.parse(srcDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = new Date();
            long seconds=TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes= TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days=TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());
//
//          System.out.println(TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime()) + " milliseconds ago");
//          System.out.println(TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) + " minutes ago");
//          System.out.println(TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) + " hours ago");
//          System.out.println(TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) + " days ago");

            if(seconds<60)
            {
                msg=seconds+" seconds ago";
                System.out.println(seconds + " seconds ago");
                Log.e("seconds",seconds+" seconds ago");
            }
            else if(minutes<60)
            {
                msg=minutes+" minutes ago";
                System.out.println(minutes+" minutes ago");
                Log.e("minutes", minutes + " minutes ago");

            }
            else if(hours<24)
            {
                msg=hours+" hours ago";
                System.out.println(hours+" hours ago");
                Log.e("hours", hours + " hours ago");

            }
            else
            {
             msg=  days+" days ago";
                System.out.println(days+" days ago");
                Log.e("days", days + " days ago");

                if(days>=7){
                    msg="false";

                }

            }


        return  msg;
    }
    }

