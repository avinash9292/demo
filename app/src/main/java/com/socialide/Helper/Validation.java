package com.socialide.Helper;

/**
 * Created by Avinash on 4/1/2017.
 */
public class Validation {


    public static boolean isValidName(String text) {

        if (text.length() == 0) {
            return false;
        }
        return true;
    }

    public static boolean isValidPassword(String text) {

        if (text.length() < 1) {
            return false;
        }

        return true;
    }


    public final static boolean isValidEmail(String text) {
        if (text == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches();
        }
    }

    public static boolean isValidContact(String text) {
        if (text.length() < 1) {
            return false;
        }
        return true;
    }
}
