package com.socialide.Helper;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.androidadvance.topsnackbar.TSnackbar;
import com.socialide.R;

/**
 * Created by Avinash on 4/15/2017.
 */
public class CustomSnackBar {


    public static void toast(Activity c, String s) {

        TSnackbar snackbar = TSnackbar.make(c.findViewById(android.R.id.content), "" + s, TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);

        View snackbarView = snackbar.getView();
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT
//        );
//        params.setMargins(0, 48, 0, 0);
//        //snackbarView.setForegroundGravity(Gravity.NO_GRAVITY);
//        snackbarView.setLayoutParams(params);
        snackbarView.setBackgroundColor(Color.parseColor("#212121"));
        TextView MyTextView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        MyTextView.setGravity(Gravity.CENTER);
        Typeface tf = Typeface.createFromAsset(c.getAssets(), "museosanscyrl.ttf");
        MyTextView.setTypeface(tf, Typeface.BOLD);
        MyTextView.setTextSize((float) 18.0);
        MyTextView.setTextColor(Color.WHITE);
        snackbar.show();
    }


//
//    public static void toast(Service c,String s)
//    {
//
//        TSnackbar snackbar = TSnackbar.make(c.findViewById(android.R.id.content), "" + s, TSnackbar.LENGTH_LONG);
//        snackbar.setActionTextColor(Color.WHITE);
//
//        View snackbarView = snackbar.getView();
////        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
////                RelativeLayout.LayoutParams.MATCH_PARENT,
////                RelativeLayout.LayoutParams.WRAP_CONTENT
////        );
////        params.setMargins(0, 48, 0, 0);
////        //snackbarView.setForegroundGravity(Gravity.NO_GRAVITY);
////        snackbarView.setLayoutParams(params);
//        snackbarView.setBackgroundColor(Color.parseColor("#212121"));
//        TextView MyTextView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
//        MyTextView.setGravity(Gravity.CENTER);
//        Typeface tf = Typeface.createFromAsset(c.getAssets(), "museosanscyrl.ttf");
//        MyTextView.setTypeface(tf, Typeface.BOLD);
//        MyTextView.setTextSize((float) 18.0);
//        MyTextView.setTextColor(Color.WHITE);
//        snackbar.show();
//    }
}
