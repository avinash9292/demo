package com.socialide.Helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckInternet {

	
	public boolean isConnectingToInternet(Context c) {
		ConnectivityManager connectivity = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
					//	Toast.makeText(getApplicationContext(), "connection = " + NetworkInfo.BeanForState.CONNECTED, 1).show();
						return true;
					}

		}
		return false;
	}
}
