package com.socialide.Helper;

import android.view.View;

/**
 * Created by Avinash on 5/18/2017.
 */
public interface ClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
