package com.socialide.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.socialide.Activities.NotificationActivity;
import com.socialide.R;

/**
 * Created by info1010 on 9/15/2017.
 */

public class DialogClass {

    public Dialog emailVarification(final Context context, final Activity activity, final String msg) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_email_varification, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();
        MyTextView txt = (MyTextView) view.findViewById(R.id.tvMsg);
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);
        MyTextView tvTitleDialog = (MyTextView) view.findViewById(R.id.tvTitleDialog);

        txt.setText(msg);
        tvTitleDialog.setText("Verification Email");
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();
        return alertDialoge;

    }


    public Dialog mobileVarification(final Context context, final Activity activity) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_mobile_verification, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();
        MyEditTextView etEmail = (MyEditTextView) view.findViewById(R.id.etEmail);
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(false);
        alertDialoge.setCanceledOnTouchOutside(false);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;

    }

    public Dialog exitDialog(final Context context, final Activity activity, String text, String dialogTitle, final String fromWhere, final NotificationActivity notificationActivity) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view.findViewById(R.id.tvMsg);
        MyTextView tvTitleDialog = (MyTextView) view.findViewById(R.id.tvTitleDialog);
        tvMsg.setText(text);
        tvTitleDialog.setText(dialogTitle);

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // NotificationActivity notificationActivity = new NotificationActivity();
                if (fromWhere.equalsIgnoreCase("exit")) {
                    activity.finish();
                } else if (fromWhere.equalsIgnoreCase("readNotification")) {
                    alertDialoge.dismiss();
                    notificationActivity.readNotification();
                } else if (fromWhere.equalsIgnoreCase("deleteNotification")) {
                    alertDialoge.dismiss();
                    notificationActivity.deleteNotification();
                }
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;
    }


}
