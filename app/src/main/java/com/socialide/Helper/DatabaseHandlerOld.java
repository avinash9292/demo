package com.socialide.Helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.socialide.Model.BeanForCountry;
import com.socialide.Model.BeanForState;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DatabaseHandlerOld extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 4;
	public static String COUNTRY="country";
	public static String STATE="state";
	public static String CITY="city";
	public static String ID="id";
	public static String STATE_ID="state_id";

	public static String COUNTRY_ID="country_id";
	public static String NAME="name";
	// Database Name
	private static final String DATABASE_NAME = "country.db";


	public Context context;



	public DatabaseHandlerOld(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;

		checkDB();
	}

	
	public void checkDB() {
		  try {
		   
		   //android default database location is : /data/data/youapppackagename/databases/
		   String packageName = context.getPackageName();
		   
		   Log.e("pacakgename",""+packageName);
		   String destPath = "/data/data/" + packageName + "/databases";
		   String fullPath = "/data/data/" + packageName + "/databases/"
		     + DATABASE_NAME;
		   
		   //this database folder location
		   File f = new File(destPath);
		   
		   //this database file location
		   File obj = new File(fullPath);

		   //check if databases folder exists or not. if not create it
		   if (!f.exists()) {
		    f.mkdirs();
		    f.createNewFile();
		   }

		   //check database file exists or not, if not copy database from assets
		   if (!obj.exists()) {
		    this.CopyDB(fullPath);
		   }

		  } catch (FileNotFoundException e) {
		   e.printStackTrace();

		  } catch (IOException e) {
		   e.printStackTrace();

		  }
		 }
	
	public void CopyDB(String path) throws IOException {

		  InputStream databaseInput = null;
		  String outFileName = path;
		  OutputStream databaseOutput = new FileOutputStream(outFileName);

		  byte[] buffer = new byte[1024];
		  int length;
		  
		  //open database file from asset folder
		  databaseInput = context.getAssets().open(DATABASE_NAME);
		  while ((length = databaseInput.read(buffer)) > 0) {
		   databaseOutput.write(buffer, 0, length);
		   databaseOutput.flush();
		  }
		  databaseInput.close();

		  databaseOutput.flush();
		  databaseOutput.close();
		 }
	
	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	public ArrayList<BeanForCountry> getCountry()
	{
		ArrayList<BeanForCountry> al= new ArrayList();
		SQLiteDatabase db=getWritableDatabase();
		Cursor c= db.rawQuery("select * from "+COUNTRY,null);
		if (c.moveToFirst()){

			do {

				al.add(new BeanForCountry(c.getString(0),c.getString(1)));
			}while (c.moveToNext());
		}
		Log.e(COUNTRY +"count ",""+al.size());
		return al;
	}
	public ArrayList<BeanForState> getState(String country_id)
	{
		ArrayList<BeanForState> al= new ArrayList();
		SQLiteDatabase db=getWritableDatabase();
		Cursor c= db.rawQuery("select * from "+STATE +" where "+COUNTRY_ID+"="+country_id,null);

		if (c.moveToFirst()){

			do {
				al.add(new BeanForState(c.getString(0),c.getString(1),c.getString(2)));
			}while (c.moveToNext());
		}
		Log.e(STATE +"count ",""+al.size());
		return al;
	}
	public ArrayList<String> getCity(String state_id)
	{
		ArrayList<String> al= new ArrayList();
		SQLiteDatabase db=getWritableDatabase();
		Cursor c= db.rawQuery("select * from "+CITY +" where "+STATE_ID+"="+state_id+"",null);

		Log.e("Query","select * from "+CITY +" where "+STATE_ID+"='"+state_id+"'");

		if (c.moveToFirst()){
			do {
				al.add(c.getString(1));
			}while (c.moveToNext());
		}
		Log.e(CITY +"count ",""+al.size());
		return al;
	}
}
