package com.socialide.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;


import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.SettingActivity;
import com.socialide.Adapters.SpinnerDataAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.BuildConfig;
import com.socialide.Helper.CustomScrollView;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DatabaseHandlerOld;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyAutoCompleteTextView;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.SpinnerData;
import com.socialide.Helper.Validation;
import com.socialide.Model.BeanForCountry;
import com.socialide.Model.BeanForState;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.socialide.Helper.Mypreferences.STABLISHMENT_YEAR;

public class OrganisationSignupSecond extends Fragment implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_continue;
    Bitmap bitmap;

    RelativeLayout rl;
    EditText edt_stablishment_year;
    int chk_read, chk_write, chk_camera;
    AVLoadingIndicatorView view;

    ImageView imageView, img_back;
    MyAutoCompleteTextView edt_city, edt_state, edt_country;
    File file;
    Uri uri;
    ArrayList<NameValuePair> params;
    Intent CamIntent, GalIntent, CropIntent;
    PopupWindow mPopupWindow;
    LinearLayout mRelativeLayout;
    MyTextView txt_upload_pic, txt_name, tvToolbarTitle;
    LoginActivity activity;
    String filePath;
    String profilePicPath = "";
    ArrayAdapter ad_country, ad_state, ad_city;
    DatabaseHandlerOld db;
    ArrayList<BeanForCountry> al_country = new ArrayList<>();
    ArrayList<String> al_country_names = new ArrayList<>();
    ArrayList<BeanForState> al_state = new ArrayList<>();
    ArrayList<String> al_state_names = new ArrayList<>();
    ArrayList<String> al_city_names = new ArrayList<>();

    ListView listview, listviewCountry, listviewState, listviewCity;
    SpinnerDataAdapter spinnerDataAdapter, SpinnerDataAdapter, spinnerStateAdapter, spinnerCityAdapter;

    public OrganisationSignupSecond() {
        // Required empty public constructor
        LoginActivity.back_handle = 1;
    }

    ArrayList<String> strings;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    int Year, Month, Day;
    DonutProgress d;
    View root;
    CustomScrollView scrollView;
    int listPosition = -1, listPositionCountry = -1, listPositionState = -1, listPositionCity = -1;
    ArrayAdapter temp;
    String stablishedYearLocal = "";
    SpinnerDataAdapter spinnerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_organisation_signup_second, container, false);

        return root;
    }

    Animation animFadeIn;
    boolean p = false;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btn_continue = (MyButton) root.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        activity = (LoginActivity) getActivity();
        mRelativeLayout = (LinearLayout) root.findViewById(R.id.lin);
        listview = (ListView) root.findViewById(R.id.listview);
        listviewCountry = (ListView) root.findViewById(R.id.listviewCountry);
        listviewState = (ListView) root.findViewById(R.id.listviewState);
        listviewCity = (ListView) root.findViewById(R.id.listviewCity);

        txt_upload_pic = (MyTextView) root.findViewById(R.id.txt_upload_pic);
        tvToolbarTitle = (MyTextView) root.findViewById(R.id.tvToolbarTitle);
        txt_name = (MyTextView) root.findViewById(R.id.txt_name);
        txt_name.setText("" + Mypreferences.ORGANISATION_NAME);
        imageView = (ImageView) root.findViewById(R.id.circleView);
        img_back = (ImageView) root.findViewById(R.id.img_back);
        rl = (RelativeLayout) root.findViewById(R.id.relative_bg);
        edt_stablishment_year = (EditText) root.findViewById(R.id.edt_stablishment_year);
        edt_city = (MyAutoCompleteTextView) root.findViewById(R.id.edt_city);
        edt_state = (MyAutoCompleteTextView) root.findViewById(R.id.edt_state);
        edt_country = (MyAutoCompleteTextView) root.findViewById(R.id.edt_country);
        scrollView = (CustomScrollView) root.findViewById(R.id.CscrollView);
        this.view = (AVLoadingIndicatorView) root.findViewById(R.id.avi);
        d = (DonutProgress) root.findViewById(R.id.loading);


        if (Build.VERSION.SDK_INT >= 23) {

            int permissionCheck = ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION);

            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        } else {
            LocationManager lManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            boolean netEnabled = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (netEnabled) {
                lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, activity);

                Location location = lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {

                    Mypreferences.Lat = location.getLatitude();
                    Mypreferences.Lang = location.getLongitude();

                }
            }
            new GeocodeAsyncTask().execute();
        }


        strings = SpinnerData.getStablishYearsList(getActivity());
        if (!Mypreferences.STABLISHMENT_YEAR.equals("") && !Mypreferences.STABLISHMENT_YEAR.equals("STABLISHMENT_YEAR")) {
             edt_stablishment_year.setText(Mypreferences.STABLISHMENT_YEAR);
        }
        // temp = SpinnerData.getStablishYearsList(getActivity());
        if (!Mypreferences.STABLISHMENT_YEAR.equals("") && !Mypreferences.STABLISHMENT_YEAR.equals("STABLISHMENT_YEAR")) {
            // listPosition = strings.indexOf(Mypreferences.STABLISHMENT_YEAR);
        }

      /*  spinnerDataAdapter = new SpinnerDataAdapter(strings, getActivity(), getContext(), listPosition);
        listview.setAdapter(spinnerDataAdapter);
        listview.setSelectionFromTop(listPosition, 0);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_stablishment_year.setText(SpinnerData.yearlist.get(position));
                listPosition = position;
                scrollView.setEnableScrolling(true);
                Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.slide_up);
                // listview.startAnimation(animFadeIn);

                listview.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                    }
                }, 250);

            }
        });*/

        btn_continue.setOnClickListener(this);
        activity = (LoginActivity) getActivity();
        txt_upload_pic.setOnClickListener(this);
        // edt_stablishment_year.setOnClickListener(this);
        edt_country.setOnClickListener(this);
        edt_state.setOnClickListener(this);
        edt_city.setOnClickListener(this);

        if (SettingActivity.profilePageCheck == 1) {
            btn_continue.setText("Save");
            tvToolbarTitle.setText("General Details");
        }

        if (!Mypreferences.Picture.equals("")) {

            com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, imageView, d, getActivity(), img_back);
        }

        db = new DatabaseHandlerOld(getActivity());
        al_country = db.getCountry();
        for (int i = 0; i < al_country.size(); i++) {

            al_country_names.add(al_country.get(i).getName());

        }

        if (!Mypreferences.COUNTRY.equals("COUNTRY") && !Mypreferences.COUNTRY.equals("")) {

            edt_country.setText("" + Mypreferences.COUNTRY);
            listPositionCountry = al_country_names.indexOf(Mypreferences.COUNTRY);

            if (!Mypreferences.STATE.equals("STATE") && !Mypreferences.STATE.equals("")) {
                setStateOnCountrySpinner(Mypreferences.COUNTRY, Mypreferences.STATE);
            }
        }

        if (!Mypreferences.STATE.equals("STATE") && !Mypreferences.STATE.equals("")) {

            edt_state.setText("" + Mypreferences.STATE);
            //  statePosition = al_country_names.indexOf(Mypreferences.STATE);
            if (!Mypreferences.CITY.equals("CITY") && !Mypreferences.CITY.equals("")) {
                setCityOnStateSpinner(Mypreferences.STATE, Mypreferences.CITY);
            }
        }


        ad_country = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.txt, al_country_names);
        //edt_country.setAdapter(ad_country);

       /* if (!Mypreferences.STABLISHMENT_YEAR.equals("") && !Mypreferences.STABLISHMENT_YEAR.equals("STABLISHMENT_YEAR")) {
            listPosition = temp.getPosition(Mypreferences.STABLISHMENT_YEAR);
            Log.v("akram", "year " + Mypreferences.STABLISHMENT_YEAR);

        }*/

        spinnerAdapter = new SpinnerDataAdapter(al_country_names, getActivity(), getContext(), listPositionCountry);
        listviewCountry.setAdapter(spinnerAdapter);
        listviewCountry.setSelectionFromTop(listPositionCountry, 0);

        listviewCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_country.setText(al_country_names.get(position));
                listPositionState = -1;
                listPositionCity = -1;
                al_city_names.clear();
                listPositionCountry = position;
                scrollView.setEnableScrolling(true);
                listviewCountry.setVisibility(View.GONE);

            }
        });
        listviewState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_state.setText(al_state_names.get(position));
                listPositionState = position;
                scrollView.setEnableScrolling(true);
                listPositionCity = -1;

                listviewState.setVisibility(View.GONE);
            }
        });


        listviewCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_city.setText(al_city_names.get(position));
                listPositionCity = position;
                scrollView.setEnableScrolling(true);

                listviewCity.setVisibility(View.GONE);
            }
        });

        /*edt_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_city.showDropDown();
                if (("" + edt_city.getText().toString()).length() > 0) {

                    if (ad_city != null) {
                        ad_city.getFilter().filter(null);
                    }
                }
            }
        });
*/

    }

    public class TextWatcher1 implements TextWatcher {
        AutoCompleteTextView e;

        public TextWatcher1(AutoCompleteTextView e) {
            this.e = e;

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            switch (e.getId()) {
                case R.id.edt_country:
                    edt_city.setText("");
                    edt_state.setText("");
                    if (("" + e.getText().toString()).length() != 0) {
                        al_state_names.clear();
                        al_state.clear();
                        String id = "";
                        for (int i = 0; i < al_country_names.size(); i++) {
                            if (al_country_names.get(i).equals(("" + e.getText().toString()))) {
                                id = al_country.get(i).getId();
                                break;
                            }
                        }

                        al_state = db.getState(id);

                        for (int i = 0; i < al_state.size(); i++) {

                            al_state_names.add(al_state.get(i).getName());

                        }


                        ad_state = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.txt, al_state_names);
                        spinnerStateAdapter = new SpinnerDataAdapter(al_state_names, getActivity(), getContext(), 0);
                        listviewState.setAdapter(spinnerStateAdapter);
                        if (al_state_names.size() < 7) {
                            setListHeightOnAdapter(listviewState);

                        } else {
                            ViewGroup.LayoutParams params = listviewState.getLayoutParams();
                            params.height = 750;
                            listviewState.setLayoutParams(params);
                            listviewState.requestLayout();
                        }
                        listviewState.setSelectionFromTop(-1, 0);

                        //edt_state.setAdapter(ad_state);
                        if (!p) {
                            edt_state.addTextChangedListener(new TextWatcher1(edt_state));
                            p = true;
                        }
                    }

                    break;
                case R.id.edt_state:
                    edt_city.setText("");

                    db = new DatabaseHandlerOld(getContext());
                    if (("" + e.getText().toString()).length() != 0) {


                        String id = "";
                        for (int i = 0; i < al_state_names.size(); i++) {
                            if (al_state_names.get(i).equals(("" + e.getText().toString()))) {
                                id = al_state.get(i).getId();
                                break;
                            }
                        }

                        al_city_names = db.getCity(id);

                        ad_city = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.txt, al_city_names);

                        spinnerCityAdapter = new SpinnerDataAdapter(al_city_names, getActivity(), getContext(), 0);

                        listviewCity.setSelectionFromTop(-1, 0);
                        listviewCity.setAdapter(spinnerCityAdapter);

                        if (al_city_names.size() < 7) {

                            setListHeightOnAdapter(listviewCity);
                        } else {

                            ViewGroup.LayoutParams params = listviewCity.getLayoutParams();
                            params.height = 750;
                            listviewCity.setLayoutParams(params);
                            listviewCity.requestLayout();

                        }
                    }

                    break;
            }
        }
    }


    public String image(Bitmap bm) {


        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.socialide");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myDir + "/" + fname;
    }


    private void execMultipartPost() throws Exception {
//        Uri uriFromPath = Uri.fromFile(new File(realPath));
        File file = new File(image(bitmap));
        String contentType = file.toURL().openConnection().getContentType();
//        final ProgressDialog p= new ProgressDialog(getActivity());
//
//
//
//        p.setIndeterminate(false);
//        p.show();

        view.setVisibility(View.VISIBLE);
        view.show();

        RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), file);

        final String filename = "file_" + System.currentTimeMillis() / 1000L;
        RequestBody requestBody;
        RequestBody requestBody1;
        if (bitmap != null) {

            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)

                    .addFormDataPart("organization_id", Mypreferences.User_id)

                    .addFormDataPart("establishment_year", "" + edt_stablishment_year.getText().toString())
                    .addFormDataPart("country", "" + edt_country.getText().toString())
                    .addFormDataPart("state", "" + edt_state.getText().toString())
                    .addFormDataPart("city", "" + edt_city.getText().toString())
                    .addFormDataPart("profile_pic", filename + ".jpg", fileBody)
                    .addFormDataPart("access_token", Mypreferences.Access_token)

                    .build();

        } else {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)


                    .addFormDataPart("organization_id", Mypreferences.User_id)
                    .addFormDataPart("establishment_year", "" + edt_stablishment_year.getText().toString())
                    .addFormDataPart("country", "" + edt_country.getText().toString())
                    .addFormDataPart("state", "" + edt_state.getText().toString())
                    .addFormDataPart("city", "" + edt_city.getText().toString())
                    .addFormDataPart("access_token", Mypreferences.Access_token)
                    // .addFormDataPart("picture", filename + ".jpg", fileBody)
                    .build();
        }


        Request request = new Request.Builder()
                .url(GloabalURI.baseURI + GloabalURI.UPDATE_ORGANIZATION)
                .addHeader("access_token", Mypreferences.Access_token)
                .post(requestBody)
                .build();

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(okhttp3.Call call, final IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //  Toast.makeText(getActivity(), "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                        CustomSnackBar.toast(getActivity(), "Something went wrong please try again");
                        view.setVisibility(View.GONE);
                        view.hide();
                    }
                });
            }

            @Override
            public void onResponse(okhttp3.Call call, final Response response) throws IOException {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            JSONObject postObject = new JSONObject(response.body().string());
                            Log.e("aaaaa", "kk" + response.body().string());
                            if (postObject.getString("success").equals("true")) {
                                JSONObject result = postObject.getJSONObject("result");

                                Mypreferences.STABLISHMENT_YEAR = "" + edt_stablishment_year.getText().toString();
                                Mypreferences.COUNTRY = "" + edt_country.getText().toString();
                                Mypreferences.STATE = "" + edt_state.getText().toString();
                                Mypreferences.CITY = "" + edt_city.getText().toString();
                                Mypreferences.Picture = result.getString("profile_pic");

                                if (SettingActivity.profilePageCheck == 1) {
                                    SettingActivity.profilePageCheck = 0;
                                    getActivity().onBackPressed();
                                } else {
                                    gotoOrgainizationOtherDetails();
                                }
                                Toast.makeText(activity, "Profile update Successfully.", Toast.LENGTH_SHORT).show();

                            } else {
                                String s = "" + postObject.getString("message");
                                CustomSnackBar.toast(getActivity(), s);
                            }

                            view.setVisibility(View.GONE);
                            view.hide();

                        } catch (IOException e) {
                            e.printStackTrace();
                            view.setVisibility(View.GONE);
                            view.hide();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            view.setVisibility(View.GONE);
                            view.hide();
                        }
                    }
                });
            }
        });
    }


    public void gotoOrgainizationOtherDetails() {
        activity.goToFragment(new OrgainizationOtherDetails());
    }


    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("organization_id", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("establishment_year", "" + edt_stablishment_year.getText().toString()));
        params.add(new BasicNameValuePair("country", "" + edt_country.getText().toString()));
        params.add(new BasicNameValuePair("state", "" + edt_state.getText().toString()));
        params.add(new BasicNameValuePair("city", "" + edt_city.getText().toString()));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));

        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if (postObject.getString("success").equals("true")) {

                // JSONObject result= postObject.getJSONObject("result");

                STABLISHMENT_YEAR = "" + edt_stablishment_year.getText().toString();
                Mypreferences.COUNTRY = "" + edt_country.getText().toString();
                Mypreferences.STATE = "" + edt_state.getText().toString();
                Mypreferences.CITY = "" + edt_city.getText().toString();

                if (SettingActivity.profilePageCheck == 1) {
                    SettingActivity.profilePageCheck = 0;
                    getActivity().onBackPressed();
                } else {
                    gotoOrgainizationOtherDetails();
                }
            } else {


            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:

                listVisibilityGone(listviewCountry);
                listVisibilityGone(listview);
                listVisibilityGone(listviewCity);
                listVisibilityGone(listviewState);

                if (isValidate()) {

                    Mypreferences.setString(STABLISHMENT_YEAR, edt_stablishment_year.getText().toString(), getActivity());


                    if (bitmap == null && Mypreferences.Picture.equals("")) {
                        popup(getActivity());
                    } else {

//                        params = getParams();
//                        AsyncRequest getPosts = new AsyncRequest(OrganisationSignupSecond.this,getActivity(), "POST", params);
//                        getPosts.execute(GloabalURI.baseURI+GloabalURI.UPDATE_ORGANIZATION);

                        try {
                            execMultipartPost();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
                break;

            case R.id.txt_upload_pic:
                listVisibilityGone(listviewCountry);
                listVisibilityGone(listview);
                listVisibilityGone(listviewCity);
                listVisibilityGone(listviewState);

                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

                break;

            case R.id.edt_stablishment_year:
                // spinnerDialog();
                listVisibilityGone(listviewCountry);
                listVisibilityGone(listviewCity);
                listVisibilityGone(listviewState);

                if (listview.getVisibility() == View.VISIBLE) {
                    Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide_up);
                    listview.startAnimation(animFadeIn);
                    scrollView.setEnableScrolling(true);
                    listview.setVisibility(View.GONE);
                    //
                } else {
                    spinnerDataAdapter = new SpinnerDataAdapter(strings, getActivity(), getContext(), listPosition);
                    listview.setAdapter(spinnerDataAdapter);
                    listview.setSelectionFromTop(listPosition, 0);
                    Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide_down);
                    listview.startAnimation(animFadeIn);
                    listview.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);
                }
                //edt_stablishment_year.showDropDown();
                break;


            case R.id.edt_country:

                listVisibilityGone(listview);
                listVisibilityGone(listviewCity);
                listVisibilityGone(listviewState);


                if (listviewCountry.getVisibility() == View.VISIBLE) {

                    scrollView.setEnableScrolling(true);
                    Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide_up);
                    listviewCountry.startAnimation(animFadeIn);

                    listviewCountry.setVisibility(View.GONE);
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {

                        }
                    }, 250);

                    //
                } else {

                    spinnerAdapter = new SpinnerDataAdapter(al_country_names, getActivity(), getContext(), listPositionCountry);
                    listviewCountry.setAdapter(spinnerAdapter);
                    listviewCountry.setSelectionFromTop(listPositionCountry, 0);

                    Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(),
                            R.anim.slide_down);
                    listviewCountry.startAnimation(animFadeIn);
                    listviewCountry.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);
                }

                break;

            case R.id.edt_state:

                listVisibilityGone(listviewCountry);
                listVisibilityGone(listview);
                listVisibilityGone(listviewCity);

                if (listviewState.getVisibility() == View.VISIBLE) {

                    scrollView.setEnableScrolling(true);

                    listviewState.setVisibility(View.GONE);

                    //
                } else {


                    if (al_state_names.size() == 0)
                        break;
                    spinnerStateAdapter = new SpinnerDataAdapter(al_state_names, getActivity(), getContext(), listPositionState);
                    listviewState.setAdapter(spinnerStateAdapter);
                    if (al_state_names.size() < 7) {
                        setListHeightOnAdapter(listviewState);

                    } else {
                        ViewGroup.LayoutParams params = listviewState.getLayoutParams();
                        params.height = 750;
                        listviewState.setLayoutParams(params);
                        listviewState.requestLayout();
                    }
                    listviewState.setSelectionFromTop(listPositionState, 0);

                    listviewState.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);

                }

                break;

            case R.id.edt_city:

                listVisibilityGone(listviewCountry);
                listVisibilityGone(listview);
                listVisibilityGone(listviewState);

                if (listviewCity.getVisibility() == View.VISIBLE) {

                    scrollView.setEnableScrolling(true);

                    listviewCity.setVisibility(View.GONE);

                    //
                } else {

                    if (al_city_names.size() == 0)
                        break;

                    spinnerCityAdapter = new SpinnerDataAdapter(al_city_names, getActivity(), getContext(), listPositionCity);

                    listviewCity.setAdapter(spinnerCityAdapter);
                    listviewCity.setSelectionFromTop(listPositionCity, 0);
                    if (al_city_names.size() < 7) {

                        setListHeightOnAdapter(listviewCity);
                    } else {
                        ViewGroup.LayoutParams params = listviewCity.getLayoutParams();
                        params.height = 750;
                        listviewCity.setLayoutParams(params);
                        listviewCity.requestLayout();
                    }

                    listviewCity.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);

                }

                break;
        }
    }

    private boolean isValidate() {

        if (!Validation.isValidName("" + edt_stablishment_year.getText().toString())) {
            // edt_stablishment_year.setError("Please select year of stablishment");
            CustomSnackBar.toast(getActivity(), "Please select year of establishment");
            return false;
        } else if (edt_stablishment_year.getText().toString().length() < 4) {
            CustomSnackBar.toast(getActivity(), "Please enter 4 digit.");
            return false;
        }
//        else
//        {
//            edt_stablishment_year.setError(null);
//
//        }


//        else
//        {
//            edt_city.setError(null);
//
//        }
        else if (!Validation.isValidName("" + edt_country.getText().toString())) {
            // edt_country.setError("Please select country");
            CustomSnackBar.toast(getActivity(), "Please select country");
            return false;
        } else if (!Validation.isValidName("" + edt_state.getText().toString())) {
            //   edt_state.setError("Please select state");
            CustomSnackBar.toast(getActivity(), "Please select state");
            return false;
        } else if (!Validation.isValidName("" + edt_city.getText().toString())) {
            // edt_city.setError("Please select city");
            CustomSnackBar.toast(getActivity(), "Please select city");
            return false;
        }


       /* if (Validation.isValidName("" + edt_stablishment_year.getText()) && Validation.isValidName("" + edt_country.getText()) && Validation.isValidName("" + edt_state.getText()) && Validation.isValidName("" + edt_city.getText())) {

            return true;
        }*/
        return true;
    }

    public void ClickImageFromCamera() {

       /* CamIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        file = new File(Environment.getExternalStorageDirectory(),
                "file" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        uri = Uri.fromFile(file);

        CamIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);

        CamIntent.putExtra("return-data", true);

        startActivityForResult(CamIntent, 0);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String date = DateFormat.getDateTimeInstance().format(new Date());

        filePath = date + "temp.jpg";

        File f = new File(Environment.getExternalStorageDirectory(), filePath);

        Uri photoURI = FileProvider.getUriForFile(getContext(),
                BuildConfig.APPLICATION_ID +
                        ".provider", f);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        startActivityForResult(intent, 0);

    }

    public void GetImageFromGallery() {

        GalIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(GalIntent, "Select Image From Gallery"), 2);

    }


    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    ClickImageFromCamera();
                    dialog.dismiss();

                } else if (options[item].equals("Choose from Gallery")) {

                    GetImageFromGallery();

                    dialog.dismiss();

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private String getPath(Uri uri) {

        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {

            File f = new File(Environment.getExternalStorageDirectory().toString());
            // Static_variable.uploaded_report_path_str=f.toString();
            for (File temp : f.listFiles()) {

                if (temp.getName().equals(filePath)) {

                    f = temp;
                    break;
                }
            }

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

            profilePicPath = f.getAbsolutePath();
            // imageView.setImageBitmap(bitmap);

            Glide.with(getContext())
                    .load(profilePicPath)
                    .into(imageView);

            Glide.with(getContext())
                    .load(profilePicPath)
                    .into(img_back);

            // imageView.setImageBitmap(bitmap);

            // ImageCropFunction();

        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {

            if (data != null) {

                uri = data.getData();

                Uri selectedImageUri = data.getData();
                String tempPath = getPath(selectedImageUri);

                //  Static_variable.usr_uploaded_img__str = tempPath;

                bitmap = BitmapFactory.decodeFile(tempPath);

                profilePicPath = tempPath;

                Glide.with(getContext())
                        .load(profilePicPath)
                        .into(imageView);

                Glide.with(getContext())
                        .load(profilePicPath)
                        .into(img_back);


              /*  imageView.setImageBitmap(bitmap);
                img_back.setImageBitmap(bitmap);*/
                // ImageCropFunction();

            }

            /*if (data != null) {

                uri = data.getData();

                ImageCropFunction();

            }*/
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            if (data != null) {

                Bundle bundle = data.getExtras();

                bitmap = bundle.getParcelable("data");

                imageView.setImageBitmap(bitmap);
                img_back.setImageBitmap(bitmap);
                txt_upload_pic.setVisibility(View.INVISIBLE);

            }
        }
    }

    public void ImageCropFunction() {

        // Image Crop Code
        try {
            CropIntent = new Intent("com.android.camera.action.CROP");

            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 180);
            CropIntent.putExtra("outputY", 180);
            CropIntent.putExtra("aspectX", 1);
            CropIntent.putExtra("aspectY", 1);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);

            startActivityForResult(CropIntent, 1);

        } catch (ActivityNotFoundException e) {

        }
    }


    class GeocodeAsyncTask extends AsyncTask<Void, Void, Address> {

        String errorMessage = "";

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Address doInBackground(Void... none) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = null;


            double latitude = Mypreferences.Lat;
            double longitude = Mypreferences.Lang;

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException ioException) {
                errorMessage = "Service Not Available";

            } catch (IllegalArgumentException illegalArgumentException) {
                errorMessage = "Invalid Latitude or Longitude Used";
            }

            if (addresses != null && addresses.size() > 0) {

                return addresses.get(0);
            }
            return null;
        }

        protected void onPostExecute(Address address) {
            if (address == null) {

            } else {
                String addressName = "";

                String cityName = address.getAddressLine(0);
                String stateName = address.getAddressLine(1);
                String countryName = address.getAddressLine(2);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressName += " --- " + address.getAddressLine(i);
                    Log.e("Addresses", "-->" + address.getAddressLine(i));

                }

                if (Mypreferences.COUNTRY.equals("COUNTRY") && Mypreferences.COUNTRY.equals("")) {
                    edt_country.setText("" + Mypreferences.COUNTRY);
                } else {

                    edt_country.setText("" + address.getCountryName());
                    listPositionCountry = al_country_names.indexOf(address.getCountryName());
                    // spinnerCountryAdapter = new SpinnerDataAdapter(al_country_names,)
                    listviewCountry.setSelectionFromTop(listPositionCountry, 0);
                    if (!address.getCountryName().equalsIgnoreCase("")) {
                        setStateOnCountrySpinner(address.getCountryName(), address.getAdminArea());
                    }
                }

                if (Mypreferences.STATE.equals("STATE") && Mypreferences.STATE.equals("")) {
                    edt_state.setText("" + Mypreferences.STATE);
                } else {
                    edt_state.setText("" + address.getAdminArea());
                    if (!address.getAdminArea().equalsIgnoreCase("")) {
                        setCityOnStateSpinner(address.getAdminArea(), address.getLocality());
                    }
                }

                if (Mypreferences.CITY.equals("CITY") && Mypreferences.CITY.equals("")) {
                    edt_city.setText("" + Mypreferences.CITY);
                } else {
                    edt_city.setText("" + address.getLocality());
                }

//                infoText.setText("Latitude: " + address.getLatitude() + "\n" +
//                        "Longitude: " + address.getLongitude() + "\n" +
//                        "Address: " + countryName + "\n" + address.getCountryName() + "\n" + address.getLocality() + "\n" + address.getFeatureName() + "\n" + address.getPremises() + "\n" + address.getLocale() + "\n" + address.describeContents() + "\n" + address.getAdminArea() + "\n" + address.getSubLocality());
            }

            edt_country.addTextChangedListener(new TextWatcher1(edt_country));
            edt_state.addTextChangedListener(new TextWatcher1(edt_state));

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    selectImage();

                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }

            }
            // other 'case' lines to check for other
            // permissions this app might request
            case 2:

                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);


                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    LocationManager lManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    boolean netEnabled = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    if (netEnabled) {

                        lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, activity);

                        Location location = lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {

                            Mypreferences.Lat = location.getLatitude();
                            Mypreferences.Lang = location.getLongitude();
                        }
                        new GeocodeAsyncTask().execute();
                    }

                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
        }
    }

    public void popup(Context c) {


        View popupView = LayoutInflater.from(c).inflate(R.layout.image_validation_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        MyTextView yes = (MyTextView) popupView.findViewById(R.id.txt_yes);
        MyTextView no = (MyTextView) popupView.findViewById(R.id.txt_no);

        // Set a click listener for the popup window close MyButton
        yes.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window

                requestPermissions(
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                        1);
                mPopupWindow.dismiss();


            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                // Dismiss the popup window

                params = getParams();
                AsyncRequest getPosts = new AsyncRequest(OrganisationSignupSecond.this, getActivity(), "POST", params, view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.UPDATE_ORGANIZATION);
                mPopupWindow.dismiss();


            }
        });


        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
    }

    public void spinnerDialog() {

        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View view = factory.inflate(R.layout.dialog_spinner, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(getContext()).create();
        ListView txt = (ListView) view.findViewById(R.id.listview);

        txt.setAdapter(temp);
        txt.setSelectionFromTop(listPosition, 0);
        txt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                edt_stablishment_year.setText(SpinnerData.yearlist.get(position));
                listPosition = position;
                alertDialoge.dismiss();
            }
        });


        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

    }

    public void setListHeightOnAdapter(ListView listview) {
        ListAdapter listadp = listview.getAdapter();
        if (listadp != null) {
            int totalHeight = 0;
            for (int i = 0; i < listadp.getCount(); i++) {
                View listItem = listadp.getView(i, null, listview);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listview.getLayoutParams();
            params.height = totalHeight + (listview.getDividerHeight() * (listadp.getCount() - 1));
            listview.setLayoutParams(params);
            listview.requestLayout();
        }
    }

    private void setCityOnStateSpinner(String stateName, String cityName) {

        String id = "";
        for (int i = 0; i < al_state_names.size(); i++) {
            if (al_state_names.get(i).equals(("" + stateName))) {
                id = al_state.get(i).getId();
                break;
            }
        }

        try {
            al_city_names = db.getCity(id);
        } catch (Exception e) {
            Log.v("akram", "city not found");
        }


        if (!cityName.equals("CITY") && !cityName.equals("")) {
            edt_city.setText("" + cityName);
            listPositionCity = al_city_names.indexOf(cityName);
        }

        spinnerCityAdapter = new SpinnerDataAdapter(al_city_names, getActivity(), getContext(), listPositionCity);

        listviewCity.setAdapter(spinnerCityAdapter);

        if (al_city_names.size() < 7) {

            setListHeightOnAdapter(listviewCity);
        } else {

            ViewGroup.LayoutParams params = listviewCity.getLayoutParams();
            params.height = 750;
            listviewCity.setLayoutParams(params);
            listviewCity.requestLayout();

        }
        listviewCity.setSelectionFromTop(listPositionCity, 0);

    }

    public void listVisibilityGone(ListView listView) {
        if (listView.getVisibility() == View.VISIBLE) {
            listView.setVisibility(View.GONE);
        }

    }

    private void setStateOnCountrySpinner(String countryName, String stateName) {

        al_state_names.clear();
        al_state.clear();

        String id = "";
        for (int i = 0; i < al_country_names.size(); i++) {
            if (al_country_names.get(i).equals(("" + countryName))) {
                id = al_country.get(i).getId();
                break;
            }
        }
        try {
            al_state = db.getState(id);
        } catch (Exception e) {
            Log.v("akram", "state not available");
        }

        for (int i = 0; i < al_state.size(); i++) {

            al_state_names.add(al_state.get(i).getName());

        }

        if (!stateName.equals("STATE") && !stateName.equals("")) {
            listPositionState = al_state_names.indexOf(stateName);
        }

        spinnerStateAdapter = new SpinnerDataAdapter(al_state_names, getActivity(), getContext(), listPositionState);
        listviewState.setAdapter(spinnerStateAdapter);
        listviewState.setSelectionFromTop(listPositionState, 0);
        if (al_state_names.size() < 7) {
            setListHeightOnAdapter(listviewState);

        } else {
            ViewGroup.LayoutParams params = listviewState.getLayoutParams();
            params.height = 750;
            listviewState.setLayoutParams(params);
            listviewState.requestLayout();
        }

    }
}
