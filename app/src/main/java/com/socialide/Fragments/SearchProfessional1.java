package com.socialide.Fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Activities.FilterSearchActiviry;
import com.socialide.Activities.InviteForEvent;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.OtherUserDetailsActivity;
import com.socialide.Adapters.FavPeofessionalforSearchAdapter;
import com.socialide.Adapters.FilterDataAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.AsyncTask.AsyncRequestForFavorite;
import com.socialide.AsyncTask.AsyncRequestForSecondAPI;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Model.BeanForFilterData;
import com.socialide.Model.BeanForProfessionalSearch;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;


public class SearchProfessional1 extends Fragment implements AsyncRequest.OnAsyncRequestComplete, View.OnClickListener, AsyncRequestForSecondAPI.OnAsyncRequestComplete, AsyncRequestForFavorite.OnAsyncRequestComplete {
    ArrayList<BeanForFilterData> filters = new ArrayList<>();
    Timer timer1;
    int pageindex = 1;
    public static String location = Mypreferences.CITY, experience = "", category = "", subcategory = "", name = "", social_score = "", verified = "0", availability = "0", country = "", state = "", city = "";
    static String locationTemp = "", experienceTemp = "", categoryTemp = "", subcategoryTemp = "", social_scoreTemp = "", verifiedTemp = "0", availabilityTemp = "0";
    public static boolean checkBackFromFilter = true;

    public static int APICALL = 0;
    public static String API_NAME = "";
    int pagecount = 10;
    ImageView img_filter;
    FilterDataAdapter filter_adapter;
    private int lastVisibleItem, totalItemCount;
    public static boolean isLoading = true, isLoad = true;
    SwipeRefreshLayout mSwipeRefreshLayout;
    public static int pos = -1;
    TextView txt_no_result, txt_clear_all;
    ArrayList<NameValuePair> params;
    public static boolean is_filter_changed = true;
    public static boolean favCheckApi = true;
    String credential_id;
    boolean check = false, type = false;
    MainActivity activity;
    LinearLayoutManager linearLayoutManager;
    public ArrayList<BeanForProfessionalSearch> professionla_list = new ArrayList<>();
    public static ArrayList<BeanForProfessionalSearch> professionla_list1 = new ArrayList<>();
    boolean isrefresh = false, isLoadmore = false, isdata = true;
    public static boolean isWebservice = true;
    AutoCompleteTextView edt_search;
    static String school_name = "";
    RecyclerView mRecyclerView, mRecyclerView_filter;
    FragmentRecruitwatch adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    View rootView;

    public SearchProfessional1() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // SearchProfessional1.location= Mypreferences.CITY;
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_professional_search, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_hori);
        mRecyclerView_filter = (RecyclerView) rootView.findViewById(R.id.recycler_view_filter);
        img_filter = (ImageView) rootView.findViewById(R.id.img_filter);

        adapterSet();
        edt_search = (AutoCompleteTextView) rootView.findViewById(R.id.edt_search);

        //  edt_search.setThreshold(2);

        edt_search.addTextChangedListener(new TextWatcher1(edt_search));


        activity = (MainActivity) getActivity();
//professionla_list.clear();
        initRecyclerView();
        view = (AVLoadingIndicatorView) activity.findViewById(R.id.avi);
        Log.v("akram", "is filter = " + is_filter_changed);
        if (is_filter_changed) {
            favCheckApi = true;
            AsyncRequestForSecondAPI getPosts = new AsyncRequestForSecondAPI(SearchProfessional1.this, getActivity(),
                    "POST", new ArrayList<NameValuePair>(), view);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.getFavoriteUser);
        }
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(getContext(), OtherUserDetailsActivity.class);
                intent.putExtra("type", "Professional");
                intent.putExtra("userId", listForfavProfessionals.get(position).getUserid());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);

            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));


        mRecyclerView_filter.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView_filter, new ClickListener() {
            @Override
            public void onClick(View view, int position) {


                if (timer1 != null) {
                    timer1.cancel();
                }
                delete_item(position);
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();

            }
        }));
        img_filter.setOnClickListener(this);
        txt_clear_all.setOnClickListener(this);
        return rootView;
    }


    public void delete_item(int position) {

        String data = filters.get(position).getData();
        int type = Integer.parseInt(filters.get(position).getType());
        switch (type) {
            case 1:
                availability = "0";
                break;
            case 2:
                verified = "0";
                break;
            case 3:
                if (data.equals(location)) {
                    location = "";
                    city = "";
                    state = "";
                    country = "";
                } else {
                    String[] locations = location.split(",");
                    for (int i = 0; i < locations.length; i++) {
                        if (data.equals(locations[i])) {

                            if (i != 0) {
                                location = location.replace("," + data, "");
                                city = city.replace("," + data, "");
                            } else {
                                location = location.replace(data + ",", "");
                                city = city.replace(data + ",", "");
                            }
                        }
                    }
                }
                break;
            case 5:
                category = "";
                subcategory = "";
                for (int i = 0; i < filters.size(); i++) {
                    if (filters.get(i).getType().equals("4")) {
                        filters.remove(i);
                    }
                }
                break;

            case 4:
                if (data.equals(subcategory)) {
                    subcategory = "";
                    category = "";
                } else {
                    String[] subcategories = subcategory.split(",");
                    for (int i = 0; i < subcategories.length; i++) {
                        if (data.equals(subcategories[i])) {

                            if (i != 0) {
                                subcategory = subcategory.replace("," + data, "");
                            } else {
                                subcategory = subcategory.replace(data + ",", "");
                            }
                        }
                    }
                }
                break;
            case 6:
                social_score = "";
                break;
            case 7:
                experience = "";
                break;

        }
        filters.remove(position);

        filter_adapter.notifyDataSetChanged();
        if (filters.size() == 0) {
            txt_clear_all.setVisibility(View.GONE);
        } else {
            txt_clear_all.setVisibility(View.VISIBLE);
        }


        final long DELAY = 1000; //
//            timer.cancel();
        timer1 = new Timer();
        timer1.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                pageindex = 1;
                                isrefresh = true;
                                isLoadmore = true;
                                name = edt_search.getText().toString();
                                params = getParams();
                                AsyncRequest getPosts = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
                                getPosts.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);

                            }
                        });


                    }
                },
                DELAY
        );


//        pageindex=1;
//        params = getParams();
//        AsyncRequest getPosts1 = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
//        getPosts1.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);

    }


    public void clear_all() {
        filters.clear();
        if (filter_adapter != null) {
            filter_adapter.notifyDataSetChanged();
        }
        location = "";
        verified = "0";
        availability = "0";
        category = "";
        subcategory = "";
        social_score = "";
        experience = "";
        txt_clear_all.setVisibility(View.GONE);
        pageindex = 1;
        city = "";
        country = "";
        state = "";
        params = getParams();
        AsyncRequest getPosts1 = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
        getPosts1.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);

    }


    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);

        txt_no_result = (TextView) rootView.findViewById(R.id.txt_no_result);
        txt_clear_all = (TextView) rootView.findViewById(R.id.txt_clear_all);
        view = (AVLoadingIndicatorView) rootView.findViewById(R.id.avi);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        Log.v("akram", "array list = " + professionla_list);
        professionla_list.addAll(professionla_list1);
        adapter = new FragmentRecruitwatch(getActivity());
        mRecyclerView.setAdapter(adapter);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                mSwipeRefreshLayout.setRefreshing(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                        refreshItems();

                    }
                }, 10);

            }
        });


        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");
                CheckInternet mCheckInternet = new CheckInternet();

                if (!mCheckInternet.isConnectingToInternet(getActivity())) {

                    isLoadmore = false;
                    //   CustomSnackBar.toast(context, "Please connect to internet!");

                } else {
                    if (isrefresh) {
                        pageindex = 1;
                        onItemsLoadComplete();


                    } else if (isdata) {
                        pageindex = pageindex + 1;
                        isLoadmore = true;
                        //  adapter.notifyItemInserted(activity_list.size() - 1);
//                        view.setVisibility(View.VISIBLE);
//                        view.show();


                        params = getParams();

                        AsyncRequest getPosts = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
                        getPosts.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);


                    }
                }
            }
        });

    }

    void refreshItems() {
        // Load items
        // ...
        Log.e("asasasasasasasas", "asassasasasasa");
        // Load complete

        CheckInternet mCheckInternet = new CheckInternet();
        if (!mCheckInternet.isConnectingToInternet(getActivity())) {

            isrefresh = false;

            //   CustomSnackBar.toast(context, "Please connect to internet!");

        } else {

            if (!isLoadmore && !isrefresh) {
                isdata = true;
                pageindex = 1;

                isLoad = true;
                Log.e("Refresh method", "Refresh method");

                onItemsLoadComplete();

                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setHasFixedSize(true);


                adapter.notifyDataSetChanged();
                adapter.setLoaded();
                lastVisibleItem = -1;
                totalItemCount = 0;
                isrefresh = true;

                isWebservice = false;

//                view.setVisibility(View.VISIBLE);
//                view.show();
                params = getParams();

                AsyncRequest getPosts = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);

            }
        }
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("location", "" + location));
        params.add(new BasicNameValuePair("experience", "" + experience));
        params.add(new BasicNameValuePair("availability", "" + availability));
        params.add(new BasicNameValuePair("social_score", "" + social_score));
        params.add(new BasicNameValuePair("category", "" + category));
        params.add(new BasicNameValuePair("subcategory", "" + subcategory));
        params.add(new BasicNameValuePair("verified", "" + verified));
        params.add(new BasicNameValuePair("name", "" + edt_search.getText().toString()));
        params.add(new BasicNameValuePair("pageindex", "" + pageindex));
        params.add(new BasicNameValuePair("pagecount", "" + pagecount));
        params.add(new BasicNameValuePair("time_offset", "" + TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60000));


        return params;
    }


    private ArrayList<NameValuePair> getParams1(String id) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("favorite_user_id", "" + id));
        return params;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_filter:
                Intent i = new Intent(getActivity(), FilterSearchActiviry.class);
                checkBackFromFilter = true;

                locationTemp = location;
                experienceTemp = experience;
                categoryTemp = category;
                subcategoryTemp = subcategory;
                social_scoreTemp = social_score;
                verifiedTemp = verified;
                availabilityTemp = availability;

//                ActivityOptions options =
//                        ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(i);
                activity.overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                break;

            case R.id.txt_clear_all:
                clear_all();
                break;
        }

    }

    @Override
    public void asyncResponse(String response) {
        Log.e("response", "" + response);
        FunctionClass.logCatLong("" + response);

        if (MainActivity.img_setting != null) {
            MainActivity.img_setting.setVisibility(View.GONE);
        }
        try {

            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {
                txt_no_result.setVisibility(View.GONE);

                if (isLoadmore) {
                    isLoadmore = false;
                }
                if (pageindex == 1) {
                    professionla_list.clear();
                    professionla_list = new ArrayList<>();
                    isrefresh = false;
                    // }
                }

                if (isrefresh) {
                    isrefresh = false;
                }
                JSONArray ja = postObject.getJSONArray("result");

                if (ja.length() == 0) {

                    isdata = false;
                }
                for (int i = 0; i < ja.length(); i++) {

                    JSONObject j = ja.getJSONObject(i);


                    professionla_list.add(new BeanForProfessionalSearch("" + j.getString("userid"), "" + j.getString("first_name"), "" + j.getString("last_name"), "" + j.getString("dob"),
                            "" + j.getString("experience"), "" + j.getString("experience"), "" + j.getString("email"), "" + j.getString("mobile_number"),
                            "" + j.getString("state"), "" + j.getString("city"), "" + j.getString("profile_pic"), "" + j.getString("gender"),
                            "" + j.getString("country"), "" + j.getString("current_company"), "" + j.getString("category"), "" + j.getString("subcategory"),
                            "" + j.getString("profession"), "" + j.getString("about_me"), "" + j.getString("maximum_event_request"), "" + j.getString("availability"),
                            "" + j.getString("social_score"), "" + j.getString("join_flow_completed"), "" + j.getString("is_available")
                            , "" + j.getString("is_verified"), "" + j.getString("is_favorite")));
                }
                if (professionla_list.size() >= pagecount) {
                    // txt_no_item.setVisibility(View.GONE);
                    adapter.setLoaded();
                } else {
                    //    txt_no_item.setVisibility(View.GONE);
                    check = true;
                }
                Log.e("Avinash", professionla_list.size() + "");
                adapter.notifyDataSetChanged();
            } else {
                adapter.notifyDataSetChanged();
                isLoad = false;
                if (pageindex == 1) {
                    txt_no_result.setVisibility(View.VISIBLE);
                    professionla_list.clear();
                    professionla_list = new ArrayList<>();
                    adapter.notifyDataSetChanged();
                } else {
                    txt_no_result.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (professionla_list.size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public void onResume() {
        super.onResume();

        // if (is_filter_changed) {
        Log.v("akram", "onResume");

        if (favCheckApi) {
            is_filter_changed = false;
        }


        if (checkBackFromFilter) {
            pageindex = 1;
            params = getParams();
            AsyncRequest getPosts1 = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
            getPosts1.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);
        } else if (!location.equalsIgnoreCase(locationTemp) || !experience.equalsIgnoreCase(experienceTemp) ||
                !category.equalsIgnoreCase(categoryTemp) || !subcategory.equalsIgnoreCase(subcategoryTemp)
                || !social_score.equalsIgnoreCase(social_scoreTemp) || !verified.equalsIgnoreCase(verifiedTemp) ||
                !availability.equalsIgnoreCase(availabilityTemp)) {
            checkBackFromFilter = true;
            pageindex = 1;
            params = getParams();
            AsyncRequest getPosts1 = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
            getPosts1.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);

        }

//    }
//    else {
//
//
//        if (adapter!=null) {
//            adapter.notifyDataSetChanged();
//        }
//        if (adapter1!=null) {
//            adapter1.notifyDataSetChanged();
//        }
//    }

        filters.clear();

        if (!availability.equals("0")) {
            filters.add(new BeanForFilterData("Availability", "1"));
        }
        if (!verified.equals("0")) {
            filters.add(new BeanForFilterData("Verified", "2"));
        }
        if (!location.equals("")) {
            if (location.contains(",")) {
                String[] locations = location.split(",");
                for (int i = 0; i < locations.length; i++) {
                    filters.add(new BeanForFilterData(locations[i], "3"));
                }
            } else {
                filters.add(new BeanForFilterData(location, "3"));

            }
        }
        if (!category.equals("")) {


            if (!subcategory.equals("")) {
                if (subcategory.contains(",")) {
                    String[] subcategories = subcategory.split(",");
                    for (int i = 0; i < subcategories.length; i++) {
                        filters.add(new BeanForFilterData(subcategories[i], "4"));
                    }
                } else {
                    filters.add(new BeanForFilterData(subcategory, "4"));

                }
            } else {
                filters.add(new BeanForFilterData(category, "5"));
            }
        }

        if (!social_score.equals("")) {
            filters.add(new BeanForFilterData("Social score " + social_score + "+", "6"));
        }

        if (!experience.equals("")) {
            filters.add(new BeanForFilterData("Experience " + experience + "+", "7"));
        }


        adapterSetForFilter();

        if (filters.size() == 0) {
            txt_clear_all.setVisibility(View.GONE);
        } else {
            txt_clear_all.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void asyncResponsefavorite(String response) {

        Log.e("response", "" + response);
        if (MainActivity.img_setting != null) {
            MainActivity.img_setting.setVisibility(View.GONE);
        }

        try {
            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {

                BeanForProfessionalSearch a = professionla_list.get(pos);
                if (!postObject.getString("message").contains("removed")) {
                    a.setIs_favorite("1");
                    listForfavProfessionals.add(a);
                } else {
                    a.setIs_favorite("0");
                    for (int i = 0; i < listForfavProfessionals.size(); i++) {
                        if (a.getUserid().equals(listForfavProfessionals.get(i).getUserid())) {

                            listForfavProfessionals.remove(listForfavProfessionals.get(i));
                        }
                    }
                }
                professionla_list.remove(pos);
                professionla_list.add(pos, a);
                adapter.notifyDataSetChanged();
                adapter1.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private class FragmentRecruitwatch extends RecyclerView.Adapter<FragmentRecruitwatch.SettingsViewHolderItem> {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;


        Context mContext;

        public FragmentRecruitwatch(Context context) {
            this.mContext = context;


            linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!SearchProfessional1.isLoading && totalItemCount <= (lastVisibleItem + 3)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        SearchProfessional1.isLoading = true;
                    }
                }
            });
        }


        @Override
        public int getItemViewType(int position) {
            return professionla_list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public SettingsViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_professional_item, parent, false);

                SettingsViewHolderItem settingsViewHolderItem = new SettingsViewHolderItem(view);

                settingsViewHolderItem.tv_name = (MyTextView) view.findViewById(R.id.tv_name);
                settingsViewHolderItem.tv_profession = (MyTextView) view.findViewById(R.id.tv_profession);
                settingsViewHolderItem.tv_city = (MyTextView) view.findViewById(R.id.tv_city);
                settingsViewHolderItem.txt_available = (MyTextView) view.findViewById(R.id.txt_available);
                settingsViewHolderItem.view_like = (ProgressBar) view.findViewById(R.id.view_like);

                settingsViewHolderItem.iv_event_profile_pic = (CircleImageView) view.findViewById(R.id.iv_event_profile_pic);
                settingsViewHolderItem.img_fav = (ImageView) view.findViewById(R.id.img_fav);
                settingsViewHolderItem.img_verify = (ImageView) view.findViewById(R.id.img_verify);
                settingsViewHolderItem.loading = (DonutProgress) view.findViewById(R.id.loading);
                settingsViewHolderItem.btn_event_req = (MyButton) view.findViewById(R.id.btn_event_req);


                return settingsViewHolderItem;
            } else if (viewType == VIEW_TYPE_LOADING) {

            }
            return null;
        }

        @Override
        public void onBindViewHolder(final SettingsViewHolderItem holder, final int position) {

            if (holder instanceof SettingsViewHolderItem) {
                //   BeanForGetNamesToChat alert = professionla_list.get(position);

                holder.tv_name.setText("" + professionla_list.get(position).getFirst_name() + " " + professionla_list.get(position).getLast_name());
                if (professionla_list.get(position).getSubcategory().equalsIgnoreCase("")) {
                    holder.tv_profession.setText("" + professionla_list.get(position).getCity());
                } else {
                    holder.tv_profession.setText("" + professionla_list.get(position).getSubcategory() + ", " + professionla_list.get(position).getCity());
                }
                holder.tv_city.setText("Social Score - " + professionla_list.get(position).getSocial_score());
                holder.img_fav.setVisibility(View.VISIBLE);
                holder.view_like.setVisibility(View.GONE);

                com.socialide.Helper.ImageLoader.image(professionla_list.get(position).getProfile_pic(), holder.iv_event_profile_pic, holder.loading, getActivity());

                Log.v("akram", "available adapter = " + professionla_list.get(position).getIs_available());
                if (("" + professionla_list.get(position).getIs_available()).equalsIgnoreCase("1")) {

                    holder.iv_event_profile_pic.setBorderColor(Color.parseColor("#3fb4b5"));
                    holder.txt_available.setVisibility(View.GONE);
                    holder.btn_event_req.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_event_profile_pic.setBorderColor(Color.parseColor("#a6b1b6"));
                    holder.txt_available.setVisibility(View.VISIBLE);
                    holder.btn_event_req.setVisibility(View.GONE);

                    holder.txt_available.setText("" + professionla_list.get(position).getFirst_name() + " " + getResources().getString(R.string.isNotAvailable));
                }
                if (("" + professionla_list.get(position).getIs_verified()).equalsIgnoreCase("0")) {
                    holder.img_verify.setImageResource(R.drawable.notverifiedicon);
                } else {
                    holder.img_verify.setImageResource(R.drawable.verifiedicon);
                }
                if (("" + professionla_list.get(position).getIs_favorite()).equalsIgnoreCase("0")) {
                    holder.img_fav.setImageResource(R.drawable.favoriteheart);
                } else {
                    holder.img_fav.setImageResource(R.drawable.favoriteheartfull);
                }
            }
            holder.btn_event_req.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), InviteForEvent.class);
                    intent.putExtra("pos", position);
                    Mypreferences.Request_an_event_from = "search";
                    professionla_list1.clear();
                    professionla_list1.addAll(professionla_list);
                    startActivity(intent);
                    activity.overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);

                }
            });
            holder.iv_event_profile_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), OtherUserDetailsActivity.class);
                    intent.putExtra("type", "Professional");
                    intent.putExtra("userId", professionla_list.get(position).getUserid());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                }
            });
            holder.tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.iv_event_profile_pic.performClick();
                }
            });
            holder.img_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckInternet mCheckInternet = new CheckInternet();

                    if (!mCheckInternet.isConnectingToInternet(getContext())) {

                        CustomSnackBar.toast(getActivity(), "Please connect to internet!");

                    } else {

                        pos = position;
                        //professionla_list.get(position).setIsliked(false);
                        //    holder.img_fav.setEnabled(false);

                        holder.view_like.setVisibility(View.VISIBLE);
                        holder.img_fav.setVisibility(View.INVISIBLE
                        );
                        //  holder.view_like.show();
                        AsyncRequestForFavorite getPosts = new AsyncRequestForFavorite(SearchProfessional1.this, getActivity(), "POST", getParams1(professionla_list.get(position).getUserid()), view, holder.view_like);
                        getPosts.execute(GloabalURI.baseURI + GloabalURI.favoriteUnfavoriteUser);

                    }


                }
            });


        }

        OnLoadMoreListener mOnLoadMoreListener;

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void setLoaded() {
            SearchProfessional1.isLoading = false;
        }

        @Override
        public int getItemCount() {
            return professionla_list.size();
        }

        class SettingsViewHolderItem extends RecyclerView.ViewHolder {

            public View view;

            public MyTextView tv_name, tv_profession, tv_city, txt_available;
            public ImageView img_fav, img_verify;
            CircleImageView iv_event_profile_pic;
            ProgressBar view_like;
            public DonutProgress loading;
            public MyButton btn_event_req;

            public SettingsViewHolderItem(View v) {
                super(v);
                this.view = v;
            }
        }


    }


    public class TextWatcher1 implements TextWatcher {
        AutoCompleteTextView e;
        Timer timer;

        public TextWatcher1(AutoCompleteTextView e) {
            this.e = e;

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {


        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (timer != null) {
                timer.cancel();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            //timer  =new Timer();
            final long DELAY = 1000; //
//            timer.cancel();
            timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (e.getId() == R.id.edt_search) {


                                        if (e.getText().toString().length() >= 0) {

                                            pageindex = 1;
                                            isrefresh = true;
                                            isLoadmore = true;
                                            name = edt_search.getText().toString();
                                            params = getParams();
                                            AsyncRequest getPosts = new AsyncRequest(SearchProfessional1.this, getActivity(), "POST", params, view);
                                            getPosts.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);


                                        }
                                    }
                                }
                            });


                        }
                    },
                    DELAY
            );


        }
    }


    int pageindex_fav = 1;
    int pagecount_fav = 10;
    AVLoadingIndicatorView view;
    RecyclerView recyclerView;
    FavPeofessionalforSearchAdapter adapter1;
    public static List<BeanForProfessionalSearch> listForfavProfessionals = new ArrayList<>();
    public static boolean isLoading1 = true;


    private void adapterSet() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), OrientationHelper.HORIZONTAL, false);
        adapter1 = new FavPeofessionalforSearchAdapter(getActivity(), recyclerView, listForfavProfessionals, linearLayoutManager, getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter1);

        adapter1.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");
                CheckInternet mCheckInternet = new CheckInternet();

                if (!mCheckInternet.isConnectingToInternet(getActivity())) {

                    //   isLoadmore = false;
                    //   CustomSnackBar.toast(context, "Please connect to internet!");

              /*  } else {
                    if (isrefresh) {
*/
                    pagecount_fav = 10;
                    pageindex_fav = 1;
                    // onItemsLoadComplete();

                } else {
                    pageindex_fav = pageindex_fav + 1;

                    //  isLoadmore = true;


//                    AsyncRequestForSecondAPI getPosts = new AsyncRequestForSecondAPI(SearchProfessional1.this,getActivity(),
//                            "POST", new ArrayList<NameValuePair>(), view);
//                    getPosts.execute(GloabalURI.baseURI + GloabalURI.getFavoriteUser);
                    // }
                }
            }
        });
    }


    private void adapterSetForFilter() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), OrientationHelper.HORIZONTAL, false);
        filter_adapter = new FilterDataAdapter(getActivity(), filters);
        mRecyclerView_filter.setLayoutManager(linearLayoutManager);
        mRecyclerView_filter.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView_filter.setAdapter(filter_adapter);

    }


    @Override
    public void asyncResponse1(String response) {
        Log.e("response", "" + response);


        try {

            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {


                if (pageindex_fav == 1) {
                    //Log.e("aaaaa", "xxxxxxxxx");
                    listForfavProfessionals.clear();
                }
                isrefresh = false;

                JSONArray ja = postObject.getJSONArray("result");
                listForfavProfessionals.clear();

                for (int i = 0; i < ja.length(); i++) {

                    JSONObject j = ja.getJSONObject(i);

                    listForfavProfessionals.add(new BeanForProfessionalSearch("" + j.getString("userid"), "" + j.getString("first_name"), "" + j.getString("last_name"), "" + j.getString("dob"),
                            "" + j.getString("experience"), "" + j.getString("experience"), "" + j.getString("email"), "" + j.getString("mobile_number"),
                            "" + j.getString("state"), "" + j.getString("city"), "" + j.getString("profile_pic"), "" + j.getString("gender"),
                            "" + j.getString("country"), "" + j.getString("current_company"), "" + j.getString("category"), "" + j.getString("subcategory"),
                            "" + j.getString("profession"), "" + j.getString("about_me"), "" + j.getString("maximum_event_request"), "" + j.getString("availability"),
                            "" + j.getString("social_score"), "" + j.getString("join_flow_completed")));

                }
                recyclerView.setVisibility(View.VISIBLE);
                adapter1.setLoaded();
                Log.v("akram", "catch " + listForfavProfessionals.size());

                adapter1 = new FavPeofessionalforSearchAdapter(getActivity(), recyclerView, listForfavProfessionals, linearLayoutManager, getActivity());

                recyclerView.setAdapter(adapter1);

            } else {
                listForfavProfessionals.clear();
                recyclerView.setVisibility(View.GONE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}