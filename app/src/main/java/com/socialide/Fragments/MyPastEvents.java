package com.socialide.Fragments;


import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.MyEventDetalils;
import com.socialide.Activities.OtherUserDetailsActivity;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPastEvents extends Fragment implements AsyncRequest.OnAsyncRequestComplete {
    ArrayList<BeanForFetchEvents> activity_list = new ArrayList<>();
    static public ArrayList<BeanForFetchEvents> activity_list1 = new ArrayList<>();
    int pagecount = 10;
    AVLoadingIndicatorView view;
    int pageindex = 1;
    MyTextView txt_no_item;
    private int lastVisibleItem, totalItemCount;
    public static boolean isLoading = true;

    ArrayList<NameValuePair> params;
    boolean check = false, type = false;
    LinearLayoutManager linearLayoutManager;
    View rootView;

    MainActivity mainActivity;

    Activity activity;
    RecyclerView mRecyclerView;
    PendingEventAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static int checkAsynck = 0;

    public static ArrayList<JSONArray> eventImageUrlJson = new ArrayList<>();

    public MyPastEvents() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_recycler_view_non_refresh, container, false);
        mainActivity = (MainActivity) getActivity();

        initRecyclerView();

        if (checkAsynck == 1 && activity_list.size() == 0) {
            txt_no_item.setVisibility(View.VISIBLE);
            txt_no_item.setText(getResources().getString(R.string.noEvent));

        } else if (activity_list.size() == 0) {
            txt_no_item.setVisibility(View.VISIBLE);
            txt_no_item.setText(getResources().getString(R.string.loading));
        } else {
            txt_no_item.setVisibility(View.GONE);
        }

        if (checkAsynck == 0) {

            params = getParams(pagecount, pageindex);
            AsyncRequest getPosts = new AsyncRequest(this, mainActivity, "POST", params, view, 1);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.MYPASTEVENT);
        }

       /* mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

//
//
//                }

            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));
*/

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        // use a linear layout manager

        txt_no_item = (MyTextView) rootView.findViewById(R.id.txt_no_item);
        view = (AVLoadingIndicatorView) mainActivity.findViewById(R.id.avi);

        mLayoutManager = new LinearLayoutManager(mainActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);


        adapter = new PendingEventAdapter(mainActivity);
        mRecyclerView.setAdapter(adapter);


        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");

                //  adapter.notifyItemInserted(activity_list.size() - 1);
                pageindex = pageindex + 1;
                type = true;
                params = getParams(pagecount, pageindex);
                AsyncRequest getPosts = new AsyncRequest(MyPastEvents.this, mainActivity, "POST", params, view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.MYPASTEVENT);


            }
        });

    }


    void refreshItems() {
        // Load items
        // ...
        Log.e("asasasasasasasas", "asassasasasasa");
        // Load complete
        pagecount = 10;
        pageindex = 1;


        mLayoutManager = new LinearLayoutManager(mainActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);


        adapter = new PendingEventAdapter(mainActivity);
        mRecyclerView.setAdapter(adapter);

        lastVisibleItem = -1;
        totalItemCount = 0;
        type = false;

        params = getParams(pagecount, pageindex);
        AsyncRequest getPosts = new AsyncRequest(this, mainActivity, "POST", params, view);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.MYPASTEVENT);
        initRecyclerView();

    }

    private ArrayList<NameValuePair> getParams(int pagecount, int pageindex) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));

        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));

        params.add(new BasicNameValuePair("pagecount", "" + pagecount));
        params.add(new BasicNameValuePair("pageindex", "" + pageindex));
        params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));

        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);
            FunctionClass.logCatLong(response);
//
            //  {"success":"true","message":"Scheduled event details fetched successfully.","result":{"event_id":"15","profile_image":null,"event_name":"inform the authorities about illegal activities ","event_title":"telling someone you love them via the phone ","event_date":"21 May 2017","contact_details":{"professional_name":"AVINASH TIWARI","professional_profession":null,"professional_email":"aa@aa.aa","professional_mobile_no":null},"userid":"183","type":"Professional","sharecontact":"1"}}

            checkAsynck = 1;
            if (postObject.getString("success").equals("true")) {
                if (pageindex == 1) {
                    Log.e("aaaaa", "xxxxxxxxx");
                    activity_list.clear();
                }
                MainActivity.img_setting.setVisibility(View.VISIBLE);
                JSONArray result = postObject.getJSONArray("result");

                for (int i = 0; i < result.length(); i++) {
                    JSONObject result_object = result.getJSONObject(i);
                    JSONObject contact_details = result_object.getJSONObject("contact_details");
                    JSONObject decline_details = result_object.getJSONObject("decline_details");

                    JSONArray jsonArray1 = result_object.getJSONArray("event_photos");

                    eventImageUrlJson.add(jsonArray1);
                    String professionalId = "";

                    if (result_object.has("professional_id")) {
                        Log.v("akram", "contain");
                        professionalId = result_object.getString("professional_id");
                    }


                    Log.v("akram", "async event time = " + result_object.getString("event_time"));
                    activity_list.add(new BeanForFetchEvents("" + result_object.getString("event_id"), "" + result_object.getString("profile_pic"), "" + result_object.getString("event_by"), "" + result_object.getString("event_title"),
                            "" + result_object.getString("event_description"), "" + result_object.getString("event_location"), "" + result_object.getString("event_time"), "" + result_object.getString("event_date"), "" + result_object.getString("contact_number"),
                            "" + result_object.getString("email"), "" + result_object.getString("message"), "" + contact_details.getString("person_name"), "" + contact_details.getString("person_designation"), "" + contact_details.getString("person_email")
                            , "" + contact_details.getString("person_mobile_number"), "" + decline_details.getString("decline_by"),
                            "" + decline_details.getString("reason"), "" + result_object.getString("userid"), "" + result_object.getString("type"), "" + result_object.getString("sharecontact"), professionalId));

                }

                if (activity_list.size() == 0) {
                    txt_no_item.setVisibility(View.VISIBLE);
                } else if (activity_list.size() >= 10) {
                    txt_no_item.setVisibility(View.GONE);

                    adapter.setLoaded();
                } else {
                    txt_no_item.setVisibility(View.GONE);

                    check = true;
                }

                adapter.notifyDataSetChanged();
                initRecyclerView();
            } else {
                if (pageindex == 1) {

                    txt_no_item.setText(getResources().getString(R.string.noEvent));
                    txt_no_item.setVisibility(View.VISIBLE);

                }
            }

        } catch (JSONException e) {
            Log.v("akram", "catch = " + e);
            e.printStackTrace();
        }

    }


    public interface ClickListener {

        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


    public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    public class PendingEventAdapter extends RecyclerView.Adapter<PendingEventAdapter.EventsViewHolder> {

        private int visibleThreshold = 1;
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        private final int BUTTON_TYPE = 2;


        Context mContext;


        public PendingEventAdapter(Context context) {
            this.mContext = context;


            linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();


                    if (!MyPastEvents.isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();


                        }
                        MyPastEvents.isLoading = true;
                    }
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            if (position == activity_list.size()) {
                return BUTTON_TYPE;
            } else if (activity_list.get(position) == null) {
                return VIEW_TYPE_LOADING;
            } else {
                return VIEW_TYPE_ITEM;
            }
        }

        @Override
        public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_event_item, parent, false);

                EventsViewHolder viewHolder = new EventsViewHolder(view);
                viewHolder.iv_event_profile_pic = (RoundedImageView) view.findViewById(R.id.iv_event_profile_pic);
                viewHolder.tv_event_organisaton = (MyTextView) view.findViewById(R.id.tv_event_organisaton);
                viewHolder.tv_event_name = (MyTextView) view.findViewById(R.id.tv_event_name);
                viewHolder.tv_city = (MyTextView) view.findViewById(R.id.tv_city);
                viewHolder.tv_detail = (MyTextView) view.findViewById(R.id.tv_detail);
                viewHolder.txt_date = (MyTextView) view.findViewById(R.id.txt_date);
                viewHolder.txt_time = (MyTextView) view.findViewById(R.id.txt_time);
                viewHolder.rl_item = (RelativeLayout) view.findViewById(R.id.rl_item);
                viewHolder.d = (DonutProgress) view.findViewById(R.id.loading);


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                return viewHolder;
            } else if (viewType == BUTTON_TYPE) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contain_view, parent, false);

                EventsViewHolder viewHolder = new EventsViewHolder(view);
                return viewHolder;
            }


            return null;
        }

        @Override
        public void onBindViewHolder(final EventsViewHolder holder, int position) {

            if (position == activity_list.size()) {
                return;
            }

            if (holder instanceof EventsViewHolder) {

                BeanForFetchEvents bean = activity_list.get(position);

                String datetime = TimeConveter.getdatetime(bean.getEvent_date(), bean.getEvent_time());


                String date = datetime.split("T")[0];
                String time = datetime.split("T")[1];
                Log.v("adapter event date = ", "" + date);
                Log.v("adapter event time = ", "" + time);
                holder.txt_time.setText("" + time);
                holder.txt_date.setText("" + date);
                holder.tv_event_name.setText("" + bean.getEvent_title());
                holder.tv_event_organisaton.setText("" + bean.getEvent_by());
                holder.tv_city.setText("" + bean.getEvent_location());
                holder.tv_detail.setText("" + bean.getMessage());
                if (bean.getMessage().equals("")) {
                    holder.tv_detail.setVisibility(View.GONE);
                } else {
                    holder.tv_detail.setVisibility(View.VISIBLE);

                }

                com.socialide.Helper.ImageLoader.image(bean.getProfile_image(), holder.iv_event_profile_pic, holder.d, mainActivity);
                holder.iv_event_profile_pic.setTag(position);
                holder.rl_item.setTag(position);
                holder.iv_event_profile_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int pos = (Integer) v.getTag();
                        BeanForFetchEvents b = activity_list.get(pos);

                        Intent intent = new Intent(mContext, OtherUserDetailsActivity.class);
                        String userId = "", userType = "";
                        if (Mypreferences.User_Type.equalsIgnoreCase("Organization")) {
                            userId = b.getProfessional_id();
                            userType = "Professional";
                        } else {
                            userId = b.getUserid();
                            userType = "Organization";
                        }
                        intent.putExtra("userId", userId);
                        intent.putExtra("type", userType);

                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);

                    }
                });

                holder.tv_event_organisaton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.iv_event_profile_pic.performClick();
                    }
                });

                holder.rl_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (Integer) view.getTag();
                        ActivityOptions options = ActivityOptions.makeCustomAnimation(mainActivity, R.anim.slide_to_right, R.anim.slide_from_left);

                        OtherUserPastEvents.checkfragment = 2;
                        Intent i = new Intent(mainActivity, MyEventDetalils.class);
                        activity_list1.clear();
                        activity_list1.addAll(activity_list);
                        EventFragment.position = pos;
                        startActivity(i, options.toBundle());
                    }
                });


            } else {

            }
        }

        @Override
        public int getItemCount() {
            if (activity_list.size() != 0) {
                return activity_list.size() + 1;
            } else {
                return 0;
            }
        }

        class EventsViewHolder extends RecyclerView.ViewHolder {

            public RoundedImageView iv_event_profile_pic, right_gray_arrow;
            public MyTextView tv_event_name, tv_event_organisaton, tv_city, tv_detail, txt_date, txt_time;

            DonutProgress d;

            public RelativeLayout rl_item;

            public EventsViewHolder(View v) {
                super(v);
            }

        }

        OnLoadMoreListener mOnLoadMoreListener;

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void setLoaded() {
            MyPastEvents.isLoading = false;
        }
    }


    class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }


}








