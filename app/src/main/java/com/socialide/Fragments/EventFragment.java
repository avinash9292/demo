package com.socialide.Fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Adapters.ViewPagerAdapterEvent;
import com.socialide.Adapters.ViewPagerAdapterEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends Fragment {

    View rootView;
    public static int position;
    public static boolean isBack = false;
    public static int checkTabPosition = 0;


    public EventFragment() {
        // Required empty public constructor
    }

    static AVLoadingIndicatorView view;
    ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_events, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        MainActivity.img_setting.setVisibility(View.GONE);
        EventsScheduled.checkAsynck = 0;
        EventsScheduled.activity_list1.clear();
        EventsPending.checkAsynck = 0;
        EventsPending.activity_list1.clear();
        EventsDeclined.checkAsynck = 0;
        EventsDeclined.activity_list1.clear();

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.viewpagertab);

        setupViewPager(viewPager);


        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(checkTabPosition);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                checkTabPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

       /* tabLayout.addTab(tabLayout.newTab().setText("Scheduled"));
        tabLayout.addTab(tabLayout.newTab().setText("Pending"));
        tabLayout.addTab(tabLayout.newTab().setText("Declined"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ViewPagerAdapterEvent adapter = new ViewPagerAdapterEvent
                (getChildFragmentManager(), 3);

        viewPager.setAdapter(adapter);*/
        /*viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {

                    goToFragment(new EventsScheduled());

                }
                if (position == 1) {

                    goToFragment(new EventsPending());

                }
                if (position == 2) {


                    goToFragment(new EventsDeclined());

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });*/

/*
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/
        return rootView;
    }

    /*public void goToFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.id_holder, fragment, "Fragment");
        fragmentTransaction.commitAllowingStateLoss();

    }*/

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new EventsScheduled(), "Scheduled");
        adapter.addFragment(new EventsPending(), "Pending");
        adapter.addFragment(new EventsDeclined(), "Declined");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
