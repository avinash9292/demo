package com.socialide.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Activities.FilterSearchActiviry;
import com.socialide.Activities.InviteForEvent;
import com.socialide.Activities.OtherUserDetailsActivity;
import com.socialide.Adapters.AvailabilityAdapterForMyProfile;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DialogClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;

import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.NonScrollExpandableListView;
import com.socialide.Model.BeanForProfessionalSearch;
import com.socialide.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtherUserProfile extends Fragment implements AsyncRequest.OnAsyncRequestComplete {
    String add_space = ":       ";
    RoundedImageView img_profile;
    CheckBox chk_verify;
    DonutProgress d;
    public static BeanForProfessionalSearch bean;
    MyButton btn_event_req;
    AvailabilityAdapterForMyProfile listAdapter;
    static List<String> listDataHeader = new ArrayList<>();
    NonScrollExpandableListView expandable_list;
    public static String is_favorite = "0";
    static HashMap<String, List<String>> listDataChild = new HashMap<>();
    MyTextView txt_name, txt_address, txt_about, txt_social_score, txt_email, txt_mobile, txt_available;
    MyTextView txt_status, txt_count, txt_category, sub_category, txt_availability, tvNoAvailability;
    MyTextView tvLoading, txt_stablishment, txt_country, txt_person_name, txt_person_email, txt_person_mobile;

    int checkAsynck = 0;

    public OtherUserProfile() {
        // Required empty public constructor
    }

    ArrayList<NameValuePair> params;
    ImageButton editMyProfile;
    View rootView, viewLine, viewLineProfessional;
    View commonView, view2;
    MyButton btnEmail, btnMobile;
    int apiCheck = 0;
    LinearLayout layoutMain;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (OtherUserDetailsActivity.type.equals("Organization")) { // Inflate the layout for this fragment
            rootView = (View) inflater.inflate(R.layout.fragment_organisation_profile, container, false);
            txt_stablishment = (MyTextView) rootView.findViewById(R.id.txt_stablishment);
            txt_country = (MyTextView) rootView.findViewById(R.id.txt_country);
            txt_person_name = (MyTextView) rootView.findViewById(R.id.txt_person_name);
            txt_person_email = (MyTextView) rootView.findViewById(R.id.txt_person_email);
            txt_person_mobile = (MyTextView) rootView.findViewById(R.id.txt_person_mobile);
            txt_name = (MyTextView) rootView.findViewById(R.id.txt_name);
            viewLine = (View) rootView.findViewById(R.id.viewLine);
            View viewDesc = (View) rootView.findViewById(R.id.viewDesc);
            RelativeLayout layoutVer = (RelativeLayout) rootView.findViewById(R.id.layoutVer);
            LinearLayout layoutMail = (LinearLayout) rootView.findViewById(R.id.layoutMail);
            LinearLayout layoutMobile = (LinearLayout) rootView.findViewById(R.id.layoutMobile);
            LinearLayout layoutDesignation = (LinearLayout) rootView.findViewById(R.id.layoutDesignation);
            LinearLayout linearContactPerson = (LinearLayout) rootView.findViewById(R.id.linearContactPerson);
            linearContactPerson.setVisibility(View.GONE);
            btnEmail = (MyButton) rootView.findViewById(R.id.btnEmail);
            btnMobile = (MyButton) rootView.findViewById(R.id.btnMobile);
            btnEmail.setVisibility(View.GONE);
            btnMobile.setVisibility(View.GONE);
            commonView = viewLine;
            layoutVer.setVisibility(View.GONE);
            layoutMail.setVisibility(View.GONE);
            layoutMobile.setVisibility(View.GONE);
            layoutDesignation.setVisibility(View.GONE);
            viewDesc.setVisibility(View.VISIBLE);


        } else {
            rootView = (View) inflater.inflate(R.layout.fragment_proffessional_profile, container, false);
            txt_status = (MyTextView) rootView.findViewById(R.id.txt_status);
            txt_count = (MyTextView) rootView.findViewById(R.id.txt_count);
            txt_category = (MyTextView) rootView.findViewById(R.id.txt_category);
            sub_category = (MyTextView) rootView.findViewById(R.id.sub_category);
            txt_name = (MyTextView) rootView.findViewById(R.id.txt_name);
            txt_availability = (MyTextView) rootView.findViewById(R.id.txt_availability);
            tvNoAvailability = (MyTextView) rootView.findViewById(R.id.tvNoAvailability);
            txt_available = (MyTextView) rootView.findViewById(R.id.txt_available);
            btn_event_req = (MyButton) rootView.findViewById(R.id.btn_event_req);
            MyTextView tvTitleSocial = (MyTextView) rootView.findViewById(R.id.tvTitleSocial);
            MyTextView tvVerificationStatus = (MyTextView) rootView.findViewById(R.id.tvVerificationStatus);
            expandable_list = (NonScrollExpandableListView) rootView.findViewById(R.id.expandable_list);
            viewLineProfessional = (View) rootView.findViewById(R.id.viewLineProfessional);
            btnEmail = (MyButton) rootView.findViewById(R.id.btnEmail);
            btnMobile = (MyButton) rootView.findViewById(R.id.btnMobile);
            View view4 = (View) rootView.findViewById(R.id.view4);
            View view3 = (View) rootView.findViewById(R.id.view3);
            View view2 = (View) rootView.findViewById(R.id.view2);
            View view = (View) rootView.findViewById(R.id.view);
            View emailView = (View) rootView.findViewById(R.id.emailView);
            RelativeLayout layoutVerification = (RelativeLayout) rootView.findViewById(R.id.layoutVerification);
            layoutVerification.setVisibility(View.GONE);
            LinearLayout layoutEmail = (LinearLayout) rootView.findViewById(R.id.layoutEmail);
            LinearLayout layoutMobile = (LinearLayout) rootView.findViewById(R.id.layoutMobile);
            commonView = viewLineProfessional;
            OtherUserDetailsActivity.iv_event_profile_pic1.setVisibility(View.VISIBLE);
            OtherUserDetailsActivity.iv_event_profile_pic1.setEnabled(false);
            OtherUserDetailsActivity.profilepic.setEnabled(false);
            view.setVisibility(View.VISIBLE);
            btnEmail.setVisibility(View.GONE);
            btnMobile.setVisibility(View.GONE);
            layoutEmail.setVisibility(View.GONE);
            layoutMobile.setVisibility(View.GONE);
            view4.setVisibility(View.GONE);
            view3.setVisibility(View.GONE);
            txt_available.setVisibility(View.GONE);
            btn_event_req.setVisibility(View.GONE);
            tvTitleSocial.setVisibility(View.GONE);
            tvVerificationStatus.setVisibility(View.GONE);
            emailView.setVisibility(View.GONE);
            expandable_list.setGroupIndicator(null);
            expandable_list.setChildIndicator(null);


            listAdapter = new AvailabilityAdapterForMyProfile(getActivity(), listDataHeader, listDataChild);

            // setting list adapter
            expandable_list.setAdapter(listAdapter);

            for (int l = 0; l < listDataHeader.size(); l++) {

                expandable_list.expandGroup(l);

            }

            expandable_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {

                    return true;
                }
            });

            btn_event_req.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), InviteForEvent.class);
                    Mypreferences.Request_an_event_from = "profile";
//                ActivityOptions options =
//                        ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_to_right, R.anim.slide_from_left);
                    startActivity(i);

                    getActivity().overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                }
            });


        }

        txt_address = (MyTextView) rootView.findViewById(R.id.txt_address);
        txt_social_score = (MyTextView) rootView.findViewById(R.id.txt_social_score);
        tvLoading = (MyTextView) rootView.findViewById(R.id.tvLoading);
        layoutMain = (LinearLayout) rootView.findViewById(R.id.layoutMain);
        txt_about = (MyTextView) rootView.findViewById(R.id.txt_about);
        txt_email = (MyTextView) rootView.findViewById(R.id.txt_email);
        txt_mobile = (MyTextView) rootView.findViewById(R.id.txt_mobile);
        chk_verify = (CheckBox) rootView.findViewById(R.id.chk_verify);
        img_profile = (RoundedImageView) rootView.findViewById(R.id.img_profile);

        txt_social_score.setVisibility(View.GONE);
        chk_verify.setVisibility(View.GONE);

        d = (DonutProgress) rootView.findViewById(R.id.loading);
        tvLoading.setVisibility(View.VISIBLE);
        layoutMain.setVisibility(View.GONE);

        params = getParams();
        AsyncRequest getPosts = new AsyncRequest(OtherUserProfile.this, getActivity(), "POST", params);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.GETPROFILEDETAIL);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (("" + is_favorite).equals("1")) {
            OtherUserDetailsActivity.iv_event_profile_pic1.setBackgroundResource(R.drawable.favoriteborder);
        } else {
            OtherUserDetailsActivity.iv_event_profile_pic1.setBackgroundResource(R.drawable.favoritebordergray);
        }
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userid", "" + OtherUserDetailsActivity.userId));
        params.add(new BasicNameValuePair("type", "" + OtherUserDetailsActivity.type));
        params.add(new BasicNameValuePair("time_offset", "" + TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60000));

        //  params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
        return params;
    }


    @Override
    public void asyncResponse(String response) {

        try {
            JSONObject postObject = new JSONObject(response);

            Log.v("akram", "resposen profile = " + response);
            checkAsynck = 1;

            // view.hide();
            if (postObject.getString("success").equals("true")) {
                tvLoading.setVisibility(View.GONE);
                layoutMain.setVisibility(View.VISIBLE);

                JSONObject result = postObject.getJSONObject("result");

                if (postObject.getString("type").equals("Professional")) {

                    OtherUserDetailsActivity.txt_name.setText("" + result.getString("first_name") + " " + result.getString("last_name"));
                    OtherUserDetailsActivity.txt_profession.setText("" + result.getString("category") + " " + result.getString("subcategory"));
                    OtherUserDetailsActivity.txt_loaction.setText("" + result.getString("city") + " " + result.getString("state"));

                    OtherUserDetailsActivity.txt_loaction.setText("" + result.getString("city") + " " + result.getString("state"));

                    com.socialide.Helper.ImageLoader.image(result.getString("profile_pic"), OtherUserDetailsActivity.profilepic, OtherUserDetailsActivity.d, getActivity());
                    com.socialide.Helper.ImageLoader.image(result.getString("profile_pic"), OtherUserDetailsActivity.img_back, OtherUserDetailsActivity.d, getActivity());

                    txt_category.setText("" + add_space + "" + result.getString("category"));
                    sub_category.setText("" + add_space + "" + result.getString("subcategory"));
                    txt_address.setText("" + result.getString("city") + " " + result.getString("state"));

                    Log.v("akram", "profile pic = " + result.getString("profile_pic"));
                    com.socialide.Helper.ImageLoader.image(result.getString("profile_pic"), img_profile, d, getActivity());


                    if (result.getString("about_me").equalsIgnoreCase("")) {
                        txt_about.setVisibility(View.GONE);
                        commonView.setVisibility(View.GONE);

                    } else {
                        txt_about.setText(result.getString("about_me"));
                    }
                    String status = result.getString("is_available");

                    is_favorite = result.getString("is_favorite");


                    if (("" + is_favorite).equals("1")) {
                        OtherUserDetailsActivity.iv_event_profile_pic1.setBackgroundResource(R.drawable.favoriteborder);
                    } else {

                        OtherUserDetailsActivity.iv_event_profile_pic1.setBackgroundResource(R.drawable.favoritebordergray);

                    }

                    if (status.equalsIgnoreCase("0")) {
                        txt_status.setText(getResources().getString(R.string.dialog_btnNO));


                    } else {
                        txt_status.setText(getResources().getString(R.string.dialog_btnYes));
                    }

                    txt_count.setText("" + result.getString("maximum_event_request"));

                    String[] reverse = result.getString("availability").split("@@");
                    listDataHeader = new ArrayList<>();
                    listDataChild = new HashMap<>();
                    for (int i = 0; i < reverse.length; i++) {
                        ArrayList<String> a = new ArrayList();
                        String[] reverse2 = reverse[i].split("_");
                        if (reverse2.length > 1) {
                            listDataHeader.add(reverse2[0]);


                            if (reverse2.length > 1) {

                                String reverse3[] = reverse2[1].split(",");
                                for (int j = 0; j < reverse3.length; j++) {

                                    a.add("" + reverse3[j]);

                                }
                            }

                            if (a.size() == 0) {
                                a.add("Not Available");
                            }
                        }
                        listDataChild.put(reverse2[0], a);
                    }

                    if (listDataHeader.size() != 0) {
                        tvNoAvailability.setVisibility(View.GONE);
                        expandable_list.setVisibility(View.VISIBLE);

                        listAdapter = new AvailabilityAdapterForMyProfile(getActivity(), listDataHeader, listDataChild);
                        // setting list adapter
                        expandable_list.setAdapter(listAdapter);

                    } else {
                        tvNoAvailability.setVisibility(View.VISIBLE);
                        tvNoAvailability.setText("Professional not set any availability");
                        expandable_list.setVisibility(View.GONE);
                    }

                    for (int l = 0; l < listDataHeader.size(); l++) {

                        expandable_list.expandGroup(l);
                    }

                    txt_name.setText("" + result.getString("first_name") + " " + result.getString("last_name"));


                    // btn_event_req.setVisibility(View.VISIBLE);


                    bean = new BeanForProfessionalSearch("" + result.getString("userid"), "" + result.getString("first_name"), "" + result.getString("last_name"), "" + result.getString("dob"),
                            "" + result.getString("experience"), "" + result.getString("experience"), "" + result.getString("email"), "" + result.getString("mobile_number"),
                            "" + result.getString("state"), "" + result.getString("city"), "" + result.getString("profile_pic"), "" + result.getString("gender"),
                            "" + result.getString("country"), "" + result.getString("current_company"), "" + result.getString("category"), "" + result.getString("subcategory"),
                            "" + result.getString("profession"), "" + result.getString("about_me"), "" + result.getString("maximum_event_request"), "" + result.getString("availability"),
                            "" + result.getString("social_score"), "" + result.getString("join_flow_completed"), "" + result.getString("is_available")
                            , "" + result.getString("is_verified"), "" + result.getString("is_favorite"));


                    OtherUserDetailsActivity.iv_event_profile_pic1.setEnabled(true);
                    OtherUserDetailsActivity.profilepic.setEnabled(true);


                    if (Mypreferences.User_Type.equals("Organization")) {


                        if ((bean.getIs_available()).equalsIgnoreCase("0")) {

                            txt_available.setVisibility(View.VISIBLE);
                            btn_event_req.setVisibility(View.GONE);
                            txt_available.setText("" + bean.getFirst_name() + " is not available");
                        } else {

                            txt_available.setVisibility(View.GONE);
                            btn_event_req.setVisibility(View.VISIBLE);
                        }
                    } else {
                        txt_available.setVisibility(View.GONE);
                        btn_event_req.setVisibility(View.GONE);
                    }

                } else if (postObject.getString("type").equals("Organization")) {

                    OtherUserDetailsActivity.txt_name.setText(result.getString("name"));
                    OtherUserDetailsActivity.txt_profession.setText("" + result.getString("contact_position"));
                    OtherUserDetailsActivity.txt_loaction.setText("" + result.getString("city") + " " + result.getString("state"));

                    com.socialide.Helper.ImageLoader.image(result.getString("profile_pic"), OtherUserDetailsActivity.profilepic, OtherUserDetailsActivity.d, getActivity());


                    txt_name.setText(result.getString("name"));
                    txt_address.setText("" + result.getString("city") + " " + result.getString("state"));

                    com.socialide.Helper.ImageLoader.image(result.getString("profile_pic"), img_profile, d, getActivity());
                    com.socialide.Helper.ImageLoader.image(result.getString("profile_pic"), OtherUserDetailsActivity.img_back, OtherUserDetailsActivity.d, getActivity());

                    if (result.getString("about").equalsIgnoreCase("")) {
                        txt_about.setVisibility(View.GONE);
                        commonView.setVisibility(View.GONE);

                    } else {
                        txt_about.setText(result.getString("about"));
                    }

                    txt_stablishment.setText("" + add_space + result.getString("establishment_year"));
                    txt_country.setText("" + add_space + result.getString("country"));
                    txt_person_name.setText("" + add_space + result.getString("contact_firstname") + " " + result.getString("contact_lastname"));
                    txt_person_email.setText("" + add_space + result.getString("email"));
                    txt_person_mobile.setText("" + add_space + result.getString("contact_number"));


                }
                OtherUserDetailsActivity.tvLoading.setVisibility(View.GONE);
            } else {
                CustomSnackBar.toast(getActivity(), postObject.getString("message"));
            }
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (JSONException e) {
            Log.v("akram", "catch = " + e);
            e.printStackTrace();
        }
    }
//
}





