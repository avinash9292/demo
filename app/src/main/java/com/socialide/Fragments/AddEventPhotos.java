package com.socialide.Fragments;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gun0912.tedpicker.ImagePickerActivity;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.OrgEventDetail;
import com.socialide.Activities.ProfessionalShceduledEvntDetail;
import com.socialide.Adapters.EventPhotosAdapter;
import com.socialide.Adapters.InformationAdapter;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.R;

import java.util.ArrayList;

public class AddEventPhotos extends Fragment implements View.OnClickListener {
    View rootView;
    RecyclerView mRecyclerView;
    EventPhotosAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        // adapter = new EventPhotosAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);
    }

    public AddEventPhotos() {

        // Required empty public constructor
    }

    private static final int INTENT_REQUEST_GET_IMAGES = 13;

    private void getImages() {

        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_events_photo_upload, container, false);
        initRecyclerView();

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                getImages();
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

        return rootView;
    }


    @Override
    public void onClick(View v) {

    }


    @Override
    public void onActivityResult(int requestCode, int resuleCode, Intent intent) {
        super.onActivityResult(requestCode, resuleCode, intent);

        if (requestCode == INTENT_REQUEST_GET_IMAGES && resuleCode == Activity.RESULT_OK) {

            ArrayList<Uri> image_uris = intent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

            //do something
        }
    }
}
