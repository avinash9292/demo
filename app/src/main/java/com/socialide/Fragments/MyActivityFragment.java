package com.socialide.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Activities.FullScreenViewActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.PostDetails1;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.AsyncTask.ImageConvertAsync;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForActivities;
import com.socialide.Model.BeanForActivityImages;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.widget.RelativeLayout.LayoutParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyActivityFragment extends Fragment implements AsyncRequest.OnAsyncRequestComplete {
    private boolean loading = true;
    int pos;
    String liked;
    MyTextView txt_no_item;
    int pagecount = 10;
    AVLoadingIndicatorView view;
    int pageindex = 1;
    int position_for_delete;

    public ArrayList<BeanForActivities> activity_list = new ArrayList<>();
    public static ArrayList<BeanForActivities> activity_list1 = new ArrayList<>();


    private int lastVisibleItem;
    public static boolean isLoading = true;
    int responsetype = 0;
    boolean check = false, islike = false;
    LinearLayoutManager linearLayoutManager;


    ArrayList<BeanForActivityImages> image_list = new ArrayList<>();
    ArrayList<NameValuePair> params;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    static PopupWindow mPopupWindow;
    static RelativeLayout rl;
    public static int checkAsync = 0;
    MainActivity mainActivity;
    Activity activity;

    public MyActivityFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_recycler_view_non_refresh, container, false);
        mainActivity = (MainActivity) getActivity();
        initRecyclerView();

        if (checkAsync == 1 && activity_list.size() == 0) {
            txt_no_item.setVisibility(View.VISIBLE);
            txt_no_item.setText(getResources().getString(R.string.noActivity));

        } else if (activity_list.size() == 0) {
            txt_no_item.setVisibility(View.VISIBLE);
            txt_no_item.setText(getResources().getString(R.string.loading));
        } else {
            txt_no_item.setVisibility(View.GONE);
        }

        if (checkAsync == 0) {

            params = getParams(pagecount, pageindex);
            AsyncRequest getPosts = new AsyncRequest(this, mainActivity, "POST", params, view, 1);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.MYACTIVITY);
        }

        return rootView;
    }

    View rootView;
    RecyclerView mRecyclerView;
    ActivityAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;


    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        // use a linear layout manager

        txt_no_item = (MyTextView) rootView.findViewById(R.id.txt_no_item);
        rl = (RelativeLayout) rootView.findViewById(R.id.rl);
        view = (AVLoadingIndicatorView) mainActivity.findViewById(R.id.avi);

        mLayoutManager = new LinearLayoutManager(mainActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);


        adapter = new ActivityAdapter(mainActivity);

        mRecyclerView.setAdapter(adapter);


        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(mainActivity, mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(mainActivity, R.anim.slide_to_right, R.anim.slide_from_left);


                Intent i = new Intent(mainActivity, PostDetails1.class);

                //     EventFragment.position = position;


            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //  adapter.notifyItemInserted(activity_list.size() - 1);
                pageindex = pageindex + 1;
                Log.v("akram", "onLoadMore");
                params = getParams(pagecount, pageindex);
                AsyncRequest getPosts = new AsyncRequest(MyActivityFragment.this, mainActivity, "POST", params, view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.MYACTIVITY);

            }
        });

    }


    private ArrayList<NameValuePair> getParams(int pagecount, int pageindex) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
//

        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        responsetype = 0;
        params.add(new BasicNameValuePair("pagecount", "" + pagecount));
        params.add(new BasicNameValuePair("pageindex", "" + pageindex));
        params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));


        return params;
    }

    public static void popup(final Context c) {


        final View popupView = LayoutInflater.from(c).inflate(R.layout.activity_option_dropdown_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                Color.TRANSPARENT));


        // Get a reference for the custom view close MyButton
        MyButton done = (MyButton) popupView.findViewById(R.id.btn_submit);
        ImageView cross = (ImageView) popupView.findViewById(R.id.img_cross);


        // Set a click listener for the popup window close button
        done.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window

                mPopupWindow.dismiss();


            }

        });

        cross.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window

                mPopupWindow.dismiss();


            }

        });
        mPopupWindow.setAnimationStyle(R.style.DialogAnimation);
        mPopupWindow.showAtLocation(rl, Gravity.BOTTOM, 0, 0);

    }


    private ArrayList<NameValuePair> getParams(String postid, String postType) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//           params.add(new BasicNameValuePair("userid", ""+ Mypreferences.User_id));
//              params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        responsetype = 1;
        params.add(new BasicNameValuePair("eventId", "" + postid));
        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
        //   params.add(new BasicNameValuePair("postType", ""+postType));


        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));

        return params;
    }


    private ArrayList<NameValuePair> getParams(String postid) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//           params.add(new BasicNameValuePair("userid", ""+ Mypreferences.User_id));
//              params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        responsetype = 2;
        params.add(new BasicNameValuePair("activity_id", "" + postid));
//        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
//        //   params.add(new BasicNameValuePair("postType", ""+postType));
//        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
//        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));

        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            Log.e("eeeeee", "sssss  " + response);
            FunctionClass.logCatLong(response);

            JSONObject postObject = new JSONObject(response);

            BeanForActivities a = null;
//
            //  {"success":"true","message":"Scheduled event details fetched successfully.","result":{"event_id":"15","profile_image":null,"event_name":"inform the authorities about illegal activities ","event_title":"telling someone you love them via the phone ","event_date":"21 May 2017","contact_details":{"professional_name":"AVINASH TIWARI","professional_profession":null,"professional_email":"aa@aa.aa","professional_mobile_no":null},"userid":"183","type":"Professional","sharecontact":"1"}}
            checkAsync = 1;

            if (postObject.getString("success").equals("true")) {

                if (postObject.getString("api_name").equals("getMyPosts")) {

                    if (pageindex == 1) {
                        activity_list = new ArrayList<>();
                    }
                    MainActivity.img_setting.setVisibility(View.VISIBLE);
                    JSONArray result = postObject.getJSONArray("activity");

                    for (int i = 0; i < result.length(); i++) {
                        JSONObject result_object = result.getJSONObject(i);

                        if (!result_object.getString("activity_type").equals("article")) {
                            JSONArray imagedata = result_object.getJSONArray("event_photos");

                            if (Mypreferences.User_id.equalsIgnoreCase(result_object.getString("organization_id"))
                                    || Mypreferences.User_id.equalsIgnoreCase(result_object.getString("professional_id"))) {


                                image_list = new ArrayList<>();
                                int check = 0;
                                for (int k = 0; k < imagedata.length(); k++) {

                                    JSONObject image = imagedata.getJSONObject(k);
                                    if (Mypreferences.User_Type.equalsIgnoreCase(image.getString("type"))) {
                                        check = 1;
                                        image_list.add(new BeanForActivityImages("" + image.getString("image_id"), "" + image.getString("image_url"), "" + image.getString("userid"), "" + image.getString("type")));
                                    }
                                }
                                if (check == 1) {

                                    activity_list.add(new BeanForActivities("" + result_object.getString("activity_type"), "" + result_object.getString("activity_id"), "" + result_object.getString("activity_user_id"), "" + result_object.getString("event_title"),
                                            "" + result_object.getString("event_date"), "" + result_object.
                                            getString("event_time"), "" + result_object.getString("message"),
                                            "" + result_object.getString("event_location"), "" + result_object.getString("professional_name"),
                                            "" + result_object.getString("organisation_name"), "" + result_object.getString("profile_pic"),
                                            "" + result_object.getString("no_of_like"), image_list, "" + result_object.getString("isUserLikedPost"),
                                            result_object.getString("activity_date_time"), true, result_object.getString("organization_id"), result_object.getString("professional_id"),
                                            "" + result_object.getString("activity_by"), "" + result_object.getString("event_description"), "" + result_object.getString("event_id"), "" + result_object.getString("isHiddenByReporting")));
                                }
                            }
                        } else {
                            if (Mypreferences.User_Type.equalsIgnoreCase(result_object.getString("activity_by")) &&
                                    Mypreferences.User_id.equalsIgnoreCase(result_object.getString("activity_user_id"))) {

                                activity_list.add(new BeanForActivities("" + result_object.getString("activity_type"), "" + result_object.getString("activity_id"), "" + result_object.getString("activity_user_id"), "" + result_object.getString("article_name"),
                                        "" + result_object.getString("article_date"), "" + result_object.getString("time"), "" + result_object.getString("article_by"),
                                        "" + result_object.getString("description"), "" + result_object.getString("profile_pic"), "" + result_object.getString("no_of_like"), "" + result_object.getString("article_image"), "" + result_object.getString("activity_by"), "" + result_object.getString("isUserLikedPost"), result_object.getString("activity_date_time"), true, "" + result_object.getString("isHiddenByReporting")));
                            }

                        }

                    }

                    if (activity_list.size() == 0) {
                        txt_no_item.setVisibility(View.VISIBLE);
                        txt_no_item.setText(getResources().getString(R.string.noActivity));
                    } else if (activity_list.size() >= pagecount) {

                        adapter.setLoaded();
                        txt_no_item.setVisibility(View.GONE);

                    } else {
                        check = true;
                        txt_no_item.setVisibility(View.GONE);

                    }


//                               adapter= new ActivityAdapter(getActivity());
//                               mRecyclerView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();

                } else if (postObject.getString("api_name").equals("deleteActivity")) {

                    activity_list.remove(position_for_delete);
                    adapter.notifyDataSetChanged();
                    CustomSnackBar.toast(getActivity(), "" + postObject.optString("message"));
                } else {

                    a = activity_list.get(pos);
                    if (postObject.getString("success").equals("true")) {


                        int like = Integer.parseInt(a.getNo_of_like());
                        if (!postObject.getString("message").contains("disliked")) {


                            like++;

                            a.setNo_of_like("" + like);
                            a.setIsUserLikedPost("1");
                            a.setIsliked(true);

                        } else {

                            like = like - 1;
                            a.setIsliked(true);

                            a.setNo_of_like("" + like);
                            a.setIsUserLikedPost("0");

                        }


                        activity_list.remove(pos);
                        activity_list.add(pos, a);
                        adapter.notifyDataSetChanged();

                    }
                }

            } else {

                if (postObject.getString("api_name").equals("getMyPosts")) {
                    if (pageindex == 1) {
                        txt_no_item.setVisibility(View.VISIBLE);
                        txt_no_item.setText(getResources().getString(R.string.noActivity));
                    }
                } else if (postObject.getString("api_name").equals("deleteActivity")) {
                    CustomSnackBar.toast(getActivity(), "" + postObject.optString("message"));
                } else {
                    if (a != null) {
                        a.setIsliked(true);
                        adapter.notifyDataSetChanged();
                    }
                }
            }

        } catch (JSONException e) {
            Log.v("akram", "catch = " + e);
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityViewHolder> implements View.OnClickListener {
        String text;
        Context c;

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        private final int BUTTON_TYPE = 2;
        Intent sharingIntent;

        ArrayList<Uri> filesUri;
        ProgressDialog dialog;
        Uri uri;
        String url = "";
        ArrayList<String> imgArr;

        @Override
        public int getItemViewType(int position) {
            if (position == activity_list.size()) {
                return BUTTON_TYPE;
            } else if (activity_list.get(position) == null) {
                return VIEW_TYPE_LOADING;
            } else {
                return VIEW_TYPE_ITEM;
            }
        }

        public ActivityAdapter(Context c) {
            this.c = c;


            linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                        if (!MyActivityFragment.isLoading && totalItemCount <= (lastVisibleItem + 1)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        MyActivityFragment.isLoading = true;
                    }
                }
            });

        }

        OnLoadMoreListener mOnLoadMoreListener;

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void setLoaded() {
            MyActivityFragment.isLoading = false;
        }

        @Override
        public ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ActivityViewHolder viewHolder = null;
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_activity_item, parent, false);

                viewHolder = new ActivityViewHolder(view);
                viewHolder.iv_activity_profilePic = (RoundedImageView) view.findViewById(R.id.iv_event_profile_pic);
                viewHolder.tv_event_time = (MyTextView) view.findViewById(R.id.tv_event_time);
                viewHolder.tv_event_description = (MyTextView) view.findViewById(R.id.tv_event_description);
                viewHolder.tv_event_name = (MyTextView) view.findViewById(R.id.tv_event_name);
                viewHolder.txt_likes = (MyTextView) view.findViewById(R.id.txt_likes);
                viewHolder.txt_like = (MyTextView) view.findViewById(R.id.txt_like);
                viewHolder.img_share = (ImageView) view.findViewById(R.id.img_share);
                viewHolder.img_share_new = (ImageView) view.findViewById(R.id.img_share_new);
                viewHolder.img_like = (ImageView) view.findViewById(R.id.img_like);
                viewHolder.img_arrow = (ImageView) view.findViewById(R.id.img_arrow);
                viewHolder.iv_activity_event = (ImageView) view.findViewById(R.id.iv_activity_event);
                viewHolder.txt_share = (MyTextView) view.findViewById(R.id.txt_share);
                viewHolder.txt_share_new = (MyTextView) view.findViewById(R.id.txt_share_new);
                viewHolder.txt_count = (MyTextView) view.findViewById(R.id.txt_count);
                viewHolder.txt_loading = (MyTextView) view.findViewById(R.id.txt_loading);
                viewHolder.view_like = (ProgressBar) view.findViewById(R.id.view_like);
                viewHolder.rl11 = (RelativeLayout) view.findViewById(R.id.rl11);
                viewHolder.d = (DonutProgress) view.findViewById(R.id.loading);
                viewHolder.d1 = (DonutProgress) view.findViewById(R.id.loading1);
                viewHolder.viewImage = (View) view.findViewById(R.id.viewImage);
                viewHolder.imgReported = (ImageView) view.findViewById(R.id.imgReported);
                //viewHolder.relativeReport = (RelativeLayout) view.findViewById(R.id.relativeReport);

                viewHolder.rl = (RelativeLayout) view.findViewById(R.id.rl);

            } else if (viewType == BUTTON_TYPE) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contain_view, parent, false);

                viewHolder = new ActivityViewHolder(view);
                return viewHolder;
            }


            Bitmap dummy = BitmapFactory.decodeResource(view.getResources(), R.drawable.profilepic);
//
//        dummy= ImageHelper.getRoundedCornerBitmap(dummy,10);
//        viewHolder.iv_activity_profilePic.setImageBitmap(dummy);


            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ActivityViewHolder holder, final int position) {

            if (position == activity_list.size()) {
                return;
            }


            if (holder instanceof ActivityViewHolder) {
                final BeanForActivities b = activity_list.get(position);
                holder.iv_activity_profilePic.setImageResource(R.drawable.profilepic);
                ArrayList<BeanForActivityImages> al = b.getEvent_photos();


                LayoutParams lp = (LayoutParams) holder.tv_event_description.getLayoutParams();
                if (b.getIsHiddenByReporting().equalsIgnoreCase("1")) {


                    // lp.addRule(RelativeLayout.BELOW, holder.relativeReport.getId());
                    holder.tv_event_name.setTextColor(Color.parseColor("#50000000"));
                    holder.tv_event_time.setTextColor(Color.parseColor("#50000000"));
                    holder.tv_event_description.setTextColor(Color.parseColor("#50000000"));
                    holder.viewImage.setVisibility(View.VISIBLE);
                    holder.imgReported.setVisibility(View.VISIBLE);
                } else {
                    // lp.addRule(RelativeLayout.BELOW, holder.rl11.getId());
                    holder.tv_event_name.setTextColor(Color.parseColor("#000000"));
                    holder.tv_event_time.setTextColor(Color.parseColor("#000000"));
                    holder.tv_event_description.setTextColor(Color.parseColor("#000000"));
                    holder.viewImage.setVisibility(View.GONE);
                    holder.imgReported.setVisibility(View.GONE);
                }

                if (!b.getIsUserLikedPost().equals("0")) {
                    holder.txt_like.setText(getResources().getString(R.string.liked));
                    holder.img_like.setImageResource(R.drawable.like_green_icon);
                    holder.txt_like.setTextColor(Color.parseColor("#3fb4b5"));
                } else {

                    holder.txt_like.setText(getResources().getString(R.string.like));
                    holder.img_like.setImageResource(R.drawable.like_gray_icon);
                    holder.txt_like.setTextColor(Color.parseColor("#727272"));


                }
                if (b.isliked()) {
                    holder.txt_like.setEnabled(true);


                    holder.view_like.setVisibility(View.GONE);
                    holder.img_like.setVisibility(View.VISIBLE);


                } else {
                    holder.txt_like.setEnabled(false);

                    holder.view_like.setVisibility(View.VISIBLE);
                    holder.img_like.setVisibility(View.GONE);
                    //  holder.view_like.show();

                }

                if (b.getActivity_type().equals("article")) {


                    String[] dater = b.getActivity_date_time().trim().split(" ");

                    holder.img_arrow.setVisibility(View.VISIBLE);


                    if (TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]).equals("false")) {
                        holder.tv_event_time.setText(getResources().getString(R.string.added) + " " + dater[0] + " " + dater[1] + " " + dater[2]);

                    } else {
                        holder.tv_event_time.setText(getResources().getString(R.string.added) + " " + TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]));
                    }
                    if (b.getDescription().equals("")) {
                        holder.tv_event_description.setVisibility(View.GONE);
                    } else {
                        holder.tv_event_description.setVisibility(View.VISIBLE);

                    }


                    //  holder.tv_event_name.setText("" + b.getArticle_name());
//3fb4b5

                    holder.tv_event_name.setText(Html.fromHtml("<b>" + b.getArticle_name() + "</b> " + getResources().getString(R.string.articleAddedBy) + " <b>" + b.getArticle_by() + "</b>"));


                    if (Integer.parseInt(b.getNo_of_like()) == 1) {
                        holder.txt_likes.setText("" + b.getNo_of_like() + " " + getResources().getString(R.string.like));

                    } else {
                        holder.txt_likes.setText("" + b.getNo_of_like() + " " + getResources().getString(R.string.likes));
                    }


                    holder.txt_share.setOnClickListener(this);
                    holder.img_share.setOnClickListener(this);

                    text = holder.tv_event_description.getText().toString();
                    holder.txt_count.setVisibility(View.GONE);
                    if (!b.getArticle_image().equals("")) {
                        holder.rl11.setVisibility(View.VISIBLE);
                        if (b.getDescription().length() >= 95) {
                            // holder.tv_event_description.setText("" + b.getDescription());

                            String s = b.getDescription().substring(0, 80) + "...";
                            holder.tv_event_description.setText(Html.fromHtml(s + "<font color='#3fb4b5'><u> " + getResources().getString(R.string.readMore) + "</u></font>"), TextView.BufferType.SPANNABLE);

                        } else {
                            holder.tv_event_description.setText(b.getDescription());
                        }

                        com.socialide.Helper.ImageLoader.image(b.getArticle_image(), holder.iv_activity_event, holder.d1, mainActivity);
                    } else {
                        holder.rl11.setVisibility(View.GONE);

                        if (b.getDescription().length() >= 450) {
                            // holder.tv_event_description.setText("" + b.getDescription());

                            String s = b.getDescription().substring(0, 430) + "...";
                            holder.tv_event_description.setText(Html.fromHtml(s + "<font color='#3fb4b5'><u> " + getResources().getString(R.string.readMore) + "</u></font>"), TextView.BufferType.SPANNABLE);

                        } else {
                            holder.tv_event_description.setText(b.getDescription());
                        }
                    }
                    com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, holder.iv_activity_profilePic, holder.d, mainActivity);
                    //      notifyItemChanged(position);

                } else {
                    String[] dater = b.getActivity_date_time().trim().split(" ");

                    holder.img_arrow.setVisibility(View.VISIBLE);

                    if (TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]).equals("false")) {
                        holder.tv_event_time.setText(getResources().getString(R.string.added) + " " + dater[0] + " " + dater[1] + " " + dater[2]);

                    } else {
                        holder.tv_event_time.setText(getResources().getString(R.string.added) + " " + TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]));
                    }


                    //    holder.tv_event_time.setText("Added " + TimeConveter.agotime(b.getEvent_date(), b.getEvent_time()));
                    holder.rl11.setVisibility(View.VISIBLE);


                    // holder.tv_event_description.setText("" + b.getMessage());
                    if (b.getMessage().equals("")) {
                        holder.tv_event_description.setVisibility(View.GONE);
                    } else {
                        holder.tv_event_description.setVisibility(View.VISIBLE);

                    }
                    if (b.getDescription().length() >= 95) {
                        // holder.tv_event_description.setText("" + b.getDescription());

                        String s = b.getDescription().substring(0, 80) + "...";
                        holder.tv_event_description.setText(Html.fromHtml(s + "<font color='#3fb4b5'><u> " + getResources().getString(R.string.readMore) + "</u></font>"));

                    } else {
                        holder.tv_event_description.setText(b.getDescription());
                    }

                    if (!Mypreferences.User_Type.equals("Organization")) {
                        holder.tv_event_name.setText(Html.fromHtml("<b>" + b.getProfessional_name() + "</b> " + getResources().getString(R.string.added) + " " + b.getEvent_photos().size() + " " + getResources().getString(R.string.photosForEvent) + " <b>" + b.getEvent_title() + "</b>"));
                    } else {
                        holder.tv_event_name.setText(Html.fromHtml("<b>" + b.getOrganisation_name() + "</b> " + getResources().getString(R.string.added) + " " + b.getEvent_photos().size() + " " + getResources().getString(R.string.photosForEvent) + " <b>" + b.getEvent_title() + "</b>"));

                    }


                    //     holder.tv_event_name.setText("" + b.getEvent_title());
                    if (Integer.parseInt(b.getNo_of_like()) == 1) {
                        holder.txt_likes.setText("" + b.getNo_of_like() + " " + getResources().getString(R.string.like));

                    } else {
                        holder.txt_likes.setText("" + b.getNo_of_like() + " " + getResources().getString(R.string.likes));
                    }
                    com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, holder.iv_activity_profilePic, holder.d, mainActivity);


                    if (b.getEvent_photos().size() > 0) {

                        Log.e("aaaa", "a" + b.getEvent_photos().
                                get(0).getId());

                        com.socialide.Helper.ImageLoader.image(b.getEvent_photos().get(0).getImageUrl(), holder.iv_activity_event, holder.d1, mainActivity);
                        if (b.getEvent_photos().size() > 1) {
                            int i = b.getEvent_photos().size() - 1;
                            holder.txt_count.setText("+ " + i);
                            holder.txt_count.setVisibility(View.VISIBLE);


                        } else {
                            holder.txt_count.setVisibility(View.GONE);
                        }

                    }


                }

                holder.img_share_new.setTag(position);
                holder.txt_share_new.setTag(position);
                holder.img_share_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int pos = (Integer) view.getTag();

                        ArrayList<BeanForActivityImages> al1;

                        imgArr = new ArrayList<>();

                        final BeanForActivities b = activity_list.get(pos);

                        if (b.getActivity_type().equals("article")) {
                            text = b.getDescription();
                            imgArr.add("" + b.getArticle_image());

                        } else {
                            text = b.getMessage();
                            al1 = b.getEvent_photos();

                            for (int k = 0; k < al1.size(); k++) {

                                imgArr.add(al1.get(k).getImageUrl());

                            }
                        }
                        filesUri = new ArrayList<Uri>();
                        uri = null;

                        new ImageConvertAsync(imgArr, text, getContext()).execute();


                    }
                });

                holder.txt_share_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        holder.img_share_new.performClick();
                    }
                });


                holder.tv_event_description.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(c, PostDetails1.class);
                        i.putExtra("pos", position);
                        i.putExtra("type", 1);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(c, R.anim.slide_to_right, R.anim.slide_from_left);
                        activity_list1.clear();
                        activity_list1.addAll(activity_list);
                        c.startActivity(i, options.toBundle());

                    }
                });


                holder.iv_activity_event.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(c, FullScreenViewActivity.class);
                        i.putExtra("pos", position);
                        i.putExtra("type", 1);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(c, R.anim.slide_to_right, R.anim.slide_from_left);
                        activity_list1.clear();
                        activity_list1.addAll(activity_list);
                        c.startActivity(i, options.toBundle());
                    }
                });

                holder.img_arrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // ActivityFragment.popup(c);
                        report_popup(position, b.getActivity_id());
                    }
                });
                like = Integer.parseInt(b.getNo_of_like());

                holder.img_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pos = position;
//                        if (holder.txt_like.getText().toString().equals("Like")) {
//
//
//                          like=  like+1;
////
////                            b.setNo_of_like("" + like);
////                            b.setIsUserLikedPost("1");
//                            holder.txt_like.setText("Liked");
//                            holder.txt_likes.setText("" + like);
//
//                        } else {
//
//                            like = like - 1;
//
//                            holder.txt_likes.setText("" + like);
//                            //a.setIsUserLikedPost("0");
//                            holder.txt_like.setText("Like");
//                        }

                        if (b.isliked()) {

                            b.setIsliked(false);
                            holder.txt_like.setEnabled(false);

                            holder.view_like.setVisibility(View.VISIBLE);
                            holder.img_like.setVisibility(View.GONE);
                            //  holder.view_like.show();
                            params = getParams(b.getActivity_id(), b.getActivity_type());
                            AsyncRequest getPosts = new AsyncRequest(MyActivityFragment.this, mainActivity, "POST", getParams(b.getActivity_id(), b.getActivity_type()), null);
                            getPosts.execute(GloabalURI.baseURI + GloabalURI.LIKE_DISLIKE);


                        }

                    }
                });

                holder.txt_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pos = position;
//                        if (holder.txt_like.getText().toString().equals("Like")) {
//
//
//                          like=  like+1;
////
////                            b.setNo_of_like("" + like);
////                            b.setIsUserLikedPost("1");
//                            holder.txt_like.setText("Liked");
//                            holder.txt_likes.setText("" + like);
//
//                        } else {
//
//                            like = like - 1;
//
//                            holder.txt_likes.setText("" + like);
//                            //a.setIsUserLikedPost("0");
//                            holder.txt_like.setText("Like");
//                        }

                        if (b.isliked()) {

                            b.setIsliked(false);
                            holder.txt_like.setEnabled(false);

                            holder.view_like.setVisibility(View.VISIBLE);
                            holder.img_like.setVisibility(View.GONE);
                            //holder.view_like.show();


                            params = getParams(b.getActivity_id(), b.getActivity_type());
                            AsyncRequest getPosts = new AsyncRequest(MyActivityFragment.this, mainActivity, "POST", getParams(b.getActivity_id(), b.getActivity_type()), null);
                            getPosts.execute(GloabalURI.baseURI + GloabalURI.LIKE_DISLIKE);


                        }

                    }
                });
            }


        }


        public Uri getLocalBitmapUri(Bitmap bmp) {
            Uri bmpUri = null;
            try {
                File file = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
                bmpUri = Uri.fromFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmpUri;
        }

        class ActivityViewHolder extends RecyclerView.ViewHolder {

            public ImageView img_share, img_share_new, img_like, iv_activity_event, img_arrow;
            public MyTextView tv_event_name;
            public RoundedImageView iv_activity_profilePic;
            DonutProgress d, d1;
            public MyTextView tv_event_time;
            public MyTextView tv_event_description;
            public MyTextView txt_share, txt_count, txt_share_new;
            RelativeLayout rl, rl11;
            ProgressBar view_like;
            public MyTextView txt_likes, txt_like, txt_loading;
            View viewImage;
            ImageView imgReported;
            // RelativeLayout relativeReport;

            public ActivityViewHolder(View itemView) {
                super(itemView);
            }
        }


        @Override
        public int getItemCount() {
            if (activity_list.size() != 0) {
                return activity_list.size() + 1;
            } else {
                return 0;
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img_share:
                    sharingIntent = new Intent(Intent.ACTION_SEND);


                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Socialide");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
                    c.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                    break;
                case R.id.txt_share:
                    sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Socialide");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, text);
                    c.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                    break;

            }
        }


        int like;


        private void report_popup(final int position, final String id) {

            final CharSequence[] options = {"Delete", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
            builder.setTitle("Delete the Post");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Delete")) {
                        position_for_delete = position;


                        AsyncRequest getPosts = new AsyncRequest(MyActivityFragment.this, mainActivity, "POST", getParams(id), view);
                        getPosts.execute(GloabalURI.baseURI + GloabalURI.DELETE_ARTICLE);

                        dialog.dismiss();

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }


        public void makeTextViewResizable(final ActivityViewHolder tv, final int maxLine, final String expandText, final boolean viewMore) {

            if (tv.tv_event_description.getTag() == null) {
                tv.tv_event_description.setTag(tv.tv_event_description.getText());
            }
            ViewTreeObserver vto = tv.tv_event_description.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @SuppressWarnings("deprecation")
                @Override
                public void onGlobalLayout() {

                    ViewTreeObserver obs = tv.tv_event_description.getViewTreeObserver();
                    obs.removeGlobalOnLayoutListener(this);

                    Layout l = tv.tv_event_description.getLayout();

                    if (l != null) {

                        if (maxLine == 0) {
                            int lineEndIndex = tv.tv_event_description.getLayout().getLineEnd(0);
                            String text = tv.tv_event_description.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                            tv.tv_event_description.setText(text);
                            tv.tv_event_description.setMovementMethod(LinkMovementMethod.getInstance());
                            tv.tv_event_description.setText(
                                    addClickablePartTextViewResizable(Html.fromHtml(tv.tv_event_description.getText().toString()), tv.tv_event_description, maxLine, expandText,
                                            viewMore), TextView.BufferType.SPANNABLE);
                        } else if (maxLine > 0 && tv.tv_event_description.getLineCount() >= maxLine) {
                            int lineEndIndex = tv.tv_event_description.getLayout().getLineEnd(maxLine - 1);
                            String text = tv.tv_event_description.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                            tv.tv_event_description.setText(text);
                            tv.tv_event_description.setMovementMethod(LinkMovementMethod.getInstance());
                            tv.tv_event_description.setText(
                                    addClickablePartTextViewResizable(Html.fromHtml(tv.tv_event_description.getText().toString()), tv.tv_event_description, maxLine, expandText,
                                            viewMore), TextView.BufferType.SPANNABLE);
                        } else {
                            int lineEndIndex = tv.tv_event_description.getLayout().getLineEnd(tv.tv_event_description.getLayout().getLineCount() - 1);
                            String text = tv.tv_event_description.getText().subSequence(0, lineEndIndex) + " " + expandText;
                            tv.tv_event_description.setText(text);
                            tv.tv_event_description.setMovementMethod(LinkMovementMethod.getInstance());
                            tv.tv_event_description.setText(
                                    addClickablePartTextViewResizable(Html.fromHtml(tv.tv_event_description.getText().toString()), tv.tv_event_description, lineEndIndex, expandText,
                                            viewMore), TextView.BufferType.SPANNABLE);
                        }
                    }
                }
            });

        }


        private SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                         final int maxLine, final String spanableText, final boolean viewMore) {
            String str = strSpanned.toString();
            SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

            if (str.contains(spanableText)) {
                ssb.setSpan(new ClickableSpan() {

                    @Override
                    public void onClick(View widget) {

//
//                        Intent i=new Intent(c,PostDetails.class);
////                        i.putExtra("pos",position);
//                        ActivityOptions options =
//                                ActivityOptions.makeCustomAnimation(c,R.anim.slide_to_right, R.anim.slide_from_left);
//                        c.startActivity(i,options.toBundle());
////


                    }
                }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

            }
            return ssb;

        }


    }

}