package com.socialide.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;


import com.github.lzyzsd.circleprogress.DonutProgress;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Adapters.EventPhotosAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DeclinePopup;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetails extends Fragment implements AsyncRequest.OnAsyncRequestComplete{
    MyTextView txt_declined_static,txt_declined,txt_message,tv_event_name,tv_organisation_name,tv_event_guests,txt_date,txt_time,txt_email,txt_contact,edt_message;
    CircleImageView iv_me_profile_pic;

    BeanForFetchEvents b;
    MyButton btn_remove;
    static RelativeLayout mRelativeLayout;
    static PopupWindow mPopupWindow;
    public EventDetails() {
        // Required empty public constructor
    }

    AVLoadingIndicatorView view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_event_details_org, container, false);

        txt_declined= (MyTextView) rootView.findViewById(R.id.txt_declined);
        txt_declined_static= (MyTextView) rootView.findViewById(R.id.txt_declined_static);
        txt_message= (MyTextView) rootView.findViewById(R.id.edt_message);
        btn_remove= (MyButton) rootView.findViewById(R.id.btn_remove);
        iv_me_profile_pic= (CircleImageView) rootView.findViewById(R.id.iv_me_profile_pic);




        tv_event_name= (MyTextView) rootView.findViewById(R.id.tv_name);
        tv_organisation_name= (MyTextView) rootView.findViewById(R.id.tv_organisation_name);
        tv_event_guests= (MyTextView) rootView.findViewById(R.id.tv_event_guests);
        txt_date= (MyTextView) rootView.findViewById(R.id.txt_date);
        txt_time= (MyTextView) rootView.findViewById(R.id.txt_time);
        txt_email= (MyTextView) rootView.findViewById(R.id.txt_email);
        txt_contact= (MyTextView) rootView.findViewById(R.id.txt_contact);
        edt_message= (MyTextView) rootView.findViewById(R.id.edt_message);
        view= (AVLoadingIndicatorView) getActivity().findViewById(R.id.avi);

        mRelativeLayout= (RelativeLayout) rootView.findViewById(R.id.rl);

        if (DetailsActivity.type.equals("p")||DetailsActivity.type.equals("s"))
        {


            if(DetailsActivity.type.equals("p"))
            {
                b= EventsPending.activity_list1.get(EventFragment.position);

            }

            else {
                b = EventsScheduled.activity_list1.get(EventFragment.position);
            }
            tv_event_name.setText(""+b.getEvent_title());
            tv_organisation_name.setText(""+b.getEvent_by());
            tv_event_guests.setText(""+b.getEvent_location());

            String datetime= TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

            String date =datetime.split("T")[0];
            String time =datetime.split("T")[1];
           txt_time.setText("" + time);
            txt_date.setText("" +date);

            SpannableString content = new SpannableString(b.getContact_number());
            content.setSpan(new UnderlineSpan(), 0, b.getContact_number().length(), 0);
            txt_contact.setText(content);

           // txt_contact.setText(""+b.getContact_number());
            txt_email.setText(""+b.getEmail());
            edt_message.setText(""+b.getMessage());



            txt_declined.setVisibility(View.GONE);
            txt_declined_static.setVisibility(View.GONE);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            params.setMargins(0, 0, 0,44);
//
//            txt_message.setLayoutParams(params);
          // btn_remove.setText("Cancel this Event");
            btn_remove.setText(getResources().getString(R.string.cancelThisEvent));
        }
        else{

            b = EventsDeclined.activity_list1.get(EventFragment.position);

            txt_declined.setVisibility(View.VISIBLE);
            txt_declined_static.setVisibility(View.VISIBLE);


            tv_event_name.setText("" + b.getEvent_title());
            tv_organisation_name.setText(""+b.getEvent_by());
            tv_event_guests.setText(""+b.getEvent_location());

            txt_contact.setText(""+b.getContact_number());
            txt_email.setText(""+b.getEmail());
            edt_message.setText("" + b.getMessage());
            txt_declined.setText("" + b.getDecline_by());


//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            params.setMargins(0, 0, 0, 0);
//
//            txt_message.setLayoutParams(params);
            btn_remove.setText(getResources().getString(R.string.remove));
        }


        String datetime= TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date =datetime.split("T")[0];
        String time =datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" +date);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profilepic)
                .showImageOnFail(R.drawable.ic_cross)
                .resetViewBeforeLoading(true)
//.cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        DonutProgress d = (DonutProgress) rootView.findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, getActivity());

        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn_remove.getText().toString().contains("this"))
                {
                    DeclinePopup.popup(getActivity(), EventDetails.this, mRelativeLayout,b.getEvent_id(),view);

                }
                else {
                    ArrayList<NameValuePair> params= new ArrayList<>();
                    params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                    params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                    params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                    params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));


                    AsyncRequest getPosts = new AsyncRequest(EventDetails.this,getActivity(), "POST", params,view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.REMOVE_AN_EVENT);


                }
            }
        });

        initRecyclerView();
        return rootView;
    }
    View rootView;
    RecyclerView mRecyclerView;
    EventPhotosAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private void initRecyclerView(){
        
    }
    public static void popup(Context c)
    {


        View popupView = LayoutInflater.from(c).inflate(R.layout.decline_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);




        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        MyButton submit = (MyButton) popupView.findViewById(R.id.btn_submit);
        ImageView cross = (ImageView) popupView.findViewById(R.id.img_cross);

        // Set a click listener for the popup window close button
        submit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();


            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();


            }
        });



        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if(postObject.getString("success").equals("true"))
            {
                CustomSnackBar.toast(getActivity(), postObject.getString("message"));
                getActivity().onBackPressed();
            }
            else
            {
                CustomSnackBar.toast(getActivity(),postObject.getString("message"));
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
