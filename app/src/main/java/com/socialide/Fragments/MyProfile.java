package com.socialide.Fragments;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.SettingActivity;
import com.socialide.Adapters.AvailabilityAdapter;
import com.socialide.Adapters.AvailabilityAdapterForMyProfile;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DialogClass;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.NonScrollExpandableListView;
import com.socialide.Helper.RoundRectCornerImageView;
import com.socialide.Helper.TimeConveter;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import static com.socialide.Activities.MainActivity.badge_layout1;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfile extends Fragment implements AsyncRequest.OnAsyncRequestComplete {
    String add_space = ":  ";
    RoundedImageView img_profile;
    CheckBox chk_verify;
    DonutProgress d;
    List<String> wednesday = new ArrayList<String>();
    List<String> friday = new ArrayList<String>();
    List<String> satureday = new ArrayList<String>();
    List<String> sunday = new ArrayList<String>();
    List<String> monday = new ArrayList<String>();
    List<String> thursday = new ArrayList<String>();
    List<String> tuesday = new ArrayList<String>();
    AvailabilityAdapterForMyProfile listAdapter;
    static List<String> listDataHeader = new ArrayList<>();
    NonScrollExpandableListView expandable_list;
    static HashMap<String, List<String>> listDataChild = new HashMap<>();
    MyTextView txt_name, txt_address, txt_about, txt_social_score, txt_email, txt_mobile;
    MyTextView txt_status, txt_count, txt_category, sub_category, txt_availability, tvNoAvailability;
    MyTextView txt_stablishment, txt_country, txt_person_name, txt_person_email, txt_person_mobile, txt_designation;
    public static int checkAsynck = 0;
    MainActivity mainActivity;
    Activity activity;

    public MyProfile() {
        // Required empty public constructor
    }

    ArrayList<NameValuePair> params;
    ImageButton editMyProfile;
    View rootView, viewLine, viewLineProfessional;
    View commonView;
    MyButton btnEmail, btnMobile;
    int apiCheck = 0;
    AVLoadingIndicatorView view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (Mypreferences.User_Type.equals("Organization")) {

            // Inflate the layout for this fragment
            rootView = (View) inflater.inflate(R.layout.fragment_organisation_profile, container, false);
            mainActivity = (MainActivity) getActivity();
            txt_stablishment = (MyTextView) rootView.findViewById(R.id.txt_stablishment);
            txt_country = (MyTextView) rootView.findViewById(R.id.txt_country);
            txt_person_name = (MyTextView) rootView.findViewById(R.id.txt_person_name);
            txt_person_email = (MyTextView) rootView.findViewById(R.id.txt_person_email);
            txt_person_mobile = (MyTextView) rootView.findViewById(R.id.txt_person_mobile);
            txt_designation = (MyTextView) rootView.findViewById(R.id.txt_designation);
            MyButton btnFav = (MyButton) rootView.findViewById(R.id.btnFav);
            txt_name = (MyTextView) rootView.findViewById(R.id.txt_name);
            viewLine = (View) rootView.findViewById(R.id.viewLine);
            commonView = viewLine;
            txt_stablishment.setText("" + add_space + Mypreferences.STABLISHMENT_YEAR);
            txt_country.setText("" + add_space + Mypreferences.COUNTRY);
            txt_person_name.setText("" + add_space + Mypreferences.FIRST_NAME + " " + Mypreferences.LAST_NAME);
            // txt_person_email.setText("" + add_space + Mypreferences.Email);
            txt_person_mobile.setText("" + add_space + Mypreferences.MOBILE_NUMBER);
            txt_name.setText("" + Mypreferences.ORGANISATION_NAME);
            txt_person_email.setText("" + add_space + Mypreferences.getString(Mypreferences.Email, mainActivity));
            txt_designation.setText("" + add_space + Mypreferences.DESIGNATION);

            btnFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    params = getParamsDemo(1);
                    AsyncRequest getPosts = new AsyncRequest(MyProfile.this, mainActivity, "POST", params, view, 1);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);
                }
            });

        } else {

            rootView = (View) inflater.inflate(R.layout.fragment_proffessional_profile, container, false);
            mainActivity = (MainActivity) getActivity();
            txt_status = (MyTextView) rootView.findViewById(R.id.txt_status);
            txt_count = (MyTextView) rootView.findViewById(R.id.txt_count);
            txt_category = (MyTextView) rootView.findViewById(R.id.txt_category);
            txt_availability = (MyTextView) rootView.findViewById(R.id.txt_availability);
            tvNoAvailability = (MyTextView) rootView.findViewById(R.id.tvNoAvailability);
            sub_category = (MyTextView) rootView.findViewById(R.id.sub_category);
            txt_name = (MyTextView) rootView.findViewById(R.id.txt_name);
            expandable_list = (NonScrollExpandableListView) rootView.findViewById(R.id.expandable_list);
            viewLineProfessional = (View) rootView.findViewById(R.id.viewLineProfessional);
            MyButton btnFav = (MyButton) rootView.findViewById(R.id.btnFav);

            commonView = viewLineProfessional;

            btnFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });


            txt_count.setText("" + add_space + Mypreferences.Maximum_Event_Request);

            if (!Mypreferences.CATEGORY.equalsIgnoreCase("")) {
                txt_category.setText("" + add_space + Mypreferences.CATEGORY);
            } else {
                txt_category.setText("" + add_space + getResources().getString(R.string.notAvailable));
            }
            if (!Mypreferences.SUB_CATEGORY.equalsIgnoreCase("")) {
                sub_category.setText("" + add_space + Mypreferences.SUB_CATEGORY);
            } else {
                sub_category.setText("" + add_space + getResources().getString(R.string.notAvailable));
            }


            txt_name.setText("" + Mypreferences.FIRST_NAME + " " + Mypreferences.LAST_NAME);
            expandable_list.setGroupIndicator(null);
            expandable_list.setChildIndicator(null);
            String[] reverse = Mypreferences.Availability.split("@@");
            listDataHeader = new ArrayList<>();
            listDataChild = new HashMap<>();
            for (int i = 0; i < reverse.length; i++) {

                String[] reverse2 = reverse[i].split("_");

                listDataHeader.add(reverse2[0]);
                ArrayList<String> a = new ArrayList();
                if (reverse2.length > 1) {
                    Log.e("reserve" + i, "" + reverse2[0]);

                    String reverse3[] = reverse2[1].split(",");

                    for (int j = 0; j < reverse3.length; j++) {

                        Log.v("reserve1" + i, "" + reverse3[j]);
                        a.add("" + reverse3[j]);

                    }
                }

                if (a.size() == 0) {
                    a.add("");
                }
                listDataChild.put(reverse2[0], a);
            }

            listAdapter = new AvailabilityAdapterForMyProfile(mainActivity, listDataHeader, listDataChild);

            // setting list adapter
            expandable_list.setAdapter(listAdapter);

            for (int l = 0; l < listDataHeader.size(); l++) {

                expandable_list.expandGroup(l);
            }

            expandable_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                                            int groupPosition, long id) {

                    return true;
                }
            });
        }

        txt_address = (MyTextView) rootView.findViewById(R.id.txt_address);
        txt_social_score = (MyTextView) rootView.findViewById(R.id.txt_social_score);
        txt_about = (MyTextView) rootView.findViewById(R.id.txt_about);
        txt_email = (MyTextView) rootView.findViewById(R.id.txt_email);
        txt_mobile = (MyTextView) rootView.findViewById(R.id.txt_mobile);
        chk_verify = (CheckBox) rootView.findViewById(R.id.chk_verify);
        img_profile = (RoundedImageView) rootView.findViewById(R.id.img_profile);
        view = (AVLoadingIndicatorView) mainActivity.findViewById(R.id.avi);
        btnEmail = (MyButton) rootView.findViewById(R.id.btnEmail);
        btnMobile = (MyButton) rootView.findViewById(R.id.btnMobile);


        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                   /* DialogClass dialogClass = new DialogClass();
                    dialogClass.emailVarification(getContext(), getActivity());*/
                apiCheck = 1;
                params = getParams(1);
                AsyncRequest getPosts = new AsyncRequest(MyProfile.this, mainActivity, "POST", params, view, 1);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.verifyEmail);
            }
        });

        btnMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiCheck = 2;
                params = getParams(2);
                AsyncRequest getPosts = new AsyncRequest(MyProfile.this, mainActivity, "POST", params, view, 1);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.getMobileVerificationCode);
            }
        });

        if (!Mypreferences.CITY.equals("") && !Mypreferences.STATE.equals("") && !Mypreferences.COUNTRY.equals("")) {
            txt_address.setText("" + Mypreferences.CITY + ", " + Mypreferences.STATE + ", " + Mypreferences.COUNTRY);
            txt_address.setVisibility(View.VISIBLE);
        } else {
            txt_address.setVisibility(View.GONE);

        }

        if (!Mypreferences.ABOUT_YOU.equals("")) {
            txt_about.setText("" + Mypreferences.ABOUT_YOU);
        } else {
            txt_about.setVisibility(View.GONE);
            commonView.setVisibility(View.GONE);
        }

        txt_social_score.setText("" + Mypreferences.Social_Score);
        txt_about.setText("" + Mypreferences.ABOUT_YOU);
        txt_email.setText("" + add_space + Mypreferences.getString(Mypreferences.Email, mainActivity));

        if (Mypreferences.User_Type.equalsIgnoreCase("Organization")) {
            txt_mobile.setText("" + add_space + Mypreferences.LANDLINE_NUMBER);

        } else {
            txt_mobile.setText("" + add_space + Mypreferences.MOBILE_NUMBER);
        }
        d = (DonutProgress) rootView.findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, img_profile, d, mainActivity);

        if (checkAsynck == 0) {

            params = getParams(0);
            AsyncRequest getPosts = new AsyncRequest(MyProfile.this, mainActivity, "POST", params, view, 1);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.GETPROFILEDETAIL);

        }
        return rootView;
    }

    private ArrayList<NameValuePair> getParams(int condition) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
        params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
        if (condition == 0) {
            params.add(new BasicNameValuePair("time_offset", "" + TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60000));
        }


        if (condition == 1) {
            params.add(new BasicNameValuePair("email", "" + Mypreferences.getString(Mypreferences.Email, mainActivity)));
        }
        if (condition == 2) {
            params.add(new BasicNameValuePair("number", "" + Mypreferences.MOBILE_NUMBER));
        }
        return params;
    }

    private ArrayList<NameValuePair> getParamsDemo(int condition) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

      /*  String dateToUTC = TimeConveter.convertToUTC("12 Oct 2017", "1:36 pm");
        Log.v("akram", "date to UTC = " + dateToUTC);

        String date = dateToUTC.split("T")[0];
        String time = dateToUTC.split("T")[1];*/

        params.add(new BasicNameValuePair("location", "Indore,Ratlam"));
        params.add(new BasicNameValuePair("pageindex", "1"));
        params.add(new BasicNameValuePair("pagecount", "30"));
        params.add(new BasicNameValuePair("time_offset", "" + TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60000));


        return params;
    }

    @Override
    public void asyncResponse(String response) {
        FunctionClass.logCatLong(response);
        switch (apiCheck) {

            case 1:
                apiCheck = 0;
                String msg = "";
                try {
                    JSONObject postObject = new JSONObject(response);

                    String success = postObject.getString("success");
                    if (success.equalsIgnoreCase("true")) {
                        msg = postObject.getString("message");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                DialogClass dialogClass = new DialogClass();
                dialogClass.emailVarification(getContext(), mainActivity, msg);
                Log.v("akram", "resposen profile = " + response);
                break;


            case 2:
                apiCheck = 0;
                String msgMobile = "";
                try {
                    JSONObject postObject = new JSONObject(response);

                    String success = postObject.getString("success");
                    if (success.equalsIgnoreCase("true")) {
                        msgMobile = postObject.getString("message");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                DialogClass dialogClass1 = new DialogClass();
                dialogClass1.mobileVarification(getContext(), mainActivity);
                Log.v("akram", "resposen profile = " + response);
                break;


            case 0:

                try {
                    JSONObject postObject = new JSONObject(response);

                    Log.v("akram", "resposen profile = " + response);
                    checkAsynck = 1;

                    // view.hide();
                    if (postObject.getString("success").equals("true")) {
                        Log.e("success", "" + response);

                        JSONObject result = postObject.getJSONObject("result");
                        MainActivity.img_setting.setVisibility(View.VISIBLE);
                        Mypreferences.User_id = result.getString("userid");
                        if (postObject.getString("type").equals("Professional")) {

                            Mypreferences.User_Type = "Professional";

                            Mypreferences.FIRST_NAME = result.getString("first_name");
                            Mypreferences.LAST_NAME = result.getString("last_name");
                            Mypreferences.DOB = result.getString("dob");
                            Mypreferences.EXPERIENCE = result.getString("experience");
                            Mypreferences.MOBILE_NUMBER = result.getString("mobile_number");
                            Mypreferences.STATE = result.getString("state");
                            Mypreferences.CITY = result.getString("city");
                            Mypreferences.GENDER = result.getString("gender");
                            Mypreferences.COUNTRY = result.getString("country");
                            Mypreferences.CATEGORY = result.getString("category");
                            Mypreferences.CURRENT_COMPANY = result.getString("current_company");
                            Mypreferences.SUB_CATEGORY = result.getString("subcategory");
                            Mypreferences.ABOUT_YOU = result.getString("about_me");

                            Mypreferences.Maximum_Event_Request = result.getString("maximum_event_request");
                            Mypreferences.Join_Flow_Completed = result.getString("join_flow_completed");

                            Mypreferences.Social_Score = result.getString("social_score");
                            Mypreferences.Picture = result.getString("profile_pic");
                            Mypreferences.Availability = result.getString("availability");

                            Mypreferences.isAvailable = result.getString("is_available");

                            if (Mypreferences.isAvailable.equalsIgnoreCase("0")) {
                                txt_status.setText(getResources().getString(R.string.dialog_btnNO));
                            } else {
                                txt_status.setText(getResources().getString(R.string.dialog_btnYes));
                            }

                            Mypreferences.Profession = result.getString("profession");


                            String[] reverse = Mypreferences.Availability.split("@@");
                            listDataHeader = new ArrayList<>();
                            listDataChild = new HashMap<>();
                            for (int i = 0; i < reverse.length; i++) {
                                ArrayList<String> a = new ArrayList();
                                String[] reverse2 = reverse[i].split("_");
                                if (reverse2.length > 1) {
                                    listDataHeader.add(reverse2[0]);


                                    if (reverse2.length > 1) {
                                        Log.e("reserve" + i, "" + reverse2[0]);


                                        String reverse3[] = reverse2[1].split(",");
                                        for (int j = 0; j < reverse3.length; j++) {

                                            Log.e("reserve1" + i, "" + reverse3[j]);
                                            a.add("" + reverse3[j]);

                                        }
                                    }

                                    if (a.size() == 0) {
                                        a.add("Not Available");
                                    }
                                }

                                listDataChild.put(reverse2[0], a);

                            }

                            if (listDataHeader.size() != 0) {
                                tvNoAvailability.setVisibility(View.GONE);
                                expandable_list.setVisibility(View.VISIBLE);
                                listAdapter = new AvailabilityAdapterForMyProfile(mainActivity, listDataHeader, listDataChild);

                                // setting list adapter
                                expandable_list.setAdapter(listAdapter);
                            } else {
                                tvNoAvailability.setVisibility(View.VISIBLE);
                                expandable_list.setVisibility(View.GONE);
                            }

                            for (int l = 0; l < listDataHeader.size(); l++) {

                                expandable_list.expandGroup(l);
                            }

                        }

                        if (postObject.getString("type").equals("Organization")) {
                            Mypreferences.User_Type = "Organization";

                            Mypreferences.ORGANISATION_NAME = result.getString("name");
                            Mypreferences.LANDLINE_NUMBER = result.getString("landline_number");
                            Mypreferences.FIRST_NAME = result.getString("contact_firstname");
                            Mypreferences.LAST_NAME = result.getString("contact_lastname");
                            Mypreferences.MOBILE_NUMBER = result.getString("contact_number");
                            Mypreferences.STATE = result.getString("state");
                            Mypreferences.CITY = result.getString("city");
                            Mypreferences.REGISTRATION_NUMBER = result.getString("u_dise_number");
                            Mypreferences.COUNTRY = result.getString("country");
                            Mypreferences.Address = result.getString("address");
                            Mypreferences.DESIGNATION = result.getString("contact_position");
                            Mypreferences.STABLISHMENT_YEAR = result.getString("establishment_year");
                            Mypreferences.ABOUT_YOU = result.getString("about");
                            Mypreferences.Contact_position = result.getString("contact_position");


                            Mypreferences.Join_Flow_Completed = result.getString("join_flow_completed");


                            Mypreferences.Picture = result.getString("profile_pic");

                        }


                        if (Mypreferences.User_Type.equals("Organization")) { // Inflate the layout for this fragment
                            Log.v("akram", "if ");
                            txt_stablishment.setText("" + add_space + Mypreferences.STABLISHMENT_YEAR);
                            txt_country.setText("" + add_space + Mypreferences.COUNTRY);
                            txt_person_name.setText("" + add_space + Mypreferences.FIRST_NAME + " " + Mypreferences.LAST_NAME);
                            txt_person_email.setText("" + add_space + Mypreferences.getString(Mypreferences.Email, mainActivity));
                            txt_person_mobile.setText("" + add_space + Mypreferences.MOBILE_NUMBER);
                            txt_name.setText("" + Mypreferences.ORGANISATION_NAME);

                        } else {


                            txt_count.setText("" + Mypreferences.Maximum_Event_Request);

                            if (!Mypreferences.CATEGORY.equalsIgnoreCase("")) {
                                txt_category.setText("" + add_space + Mypreferences.CATEGORY);
                            } else {
                                txt_category.setText("" + add_space + getResources().getString(R.string.notAvailable));
                            }
                            if (!Mypreferences.SUB_CATEGORY.equalsIgnoreCase("")) {
                                sub_category.setText("" + add_space + Mypreferences.SUB_CATEGORY);
                            } else {
                                sub_category.setText("" + add_space + getResources().getString(R.string.notAvailable));
                            }

                            txt_name.setText("" + Mypreferences.FIRST_NAME + " " + Mypreferences.LAST_NAME);

                        }

                        if (!Mypreferences.CITY.equals("") && !Mypreferences.STATE.equals("") && !Mypreferences.COUNTRY.equals("")) {
                            txt_address.setText("" + Mypreferences.CITY + ", " + Mypreferences.STATE + ", " + Mypreferences.COUNTRY);
                            txt_address.setVisibility(View.VISIBLE);
                        } else {
                            txt_address.setVisibility(View.GONE);

                        }

                        if (!Mypreferences.ABOUT_YOU.equals("")) {
                            txt_about.setText("" + Mypreferences.ABOUT_YOU);
                        } else {
                            txt_about.setVisibility(View.GONE);
                        }

                        txt_social_score.setText("" + Mypreferences.Social_Score);
                        txt_about.setText("" + Mypreferences.ABOUT_YOU);

                        if (mainActivity == null) {
                            Log.v("akram", "yes activity null");
                        }

                        txt_email.setText("" + add_space + Mypreferences.getString(Mypreferences.Email, mainActivity));


                        if (Mypreferences.User_Type.equalsIgnoreCase("Organization")) {
                            txt_mobile.setText("" + add_space + Mypreferences.LANDLINE_NUMBER);

                        } else {
                            txt_mobile.setText("" + add_space + Mypreferences.MOBILE_NUMBER);
                        }
                        d = (DonutProgress) rootView.findViewById(R.id.loading);
                        com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, img_profile, d, mainActivity);


                    } else {
                        CustomSnackBar.toast(mainActivity, postObject.getString("message"));
                    }
                    mainActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;


        }


    }
//
}





