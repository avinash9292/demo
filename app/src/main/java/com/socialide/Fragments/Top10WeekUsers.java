package com.socialide.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.socialide.Activities.MainActivity;
import com.socialide.Adapters.InformationAdapter;
import com.socialide.Adapters.TopUsersAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.Model.BeanForTopUser;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Top10WeekUsers extends Fragment implements AsyncRequest.OnAsyncRequestComplete {
    AVLoadingIndicatorView view;
    MyTextView txt_no_item;

    ArrayList<NameValuePair> params;
    ArrayList<BeanForTopUser> user_list = new ArrayList<>();

    public Top10WeekUsers() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_recycler_view_non_refresh, container, false);
        initRecyclerView();
        MainActivity.img_setting.setVisibility(View.GONE);
        if (user_list.size() == 0) {
            params = getParams();
            AsyncRequest getPosts = new AsyncRequest(this, getActivity(), "POST", params, view);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.TOP10WEEKLYUSER);

        }
        return rootView;
    }

    private ArrayList<NameValuePair> getParams() {

        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));
        return params;
    }


    RecyclerView mRecyclerView;
    TopUsersAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    View rootView;

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        view = (AVLoadingIndicatorView) getActivity().findViewById(R.id.avi);
        txt_no_item = (MyTextView) rootView.findViewById(R.id.txt_no_item);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        adapter = new TopUsersAdapter(getActivity(), user_list, getActivity());
        mRecyclerView.setAdapter(adapter);
    }


    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);
            Log.v("akram", "response" + response);
            MainActivity.img_setting.setVisibility(View.GONE);
            if (postObject.getString("success").equals("true")) {

                user_list = new ArrayList<>();

                JSONArray Weeklyusers = postObject.getJSONArray("Weeklyuser");

                for (int i = 0; i < Weeklyusers.length(); i++) {
                    JSONObject user = Weeklyusers.getJSONObject(i);

                    if (user.getString("type").equals("Organization")) {

                        user_list.add(new BeanForTopUser(user.getString("userid"), user.getString("name"), user.getString("contact_firstname"), user.getString("contact_lastname"), user.getString("contact_number"), user.getString("contact_position"), user.getString("state"), user.getString("city"), user.getString("profile_pic"), user.getString("u_dise_number"), user.getString("country"), user.getString("address"), user.getString("all_event_count"), user.getString("this_week_event_count"), user.getString("type")));
                    } else {
                        user_list.add(new BeanForTopUser(user.getString("userid"), user.getString("first_name"), user.getString("last_name"), user.getString("profession"), user.getString("experience"), user.getString("mobile_number"), user.getString("state"), user.getString("city"), user.getString("profile_pic"), user.getString("country"), user.getString("current_company"), user.getString("category"), user.getString("subcategory"), user.getString("all_event_count"), user.getString("this_week_event_count"), user.getString("type")));
                    }
                }

                //  adapter.notifyDataSetChanged();

                adapter = new TopUsersAdapter(getActivity(), user_list, getActivity());
                mRecyclerView.setAdapter(adapter);

                if (user_list.size() == 0) {
                    txt_no_item.setText(getResources().getString(R.string.noUser));
                }

            } else {
                txt_no_item.setText(getResources().getString(R.string.noUser));

                txt_no_item.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        super.onResume();


        if (user_list.size() == 0) {
            txt_no_item.setVisibility(View.VISIBLE);
            txt_no_item.setText(getResources().getString(R.string.loading));
        } else {


            txt_no_item.setVisibility(View.GONE);

        }
    }
}
