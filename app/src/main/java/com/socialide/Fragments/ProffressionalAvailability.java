package com.socialide.Fragments;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import android.widget.TimePicker;

import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.SettingActivity;
import com.socialide.Adapters.AvailabilityAdapter;
import com.socialide.Adapters.ReapeatAvailabilityPopup;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.Alert;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.NonScrollExpandableListView;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ProffressionalAvailability extends Fragment implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_continue, btn_do_later;
    private static RelativeLayout mRelativeLayout;

    static LoginActivity activity;
    static boolean first = true;
    ImageView img_plus, img_minus;
    MyTextView txt_count;
    static String Availabilty = "";
    static PopupWindow mPopupWindow;
    ArrayList<NameValuePair> params;

    List<String> wednesday = new ArrayList<String>();

    List<String> friday = new ArrayList<String>();
    List<String> satureday = new ArrayList<String>();
    List<String> sunday = new ArrayList<String>();
    List<String> monday = new ArrayList<String>();
    List<String> thursday = new ArrayList<String>();
    List<String> tuesday = new ArrayList<String>();
    static List<String> listDataHeader;
    AVLoadingIndicatorView view;
    static HashMap<String, List<String>> listDataChild;

    static Calendar cal1, cl2;
    public int event_count = 1;
    static String s1 = "07:00", s2 = "09:00";
    public static int group = 0, child = 0;

    List<String> comingSoon = new ArrayList<String>();
    static AvailabilityAdapter listAdapter;
    static NonScrollExpandableListView expListView;
    SwitchCompat SCNotification1;
    int isAvailable = 0;
    RelativeLayout layoutActive;

    public ProffressionalAvailability() {
        // Required empty public constructor
        LoginActivity.back_handle = 1;
    }

    static DecimalFormat formatter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_availability, container, false);
        btn_continue = (MyButton) root.findViewById(R.id.btn_continue);
        btn_do_later = (MyButton) root.findViewById(R.id.btn_do_later);
        btn_do_later.setVisibility(View.GONE);
        mRelativeLayout = (RelativeLayout) root.findViewById(R.id.lin);
        expListView = (NonScrollExpandableListView) root.findViewById(R.id.expandable_list);
        img_plus = (ImageView) root.findViewById(R.id.img_plus);
        img_minus = (ImageView) root.findViewById(R.id.img_minus);
        txt_count = (MyTextView) root.findViewById(R.id.txt_count);
        view = (AVLoadingIndicatorView) root.findViewById(R.id.avi);
        SCNotification1 = (SwitchCompat) root.findViewById(R.id.SCNotification1);
        layoutActive = (RelativeLayout) root.findViewById(R.id.layoutActive);
        formatter = new DecimalFormat("00");
        img_plus.setOnClickListener(this);
        btn_do_later.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
        img_minus.setOnClickListener(this);
        img_plus.setFocusableInTouchMode(true);
        img_plus.requestFocus();
        prepareListData();
        expListView.setGroupIndicator(null);
        expListView.setChildIndicator(null);

        if (Mypreferences.isAvailable.equalsIgnoreCase("0")) {
            SCNotification1.setChecked(false);
            isAvailable = 0;
        } else {
            isAvailable = 1;
            SCNotification1.setChecked(true);
        }

        if (SettingActivity.profilePageCheck == 1) {
            btn_continue.setText(getResources().getString(R.string.btn_save));
        } else {
            layoutActive.setVisibility(View.GONE);
        }

        if (SettingActivity.profilePageCheck == 1) {

            String[] reverse = Mypreferences.Availability.split("@@");
            listDataHeader = new ArrayList<>();
            listDataChild = new HashMap<>();
            for (int i = 0; i < reverse.length; i++) {
                String[] reverse2 = reverse[i].split("_");
                listDataHeader.add(reverse2[0]);
                ArrayList<String> a = new ArrayList();

                if (reverse2.length > 1) {


                    String reverse3[] = reverse2[1].split(",");
                    for (int j = 0; j < reverse3.length; j++) {

                        a.add("" + reverse3[j]);

                    }
                }


                if (a.size() == 0) {

                }
                listDataChild.put(reverse2[0], a);
            }
            // listAdapter.notifyDataSetChanged();
            //  expListView.expandGroup(0);

        }

        SCNotification1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isAvailable = 1;
                } else {
                    isAvailable = 0;
                }

            }
        });


        listAdapter = new AvailabilityAdapter(getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        if (SettingActivity.profilePageCheck == 1) {

            String[] reverse = Mypreferences.Availability.split("@@");

            for (int i = 0; i < reverse.length; i++) {

                String[] reverse2 = reverse[i].split("_");

                if (reverse2.length > 1) {
                    expListView.expandGroup(i);
                }
            }
        }


        activity = (LoginActivity) getActivity();


        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return true;
            }
        });

        if (!Mypreferences.Maximum_Event_Request.equals("")) {
            txt_count.setText(Mypreferences.Maximum_Event_Request);
            event_count = Integer.parseInt(Mypreferences.Maximum_Event_Request);
        }
        return root;
    }

    public void gotoProffressionalOtherDetails() {
        if (activity != null) {
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
            startActivity(new Intent(activity, MainActivity.class), options.toBundle());
            first = true;
            activity.finish();

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:

                Availabilty = "";

                if (event_count == 0) {
                    CustomSnackBar.toast(getActivity(), "Event Request can not be zero");
                } else {

//                for(Map.Entry m:listDataChild.entrySet()){
//                    System.out.println(m.getKey()+" "+m.getValue());

                    for (int i = 0; i < listDataHeader.size(); i++) {

                        String s = listDataHeader.get(i);

                        if (i != 0) {
                            Availabilty += "@@" + s;
                        } else {
                            Availabilty += s;
                        }
                        List<String> al = listDataChild.get(s);

                        for (int k = 0; k < al.size(); k++) {
                            if (k == 0) {
                                Availabilty += "_";
                            }
                            if (k < al.size() - 1) {
                                Availabilty += al.get(k) + ",";
                            } else {
                                Availabilty += al.get(k);
                            }
                        }
                    }
                    Log.e("Availability", "a: " + Availabilty);
                    params = getParams();
                    AsyncRequest getPosts = new AsyncRequest(ProffressionalAvailability.this, getActivity(), "POST", params, view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.UPDATE_PROFESSIONAL);

                }


                break;
            case R.id.img_plus:
                if (event_count < 20) {
                    event_count++;
                    txt_count.setText("" + event_count);
                }
                break;
            case R.id.img_minus:
                if (event_count >= 2) {
                    event_count--;
                    txt_count.setText("" + event_count);
                }
                break;

            case R.id.btn_do_later:
                gotoProffressionalOtherDetails();
                break;

        }
    }


    public static void popup(final Context c) {

        String s = listDataHeader.get(group);
        final List<String> al = listDataChild.get(s);


        al.add("7:00  -  9:00");


        listDataChild.put(s, al);
        listAdapter.notifyDataSetChanged();
        //setListViewHeight(expListView, group);
        expListView.expandGroup(group);


        final View popupView = LayoutInflater.from(c).inflate(R.layout.availability_time_selection_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }
        cal1 = Calendar.getInstance();
        cal1.set(Calendar.HOUR_OF_DAY, 7);
        cal1.set(Calendar.MINUTE, 00);
        cal1.set(Calendar.SECOND, 0);

        cl2 = Calendar.getInstance();
        cl2.set(Calendar.HOUR_OF_DAY, 9);
        cl2.set(Calendar.MINUTE, 00);
        final TimePicker time_from = (TimePicker) popupView.findViewById(R.id.time_from);
        final TimePicker time_to = (TimePicker) popupView.findViewById(R.id.time_to);
        time_from.setIs24HourView(false);
        time_to.setIs24HourView(false);
//
        time_from.setCurrentHour(7);
        time_from.setCurrentMinute(00);
        time_to.setCurrentHour(9);
        time_to.setCurrentMinute(00);

        time_from.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                s1 = formatter.format(hourOfDay) + ":" + formatter.format(minute) + "";


                cal1 = Calendar.getInstance();
                cal1.set(Calendar.HOUR_OF_DAY, hourOfDay);
                cal1.set(Calendar.MINUTE, 00);
                cal1.set(Calendar.SECOND, 0);

                cal1.set(Calendar.MILLISECOND, 0);


            }


        });


        time_to.setIs24HourView(false);
        time_to.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                s2 = formatter.format(hourOfDay) + ":" + formatter.format(minute) + "";


                cl2 = Calendar.getInstance();
                cl2.set(Calendar.HOUR_OF_DAY, hourOfDay);
                cl2.set(Calendar.MINUTE, 00);


            }


        });
        // Get a reference for the custom view close MyButton
        MyButton done = (MyButton) popupView.findViewById(R.id.btn_done);


        // Set a click listener for the popup window close button
        done.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window

                long diff = cl2.getTimeInMillis() - cal1.getTimeInMillis();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diff);
                int hour = (int) (diffInSec / (60 * 60));
                if (hour >= 2) {

                    boolean checkAvailable = false;
                    boolean x = true;
                    String s = listDataHeader.get(group);
                    List<String> al = listDataChild.get(s);

                    for (int i = 0; i < al.size() - 1; i++) {

                        if (!al.get(i).equalsIgnoreCase("Not Available")) {


                            String time[] = al.get(i).split("-");


                            String hhmm1[] = time[0].split(":");
                            int h1 = Integer.parseInt(hhmm1[0].trim());
                            int m1 = Integer.parseInt(hhmm1[1].trim());


                            String hhmm2[] = time[1].split(":");
                            int h2 = Integer.parseInt(hhmm2[0].trim());
                            int m2 = Integer.parseInt(hhmm2[1].trim());


                            String chh1[] = s1.split(":");

                            String chh2[] = s2.split(":");

                            int ch1 = Integer.parseInt(chh1[0]);
                            int ch2 = Integer.parseInt(chh2[0]);
                            int mh1 = Integer.parseInt(chh1[1]);
                            int mh2 = Integer.parseInt(chh2[1]);

/*--------------------------for checking "from hour" to previous "from hours " of same day */
                            if (h1 <= ch1 && ch1 < h2) {

                                x = false;

                            }
                        /*--------------------------for checking "from hour" to previous "from_hours and to_hour" of same day */

                            else if (ch1 < h1 && ch2 > h1) {
                                x = false;


                            } else if (ch1 < h1 && ch2 == h1 && mh2 >= m1) {
                                x = false;


                            } else if (ch1 == h2 && mh1 <= m2) {


                                x = false;


                            } else {

                                x = true;


                            }
                            if (!x)
                                break;
                        } else {
                            checkAvailable = true;
                        }
                    }

                    if (x) {
                        mPopupWindow.dismiss();
                        s1 = s1 + "  -  " + s2;

                        al.remove(al.size() - 1);
                        al.add(s1);


                        listDataChild.put(s, al);
                        listAdapter.notifyDataSetChanged();
                        //setListViewHeight(expListView, group);
                        s1 = "07:00";
                        s2 = "09:00";

                        if (first)
                            popup1(c);
                    } else {
                        //     CustomSnackBar.toast(activity,"comnnjj"+hour);

                        mPopupWindow.dismiss();

                        al.remove(al.size() - 1);

                        listAdapter.notifyDataSetChanged();
                        //setListViewHeight(expListView, group);
                        s1 = "07:00";
                        s2 = "09:00";
                        Alert.alert(activity, "Time you entered overlapping with existing time for same day, please select any other time");


                    }


                } else {
                    mPopupWindow.dismiss();
                    mPopupWindow.dismiss();

                    al.remove(al.size() - 1);

                    listAdapter.notifyDataSetChanged();
                    //setListViewHeight(expListView, group);
                    s1 = "07:00";
                    s2 = "09:00";
                    Alert.alert(activity, "Time interval should be more than 2 hours.");

                }
            }
        });
        mPopupWindow.setAnimationStyle(R.style.animationName);
        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.BOTTOM, 0, 0);

    }

    public static void popup1(Context c) {
        RecyclerView mRecyclerView;
        first = false;
        RecyclerView.LayoutManager mLayoutManager;
        ReapeatAvailabilityPopup adapters;
        View popupView = LayoutInflater.from(c).inflate(R.layout.dialog_for_reapeat_availability, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );


        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mRecyclerView = (RecyclerView) popupView.findViewById(R.id.list_days);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(c);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        adapters = new ReapeatAvailabilityPopup(c);
        mRecyclerView.setAdapter(adapters);
        // Get a reference for the custom view close button
        MyButton done = (MyButton) popupView.findViewById(R.id.btn_apply);
        ImageView img_cross = (ImageView) popupView.findViewById(R.id.img_cross);

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();

            }
        });
        // Set a click listener for the popup window close button
        done.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                String s = listDataHeader.get(group);
                List<String> al = listDataChild.get(s);
                for (int k = 0; k < ReapeatAvailabilityPopup.selection.size(); k++) {

                    listDataChild.put(listDataHeader.get(ReapeatAvailabilityPopup.selection.get(k)), new ArrayList<>(al));
                    listAdapter.notifyDataSetChanged();
                    expListView.expandGroup(ReapeatAvailabilityPopup.selection.get(k));

                }


                mPopupWindow.dismiss();


            }
        });
        mPopupWindow.setAnimationStyle(R.style.animationName);
        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add(getString(R.string.txt_weekday1));
        listDataHeader.add(getString(R.string.txt_weekday2));
        listDataHeader.add(getString(R.string.txt_weekday3));
        listDataHeader.add(getString(R.string.txt_weekday4));
        listDataHeader.add(getString(R.string.txt_weekday5));
        listDataHeader.add(getString(R.string.txt_weekday6));
        listDataHeader.add(getString(R.string.txt_weekday7));

        // Adding child data

        listDataChild.put(listDataHeader.get(6), sunday);
        listDataChild.put(listDataHeader.get(1), tuesday);
        listDataChild.put(listDataHeader.get(2), wednesday);
        listDataChild.put(listDataHeader.get(3), thursday);
        listDataChild.put(listDataHeader.get(4), friday);
        listDataChild.put(listDataHeader.get(5), satureday);
        listDataChild.put(listDataHeader.get(0), monday);

    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;

        } else {
            return false;
        }

    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("professional_id", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("maximum_event_request", "" + event_count));
        params.add(new BasicNameValuePair("availability", "" + Availabilty));
        params.add(new BasicNameValuePair("join_flow_completed", "" + 1));
        params.add(new BasicNameValuePair("is_available", "" + isAvailable));
        params.add(new BasicNameValuePair("time_offset", "+" + TimeZone.getDefault().getOffset(System.currentTimeMillis())/60000));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));



        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);
            Log.v("akram", "response ava = " + response);

            if (postObject.getString("success").equals("true")) {
                // JSONObject result= postObject.getJSONObject("result");

                Mypreferences.Maximum_Event_Request = "" + event_count;
                Mypreferences.Availability = "" + Availabilty;
                Mypreferences.isAvailable = isAvailable + "";
                if (SettingActivity.profilePageCheck == 1) {
                    SettingActivity.profilePageCheck = 0;
                    getActivity().onBackPressed();

                } else {
                    gotoProffressionalOtherDetails();
                }
            } else {
                String s = "" + postObject.getString("message");

                CustomSnackBar.toast(getActivity(), s);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

/*
Monday_7:00 - 9:00@@Tuesday_7:00 - 9:00@@Wednesday@@Thursday@@Friday@@Saturday@@Sunday*/
