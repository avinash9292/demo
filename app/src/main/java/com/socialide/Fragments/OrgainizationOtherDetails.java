package com.socialide.Fragments;


import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.SettingActivity;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.Validation;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.socialide.Helper.Mypreferences.ABOUT_YOU;
import static com.socialide.Helper.Mypreferences.DESIGNATION;
import static com.socialide.Helper.Mypreferences.FIRST_NAME;
import static com.socialide.Helper.Mypreferences.LAST_NAME;
import static com.socialide.Helper.Mypreferences.MOBILE_NUMBER;

public class OrgainizationOtherDetails extends Fragment implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_continue;
    LoginActivity activity;
    AVLoadingIndicatorView view;
    MyEditTextView edt_firstname, tvToolbarTitle, edt_lastname, edt_contact_number, edt_about_organisation, edt_designation;
    ArrayList<NameValuePair> params;

    public OrgainizationOtherDetails() {
        LoginActivity.back_handle = 1;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.


        View root = inflater.inflate(R.layout.fragment_organization_other_details, container, false);
        btn_continue = (MyButton) root.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
        activity = (LoginActivity) getActivity();

        edt_firstname = (MyEditTextView) root.findViewById(R.id.edt_firstname);
        edt_lastname = (MyEditTextView) root.findViewById(R.id.edt_lastname);
        edt_contact_number = (MyEditTextView) root.findViewById(R.id.edt_contact_number);
        edt_about_organisation = (MyEditTextView) root.findViewById(R.id.edt_about_organisation);
        edt_designation = (MyEditTextView) root.findViewById(R.id.edt_designation);
        view = (AVLoadingIndicatorView) root.findViewById(R.id.avi);

        if (!Mypreferences.FIRST_NAME.equals("") && !Mypreferences.FIRST_NAME.equals("FIRST_NAME")) {
            edt_firstname.setText("" + Mypreferences.FIRST_NAME);
        }
        if (!Mypreferences.LAST_NAME.equals("") && !Mypreferences.LAST_NAME.equals("LAST_NAME")) {
            edt_lastname.setText("" + Mypreferences.LAST_NAME);
        }
        if (!Mypreferences.DESIGNATION.equals("") && !Mypreferences.DESIGNATION.equals("DESIGNATION")) {
            edt_designation.setText("" + Mypreferences.DESIGNATION);
        }
        if (!Mypreferences.MOBILE_NUMBER.equals("") && !Mypreferences.MOBILE_NUMBER.equals("MOBILE_NUMBER")) {
            edt_contact_number.setText("" + Mypreferences.MOBILE_NUMBER);
        }
        if (!Mypreferences.ABOUT_YOU.equals("") && !Mypreferences.ABOUT_YOU.equals("ABOUT_YOU")) {
            edt_about_organisation.setText("" + Mypreferences.ABOUT_YOU);
        }


        return root;
    }

    private boolean isValidate() {

        if (!Validation.isValidName("" + edt_firstname.getText().toString())) {
            // edt_firstname.setError("Please enter your first name");
            CustomSnackBar.toast(getActivity(), "Please enter first name");

        }
//        else
//        {
//            edt_firstname.setError(null);
//
//        }
        else if (!Validation.isValidName("" + edt_lastname.getText().toString())) {
            //     edt_lastname.setError("Please enter your last name");
            CustomSnackBar.toast(getActivity(), "Please enter last name");

        }
//        else
//        {
//            edt_lastname.setError(null);
//
//        }


        else if (!Validation.isValidName("" + edt_designation.getText().toString())) {
            // edt_designation.setError("Please enter designation");
            CustomSnackBar.toast(getActivity(), "Please enter designation in organisation");

        }
//        else {
//            edt_designation.setError(null);
//
//
//        }
        else if (!Validation.isValidContact("" + edt_contact_number.getText().toString())) {
            //  edt_contact_number.setError("Please enter a valid contact number");

            CustomSnackBar.toast(getActivity(), "Please enter mobile number");

        }
//        else {
//            edt_contact_number.setError(null);
//
//
//        }

        if (Validation.isValidName("" + edt_designation.getText()) && Validation.isValidName("" + edt_firstname.getText()) && Validation.isValidName("" + edt_lastname.getText()) && Validation.isValidContact("" + edt_contact_number.getText())) {

            return true;
        }
        return false;
    }


    public void gotoProffressionalOtherDetails() {

        if (activity != null) {
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(activity, R.anim.slide_to_right, R.anim.slide_from_left);
            startActivity(new Intent(activity, MainActivity.class), options.toBundle());
            activity.finishAffinity();
          //  activity.finish();

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                if (isValidate()) {
                    params = getParams();
                    AsyncRequest getPosts = new AsyncRequest(OrgainizationOtherDetails.this, getActivity(), "POST", params, view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.UPDATE_ORGANIZATION);
                }
                break;

        }
    }


    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("organization_id", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("contact_number", "" + edt_contact_number.getText().toString()));
        params.add(new BasicNameValuePair("contact_firstname", "" + edt_firstname.getText().toString()));
        params.add(new BasicNameValuePair("contact_lastname", "" + edt_lastname.getText().toString()));
        params.add(new BasicNameValuePair("about", "" + edt_about_organisation.getText().toString()));
        params.add(new BasicNameValuePair("contact_position", "" + edt_designation.getText().toString()));
        params.add(new BasicNameValuePair("join_flow_completed", "" + 1));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));


        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {
                // JSONObject result= postObject.getJSONObject("result");

                FIRST_NAME = "" + edt_firstname.getText().toString();
                Mypreferences.LAST_NAME = "" + edt_lastname.getText().toString();
                Mypreferences.DESIGNATION = "" + edt_designation.getText().toString();
                Mypreferences.ABOUT_YOU = "" + edt_about_organisation.getText().toString();
                Mypreferences.MOBILE_NUMBER = "" + edt_contact_number.getText().toString();

                if (SettingActivity.profilePageCheck == 1) {
                    SettingActivity.profilePageCheck = 0;
                    getActivity().onBackPressed();
                } else {
                    gotoProffressionalOtherDetails();
                }

            } else {
                String s = "" + postObject.getString("message");

                CustomSnackBar.toast(getActivity(), s);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
