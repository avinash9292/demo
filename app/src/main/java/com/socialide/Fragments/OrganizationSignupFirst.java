package com.socialide.Fragments;


import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.SplashScreen;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.Config;
import com.socialide.Helper.CustomEditText;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DrawableClickListener;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.Validation;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrganizationSignupFirst extends Fragment implements View.OnClickListener,DrawableClickListener,AsyncRequest.OnAsyncRequestComplete{
    MyButton btn_continue;
    ArrayList<NameValuePair> params;
    LoginActivity activity;
    AVLoadingIndicatorView view;
    MyEditTextView edt_organisation_name,edt_registration_number,edt_landline_number,edt_email;
    CustomEditText edt_password;
int toggle=1;
    public OrganizationSignupFirst() {
        // Required empty public constructor
        LoginActivity.back_handle=0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_organisation_signup_first, container, false);
        btn_continue= (MyButton) root.findViewById(R.id.btn_signup);
        edt_organisation_name= (MyEditTextView) root.findViewById(R.id.edt_organisation_name);
        edt_registration_number= (MyEditTextView) root.findViewById(R.id.edt_registration_number);
        edt_landline_number= (MyEditTextView) root.findViewById(R.id.edt_landline_number);
        edt_email= (MyEditTextView) root.findViewById(R.id.edt_email);
        view= (AVLoadingIndicatorView) root.findViewById(R.id.avi);
        edt_password= (CustomEditText) root.findViewById(R.id.edt_password);
        edt_password.setDrawableClickListener(this);
        btn_continue.setOnClickListener(this);
        toggle=1;
        activity = (LoginActivity)getActivity();
        return root;

    }

    public void gotoOrganisationSignupSecond(){
        LoginActivity.i =1;
        activity.goToFragment(new OrganisationSignupSecond());
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
               if (isValidate()) {
                   params = getParams();
                   AsyncRequest getPosts = new AsyncRequest(OrganizationSignupFirst.this,getActivity(), "POST", params,view);
                   getPosts.execute(GloabalURI.baseURI+GloabalURI.SIGNUP);
               }
                break;

        }
    }

    private boolean isValidate() {
        if(!Validation.isValidName("" + edt_organisation_name.getText().toString())){
           // edt_organisation_name.setError("Please enter organisation name");

            CustomSnackBar.toast(getActivity(), "Please enter organisation name");

        }
        else if(!Validation.isValidName("" + edt_email.getText().toString()))
        {
            //   edt_email.setError("Please enter email");
            CustomSnackBar.toast(getActivity(), "Please enter email");


        }
        else if (!Validation.isValidEmail("" + edt_email.getText().toString())) {
            // edt_email.setError("Please enter a valid Email");

            CustomSnackBar.toast(getActivity(), "Please enter correct email");

        }

        else    if(!Validation.isValidPassword("" + edt_password.getText().toString())){
            //     edt_password.setError("Please set a valid password");
            CustomSnackBar.toast(getActivity(), "Please enter password");

        }
//        else
//        {
//            edt_organisation_name.setError(null);
//
//        }
        else if(!Validation.isValidName("" + edt_registration_number.getText().toString())){
         //   edt_registration_number.setError("Please enter registration number");

            CustomSnackBar.toast(getActivity(), "Please enter registration number");

        }
//        else
//        {
//            edt_registration_number.setError(null);
//
//        }


//        else
//        {
//            edt_password.setError(null);
//
//        }

//        else {
//            edt_email.setError(null);
//
//
//        }
        else   if (!Validation.isValidContact("" + edt_landline_number.getText().toString())) {
          //  edt_landline_number.setError("Please enter a valid landlines number");

            CustomSnackBar.toast(getActivity(),"Please enter landlines number");
        }
//        else {
//            edt_landline_number.setError(null);
//
//
//        }

        if(Validation.isValidEmail(""+edt_email.getText())&&Validation.isValidPassword("" + edt_password.getText())&&Validation.isValidName("" + edt_organisation_name.getText())&&Validation.isValidName(""+edt_registration_number.getText())&&Validation.isValidContact(""+edt_landline_number.getText())){

            return true;
        }
        return false;
    }


    @Override
    public void onClick(DrawablePosition target) {
        switch (target) {



            case RIGHT:
                //Do something here
                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "museosanscyrl.ttf");
                edt_password.setTypeface(tf);

                if (toggle%2==0){
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_password.setSelection(edt_password.getText().length());
                    edt_password.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.hide_password_green_icon, 0);

                }
                else {

                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    edt_password.setSelection(edt_password.getText().length());
                    edt_password.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.show_password_green_icon, 0);

                }
                toggle++;

                break;

            default:
                break;
        }

    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("name", ""+edt_organisation_name.getText().toString()));
        params.add(new BasicNameValuePair("u_dise_number", ""+edt_registration_number.getText().toString()));
        params.add(new BasicNameValuePair("password", ""+edt_password.getText().toString()));
        params.add(new BasicNameValuePair("email", ""+edt_email.getText().toString()));
        params.add(new BasicNameValuePair("landline_number", ""+edt_landline_number.getText().toString()));
        params.add(new BasicNameValuePair("type", "Organization"));
        params.add(new BasicNameValuePair("device_type", "android"));


        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("regiiid", "aaa   " + regId);
        params.add(new BasicNameValuePair("device_token", ""+regId));
     //   params.add(new BasicNameValuePair("device_token", ""+ SplashScreen.device_token));
        return params;
    }

    @Override
    public void asyncResponse(String response) {

        try {
            JSONObject postObject = new JSONObject(response);
            Log.e("Response",""+response);
            if(postObject.getString("success").equals("true"))
            {
                JSONObject result= postObject.getJSONObject("result");

                Mypreferences.ORGANISATION_NAME=result.getString("name");
                Mypreferences.REGISTRATION_NUMBER=result.getString("u_dise_number");
                Mypreferences.LANDLINE_NUMBER=result.getString("landline_number");
                Mypreferences.Access_token=result.getString("access_token");

                Mypreferences.setString(Mypreferences.Email,result.getString("email"),getActivity());
                Mypreferences.setString(Mypreferences.Pass,edt_password.getText().toString(),getActivity());

                Toast.makeText(activity, "Profile update Successfully.", Toast.LENGTH_SHORT).show();

                Mypreferences.User_id=result.getString("userid");
             //   Mypreferences.Join_Flow_Completed=result.getString("join_flow_completed");

                gotoOrganisationSignupSecond();
            }
            else
            {
                String s=""+postObject.getString("message");

                CustomSnackBar.toast(getActivity(),s);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
