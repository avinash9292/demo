package com.socialide.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProDeclinedEventDetails extends Fragment implements AsyncRequest.OnAsyncRequestComplete{
    MyTextView tv_event_name,tv_organisation_name,tv_event_guests,txt_date,txt_time,txt_email,txt_landline_number,edt_message,txt_reason,txt_declined;
    CircleImageView iv_me_profile_pic;
    Button btn_remove;
    BeanForFetchEvents b;
AVLoadingIndicatorView view;
    public ProDeclinedEventDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_event_details_pro_declined, container, false);

        iv_me_profile_pic= (CircleImageView) rootView.findViewById(R.id.iv_me_profile_pic);
        btn_remove= (Button) rootView.findViewById(R.id.btn_remove);




        tv_event_name= (MyTextView) rootView.findViewById(R.id.tv_name);
        tv_organisation_name= (MyTextView) rootView.findViewById(R.id.tv_organisation_name);
        tv_event_guests= (MyTextView) rootView.findViewById(R.id.tv_event_guests);
        txt_date= (MyTextView) rootView.findViewById(R.id.txt_date);
        txt_time= (MyTextView) rootView.findViewById(R.id.txt_time);
        txt_email= (MyTextView) rootView.findViewById(R.id.txt_email);
        txt_landline_number= (MyTextView) rootView.findViewById(R.id.txt_landline_number);
        edt_message= (MyTextView) rootView.findViewById(R.id.edt_message);
        txt_reason= (MyTextView) rootView.findViewById(R.id.txt_reason);
        txt_declined= (MyTextView) rootView.findViewById(R.id.txt_declined);
        view= (AVLoadingIndicatorView) getActivity().findViewById(R.id.avi);



         b= EventsDeclined.activity_list1.get(EventFragment.position);
        tv_event_name.setText(""+b.getEvent_title());
        tv_organisation_name.setText(""+b.getEvent_by());
        tv_event_guests.setText(""+b.getEvent_location());

        String datetime= TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date =datetime.split("T")[0];
        String time =datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" +date);
        txt_landline_number.setText(""+b.getContact_number());
        txt_email.setText(""+b.getEmail());
        edt_message.setText(""+b.getMessage());
        txt_declined.setText("" + b.getDecline_by());
        txt_reason.setText("" + b.getReason());



        DonutProgress d = (DonutProgress) rootView.findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, getActivity());
        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<NameValuePair> params= new ArrayList<>();
                params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));


                AsyncRequest getPosts = new AsyncRequest(ProDeclinedEventDetails.this,getActivity(), "POST", params,view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.REMOVE_AN_EVENT);

            }
        });



        return rootView;
    }


    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if(postObject.getString("success").equals("true"))
            {
                CustomSnackBar.toast(getActivity(), postObject.getString("message"));
                EventsDeclined.activity_list1.remove(EventFragment.position);
                getActivity().onBackPressed();
            }
            else
            {
                CustomSnackBar.toast(getActivity(),postObject.getString("message"));
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
