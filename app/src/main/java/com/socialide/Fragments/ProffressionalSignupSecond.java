package com.socialide.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.SettingActivity;
import com.socialide.Adapters.SpinnerDataAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.BuildConfig;
import com.socialide.Helper.CustomScrollView;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DatabaseHandlerOld;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyAutoCompleteTextView;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.NotificationUtils;
import com.socialide.Helper.SpinnerData;
import com.socialide.Helper.Validation;
import com.socialide.Model.BeanForCountry;
import com.socialide.Model.BeanForState;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import okhttp3.Callback;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import static com.socialide.Helper.Mypreferences.CITY;
import static com.socialide.Helper.Mypreferences.COUNTRY;
import static com.socialide.Helper.Mypreferences.STATE;

public class ProffressionalSignupSecond extends Fragment implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete, LocationListener {
    MyButton btn_continue;
    MyTextView btn_do_later;
    LoginActivity activity;

    boolean p = false;

    Bitmap bitmap;
    RelativeLayout rl;
    MyEditTextView edt_dob;
    ImageView imageView, img_back;
    String path;
    MyAutoCompleteTextView edt_gender, edt_city, edt_state, edt_country;

    ArrayList<NameValuePair> params;
    Intent CamIntent, GalIntent, CropIntent;
    File file;
    Uri uri;
    String filePath;


    PopupWindow mPopupWindow;
    LinearLayout mRelativeLayout;
    ByteArrayBody bab = null;
    AVLoadingIndicatorView view;
    MyTextView txt_upload_pic, txt_name, tvToolbarTitle;


    ArrayAdapter ad_country, ad_state, ad_city;
    DatabaseHandlerOld db;
    ArrayList<BeanForCountry> al_country = new ArrayList<>();
    ArrayList<String> al_country_names = new ArrayList<>();
    ArrayList<BeanForState> al_state = new ArrayList<>();
    ArrayList<String> al_state_names = new ArrayList<>();
    ArrayList<String> al_city_names = new ArrayList<>();
    View root;

    public ProffressionalSignupSecond() {
        // Required empty public constructor
        LoginActivity.back_handle = 1;
    }

    Calendar calendar;
    DatePickerDialog datePickerDialog;
    int Year, Month, Day;
    DonutProgress d;
    int checkPopup = 0;
    ListView lvGender, lvCountry, lvState, lvCity;
    SpinnerDataAdapter spinnerGenderAdapter, spinnerCountryAdapter, spinnerStateAdapter, spinnerCityAdapter;
    ArrayList<String> listGender, listCountry, listState, listCity;
    int genderPosition = -1, countryPosition = -1, statePosition = -1, cityPosition = -1;
    CustomScrollView scrollView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_professional_signup_second, container, false);

        return root;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_continue = (MyButton) root.findViewById(R.id.btn_continue);
        btn_do_later = (MyTextView) root.findViewById(R.id.btn_do_later);
        txt_upload_pic = (MyTextView) root.findViewById(R.id.txt_upload_pic);
        tvToolbarTitle = (MyTextView) root.findViewById(R.id.tvToolbarTitle);
        txt_name = (MyTextView) root.findViewById(R.id.txt_name);
        txt_name.setText("" + Mypreferences.FIRST_NAME + " " + Mypreferences.LAST_NAME);
        imageView = (ImageView) root.findViewById(R.id.circleView);
        img_back = (ImageView) root.findViewById(R.id.img_back);
        rl = (RelativeLayout) root.findViewById(R.id.relative_bg);
        mRelativeLayout = (LinearLayout) root.findViewById(R.id.lin);
        edt_dob = (MyEditTextView) root.findViewById(R.id.edt_dob);
        this.view = (AVLoadingIndicatorView) root.findViewById(R.id.avi);
        d = (DonutProgress) root.findViewById(R.id.loading);

        lvGender = (ListView) root.findViewById(R.id.lvGender);
        lvCountry = (ListView) root.findViewById(R.id.lvCountry);
        lvState = (ListView) root.findViewById(R.id.lvState);
        lvCity = (ListView) root.findViewById(R.id.lvCity);

        scrollView = (CustomScrollView) root.findViewById(R.id.scrollView);

        edt_gender = (MyAutoCompleteTextView) root.findViewById(R.id.edt_gender);
        edt_city = (MyAutoCompleteTextView) root.findViewById(R.id.edt_city);
        edt_state = (MyAutoCompleteTextView) root.findViewById(R.id.edt_state);
        edt_country = (MyAutoCompleteTextView) root.findViewById(R.id.edt_country);
        View view_do = (View) root.findViewById(R.id.view_do);
        btn_continue.setOnClickListener(this);
        activity = (LoginActivity) getActivity();
        txt_upload_pic.setOnClickListener(this);
        edt_gender.setOnClickListener(this);
        edt_dob.setOnClickListener(this);
        btn_do_later.setOnClickListener(this);
        edt_country.setOnClickListener(this);
        edt_state.setOnClickListener(this);
        edt_city.setOnClickListener(this);


        if (Build.VERSION.SDK_INT >= 23) {

            int permissionCheck = ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION);

            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        } else {
            LocationManager lManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            boolean netEnabled = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (netEnabled) {
                lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, activity);

                Location location = lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {

                    Mypreferences.Lat = location.getLatitude();
                    Mypreferences.Lang = location.getLongitude();

                }
                new GeocodeAsyncTask().execute();
            } else {
                edt_country.addTextChangedListener(new TextWatcher1(edt_country));
            }

        }


        if (SettingActivity.profilePageCheck == 1) {
            btn_do_later.setVisibility(View.INVISIBLE);
            view_do.setVisibility(View.GONE);
            btn_continue.setText(getResources().getString(R.string.btn_save));
            tvToolbarTitle.setText(getResources().getString(R.string.generalDetails));
            checkPopup = 1;
        }

        if (!Mypreferences.Picture.equals("")) {

            com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, imageView, d, getActivity(), img_back);
        }

        if (!Mypreferences.DOB.equals("DOB"))
            edt_dob.setText("" + Mypreferences.DOB);

        listGender = SpinnerData.getGenderAdapter(getActivity());

        if (!Mypreferences.GENDER.equals("GENDER")) {
            edt_gender.setText("" + Mypreferences.GENDER);
            genderPosition = listGender.indexOf(Mypreferences.GENDER);
        }
        spinnerGenderAdapter = new SpinnerDataAdapter(listGender, getActivity(), getContext(), genderPosition);
        lvGender.setAdapter(spinnerGenderAdapter);
        lvGender.setSelectionFromTop(genderPosition, 0);
        setListHeightOnAdapter(lvGender);

        lvGender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_gender.setText(listGender.get(position));

                genderPosition = position;
                scrollView.setEnableScrolling(true);
                lvGender.setVisibility(View.GONE);
            }
        });

        calendar = Calendar.getInstance();

        Year = calendar.get(Calendar.YEAR);
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);


        db = new DatabaseHandlerOld(getActivity());
        al_country = db.getCountry();
        for (int i = 0; i < al_country.size(); i++) {
            al_country_names.add(al_country.get(i).getName());
        }


        if (!Mypreferences.COUNTRY.equals("COUNTRY") && !Mypreferences.COUNTRY.equals("")) {
            Log.v("akram", "coun if = " + Mypreferences.COUNTRY);

            edt_country.setText("" + Mypreferences.COUNTRY);
            countryPosition = al_country_names.indexOf(Mypreferences.COUNTRY);

            if (!Mypreferences.STATE.equals("STATE") && !Mypreferences.STATE.equals("")) {
                setStateOnCountrySpinner(Mypreferences.COUNTRY, Mypreferences.STATE);
            }
        }

        if (!Mypreferences.STATE.equals("STATE") && !Mypreferences.STATE.equals("")) {

            edt_state.setText("" + Mypreferences.STATE);
            //  statePosition = al_country_names.indexOf(Mypreferences.STATE);
            if (!Mypreferences.CITY.equals("CITY") || !Mypreferences.CITY.equals("")) {
                setCityOnStateSpinner(Mypreferences.STATE, Mypreferences.CITY);
            }

        }

        if (!Mypreferences.CITY.equals("CITY") && !Mypreferences.CITY.equals("")) {

            edt_city.setText("" + Mypreferences.CITY);
            // countryPosition = al_country_names.indexOf(Mypreferences.CITY);
        }


        spinnerCountryAdapter = new SpinnerDataAdapter(al_country_names, getActivity(), getContext(), countryPosition);

        lvCountry.setAdapter(spinnerCountryAdapter);
        lvCountry.setSelectionFromTop(countryPosition, 0);

        lvCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_country.setText(al_country_names.get(position));
                cityPosition = -1;
                statePosition = -1;
                edt_city.setText("");
                edt_state.setText("");
                al_city_names.clear();
                countryPosition = position;
                scrollView.setEnableScrolling(true);
                lvCountry.setVisibility(View.GONE);
            }
        });

        lvState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                edt_state.setText(al_state_names.get(position));
                edt_city.setText("");
                statePosition = position;
                cityPosition = -1;
                scrollView.setEnableScrolling(true);
                lvState.setVisibility(View.GONE);
            }
        });

        lvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_city.setText(al_city_names.get(position));
                cityPosition = position;
                scrollView.setEnableScrolling(true);
                lvCity.setVisibility(View.GONE);
            }
        });
       /* edt_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_country.showDropDown();
                if (("" + edt_country.getText().toString()).length() > 0) {

                    if (ad_country != null) {
                        ad_country.getFilter().filter(null);
                    }
                }
            }
        });*/
       /* edt_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_state.showDropDown();
                if (("" + edt_state.getText().toString()).length() > 0) {
                    if (ad_state != null) {
                        ad_state.getFilter().filter(null);
                    }
                }
            }
        });*/
      /*  edt_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_city.showDropDown();
                if (("" + edt_city.getText().toString()).length() > 0) {

                    if (ad_city != null) {
                        ad_city.getFilter().filter(null);
                    }
                }
            }
        });*/


    }

    public class TextWatcher1 implements TextWatcher {
        AutoCompleteTextView e;

        public TextWatcher1(AutoCompleteTextView e) {
            this.e = e;

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {


            switch (e.getId()) {
                case R.id.edt_country:
                    edt_city.setText("");
                    edt_state.setText("");

                    if (("" + e.getText().toString()).length() != 0) {
                        al_state_names.clear();
                        al_state.clear();

                        String id = "";
                        for (int i = 0; i < al_country_names.size(); i++) {
                            if (al_country_names.get(i).equals(("" + e.getText().toString()))) {
                                id = al_country.get(i).getId();
                                break;
                            }
                        }

                        al_state = db.getState(id);

                        for (int i = 0; i < al_state.size(); i++) {

                            al_state_names.add(al_state.get(i).getName());

                        }

                        spinnerStateAdapter = new SpinnerDataAdapter(al_state_names, getActivity(), getContext(), 0);
                        lvState.setAdapter(spinnerStateAdapter);
                        if (al_state_names.size() < 7) {
                            setListHeightOnAdapter(lvState);

                        } else {
                            ViewGroup.LayoutParams params = lvState.getLayoutParams();
                            params.height = 750;
                            lvState.setLayoutParams(params);
                            lvState.requestLayout();
                        }
                        lvState.setSelectionFromTop(-1, 0);


                        //ad_state = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.txt, al_state_names);
                        //edt_state.setAdapter(ad_state);
                        if (!p) {
                            edt_state.addTextChangedListener(new TextWatcher1(edt_state));
                            p = true;
                        }
                    }

                    break;
                case R.id.edt_state:
                    edt_city.setText("");

                    db = new DatabaseHandlerOld(getContext());
                    if (("" + e.getText().toString()).length() != 0) {


                        String id = "";
                        for (int i = 0; i < al_state_names.size(); i++) {
                            if (al_state_names.get(i).equals(("" + e.getText().toString()))) {
                                id = al_state.get(i).getId();
                                break;
                            }
                        }

                        al_city_names = db.getCity(id);


                        spinnerCityAdapter = new SpinnerDataAdapter(al_city_names, getActivity(), getContext(), 0);

                        lvCity.setSelectionFromTop(-1, 0);
                        lvCity.setAdapter(spinnerCityAdapter);

                        if (al_city_names.size() < 7) {

                            setListHeightOnAdapter(lvCity);
                        } else {

                            ViewGroup.LayoutParams params = lvCity.getLayoutParams();
                            params.height = 750;
                            lvCity.setLayoutParams(params);
                            lvCity.requestLayout();

                        }

                        //  ad_city = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.txt, al_city_names);
                        //  edt_city.setAdapter(ad_city);

                    }

                    break;

            }


        }
    }


    public void gotoProffressionalOtherDetails() {
        activity.goToFragment(new ProffressionalOtherDetails());
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        if (bitmap != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            byte[] data = bos.toByteArray();

            bab = new ByteArrayBody(data, "aaa.jpg");

        }
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("professional_id", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("dob", "" + edt_dob.getText().toString()));
        params.add(new BasicNameValuePair("gender", "" + edt_gender.getText().toString()));
        params.add(new BasicNameValuePair("country", "" + edt_country.getText().toString()));
        params.add(new BasicNameValuePair("state", "" + edt_state.getText().toString()));
        params.add(new BasicNameValuePair("city", "" + edt_city.getText().toString()));
        params.add(new BasicNameValuePair("profile_pic", "" + bab));

        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.getString("success").equals("true")) {
                // JSONObject result= postObject.getJSONObject("result");

                Log.e("response", "" + response);
                Mypreferences.GENDER = "" + edt_gender.getText().toString();
                Mypreferences.DOB = "" + edt_dob.getText().toString();

                COUNTRY = "" + edt_country.getText().toString();
                STATE = "" + edt_state.getText().toString();
                CITY = "" + edt_city.getText().toString();

                gotoProffressionalOtherDetails();
            } else {

                String s = "" + postObject.getString("message");

                CustomSnackBar.toast(getActivity(), s);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                listVisibilityGone(lvGender);
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvState);
                listVisibilityGone(lvCity);


                if (isValidate()) {
                    Mypreferences.setString(Mypreferences.DOB, edt_dob.getText().toString(), getActivity());
                    if (bitmap == null) {

                        if (checkPopup == 0 && Mypreferences.Picture.equals("")) {
                            popup(getActivity());
                        } else {
                            try {
                                execMultipartPost();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {

//                        params = getParams();
//                        AsyncRequest getPosts = new AsyncRequest(ProffressionalSignupSecond.this,getActivity(), "POST", params);
//                        getPosts.execute(GloabalURI.baseURI+GloabalURI.UPDATE_PROFESSIONAL);
                        try {
                            execMultipartPost();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case R.id.txt_upload_pic:
                listVisibilityGone(lvGender);
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvState);
                listVisibilityGone(lvCity);

                requestPermissions(
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                        1);
                break;
            case R.id.btn_do_later:
                listVisibilityGone(lvGender);
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvState);
                listVisibilityGone(lvCity);
                gotoProffressionalOtherDetails();


                break;
            case R.id.edt_gender:
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvState);
                listVisibilityGone(lvCity);
                if (lvGender.getVisibility() == View.VISIBLE) {

                    scrollView.setEnableScrolling(true);
                    lvGender.setVisibility(View.GONE);

                    //
                } else {

                    spinnerGenderAdapter = new SpinnerDataAdapter(listGender, getActivity(), getContext(), genderPosition);
                    lvGender.setAdapter(spinnerGenderAdapter);
                    lvGender.setSelectionFromTop(genderPosition, 0);
                    setListHeightOnAdapter(lvGender);
                    lvGender.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);
                }

                // edt_gender.showDropDown();
                break;
            case R.id.edt_dob:
                listVisibilityGone(lvGender);
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvState);
                listVisibilityGone(lvCity);

                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text

                                String day, month;
                                day = "" + dayOfMonth;
                                monthOfYear++;
                                month = "" + monthOfYear;
                                if (dayOfMonth < 10) {
                                    day = 0 + day;
                                }
                                if (monthOfYear < 10) {
                                    month = 0 + month;
                                }

                                edt_dob.setText(day + "-"
                                        + month + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

                break;


            case R.id.edt_country:
                listVisibilityGone(lvGender);
                listVisibilityGone(lvState);
                listVisibilityGone(lvCity);
                if (lvCountry.getVisibility() == View.VISIBLE) {

                    scrollView.setEnableScrolling(true);
                    lvCountry.setVisibility(View.GONE);

                    //
                } else {

                    spinnerCountryAdapter = new SpinnerDataAdapter(al_country_names, getActivity(), getContext(), countryPosition);
                    lvCountry.setAdapter(spinnerCountryAdapter);
                    lvCountry.setSelectionFromTop(countryPosition, 0);


                    lvCountry.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);
                }


                break;

            case R.id.edt_state:
                listVisibilityGone(lvGender);
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvCity);

                //  listVisibilityGone(lvCountry);

                if (lvState.getVisibility() == View.VISIBLE) {

                    scrollView.setEnableScrolling(true);

                    lvState.setVisibility(View.GONE);

                    //
                } else {


                    if (al_state_names.size() == 0)
                        break;
                    spinnerStateAdapter = new SpinnerDataAdapter(al_state_names, getActivity(), getContext(), statePosition);
                    lvState.setAdapter(spinnerStateAdapter);
                    if (al_state_names.size() < 7) {
                        setListHeightOnAdapter(lvState);

                    } else {
                        ViewGroup.LayoutParams params = lvState.getLayoutParams();
                        params.height = 750;
                        lvState.setLayoutParams(params);
                        lvState.requestLayout();
                    }
                    lvState.setSelectionFromTop(statePosition, 0);

                    lvState.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);

                }
                break;


            case R.id.edt_city:

                listVisibilityGone(lvGender);
                listVisibilityGone(lvCountry);
                listVisibilityGone(lvState);

                if (lvCity.getVisibility() == View.VISIBLE) {
                    scrollView.setEnableScrolling(true);
                    lvCity.setVisibility(View.GONE);
                    //
                } else {


                    if (al_city_names.size() == 0)
                        break;
                    spinnerCityAdapter = new SpinnerDataAdapter(al_city_names, getActivity(), getContext(), cityPosition);
                    lvCity.setAdapter(spinnerCityAdapter);
                    if (al_city_names.size() < 7) {
                        setListHeightOnAdapter(lvState);

                    } else {
                        ViewGroup.LayoutParams params = lvCity.getLayoutParams();
                        params.height = 750;
                        lvCity.setLayoutParams(params);
                        lvCity.requestLayout();
                    }
                    lvCity.setSelectionFromTop(cityPosition, 0);

                    lvCity.setVisibility(View.VISIBLE);

                    scrollView.setEnableScrolling(false);

                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    selectImage();

                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }

            }

            case 2:

                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);


                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    LocationManager lManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    boolean netEnabled = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    if (netEnabled) {

                        lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, activity);

                        Location location = lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {

                            Mypreferences.Lat = location.getLatitude();
                            Mypreferences.Lang = location.getLongitude();
                        }
                        new GeocodeAsyncTask().execute();
                    } else {
                        edt_country.addTextChangedListener(new TextWatcher1(edt_country));
                    }

                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }

                break;
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private boolean isValidate() {
        if (!Validation.isValidName("" + edt_dob.getText().toString())) {
            //edt_dob.setError("Please select your date of birth");
            CustomSnackBar.toast(getActivity(), "Please select your date of birth");

        }
//        else
//        {
//            edt_dob.setError(null);
//
//        }

        else if (!Validation.isValidName("" + edt_gender.getText().toString())) {
            //  edt_gender.setError("Please select gender");
            CustomSnackBar.toast(getActivity(), "Please select gender");

        }
//        else
//        {
//            edt_gender.setError(null);
//
//        }
        else if (!Validation.isValidName("" + edt_country.getText().toString())) {
            //  edt_country.setError("Please select country");
            CustomSnackBar.toast(getActivity(), "Please select country");

        }
//        else {
//            edt_country.setError(null);
//
//
//        }
        else if (!Validation.isValidName("" + edt_state.getText().toString())) {
            // edt_state.setError("Please select state");
            CustomSnackBar.toast(getActivity(), "Please select state");

        }
//        else {
//            edt_state.setError(null);
//
//
//        }
        else if (!Validation.isValidName("" + edt_city.getText().toString())) {
            // edt_city.setError("Please select city");
            CustomSnackBar.toast(getActivity(), "Please select city");

        }
//        else
//        {
//            edt_city.setError(null);
//
//        }


        if (Validation.isValidName("" + edt_dob.getText()) && Validation.isValidName("" + edt_gender.getText()) && Validation.isValidName("" + edt_country.getText()) && Validation.isValidName("" + edt_state.getText()) && Validation.isValidName("" + edt_city.getText())) {

            return true;
        }
        return false;
    }

    public void ClickImageFromCamera() {

       /* CamIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        file = new File(Environment.getExternalStorageDirectory(),
                "file" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        uri = Uri.fromFile(file);

        CamIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);

        CamIntent.putExtra("return-data", true);

        startActivityForResult(CamIntent, 0);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String date = DateFormat.getDateTimeInstance().format(new Date());

        filePath = date + "temp.jpg";

        File f = new File(Environment.getExternalStorageDirectory(), filePath);

        Uri photoURI = FileProvider.getUriForFile(getContext(),
                BuildConfig.APPLICATION_ID +
                        ".provider", f);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        startActivityForResult(intent, 0);

    }

    public void GetImageFromGallery() {

        GalIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(GalIntent, "Select Image From Gallery"), 2);

    }


    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    ClickImageFromCamera();
                    dialog.dismiss();

                } else if (options[item].equals("Choose from Gallery")) {

                    GetImageFromGallery();

                    dialog.dismiss();

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {

            File f = new File(Environment.getExternalStorageDirectory().toString());
            // Static_variable.uploaded_report_path_str=f.toString();
            for (File temp : f.listFiles()) {

                if (temp.getName().equals(filePath)) {

                    f = temp;
                    break;
                }
            }

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

            path = f.getAbsolutePath();
            // imageView.setImageBitmap(bitmap);

            Glide.with(getContext())
                    .load(path)
                    .into(imageView);

            Glide.with(getContext())
                    .load(path)
                    .into(img_back);


        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {

            if (data != null) {

                uri = data.getData();

                Uri selectedImageUri = data.getData();
                String tempPath = getPath(selectedImageUri);

                path = tempPath;

                Glide.with(getContext())
                        .load(path)
                        .into(imageView);

                Glide.with(getContext())
                        .load(path)
                        .into(img_back);
                //  Static_variable.usr_uploaded_img__str = tempPath;

                bitmap = BitmapFactory.decodeFile(tempPath);

                //  com.socialide.Helper.ImageLoader.image(tempPath, imageView, d, getActivity(), img_back);

                /*imageView.setImageBitmap(bitmap);
                img_back.setImageBitmap(bitmap);*/
                // ImageCropFunction();

            }

           /* if (data != null) {

                uri = data.getData();

                ImageCropFunction();

            }*/
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            if (data != null) {

                Bundle bundle = data.getExtras();

                bitmap = bundle.getParcelable("data");

                imageView.setImageBitmap(bitmap);
                img_back.setImageBitmap(bitmap);
                int screen_width = getResources().getDisplayMetrics().widthPixels;


                int w = bitmap.getWidth();
                int h = bitmap.getHeight();
                float x = screen_width / w;

                int newh = (int) (x * h);
                Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, screen_width, newh, true);
                // bitmap=renderToBitmap(bitmap,screen_width);
                Drawable dr = new BitmapDrawable(bitmap);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap2);

                bitmapDrawable.setGravity(Gravity.CENTER);
                (rl).setBackgroundDrawable(bitmapDrawable);
                txt_upload_pic.setText(getResources().getString(R.string.changePic));

            }
        }
    }


    private String getPath(Uri uri) {

        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    public Bitmap renderToBitmap(Bitmap svg, int requiredWidth) {
        //  if(requiredWidth < 1) requiredWidth = (int)svg.getWidth();

        float scaleFactor = (float) requiredWidth / (float) svg.getWidth();
        int adjustedHeight = (int) (scaleFactor * svg.getHeight());

        Bitmap bitmap1 = Bitmap.createBitmap(requiredWidth, adjustedHeight, Bitmap.Config.ARGB_8888);


        return bitmap1;
    }

    public void ImageCropFunction() {

        // Image Crop Code
        try {
            CropIntent = new Intent("com.android.camera.action.CROP");

            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 180);
            CropIntent.putExtra("outputY", 180);
            CropIntent.putExtra("aspectX", 1);
            CropIntent.putExtra("aspectY", 1);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);

            startActivityForResult(CropIntent, 1);

        } catch (ActivityNotFoundException e) {

        }
    }

    class GeocodeAsyncTask extends AsyncTask<Void, Void, Address> {

        String errorMessage = "";

        @Override
        protected void onPreExecute() {
            Log.v("Addresses  pre", "-->");
        }

        @Override
        protected Address doInBackground(Void... none) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = null;
            Log.v("Addresses", "lat " + Mypreferences.Lat);
            Log.v("Addresses", "longi " + Mypreferences.Lang);
            double latitude = Double.parseDouble(String.valueOf(Mypreferences.Lat));
            double longitude = Double.parseDouble(String.valueOf(Mypreferences.Lang));

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException ioException) {
                errorMessage = "Service Not Available";

            } catch (IllegalArgumentException illegalArgumentException) {
                errorMessage = "Invalid Latitude or Longitude Used";
            }
            if (addresses != null && addresses.size() > 0) {
                Log.e("Addresses", "-->" + addresses);
                return addresses.get(0);
            }
            return null;
        }

        protected void onPostExecute(Address address) {
            Log.v("Addresses", "-->" + address);
            if (address == null) {

            } else {
                String addressName = "";

                String cityName = address.getAddressLine(0);
                String stateName = address.getAddressLine(1);
                String countryName = address.getAddressLine(2);
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressName += " --- " + address.getAddressLine(i);
                    Log.e("Addresses", "-->" + address.getAddressLine(i));

                }

                Log.v("akram", "city = " + address.getLocality());
                Log.v("akram", "state = " + address.getAdminArea());
                Log.v("akram", "country = " + address.getCountryName());

                if (!Mypreferences.COUNTRY.equals("COUNTRY") && !Mypreferences.COUNTRY.equals("")) {
                    edt_country.setText("" + Mypreferences.COUNTRY);
                } else {
                    edt_country.setText("" + address.getCountryName());
                    countryPosition = al_country_names.indexOf(address.getCountryName());
                    // spinnerCountryAdapter = new SpinnerDataAdapter(al_country_names,)
                    lvCountry.setSelectionFromTop(countryPosition, 0);
                    if (!address.getCountryName().equalsIgnoreCase("")) {
                        setStateOnCountrySpinner(address.getCountryName(), address.getAdminArea());
                    }
                }

                if (!Mypreferences.STATE.equals("STATE") && !Mypreferences.STATE.equals("")) {
                    edt_state.setText("" + Mypreferences.STATE);
                } else {
                    edt_state.setText("" + address.getAdminArea());
                    if (!address.getAdminArea().equalsIgnoreCase("")) {
                        setCityOnStateSpinner(address.getAdminArea(), address.getLocality());
                    }
                }

                if (Mypreferences.CITY.equals("CITY") && Mypreferences.CITY.equals("")) {
                    edt_city.setText("" + Mypreferences.CITY);
                } else {
                    edt_city.setText("" + address.getLocality());
                }

//                infoText.setText("Latitude: " + address.getLatitude() + "\n" +
//                        "Longitude: " + address.getLongitude() + "\n" +
//                        "Address: " + countryName + "\n" + address.getCountryName() + "\n" + address.getLocality() + "\n" + address.getFeatureName() + "\n" + address.getPremises() + "\n" + address.getLocale() + "\n" + address.describeContents() + "\n" + address.getAdminArea() + "\n" + address.getSubLocality());
            }
            edt_country.addTextChangedListener(new TextWatcher1(edt_country));
        }
    }


    public void popup(Context c) {


        View popupView = LayoutInflater.from(c).inflate(R.layout.image_validation_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        MyTextView yes = (MyTextView) popupView.findViewById(R.id.txt_yes);
        MyTextView no = (MyTextView) popupView.findViewById(R.id.txt_no);

        // Set a click listener for the popup window close button
        yes.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window

                requestPermissions(
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},

                        1);
                mPopupWindow.dismiss();


            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window

                mPopupWindow.dismiss();

//                params = getParams();
//                AsyncRequest getPosts = new AsyncRequest(ProffressionalSignupSecond.this,getActivity(), "POST", params);
//                getPosts.execute(GloabalURI.baseURI + GloabalURI.UPDATE_PROFESSIONAL);
                try {
                    execMultipartPost();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
    }


    public String image(Bitmap bm) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.socialide");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return myDir + "/" + fname;
    }

    private void execMultipartPost() throws Exception {
//        Uri uriFromPath = Uri.fromFile(new File(realPath));

        File file = new File(image(bitmap));
        String contentType = file.toURL().openConnection().getContentType();
        final ProgressDialog p = new ProgressDialog(getActivity());


//        p.setIndeterminate(false);
//        p.show();

        view.setVisibility(View.VISIBLE);
        view.show();

        RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), file);

        final String filename = "file_" + System.currentTimeMillis() / 1000L;
        RequestBody requestBody;

        if (bitmap != null) {
            Log.v("akram", "access token  = " + Mypreferences.Access_token);
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("professional_id", Mypreferences.User_id)
                    .addFormDataPart("dob", "" + edt_dob.getText().toString())
                    .addFormDataPart("gender", "" + edt_gender.getText().toString())
                    .addFormDataPart("country", "" + edt_country.getText().toString())
                    .addFormDataPart("state", "" + edt_state.getText().toString())
                    .addFormDataPart("city", "" + edt_city.getText().toString())
                    .addFormDataPart("profile_pic", file.getName(), fileBody)
                    .addFormDataPart("access_token", Mypreferences.Access_token)
                    .build();
        } else {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)

                    .addFormDataPart("professional_id", Mypreferences.User_id)
                    .addFormDataPart("dob", "" + edt_dob.getText().toString())
                    .addFormDataPart("gender", "" + edt_gender.getText().toString())
                    .addFormDataPart("country", "" + edt_country.getText().toString())
                    .addFormDataPart("state", "" + edt_state.getText().toString())
                    .addFormDataPart("city", "" + edt_city.getText().toString())
                    .addFormDataPart("access_token", Mypreferences.Access_token)
                    // .addFormDataPart("picture", filename + ".jpg", fileBody)
                    .build();
        }


        Request request = new Request.Builder()
                .url(GloabalURI.baseURI + GloabalURI.UPDATE_PROFESSIONAL)
                .addHeader("access_token", Mypreferences.Access_token)
                .post(requestBody)
                .build();

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(getActivity(), "Something went wrong, Please try again", Toast.LENGTH_SHORT).show();
                        //p.dismiss();

                        view.setVisibility(View.GONE);
                        view.hide();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String responseJson = response.body().string();
                            Log.v("akram", "response12 = " + responseJson);

                            JSONObject postObject = new JSONObject(responseJson);
                            if (postObject.getString("success").equals("true")) {
                                JSONObject result = postObject.getJSONObject("result");

                                Mypreferences.Picture = result.getString("profile_pic");

                                Mypreferences.GENDER = "" + edt_gender.getText().toString();
                                Mypreferences.DOB = "" + edt_dob.getText().toString();
                                COUNTRY = "" + edt_country.getText().toString();
                                STATE = "" + edt_state.getText().toString();
                                CITY = "" + edt_city.getText().toString();

                                if (SettingActivity.profilePageCheck == 1) {
                                    SettingActivity.profilePageCheck = 0;
                                    getActivity().onBackPressed();
                                } else {
                                    gotoProffressionalOtherDetails();
                                }

                            } else {

                            }

                            view.setVisibility(View.GONE);
                            view.hide();

                        } catch (IOException e) {
                            e.printStackTrace();

                            view.setVisibility(View.GONE);
                            view.hide();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            view.setVisibility(View.GONE);
                            view.hide();
                        }
                    }
                });
            }
        });
    }

    public void listVisibilityGone(ListView listView) {
        if (listView.getVisibility() == View.VISIBLE) {
            listView.setVisibility(View.GONE);
        }

    }

    public void setListHeightOnAdapter(ListView listview) {
        ListAdapter listadp = listview.getAdapter();
        if (listadp != null) {
            int totalHeight = 0;
            for (int i = 0; i < listadp.getCount(); i++) {
                View listItem = listadp.getView(i, null, listview);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listview.getLayoutParams();
            params.height = totalHeight + (listview.getDividerHeight() * (listadp.getCount() - 1));
            listview.setLayoutParams(params);
            listview.requestLayout();
        }
    }


    private void setStateOnCountrySpinner(String countryName, String stateName) {

        al_state_names.clear();
        al_state.clear();

        String id = "";
        for (int i = 0; i < al_country_names.size(); i++) {
            if (al_country_names.get(i).equals(("" + countryName))) {
                id = al_country.get(i).getId();
                break;
            }
        }
        try {
            al_state = db.getState(id);
        } catch (Exception e) {
            Log.v("akram", "state not available");
        }
        for (int i = 0; i < al_state.size(); i++) {

            al_state_names.add(al_state.get(i).getName());

        }

        if (!stateName.equals("STATE") && !stateName.equals("")) {
            statePosition = al_state_names.indexOf(stateName);
        }

        spinnerStateAdapter = new SpinnerDataAdapter(al_state_names, getActivity(), getContext(), statePosition);
        lvState.setAdapter(spinnerStateAdapter);

        if (al_state_names.size() < 7) {
            setListHeightOnAdapter(lvState);

        } else {
            ViewGroup.LayoutParams params = lvState.getLayoutParams();
            params.height = 750;
            lvState.setLayoutParams(params);
            lvState.requestLayout();
        }
        lvState.setSelectionFromTop(statePosition, 0);
    }

    private void setCityOnStateSpinner(String stateName, String cityName) {

        String id = "";
        for (int i = 0; i < al_state_names.size(); i++) {
            if (al_state_names.get(i).equals(("" + stateName))) {
                id = al_state.get(i).getId();
                break;
            }
        }
        try {
            al_city_names = db.getCity(id);
        } catch (Exception e) {
            Log.v("akram", "city not found");
        }

        if (!cityName.equals("CITY") && !cityName.equals("")) {

            cityPosition = al_city_names.indexOf(cityName);
        }

        spinnerCityAdapter = new SpinnerDataAdapter(al_city_names, getActivity(), getContext(), cityPosition);


        lvCity.setAdapter(spinnerCityAdapter);
        lvCity.setSelectionFromTop(cityPosition, 0);
        if (al_city_names.size() < 7) {

            setListHeightOnAdapter(lvCity);
        } else {

            ViewGroup.LayoutParams params = lvCity.getLayoutParams();
            params.height = 750;
            lvCity.setLayoutParams(params);
            lvCity.requestLayout();

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
