package com.socialide.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.socialide.Activities.MainActivity;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class SearchProfessionals extends Fragment implements AsyncRequest.OnAsyncRequestComplete,View.OnClickListener{
Boolean isvisible=true;
MainActivity activity;
    AVLoadingIndicatorView view;
    ArrayList<NameValuePair> params;
int school_type=0,player_type=0;
    AutoCompleteTextView edt_search;
    Button btn_register;
    TextView term;
    ArrayAdapter ad;
    ArrayList<String> al= new ArrayList();
 boolean isset=false;
    static String school_name="";

    public static String location="",experience="",category="",subcategory="",name="";
    int social_score ,verified,availability;
AutoCompleteTextView edt_player_type;
    LinearLayout lin_proffesional,lin_organization;
    public SearchProfessionals() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_professional_search, container, false);

        edt_search=(AutoCompleteTextView)root.findViewById(R.id.edt_search);
        view=(AVLoadingIndicatorView)root.findViewById(R.id.avi);
        edt_search.setThreshold(2);
        edt_search.setOnClickListener(this);
        edt_search.addTextChangedListener(new TextWatcher1(edt_search));

        edt_search.showDropDown();

        activity=(MainActivity) getActivity();

        return root;


    }




    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("location", ""+location));
        params.add(new BasicNameValuePair("experience", ""+experience));
        params.add(new BasicNameValuePair("availability", ""+availability));
        params.add(new BasicNameValuePair("social_score", ""+social_score));
        params.add(new BasicNameValuePair("category", ""+category));
        params.add(new BasicNameValuePair("subcategory", ""+subcategory));
        params.add(new BasicNameValuePair("verified", ""+verified));
        params.add(new BasicNameValuePair("name", ""+verified));

        params.add(new BasicNameValuePair("pageindex", "1"));
        params.add(new BasicNameValuePair("pagesize", "100"));





        return params;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {


        }





    }
    @Override
    public void asyncResponse(String response) {
        Log.e("response", "" + response);


        try {
            JSONObject postObject = new JSONObject(response);
al= new ArrayList();
            if(postObject.getJSONArray("Status").getJSONObject(0).getString("Status").equals("200"))
            {
               JSONArray ja= postObject.getJSONArray("Data");
                for(int i= 0;i<ja.length();i++){

                    JSONObject j=ja.getJSONObject(i);
                    al.add(""+j.getString("strPublicSchoolName"));

                }

if(isvisible) {
    ad = new ArrayAdapter(getActivity(), R.layout.spinner_item, R.id.txt, al);
    edt_search.setAdapter(ad);
    edt_search.showDropDown();
}

            }
            else
            {


                JSONArray ja= postObject.getJSONArray("Message");
                JSONObject j= ja.getJSONObject(0);
                CustomSnackBar.toast(getActivity(),j.getString("Message"));

            }



        } catch (JSONException e) {
            e.printStackTrace();
        }




    }

    @Override
    public void onResume() {
        super.onResume();
        isvisible=true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isvisible=false;
    }

    public class TextWatcher1 implements TextWatcher {
        AutoCompleteTextView e;
        Timer timer;
        public  TextWatcher1(AutoCompleteTextView e){
            this.e=e;

        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {



        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (timer != null) {
                timer.cancel();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        //timer  =new Timer();
        final long DELAY = 1000; //
//            timer.cancel();
        timer = new Timer();
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (e.getId() == R.id.edt_search) {


                                                                if (e.getText().toString().length() >= 2) {
                                                                    for (int i = 0; i < al.size(); i++) {
                                                                        if (e.getText().toString().equals(al.get(i))) {
                                                                            school_name = al.get(i);
                                                                            isset = true;
                                                                            activity.onBackPressed();
                                                                            break;
                                                                        }
                                                                    }


                                                                    if (!isset) {
                                                                        params = getParams();
                                                                        AsyncRequest getPosts = new AsyncRequest(SearchProfessionals.this, getActivity(), "POST", params, view);
                                                                        getPosts.execute(GloabalURI.baseURI + GloabalURI.searchProfessional);
                                                                    }
                                                                }


                                                            }

                                                        }
                        });


                    }
                                                    },
                                DELAY
                        );





        }
    }







}
