package com.socialide.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.SplashScreen;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.Config;
import com.socialide.Helper.CustomEditText;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DrawableClickListener;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.Validation;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProffressionalSignupFirst extends Fragment implements View.OnClickListener, DrawableClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_continue;

    ArrayList<NameValuePair> params;
    int toggle = 1;
    AVLoadingIndicatorView view;
    LoginActivity activity;
    MyEditTextView edt_firstname, edt_lastname, edt_contact, edt_email;
    CustomEditText edt_password;

    public ProffressionalSignupFirst() {
        // Required empty public constructor
        LoginActivity.back_handle = 0;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_proffesional_signup_first, container, false);
        btn_continue = (MyButton) root.findViewById(R.id.btn_signup);
        edt_firstname = (MyEditTextView) root.findViewById(R.id.edt_firstname);
        edt_lastname = (MyEditTextView) root.findViewById(R.id.edt_lastname);
        edt_contact = (MyEditTextView) root.findViewById(R.id.edt_contact);
        edt_email = (MyEditTextView) root.findViewById(R.id.edt_email);
        view = (AVLoadingIndicatorView) root.findViewById(R.id.avi);

        edt_password = (CustomEditText) root.findViewById(R.id.edt_password);
        edt_password.setDrawableClickListener(this);
        btn_continue.setOnClickListener(this);
        toggle = 1;

        activity = (LoginActivity) getActivity();
        return root;
    }

    public void gotoProffressionalSignupSecond() {
        LoginActivity.i = 1;
        activity.goToFragment(new ProffressionalSignupSecond());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                if (isValidate()) {
                    params = getParams();
                    AsyncRequest getPosts = new AsyncRequest(ProffressionalSignupFirst.this, getActivity(), "POST", params, view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.SIGNUP);
                }

                break;

        }
    }

    private boolean isValidate() {
        if (!Validation.isValidName("" + edt_firstname.getText().toString())) {
            // edt_firstname.setError("Please enter your first name");
            CustomSnackBar.toast(getActivity(), "Please enter your first name");

        }
//        else
//        {
//            edt_firstname.setError(null);
//
//        }
        else if (!Validation.isValidName("" + edt_lastname.getText().toString())) {
            //  edt_lastname.setError("Please enter your last name");
            CustomSnackBar.toast(getActivity(), "Please enter your last name");

        }
//        else
//        {
//            edt_lastname.setError(null);
//
//        }


        else if (!Validation.isValidContact("" + edt_contact.getText().toString())) {
            //  edt_contact.setError("Please enter mobile number");
            CustomSnackBar.toast(getActivity(), "Please enter mobile number");

        }
//        else {
//            edt_contact.setError(null);
//
//
//        }

        else if (!Validation.isValidName("" + edt_email.getText().toString())) {
            //   edt_email.setError("Please enter email");
            CustomSnackBar.toast(getActivity(), "Please enter email");


        } else if (!Validation.isValidEmail("" + edt_email.getText().toString())) {
            //    edt_email.setError("Please enter correct email");
            CustomSnackBar.toast(getActivity(), "Please enter correct email");

        }
//        else {
//            edt_email.setError(null);
//
//
//        }

        else if (!Validation.isValidPassword("" + edt_password.getText().toString())) {
            // edt_password.setError("Please enter password");

            CustomSnackBar.toast(getActivity(), "Please enter password");

        }
//        else
//        {
//            edt_password.setError(null);
//
//        }


        if (Validation.isValidEmail("" + edt_email.getText()) && Validation.isValidPassword("" + edt_password.getText()) && Validation.isValidName("" + edt_firstname.getText()) && Validation.isValidName("" + edt_lastname.getText()) && Validation.isValidContact("" + edt_contact.getText())) {

            return true;
        }
        return false;
    }


    @Override
    public void onClick(DrawablePosition target) {
        switch (target) {
            case RIGHT:
                //Do something here
                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "museosanscyrl.ttf");
                edt_password.setTypeface(tf);
                if (toggle % 2 == 0) {
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_password.setSelection(edt_password.getText().length());
                    edt_password.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.hide_password_green_icon, 0);

                } else {

                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    edt_password.setSelection(edt_password.getText().length());
                    edt_password.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.show_password_green_icon, 0);

                }
                toggle++;

                break;

            default:
                break;
        }

    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("first_name", "" + edt_firstname.getText().toString()));
        params.add(new BasicNameValuePair("last_name", "" + edt_lastname.getText().toString()));
        params.add(new BasicNameValuePair("password", "" + edt_password.getText().toString()));
        params.add(new BasicNameValuePair("email", "" + edt_email.getText().toString()));
        params.add(new BasicNameValuePair("mobile_number", "" + edt_contact.getText().toString()));
        params.add(new BasicNameValuePair("type", "Professional"));
        params.add(new BasicNameValuePair("device_type", "android"));

        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("regiiid", "aaa   " + regId);
        params.add(new BasicNameValuePair("device_token", "" + regId));


        // params.add(new BasicNameValuePair("device_token", ""+ SplashScreen.device_token));
        return params;
    }

    @Override
    public void asyncResponse(String response) {
        Log.e("Response", "" + response);
        try {
            JSONObject postObject = new JSONObject(response);


            if (postObject.getString("success").equals("true")) {
                JSONObject result = postObject.getJSONObject("result");

                Mypreferences.FIRST_NAME = result.getString("first_name");
                Mypreferences.LAST_NAME = result.getString("last_name");
                Mypreferences.MOBILE_NUMBER = result.getString("mobile_number");

                Mypreferences.User_id = result.getString("userid");
                //  Mypreferences.Join_Flow_Completed=result.getString("join_flow_completed");
                Mypreferences.setString(Mypreferences.Email, result.getString("email"), getActivity());
                //Mypreferences.setString(Mypreferences.Pass, result.getString("password"), getActivity());
                Mypreferences.Access_token = result.getString("access_token");

                //     Mypreferences.User_id=result.getString("userid");
                //       Mypreferences.Join_Flow_Completed=result.getString("join_flow_completed");
                Mypreferences.setString(Mypreferences.Email, edt_email.getText().toString(), getActivity());
                Mypreferences.setString(Mypreferences.Pass, edt_password.getText().toString(), getActivity());

                gotoProffressionalSignupSecond();
            } else {
                String s = "" + postObject.getString("message");
                CustomSnackBar.toast(getActivity(), s);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
