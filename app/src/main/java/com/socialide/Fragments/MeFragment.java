package com.socialide.Fragments;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.NotificationActivity;
import com.socialide.Adapters.InformationAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.socialide.Activities.MainActivity.badge_layout1;
import static com.socialide.Activities.MainActivity.relative_layout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeFragment extends Fragment {

    public MeFragment() {
        // Required empty public constructor
    }

    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView img_back;
    CircleImageView profilepic;
    RelativeLayout relative_bg;
    DonutProgress d;
    RecyclerView mRecyclerView;
    InformationAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    View rootView;
    MyTextView txt_name, txt_profession, txt_loaction;
    ImageButton backMyButton;
    public static int checkTabPosition = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.demo1, container, false);
        // initRecyclerView();
        DetailsActivity.type = "s";

        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        txt_name = (MyTextView) rootView.findViewById(R.id.txt_name);
        txt_profession = (MyTextView) rootView.findViewById(R.id.txt_profession);
        txt_loaction = (MyTextView) rootView.findViewById(R.id.txt_loaction);
        profilepic = (CircleImageView) rootView.findViewById(R.id.circleView);
        img_back = (ImageView) rootView.findViewById(R.id.img_back);
        relative_bg = (RelativeLayout) rootView.findViewById(R.id.relative_bg);
        d = (DonutProgress) rootView.findViewById(R.id.loading);

        MyActivityFragment.checkAsync = 0;
        MyActivityFragment.activity_list1.clear();
        MyPastEvents.checkAsynck = 0;
        MyPastEvents.activity_list1.clear();
        MyProfile.checkAsynck = 0;
        MainActivity.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.badge_layout1.performClick();
            }
        });

        MainActivity.badge_layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));

            }
        });

        MainActivity.relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.badge_layout1.performClick();
            }
        });

        MainActivity.tvNotificationCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.badge_layout1.performClick();
            }
        });


        MainActivity.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.badge_layout1.performClick();
            }
        });
        if (Mypreferences.NotificationCount == 0) {
            MainActivity.tvNotificationCount.setVisibility(View.GONE);
        } else {
            MainActivity.tvNotificationCount.setVisibility(View.VISIBLE);
            if (Mypreferences.NotificationCount <= 9) {
                MainActivity.tvNotificationCount.setTextSize(11);
                MainActivity.tvNotificationCount.setText(" " + Mypreferences.NotificationCount + " ");
            } else if (Mypreferences.NotificationCount <= 99) {
                MainActivity.tvNotificationCount.setTextSize(10);
                MainActivity.tvNotificationCount.setText("" + Mypreferences.NotificationCount + "");
            } else {
                MainActivity.tvNotificationCount.setTextSize(8);
                MainActivity.tvNotificationCount.setText("99+");
            }
        }


        com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, profilepic, d, getActivity(), img_back);
        // com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, profilepic, d, getActivity());

        if (Mypreferences.User_Type.equals("Organization")) {

            txt_name.setText("" + Mypreferences.ORGANISATION_NAME);
            txt_loaction.setText("" + Mypreferences.CITY);
            txt_profession.setVisibility(View.GONE);
        } else {

            txt_name.setText("" + Mypreferences.FIRST_NAME + " " + Mypreferences.LAST_NAME);

            if (!Mypreferences.CITY.equals("") && !Mypreferences.STATE.equals("") && !Mypreferences.COUNTRY.equals("")) {
                txt_loaction.setText("" + Mypreferences.CITY + ", " + Mypreferences.STATE);

            } else {
                txt_loaction.setVisibility(View.GONE);
            }
            if (!Mypreferences.Profession.equals("")) {
                txt_profession.setText("" + Mypreferences.Profession);
            } else {
                txt_profession.setVisibility(View.GONE);
            }

        }


        setupViewPager(viewPager);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.primary));
        viewPager.setCurrentItem(checkTabPosition);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                checkTabPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return rootView;
    }

//    private void initRecyclerView(){
//        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
//        // use a linear layout manager
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setHasFixedSize(true);
//
//        adapter = new InformationAdapter(getActivity(),1,this);
//        mRecyclerView.setAdapter(adapter);
//    }

    public void goToMyProfile() {
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new MyActivityFragment(), "Posts");
        adapter.addFragment(new MyPastEvents(), "Events");
        adapter.addFragment(new MyProfile(), "Profile");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
