package com.socialide.Fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.socialide.Adapters.ViewPagerAdapterEvent;
import com.socialide.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment1 extends Fragment {

    View rootView;
    public static int position;

    public EventFragment1() {
        // Required empty public constructor
    }
    ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_events, container, false);

          viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
















        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.viewpagertab);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_sheduledEvent)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_pendingEvent)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.tab_declinedEvent)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


         ViewPagerAdapterEvent adapter = new ViewPagerAdapterEvent
                (getChildFragmentManager(),3);



        viewPager.setAdapter(adapter);
    //    tabLayout.setupWithViewPager(viewPager);



        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {



//
//                if (position == 0) {
//
//
//                    goToFragment(new EventsScheduled());
//                }
//                if (position == 1) {
//
//                    goToFragment(new EventsPending());
//
//                }
//                if (position == 2) {
//
//
//
//
//                    goToFragment(new EventsDeclined());
//
//                }

                viewPager.setCurrentItem(tab.getPosition());
            }



            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return rootView;
    }


    public void goToFragment(Fragment fragment){


        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();


        //  fragmentTransaction.setCustomAnimations(R.anim.slide_to_left, R.anim.slide_from_right);

        fragmentTransaction.replace(R.id.id_holder, fragment, "Fragment");
        fragmentTransaction.commitAllowingStateLoss();

    }





}
