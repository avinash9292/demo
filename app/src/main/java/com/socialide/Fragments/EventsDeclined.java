package com.socialide.Fragments;


import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.OrgEventDetail;
import com.socialide.Activities.OtherUserDetailsActivity;
import com.socialide.Activities.PostDetails;
import com.socialide.Activities.ProfessionalDeclinedEventDetail;
import com.socialide.Adapters.DeclinedEventAdapter;
import com.socialide.Adapters.PendingEventAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.DividerItemDecoration;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.ImageLoader;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */


public class EventsDeclined extends Fragment implements AsyncRequest.OnAsyncRequestComplete {
    ArrayList<BeanForFetchEvents> activity_list = new ArrayList<>();
    static public ArrayList<BeanForFetchEvents> activity_list1 = new ArrayList<>();
    int pagecount = 10;
    int pageindex = 1;
    public static boolean isWebservice = true;
    AVLoadingIndicatorView view;
    private int lastVisibleItem, totalItemCount;
    public static boolean isLoading = true;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<NameValuePair> params;
    boolean check = false, type = false;
    LinearLayoutManager linearLayoutManager;
    View rootView;
    MyTextView txt_no_item;

    boolean isrefresh = false, isLoadmore = false, isFirsttime;

    RecyclerView mRecyclerView;
    PendingEventAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    static int checkAsynck = 0;

    public EventsDeclined() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        initRecyclerView();
        isFirsttime = true;
        MainActivity.img_setting.setVisibility(View.GONE);
        getGMTTime();
        //if (checkAsynck == 0) {
        isWebservice = false;
        params = getParams(pagecount, pageindex);
        AsyncRequest getPosts = new AsyncRequest(this, getActivity(), "POST", params, view);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.GET_DECLINE_EVENTS);
        //}

        return rootView;
    }


    private void getGMTTime() {
        Calendar current = Calendar.getInstance();
        //txtCurrentTime.setText("" + current.getTime());

        // miliSeconds = current.getTimeInMillis();

        TimeZone tzCurrent = current.getTimeZone();
        int offset = tzCurrent.getRawOffset();
        if (tzCurrent.inDaylightTime(new Date())) {
            offset = offset + tzCurrent.getDSTSavings();
        }
    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        // use a linear layout manager
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        txt_no_item = (MyTextView) rootView.findViewById(R.id.txt_no_item);
        view = (AVLoadingIndicatorView) getActivity().findViewById(R.id.avi);

      /*  mRecyclerView.addItemDecoration(
                new DividerItemDecoration(ContextCompat.getDrawable(getContext(),
                        R.drawable.item_decorator)));*/

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.getRecycledViewPool().clear();
        activity_list.clear();
        adapter = new PendingEventAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                mSwipeRefreshLayout.setRefreshing(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);

                        refreshItems();


                    }
                }, 10);

            }
        });


       /* mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {


            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));*/

        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("haint", "Load More");
                CheckInternet mCheckInternet = new CheckInternet();
                if (!mCheckInternet.isConnectingToInternet(getActivity())) {

                    isFirsttime = false;
                    //   CustomSnackBar.toast(context, "Please connect to internet!");

                } else {              //  adapter.notifyItemInserted(activity_list.size() - 1);

                    if (isrefresh && !isrefresh && !isFirsttime) {

                        pagecount = 10;
                        pageindex = 1;
                        onItemsLoadComplete();

                        isWebservice = false;
                    } else {
                        //  adapter.notifyItemInserted(activity_list.size() - 1);
                        pageindex = pageindex + 1;
                        type = true;
                        isWebservice = false;
                        isFirsttime = false;
                        isLoadmore = true;
                        params = getParams(pagecount, pageindex);
                        AsyncRequest getPosts = new AsyncRequest(EventsDeclined.this, getActivity(), "POST", params, view);
                        getPosts.execute(GloabalURI.baseURI + GloabalURI.GET_DECLINE_EVENTS);


                    }

                }
            }
        });

    }

    public interface ClickListener {

        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


   /* public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }


        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }


        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }*/


    private void refreshItems() {
        // Load items
        // ...
        CheckInternet mCheckInternet = new CheckInternet();
        if (!mCheckInternet.isConnectingToInternet(getActivity())) {

            //   CustomSnackBar.toast(context, "Please connect to internet!");

        } else {

            if (!isLoadmore && !isrefresh) {
                // Load complete
                pagecount = 10;
                pageindex = 1;
                onItemsLoadComplete();

               /* mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setHasFixedSize(true);*/


              /*  adapter = new PendingEventAdapter(getActivity());
                mRecyclerView.setAdapter(adapter);*/

                lastVisibleItem = -1;
                totalItemCount = 0;
                type = false;
                isrefresh = true;
                params = getParams(pagecount, pageindex);
                isWebservice = false;
                isFirsttime = false;
                AsyncRequest getPosts = new AsyncRequest(this, getActivity(), "POST", params, view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.GET_DECLINE_EVENTS);
                //  initRecyclerView();
            }
        }
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private ArrayList<NameValuePair> getParams(int pagecount, int pageindex) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
        Log.e("pagecount", "" + pagecount);
        Log.e("pageindex", "" + pageindex);
        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
        params.add(new BasicNameValuePair("pagecount", "" + pagecount));
        params.add(new BasicNameValuePair("pageindex", "" + pageindex));

        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);
            Log.e("post", postObject + "");
            checkAsynck = 1;
            MainActivity.img_setting.setVisibility(View.GONE);
            if (postObject.getString("success").equals("true")) {
                if (!isFirsttime) {
                    if (isLoadmore) {
                        isLoadmore = false;
                    }
                    if (pageindex == 1) {
                        Log.e("aaaaa", "xxxxxxxxx");


                        activity_list.clear();

                        activity_list = new ArrayList<>();
                        isrefresh = false;

//                        if (isrefresh) {
//                            activity_list = new ArrayList<>();
//                            isrefresh = false;
//                        }
                    }
                } else {
                    activity_list.clear();

                    activity_list = new ArrayList<>();
                    isFirsttime = false;
                }

                JSONArray result = postObject.getJSONArray("result");

                for (int i = 0; i < result.length(); i++) {
                    JSONObject result_object = result.getJSONObject(i);
                    JSONObject contact_details = result_object.getJSONObject("contact_details");
                    JSONObject decline_details = result_object.getJSONObject("decline_details");

                    String professionalId = "";

                    if (result_object.has("professional_id")) {
                        professionalId = result_object.getString("professional_id");
                    }


                    activity_list.add(new BeanForFetchEvents("" + result_object.getString("event_id"), "" + result_object.getString("profile_pic"), "" + result_object.getString("event_by"), "" + result_object.getString("event_title"),
                            "" + result_object.getString("event_description"), "" + result_object.getString("event_location"), "" + result_object.getString("event_time"), "" + result_object.getString("event_date"), "" + result_object.getString("contact_number"),
                            "" + result_object.getString("email"), "" + result_object.getString("message"), "" + contact_details.getString("person_name"), "" + contact_details.getString("person_designation"), "" + contact_details.getString("person_email")
                            , "" + contact_details.getString("person_mobile_number"), "" + decline_details.getString("decline_by"),
                            "" + decline_details.getString("reason"), "" + result_object.getString("userid"), "" + result_object.getString("type"), "" + result_object.getString("sharecontact"), professionalId));


                    // activity_list.remove(activity_list.size() - 1);

                    // adapter.notifyItemRemoved(activity_list.size());
                    if (activity_list.size() == 0) {
                        txt_no_item.setVisibility(View.VISIBLE);
                        txt_no_item.setText(getResources().getString(R.string.noDeclineEvent));
                    } else if (activity_list.size() >= 10) {
                        txt_no_item.setVisibility(View.GONE);

                        adapter.setLoaded();
                    } else {
                        txt_no_item.setVisibility(View.GONE);

                        check = true;
                    }


                    isWebservice = true;


                    if (ActivityFragment.isWebservice && EventsDeclined.isWebservice && EventsPending.isWebservice && EventsScheduled.isWebservice) {
                        view.setVisibility(View.GONE);

                        view.hide();
                    } else {
                        view.show();
                        view.setVisibility(View.VISIBLE);
                    }
                    mRecyclerView.getRecycledViewPool().clear();
                    adapter.notifyDataSetChanged();
                }
            } else {


                if (isLoadmore) {

                    isLoadmore = false;
                } else {
                    txt_no_item.setText(getResources().getString(R.string.noDeclineEvent));
                    activity_list.clear();
                    mRecyclerView.getRecycledViewPool().clear();
                    adapter.notifyDataSetChanged();
                    txt_no_item.setVisibility(View.VISIBLE);

                }


                isWebservice = true;


                if (ActivityFragment.isWebservice && EventsDeclined.isWebservice && EventsPending.isWebservice && EventsScheduled.isWebservice) {
                    view.setVisibility(View.GONE);

                    view.hide();
                } else {
                    view.show();
                    view.setVisibility(View.VISIBLE);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        if (EventFragment.isBack) {
            EventFragment.isBack = false;
            //adapter.notifyDataSetChanged();

            if (activity_list.size() == 0) {
                txt_no_item.setVisibility(View.VISIBLE);
                txt_no_item.setText(getResources().getString(R.string.noPendingEvent));
            } else {
                txt_no_item.setVisibility(View.GONE);

            }
        } else {

            if (checkAsynck == 1 && activity_list.size() == 0) {

                txt_no_item.setVisibility(View.VISIBLE);
                txt_no_item.setText(getResources().getString(R.string.noPendingEvent));

            } else if (activity_list.size() == 0) {
                txt_no_item.setVisibility(View.VISIBLE);
                txt_no_item.setText(getResources().getString(R.string.loading));
            } else {
                txt_no_item.setVisibility(View.GONE);
            }
        }
    }

    public class PendingEventAdapter extends RecyclerView.Adapter<PendingEventAdapter.EventsViewHolder> {


        private int visibleThreshold = 1;

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        private final int BUTTON_TYPE = 2;

        Context mContext;


        public PendingEventAdapter(Context context) {
            this.mContext = context;


            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();


                    Log.e("zzzzzzzzzzzzzz", "zzzzzzzzzzzzzzzzzz" + totalItemCount + lastVisibleItem);

                    if (!EventsDeclined.isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();


                        }
                        EventsDeclined.isLoading = true;
                    }
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            if (position == activity_list.size()) {
                return BUTTON_TYPE;
            } else if (activity_list.get(position) == null) {
                return VIEW_TYPE_LOADING;
            } else {
                return VIEW_TYPE_ITEM;
            }
        }

        @Override
        public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            EventsViewHolder viewHolder;
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_event_item, parent, false);

                viewHolder = new EventsViewHolder(view);
                viewHolder.iv_event_profile_pic = (RoundedImageView) view.findViewById(R.id.iv_event_profile_pic);
                viewHolder.tv_event_organisaton = (MyTextView) view.findViewById(R.id.tv_event_organisaton);
                viewHolder.tv_event_name = (MyTextView) view.findViewById(R.id.tv_event_name);
                viewHolder.tv_city = (MyTextView) view.findViewById(R.id.tv_city);
                viewHolder.tv_detail = (MyTextView) view.findViewById(R.id.tv_detail);
                //divider final
                //  viewHolder.viewLine = (View) view.findViewById(R.id.viewLine);

                viewHolder.txt_date = (MyTextView) view.findViewById(R.id.txt_date);
                viewHolder.txt_time = (MyTextView) view.findViewById(R.id.txt_time);
                viewHolder.rl_item = (RelativeLayout) view.findViewById(R.id.rl_item);
                viewHolder.d = (DonutProgress) view.findViewById(R.id.loading);


                return viewHolder;
            } else if (viewType == BUTTON_TYPE) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contain_view, parent, false);

                viewHolder = new EventsViewHolder(view);
                return viewHolder;
            }

            return null;
        }

        @Override
        public void onBindViewHolder(final EventsViewHolder holder, int position) {

            if (position == activity_list.size()) {
                return;
            }

            if (holder instanceof EventsViewHolder) {

                //divider final
               /* if (position == activity_list.size() - 1) {
                    holder.viewLine.setVisibility(View.GONE);
                }*/

                BeanForFetchEvents bean = activity_list.get(position);

                String datetime = TimeConveter.getdatetime(bean.getEvent_date(), bean.getEvent_time());

                String date = datetime.split("T")[0];
                String time = datetime.split("T")[1];
                holder.txt_time.setText("" + time);
                holder.txt_date.setText("" + date);
                holder.tv_event_name.setText(Html.fromHtml("<b>" + bean.getEvent_title() + "</b>"));
                holder.tv_event_organisaton.setText("" + bean.getEvent_by());


                holder.tv_city.setText("" + bean.getEvent_location());
                if (bean.getReason().equals("")) {
                    holder.tv_detail.setVisibility(View.GONE);

                } else {
                    holder.tv_detail.setVisibility(View.VISIBLE);
                    holder.tv_detail.setText("" + bean.getReason());
                }

                ImageLoader.image(bean.getProfile_image(), holder.iv_event_profile_pic, holder.d, getActivity());

                holder.iv_event_profile_pic.setTag(position);
                holder.rl_item.setTag(position);
                holder.iv_event_profile_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (Integer) v.getTag();
                        BeanForFetchEvents b = activity_list.get(pos);

                        Intent intent = new Intent(mContext, OtherUserDetailsActivity.class);

                        if (Mypreferences.User_Type.equalsIgnoreCase("Organization")) {
                            intent.putExtra("type", "Professional");
                            intent.putExtra("userId", b.getProfessional_id());

                        } else {
                            intent.putExtra("type", "Organization");
                            intent.putExtra("userId", b.getUserid());
                        }

                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);


                        /*intent.putExtra("userId", b.getUserid());
                        intent.putExtra("type", b.getType());

                        if (b.getUserid().equalsIgnoreCase(Mypreferences.User_id)) {

                            if (!b.getType().equalsIgnoreCase(Mypreferences.User_Type)) {
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                            }

                        } else {

                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);

                        }*/
                    }
                });
                holder.tv_event_organisaton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.iv_event_profile_pic.performClick();
                    }
                });

                holder.rl_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (Integer) view.getTag();
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_to_right, R.anim.slide_from_left);

                        if (Mypreferences.User_Type.equals("Organization")) {

                            Intent i = new Intent(getActivity(), OrgEventDetail.class);

                            EventFragment.position = pos;
                            DetailsActivity.type = "d";
                            activity_list1.clear();
                            activity_list1.addAll(activity_list);
                            startActivity(i, options.toBundle());

                        } else {

                            Intent i = new Intent(getActivity(), ProfessionalDeclinedEventDetail.class);
                            EventFragment.position = pos;
                            activity_list1.clear();
                            activity_list1.addAll(activity_list);
                            startActivity(i, options.toBundle());
                        }
                    }
                });


            } else {

            }
        }

        @Override
        public int getItemCount() {
            Log.v("akram", "activity sizeee = " + activity_list.size());
            if (activity_list.size() != 0) {
                return activity_list.size() + 1;
            } else {
                return 0;
            }
        }

        class EventsViewHolder extends RecyclerView.ViewHolder {


            public RoundedImageView iv_event_profile_pic, right_gray_arrow;
            public MyTextView tv_event_name, tv_event_organisaton, tv_city, tv_detail, txt_date, txt_time;
            DonutProgress d;
            public View viewLine;


            public RelativeLayout rl_item;


            public EventsViewHolder(View v) {
                super(v);
            }

        }

        OnLoadMoreListener mOnLoadMoreListener;

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        public void setLoaded() {
            EventsDeclined.isLoading = false;
        }
    }


    class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }


}
