package com.socialide.Fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopUsersFragment extends Fragment {

    View rootView;

    public TopUsersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_top_users, container, false);
        MainActivity.img_setting.setVisibility(View.GONE);
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getChildFragmentManager(), FragmentPagerItems.with(getActivity())
                .add("Top 10 weekly", Top10WeekUsers.class)
                .add("Top 10 overall", TopOverallUsers.class)
                .create());

        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        final SmartTabLayout viewPagerTab = (SmartTabLayout) rootView.findViewById(R.id.viewpagertab);

        viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                for (int i = 0; i <= 1; i++) {
                    if (i == position) {
                        viewPagerTab.getTabAt(i).setBackgroundColor(Color.parseColor("#3fb4b5"));
                    } else {
                        viewPagerTab.getTabAt(i).setBackgroundColor(Color.parseColor("#ffffff"));
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPagerTab.setViewPager(viewPager);

        return rootView;
    }


}
