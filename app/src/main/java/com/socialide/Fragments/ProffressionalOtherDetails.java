package com.socialide.Fragments;


import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;


import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.SettingActivity;
import com.socialide.Adapters.SpinnerDataAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyAutoCompleteTextView;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.SpinnerData;
import com.socialide.Helper.Validation;
import com.socialide.Helper.getDropdownData;
import com.socialide.Model.BeanForProfessionalCategory;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.socialide.Helper.Mypreferences.ABOUT_YOU;
import static com.socialide.Helper.Mypreferences.CATEGORY;
import static com.socialide.Helper.Mypreferences.CURRENT_COMPANY;
import static com.socialide.Helper.Mypreferences.EXPERIENCE;
import static com.socialide.Helper.Mypreferences.SUB_CATEGORY;

public class ProffressionalOtherDetails extends Fragment implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_continue;
    MyTextView btn_do_later;
    LoginActivity activity;
    ArrayList<NameValuePair> params;
    AVLoadingIndicatorView view;
    MyEditTextView edt_current_company;
    EditText edt_about_you;
    MyAutoCompleteTextView edt_category, edt_sub_category, edt_total_experience, edt_other;
    LinearLayout lin_other, lin_other1;
    MyTextView tvToolbarTitle;
    ArrayAdapter ad_cat, ad;
    ArrayList<BeanForProfessionalCategory> cat_saubcat_list = new ArrayList<>();
    ArrayList<String> cat_list = new ArrayList<>();
    ListView lvSubCat, lvCat, lvExperience;
    SpinnerDataAdapter spinnerExpAdapter, spinnerCatAdapter, spinnerSubCatAdapter;
    ArrayList<String> listExp, listCat, listSubCat;
    int expPosition = -1, catPosition = -1, subCatPosition = -1;

    public ProffressionalOtherDetails() {
        // Required empty public constructor
        LoginActivity.back_handle = 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_professional_other_details, container, false);
        btn_continue = (MyButton) root.findViewById(R.id.btn_continue);
        btn_do_later = (MyTextView) root.findViewById(R.id.btn_do_later);
        tvToolbarTitle = (MyTextView) root.findViewById(R.id.tvToolbarTitle);

        lvExperience = (ListView) root.findViewById(R.id.lvExperience);
        lvCat = (ListView) root.findViewById(R.id.lvCat);
        lvSubCat = (ListView) root.findViewById(R.id.lvSubCat);

        edt_about_you = (EditText) root.findViewById(R.id.edt_about_you);
        edt_total_experience = (MyAutoCompleteTextView) root.findViewById(R.id.edt_total_experience);
        edt_current_company = (MyEditTextView) root.findViewById(R.id.edt_current_company);
        edt_category = (MyAutoCompleteTextView) root.findViewById(R.id.edt_category);
        edt_other = (MyAutoCompleteTextView) root.findViewById(R.id.edt_other);
        lin_other = (LinearLayout) root.findViewById(R.id.lin_other);
        lin_other1 = (LinearLayout) root.findViewById(R.id.lin_other1);
        View view_do = (View) root.findViewById(R.id.view_do);


        edt_sub_category = (MyAutoCompleteTextView) root.findViewById(R.id.edt_sub_category);
        edt_category.setFocusable(false);
        edt_sub_category.setFocusable(false);
        view = (AVLoadingIndicatorView) root.findViewById(R.id.avi);
        cat_saubcat_list = getDropdownData.getStateList(getContext());


        for (int i = 0; i < cat_saubcat_list.size(); i++) {
            if (!("" + cat_saubcat_list.get(i).getCategory()).equalsIgnoreCase("Other")) {
                cat_list.add(cat_saubcat_list.get(i).getCategory());
            }
        }


        if (!Mypreferences.CATEGORY.equals("CATEGORY")) {
            catPosition = cat_list.indexOf(Mypreferences.CATEGORY);
        }
        spinnerCatAdapter = new SpinnerDataAdapter(cat_list, getActivity(), getContext(), catPosition);
        lvCat.setAdapter(spinnerCatAdapter);
        lvCat.setSelectionFromTop(catPosition, 0);

        lvCat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_category.setText(cat_list.get(position));
                catPosition = position;
                subCatPosition = -1;
                lvCat.setVisibility(View.GONE);
            }
        });

        edt_category.addTextChangedListener(new TextWatcher1(edt_category));
        edt_sub_category.addTextChangedListener(new TextWatcher1(edt_sub_category));

        if (SettingActivity.profilePageCheck == 1) {
            tvToolbarTitle.setText(getResources().getString(R.string.personalDetails));
            btn_do_later.setVisibility(View.INVISIBLE);
            view_do.setVisibility(View.INVISIBLE);
            btn_continue.setText(getResources().getString(R.string.btn_save));
        }

        if (!Mypreferences.CURRENT_COMPANY.equals("CURRENT_COMPANY")) {
            edt_current_company.setText("" + Mypreferences.CURRENT_COMPANY);
        }

        if (!Mypreferences.CATEGORY.equals("CATEGORY")) {
            edt_category.setText("" + Mypreferences.CATEGORY);
        }

        if (!Mypreferences.SUB_CATEGORY.equals("SUB_CATEGORY")) {
            edt_sub_category.setText("" + Mypreferences.SUB_CATEGORY);
        }

        if (!Mypreferences.ABOUT_YOU.equals("ABOUT_YOU")) {
            edt_about_you.setText("" + Mypreferences.ABOUT_YOU);
        }

        btn_continue.setOnClickListener(this);
        activity = (LoginActivity) getActivity();

        listExp = SpinnerData.getexpYearsList(getActivity());
        if (!Mypreferences.EXPERIENCE.equals("EXPERIENCE")) {
            edt_total_experience.setText(Mypreferences.EXPERIENCE);
            expPosition = listExp.indexOf(Mypreferences.EXPERIENCE);
        }
        spinnerExpAdapter = new SpinnerDataAdapter(listExp, getActivity(), getContext(), expPosition);
        lvExperience.setAdapter(spinnerExpAdapter);
        lvExperience.setSelectionFromTop(expPosition, 0);

        lvExperience.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_total_experience.setText(listExp.get(position));
                expPosition = position;

                lvExperience.setVisibility(View.GONE);
            }
        });

        edt_total_experience.setOnClickListener(this);

        edt_category.setOnClickListener(this);
        edt_sub_category.setOnClickListener(this);
        btn_do_later.setOnClickListener(this);


        edt_current_company.setOnClickListener(this);

        return root;
    }

    public void gotoProffressionalAvailability() {

        activity.goToFragment(new ProffressionalAvailability());

    }

    public void gotoNext() {
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_to_left, R.anim.slide_from_right);
        startActivity(new Intent(getActivity(), MainActivity.class), options.toBundle());

        activity.finish();
    }


    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("professional_id", "" + Mypreferences.User_id));

        params.add(new BasicNameValuePair("category", "" + edt_category.getText().toString()));


        if (("" + edt_sub_category.getText().toString()).equalsIgnoreCase("Other")) {
            params.add(new BasicNameValuePair("subcategory", "" + edt_other.getText().toString()));

        } else {
            params.add(new BasicNameValuePair("subcategory", "" + edt_sub_category.getText().toString()));
        }
        //   params.add(new BasicNameValuePair("subcategory", "" + edt_sub_category.getText().toString()));
        params.add(new BasicNameValuePair("current_company", "" + edt_current_company.getText().toString()));
        params.add(new BasicNameValuePair("experience", "" + edt_total_experience.getText().toString()));
        params.add(new BasicNameValuePair("about_me", "" + edt_about_you.getText().toString()));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));

        return params;
    }

    @Override
    public void asyncResponse(String response) {

        try {
            JSONObject postObject = new JSONObject(response);


            if (postObject.getString("success").equals("true")) {
                // JSONObject result= postObject.getJSONObject("result");

                ABOUT_YOU = "" + edt_about_you.getText().toString();
                EXPERIENCE = "" + edt_total_experience.getText().toString();

                CURRENT_COMPANY = "" + edt_current_company.getText().toString();
                CATEGORY = "" + edt_category.getText().toString();
                SUB_CATEGORY = "" + edt_sub_category.getText().toString();

                if (SettingActivity.profilePageCheck == 1) {
                    getActivity().onBackPressed();
                } else {
                    gotoProffressionalAvailability();
                }
            } else {
                String s = "" + postObject.getString("message");

                CustomSnackBar.toast(getActivity(), s);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                listVisibilityGone(lvExperience);
                listVisibilityGone(lvCat);
                listVisibilityGone(lvSubCat);
                if (isValidate()) {

                    params = getParams();
                    AsyncRequest getPosts = new AsyncRequest(ProffressionalOtherDetails.this, getActivity(), "POST", params, view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.UPDATE_PROFESSIONAL);

                }
                break;
            case R.id.btn_do_later:
                listVisibilityGone(lvExperience);
                listVisibilityGone(lvCat);
                listVisibilityGone(lvSubCat);
                gotoProffressionalAvailability();

                break;

            case R.id.edt_total_experience:

                listVisibilityGone(lvCat);
                listVisibilityGone(lvSubCat);
                if (lvExperience.getVisibility() == View.VISIBLE) {

                    lvExperience.setVisibility(View.GONE);
                    //
                } else {
                    spinnerExpAdapter = new SpinnerDataAdapter(listExp, getActivity(), getContext(), expPosition);
                    lvExperience.setAdapter(spinnerExpAdapter);
                    lvExperience.setSelectionFromTop(expPosition, 0);
                    lvExperience.setVisibility(View.VISIBLE);
                }


//                if((""+edt_total_experience.getText().toString()).length()>0){
//
//                    edt_total_experience.getAdapter().getFilter().filter(null);
//                }
                // edt_total_experience.showDropDown();
                break;
            case R.id.edt_category:

               /* if (("" + edt_category.getText().toString()).length() > 0) {

                    ad_cat.getFilter().filter(null);


                }*/

                listVisibilityGone(lvSubCat);

                if (lvCat.getVisibility() == View.VISIBLE) {

                    lvCat.setVisibility(View.GONE);
                    //
                } else {
                    spinnerCatAdapter = new SpinnerDataAdapter(cat_list, getActivity(), getContext(), catPosition);
                    lvCat.setAdapter(spinnerCatAdapter);
                    lvCat.setSelectionFromTop(catPosition, 0);
                    lvCat.setVisibility(View.VISIBLE);
                }

                // edt_category.showDropDown();
                break;
            case R.id.edt_sub_category:

                /*if (("" + edt_sub_category.getText().toString()).length() > 0) {

                    ad.getFilter().filter(null);
                }*/

                if (lvSubCat.getVisibility() == View.VISIBLE) {

                    lvSubCat.setVisibility(View.GONE);
                    //
                } else {
                    spinnerSubCatAdapter = new SpinnerDataAdapter(listSubCat, getActivity(), getContext(), subCatPosition);
                    lvSubCat.setAdapter(spinnerSubCatAdapter);

                    if (listSubCat.size() < 6) {
                        setListHeightOnAdapter(lvSubCat);

                    } else {
                        ViewGroup.LayoutParams params = lvSubCat.getLayoutParams();
                        params.height = 600;
                        lvSubCat.setLayoutParams(params);
                        lvSubCat.requestLayout();
                    }

                    lvSubCat.setSelectionFromTop(subCatPosition, 0);
                    lvSubCat.setVisibility(View.VISIBLE);
                }

                // edt_sub_category.showDropDown();
                break;

            case R.id.edt_current_company:

                listVisibilityGone(lvExperience);
                listVisibilityGone(lvCat);
                listVisibilityGone(lvSubCat);

                break;


        }
    }

    public void listVisibilityGone(ListView listView) {
        if (listView.getVisibility() == View.VISIBLE) {
            listView.setVisibility(View.GONE);
        }

    }


    private boolean isValidate() {

        if (!Validation.isValidName("" + edt_current_company.getText().toString())) {
            //    edt_current_company.setError("Please enter your current company");
            CustomSnackBar.toast(getActivity(), "Please enter your current company name");

        }
//        else
//        {
//            edt_current_company.setError(null);
//
//        }
        else if (!Validation.isValidName("" + edt_total_experience.getText().toString())) {
            //   edt_total_experience.setError("Please enter your total experience");
            CustomSnackBar.toast(getActivity(), "Please enter experience");

        }
//        else
//        {
//            edt_total_experience.setError(null);
//
//        }
        else if (!Validation.isValidName("" + edt_category.getText().toString())) {
            //  edt_category.setError("Please select category");
            CustomSnackBar.toast(getActivity(), "Please enter category");

        }
//        else {
//            edt_category.setError(null);
//
//
//        }
        else if (!Validation.isValidName("" + edt_sub_category.getText().toString())) {
            // edt_sub_category.setError("Please select sub category");
            CustomSnackBar.toast(getActivity(), "Please enter subcategory");
        } else if (edt_other.getVisibility() == View.VISIBLE) {

            if (!Validation.isValidName("" + edt_other.getText().toString())) {
                // edt_sub_category.setError("Please select sub category");
                CustomSnackBar.toast(getActivity(), "Please enter subcategory");
            }


            if (Validation.isValidName("" + edt_total_experience.getText()) && Validation.isValidName("" + edt_current_company.getText()) && Validation.isValidName("" + edt_category.getText()) && Validation.isValidName("" + edt_other.getText())) {

                return true;
            }

        }
//        else {
//            edt_sub_category.setError(null);
//
//
//        }


        else if (Validation.isValidName("" + edt_total_experience.getText()) && Validation.isValidName("" + edt_current_company.getText()) && Validation.isValidName("" + edt_category.getText()) && Validation.isValidName("" + edt_sub_category.getText())) {

            return true;
        }
        return false;
    }


    public class TextWatcher1 implements TextWatcher {
        AutoCompleteTextView e;

        public TextWatcher1(AutoCompleteTextView e) {
            this.e = e;

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (e.getId()) {
                case R.id.edt_category:
                    ArrayList<String> al = new ArrayList<>();
                    edt_sub_category.setText("");
                    if (("" + edt_category.getText().toString()).length() > 0) {

                        for (int i = 0; i < cat_list.size(); i++) {
                            if (("" + edt_category.getText().toString()).equals("" + cat_list.get(i))) {

                                al = cat_saubcat_list.get(i).getSub_category();
                            }
                        }
                    }
                    listSubCat = al;
                    if (!Mypreferences.SUB_CATEGORY.equals("SUB_CATEGORY")) {
                        subCatPosition = listSubCat.indexOf(Mypreferences.SUB_CATEGORY);
                    }
                    spinnerSubCatAdapter = new SpinnerDataAdapter(listSubCat, getActivity(), getContext(), subCatPosition);
                    lvSubCat.setAdapter(spinnerSubCatAdapter);

                    if (listSubCat.size() < 6) {
                        setListHeightOnAdapter(lvSubCat);

                    } else {
                        ViewGroup.LayoutParams params = lvSubCat.getLayoutParams();
                        params.height = 600;
                        lvSubCat.setLayoutParams(params);
                        lvSubCat.requestLayout();
                    }

                    lvSubCat.setSelectionFromTop(subCatPosition, 0);

                    lvSubCat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            edt_sub_category.setText(listSubCat.get(position));
                            subCatPosition = position;

                            lvSubCat.setVisibility(View.GONE);
                        }
                    });

                    // ad = new ArrayAdapter(getContext(), R.layout.spinner_item, R.id.txt, al);
                    //  edt_sub_category.setAdapter(ad);
                    break;
                case R.id.edt_sub_category:
                    if (("" + edt_sub_category.getText().toString()).equalsIgnoreCase("Other")) {

                        edt_other.setVisibility(View.VISIBLE);
                        lin_other.setVisibility(View.VISIBLE);
                        lin_other1.setVisibility(View.GONE);
                    } else {
                        edt_other.setVisibility(View.GONE);
                        lin_other.setVisibility(View.GONE);
                        lin_other1.setVisibility(View.INVISIBLE);


                    }
                    break;
            }

        }
    }

    public void setListHeightOnAdapter(ListView listview) {
        ListAdapter listadp = listview.getAdapter();
        if (listadp != null) {
            int totalHeight = 0;
            for (int i = 0; i < listadp.getCount(); i++) {
                View listItem = listadp.getView(i, null, listview);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listview.getLayoutParams();
            params.height = totalHeight + (listview.getDividerHeight() * (listadp.getCount() - 1));
            listview.setLayoutParams(params);
            listview.requestLayout();
        }
    }

}
