package com.socialide.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DeclinePopup;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProPendingEventDetails extends Fragment implements AsyncRequest.OnAsyncRequestComplete{
MyButton btn_decline,btn_accept;

    MyTextView tv_event_name,tv_organisation_name,tv_event_guests,txt_date,txt_time,txt_email,txt_contact,edt_message;
    CircleImageView iv_me_profile_pic;
    AVLoadingIndicatorView view;

    static RelativeLayout mRelativeLayout;
    static PopupWindow mPopupWindow;

    public ProPendingEventDetails() {
        // Required empty public constructor
    }

    public interface MyListener {
        public void onMyCancel();
    }

    private MyListener callback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callback= (MyListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View rootView= inflater.inflate(R.layout.fragment_event_details_pro_pending, container, false);

        btn_decline= (MyButton) rootView.findViewById(R.id.btn_decline);
        btn_accept= (MyButton) rootView.findViewById(R.id.btn_accept);
        mRelativeLayout= (RelativeLayout) rootView.findViewById(R.id.rl);

        iv_me_profile_pic= (CircleImageView) rootView.findViewById(R.id.iv_me_profile_pic);




        tv_event_name= (MyTextView) rootView.findViewById(R.id.tv_name);
        tv_organisation_name= (MyTextView) rootView.findViewById(R.id.tv_organisation_name);
        tv_event_guests= (MyTextView) rootView.findViewById(R.id.tv_event_guests);
        txt_date= (MyTextView) rootView.findViewById(R.id.txt_date);
        txt_time= (MyTextView) rootView.findViewById(R.id.txt_time);
        txt_email= (MyTextView) rootView.findViewById(R.id.txt_email);
        txt_contact= (MyTextView) rootView.findViewById(R.id.txt_contact);
        edt_message= (MyTextView) rootView.findViewById(R.id.edt_message);
        view= (AVLoadingIndicatorView) getActivity().findViewById(R.id.avi);



        final BeanForFetchEvents b= EventsPending.activity_list1.get(EventFragment.position);
        tv_event_name.setText(""+b.getEvent_title());

        DonutProgress d = (DonutProgress) rootView.findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, getActivity());
        tv_organisation_name.setText(""+b.getEvent_by());
        tv_event_guests.setText(""+b.getEvent_location());

        txt_contact.setText(""+b.getContact_number());
        txt_email.setText(""+b.getEmail());
        edt_message.setText(""+b.getMessage());


        String datetime= TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date =datetime.split("T")[0];
        String time =datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" +date);

        btn_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeclinePopup.popup(getActivity(), ProPendingEventDetails.this, mRelativeLayout,b.getEvent_id(),view);
            }
        });


        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<NameValuePair>params= new ArrayList<>();
                params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));


                AsyncRequest getPosts = new AsyncRequest(ProPendingEventDetails.this,getActivity(), "POST", params,view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.ACCEPT_AN_EVENT);




            }
        });


        return rootView;
    }


//    public static void popup(Context c)
//    {
//
//
//        View popupView = LayoutInflater.from(c).inflate(R.layout.decline_popup, null);
//        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//
//
//
//        mPopupWindow = new PopupWindow(
//                popupView,
//                RelativeLayout.LayoutParams.MATCH_PARENT,
//                RelativeLayout.LayoutParams.MATCH_PARENT
//        );
//
//
//        // Set an elevation value for popup window
//        // Call requires API level 21
//        if (Build.VERSION.SDK_INT >= 21) {
//            mPopupWindow.setElevation(5.0f);
//        }
//
//        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
//                android.graphics.Color.TRANSPARENT));
//
//        MyButton submit = (MyButton) popupView.findViewById(R.id.btn_submit);
//        ImageView cross = (ImageView) popupView.findViewById(R.id.img_cross);
//
//        // Set a click listener for the popup window close button
//        submit.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("NewApi")
//            @Override
//            public void onClick(View view) {
//                // Dismiss the popup window
//           mPopupWindow.dismiss();
//
//
//            }
//        });
//
//        cross.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("NewApi")
//            @Override
//            public void onClick(View view) {
//                // Dismiss the popup window
//                mPopupWindow.dismiss();
//
//
//            }
//        });
//
//
//
//        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
//    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if(postObject.getString("success").equals("true"))
            {
                CustomSnackBar.toast(getActivity(),postObject.getString("message"));
                EventsPending.activity_list1.remove(EventFragment.position);
                callback.onMyCancel();

            }
            else
            {
                CustomSnackBar.toast(getActivity(),postObject.getString("message"));
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    /*
     * Preparing the list data
     */




}
