package com.socialide.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.socialide.Adapters.InformationAdapter;
import com.socialide.R;

public class InfoFragment extends Fragment {


    public InfoFragment() {
        // Required empty public constructor
    }




    ImageButton backButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_info, container, false);
        initRecyclerView();
        return rootView;
    }
    View rootView;
    RecyclerView mRecyclerView;
    InformationAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private void initRecyclerView(){
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_info_recycler_view);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        adapter = new InformationAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);
    }


}
