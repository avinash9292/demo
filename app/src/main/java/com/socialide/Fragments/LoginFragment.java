package com.socialide.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;


import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.SplashScreen;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.Config;
import com.socialide.Helper.CustomEditText;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DrawableClickListener;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.Validation;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import static com.socialide.Activities.MainActivity.tabPosition;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, DrawableClickListener, AsyncRequest.OnAsyncRequestComplete, LocationListener {
    MyEditTextView edt_email;
    CustomEditText edt_pass;
    ImageView info;
    ArrayList<NameValuePair> params;
    int toggle = 1;
    AVLoadingIndicatorView view;
    MyButton submit, btn_signup;
    MyTextView tvForgotMypassword;
    View rootView;
    LoginActivity activity;
    Location location = null;

    public LoginFragment() {
        LoginActivity.i = 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_login, container, false);

        submit = (MyButton) rootView.findViewById(R.id.btn_submit);
        btn_signup = (MyButton) rootView.findViewById(R.id.btn_signup);
        tvForgotMypassword = (MyTextView) rootView.findViewById(R.id.tv_forgot_password);
        edt_email = (MyEditTextView) rootView.findViewById(R.id.et_email);
        edt_pass = (CustomEditText) rootView.findViewById(R.id.et_password);
        view = (AVLoadingIndicatorView) rootView.findViewById(R.id.avi);
        tvForgotMypassword.setOnClickListener(this);
        toggle = 1;
        edt_pass.setDrawableClickListener(this);
        submit.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        activity = (LoginActivity) getActivity();

        if (Build.VERSION.SDK_INT >= 23) {

            int permissionCheck = ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION);

            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            LocationManager lManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            boolean netEnabled = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (netEnabled) {
                lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, activity);

                location = lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {

                    Mypreferences.Lat = location.getLatitude();
                    Mypreferences.Lang = location.getLongitude();

                }
            }
        }

        Fragment f = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (f instanceof ActivityFragment) {
            Log.v("akram", "is fragment " + Mypreferences.Lat);
        } else {
            Log.v("akram", "is fragment not " + Mypreferences.Lat);
        }


        return rootView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_signup:
                geToUserTypeSelectionFragment();
                break;
            case R.id.btn_submit:
                if (isValidate()) {

                    getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    params = getParams();
                    AsyncRequest getPosts = new AsyncRequest(LoginFragment.this, getActivity(), "POST", params, view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.LOGIN);
                }
                break;
            case R.id.tv_forgot_password:
                goToForgotPasswordFragment();

                break;
            default:
                break;

        }
    }

    public void goToInfoFragment() {
        activity.goToFragment(new InfoFragment());
    }

    public void goToForgotPasswordFragment() {
        activity.goToFragment(new ForgotPasswordFragment());
    }

    public void geToUserTypeSelectionFragment() {
        activity.goToFragment(new UserTypeSelectionFragment());
    }

    public void submitLogin() {

        if (Mypreferences.Join_Flow_Completed.equals("0")) {
            Intent i = new Intent(getActivity(), LoginActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_to_right, R.anim.slide_from_left);
            startActivity(i, options.toBundle());
            activity.finish();

        } else {
            tabPosition = 0;
            Intent i = new Intent(getActivity(), MainActivity.class);
            ActivityOptions options =
                    ActivityOptions.makeCustomAnimation(getActivity(), R.anim.slide_to_right, R.anim.slide_from_left);
            startActivity(i, options.toBundle());
            activity.finish();
        }
        // activity.continueToSecondScreen();
    }


    private boolean isValidate() {
        if (!Validation.isValidName("" + edt_email.getText().toString())) {
            //   edt_email.setError("Please enter email");
            CustomSnackBar.toast(getActivity(), "Please enter email");

        } else if (!Validation.isValidEmail("" + edt_email.getText().toString())) {
            // edt_email.setError("Please enter a valid Email");

            CustomSnackBar.toast(getActivity(), "Please enter correct email");

        } else if (!Validation.isValidPassword("" + edt_pass.getText().toString())) {
            //     edt_password.setError("Please set a valid password");
            CustomSnackBar.toast(getActivity(), "Please enter password");

        }

        if (Validation.isValidEmail("" + edt_email.getText()) && Validation.isValidPassword("" + edt_pass.getText())) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(DrawablePosition target) {
        switch (target) {
            case RIGHT:
                //Do something here
                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "museosanscyrl.ttf");
                edt_pass.setTypeface(tf);
                if (toggle % 2 == 0) {
                    edt_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    edt_pass.setSelection(edt_pass.getText().length());
                    edt_pass.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.hide_password_green_icon, 0);

                } else {

                    edt_pass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    edt_pass.setSelection(edt_pass.getText().length());
                    edt_pass.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password_icon, 0, R.drawable.show_password_green_icon, 0);

                }
                toggle++;

                break;

            default:
                break;
        }

    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("password", "" + edt_pass.getText().toString()));
        params.add(new BasicNameValuePair("email", "" + edt_email.getText().toString()));

        params.add(new BasicNameValuePair("device_type", "android"));
        params.add(new BasicNameValuePair("time_offset", "" + TimeZone.getDefault().getOffset(System.currentTimeMillis())/60000));

        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("regiiid", "aaa   " + regId);
        params.add(new BasicNameValuePair("device_token", "" + regId));

        return params;
    }

    @Override
    public void asyncResponse(String response) {

        Log.v("akram", "response login = " + response);
        if (response != null) {
            try {
                JSONObject postObject = new JSONObject(response);


                if (postObject.getString("success").equals("true")) {

                    JSONObject result = postObject.getJSONObject("result");

                    Mypreferences.User_id = result.getString("userid");
                    if (postObject.getString("type").equals("Professional")) {

                        Mypreferences.User_Type = "Professional";


                        Mypreferences.FIRST_NAME = result.getString("first_name");
                        Mypreferences.LAST_NAME = result.getString("last_name");
                        Mypreferences.DOB = result.getString("dob");
                        Mypreferences.EXPERIENCE = result.getString("experience");
                        Mypreferences.MOBILE_NUMBER = result.getString("mobile_number");
                        Mypreferences.STATE = result.getString("state");
                        Mypreferences.CITY = result.getString("city");
                        Mypreferences.GENDER = result.getString("gender");
                        Mypreferences.COUNTRY = result.getString("country");
                        Mypreferences.CATEGORY = result.getString("category");
                        Mypreferences.CURRENT_COMPANY = result.getString("current_company");
                        Mypreferences.SUB_CATEGORY = result.getString("subcategory");
                        Mypreferences.ABOUT_YOU = result.getString("about_me");
                        Mypreferences.Access_token = result.getString("access_token");

                        Mypreferences.Maximum_Event_Request = result.getString("maximum_event_request");
                        Mypreferences.Join_Flow_Completed = result.getString("join_flow_completed");

                        Mypreferences.Social_Score = result.getString("social_score");
                        Mypreferences.Picture = result.getString("profile_pic");
                        Mypreferences.Availability = result.getString("availability");
                        Mypreferences.Profession = result.getString("profession");
                        Mypreferences.NotificationCount = result.getInt("notification_count");
                        Log.v("akram", "notification " + Mypreferences.NotificationCount);
                    }

                    if (postObject.getString("type").equals("Organization")) {
                        Mypreferences.User_Type = "Organization";


                        Mypreferences.ORGANISATION_NAME = result.getString("name");
                        Mypreferences.LANDLINE_NUMBER = result.getString("landline_number");
                        Mypreferences.FIRST_NAME = result.getString("contact_firstname");
                        Mypreferences.LAST_NAME = result.getString("contact_lastname");
                        Mypreferences.MOBILE_NUMBER = result.getString("contact_number");
                        Mypreferences.STATE = result.getString("state");
                        Mypreferences.CITY = result.getString("city");
                        Mypreferences.REGISTRATION_NUMBER = result.getString("u_dise_number");
                        Mypreferences.COUNTRY = result.getString("country");
                        Mypreferences.Address = result.getString("address");
                        Mypreferences.DESIGNATION = result.getString("contact_position");
                        Mypreferences.STABLISHMENT_YEAR = result.getString("establishment_year");
                        Mypreferences.ABOUT_YOU = result.getString("about");
                        Mypreferences.Contact_position = result.getString("contact_position");
                        Mypreferences.Access_token = result.getString("access_token");


                        Mypreferences.Join_Flow_Completed = result.getString("join_flow_completed");


                        Mypreferences.Picture = result.getString("profile_pic");
                        Mypreferences.NotificationCount = result.getInt("notification_count");

                    }


                    view.hide();

                    Mypreferences.setString(Mypreferences.Email, edt_email.getText().toString(), getActivity());
                    Mypreferences.setString(Mypreferences.Pass, edt_pass.getText().toString(), getActivity());

                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    submitLogin();
                } else {
                    view.hide();
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    CustomSnackBar.toast(getActivity(), postObject.getString("message"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            CustomSnackBar.toast(getActivity(),"something went wrong please try again");
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);


                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted
                    // insertDummyContact();

                    LocationManager lManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    boolean netEnabled = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    if (netEnabled) {

                        lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, activity);


                        location = lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {

                            Mypreferences.Lat = location.getLatitude();
                            Mypreferences.Lang = location.getLongitude();
                            Log.v("akram", "Lat = " + Mypreferences.Lat);
                            Log.v("akram", "Lang = " + Mypreferences.Lang);

                        }
                    }

                } else {
                    // Permission Denied
                    Toast.makeText(getContext(), "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }

            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
