package com.socialide.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.socialide.Activities.LoginActivity;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;

public class UserTypeSelectionFragment extends Fragment implements View.OnClickListener {
    String type = "";
    LoginActivity activity;
    MyButton btn_continue;
    CheckBox chk_pro, chk_org;

    LinearLayout lin_proffesional, lin_organization;

    public UserTypeSelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_user_type_selection, container, false);
        lin_organization = (LinearLayout) root.findViewById(R.id.lin_org);
        lin_proffesional = (LinearLayout) root.findViewById(R.id.lin_pro);

        chk_pro = (CheckBox) root.findViewById(R.id.chk_pro);
        chk_org = (CheckBox) root.findViewById(R.id.chk_org);
        btn_continue = (MyButton) root.findViewById(R.id.btn_continue);
        activity = (LoginActivity) getActivity();
        lin_proffesional.setOnClickListener(this);
        lin_organization.setOnClickListener(this);

        chk_org.setOnClickListener(this);
        chk_pro.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
        return root;


    }


    public void gotoSignupFirst() {

        if (type.contains("p")) {
            activity.goToFragment(new ProffressionalSignupFirst());

            Log.e("type", "" + type);

        } else {
            activity.goToFragment(new OrganizationSignupFirst());
            Log.e("type1", "" + type);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_org:
                type = "org";
                Mypreferences.setString(Mypreferences.User_Type, "o", getActivity());
                Mypreferences.User_Type = "Organization";
                chk_pro.setChecked(false);
                chk_org.setChecked(true);
                break;
            case R.id.lin_pro:
                type = "pro";
                Mypreferences.setString(Mypreferences.User_Type, "p", getActivity());
                Mypreferences.User_Type = "Professional";
                chk_pro.setChecked(true);
                chk_org.setChecked(false);
                break;
            case R.id.btn_continue:

                if (!type.equals("")) {
                    gotoSignupFirst();
                } else {

                    Toast.makeText(getActivity(), "Please choose your type", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.chk_org:
                type = "org";
                Mypreferences.setString(Mypreferences.User_Type, "o", getActivity());
                Mypreferences.User_Type = "Organization";
                chk_pro.setChecked(false);
                chk_org.setChecked(true);
                break;
            case R.id.chk_pro:
                type = "pro";
                Mypreferences.setString(Mypreferences.User_Type, "p", getActivity());
                Mypreferences.User_Type = "Professional";
                chk_pro.setChecked(true);
                chk_org.setChecked(false);
                break;
            default:
                break;

        }
    }

}
