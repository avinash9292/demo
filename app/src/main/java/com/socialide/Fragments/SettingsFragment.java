package com.socialide.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.socialide.Adapters.InformationAdapter;
import com.socialide.Adapters.SettingsAdapter;
import com.socialide.Adapters.TopUsersAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    RelativeLayout rl1, rl2;
    ArrayList<NameValuePair> params;
    AVLoadingIndicatorView view;
    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setting, container, false);
//        rl1= (RelativeLayout) rootView.findViewById(R.id.rl1);
//        rl2= (RelativeLayout) rootView.findViewById(R.id.rl2);
//
//        rl1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//
//        rl2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//        initRecyclerView();



        return rootView;
    }




    RecyclerView mRecyclerView;
    InformationAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    View rootView;

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        adapter = new InformationAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);
    }



}
