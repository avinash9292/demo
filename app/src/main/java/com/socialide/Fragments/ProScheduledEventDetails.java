package com.socialide.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DeclinePopup;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProScheduledEventDetails extends Fragment implements View.OnClickListener,AsyncRequest.OnAsyncRequestComplete{
    MyButton btn_decline;
    static RelativeLayout mRelativeLayout;
    static PopupWindow mPopupWindow;
    MyTextView txt_email1,tv_event_name,tv_organisation_name,tv_event_guests,txt_date,txt_time,txt_email,txt_landline_number,edt_message,txt_person_name,txt_designation,txt_contact;
    CircleImageView iv_me_profile_pic;
AVLoadingIndicatorView view;
    BeanForFetchEvents b;

    public ProScheduledEventDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_event_details_pro_scheduled, container, false);

        btn_decline= (MyButton) rootView.findViewById(R.id.btn_remove);
        txt_email1= (MyTextView) rootView.findViewById(R.id.txt_email1);
        txt_contact= (MyTextView) rootView.findViewById(R.id.txt_contact);
        mRelativeLayout= (RelativeLayout) rootView.findViewById(R.id.rl);

        iv_me_profile_pic= (CircleImageView) rootView.findViewById(R.id.iv_me_profile_pic);


        view= (AVLoadingIndicatorView) getActivity().findViewById(R.id.avi);


        tv_event_name= (MyTextView) rootView.findViewById(R.id.tv_name);
        tv_organisation_name= (MyTextView) rootView.findViewById(R.id.tv_organisation_name);
        tv_event_guests= (MyTextView) rootView.findViewById(R.id.tv_event_guests);
        txt_date= (MyTextView) rootView.findViewById(R.id.txt_date);
        txt_time= (MyTextView) rootView.findViewById(R.id.txt_time);
        txt_email= (MyTextView) rootView.findViewById(R.id.txt_email);
        txt_contact= (MyTextView) rootView.findViewById(R.id.txt_contact);
        edt_message= (MyTextView) rootView.findViewById(R.id.edt_message);
        txt_landline_number= (MyTextView) rootView.findViewById(R.id.txt_landline_number);
        txt_person_name= (MyTextView) rootView.findViewById(R.id.txt_person_name);
        txt_designation= (MyTextView) rootView.findViewById(R.id.txt_designation);




         b= EventsScheduled.activity_list1.get(EventFragment.position);


        DisplayImageOptions  options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profilepic)
                .showImageOnFail(R.drawable.ic_cross)
                .resetViewBeforeLoading(true)
//.cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        DonutProgress  d = (DonutProgress) rootView.findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, getActivity());

        tv_event_name.setText(""+b.getEvent_title());
        tv_organisation_name.setText(""+b.getEvent_by());
        tv_event_guests.setText(""+b.getEvent_location());

        String datetime= TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date =datetime.split("T")[0];
        String time =datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" +date);
        txt_landline_number.setText(""+b.getContact_number());
        txt_email.setText(""+b.getEmail());
        edt_message.setText(""+b.getMessage());
        txt_contact.setText(""+b.getPerson_mobile_number());
        txt_person_name.setText(""+b.getPerson_name());
        txt_designation.setText(""+b.getPerson_designation());
        txt_email1.setText(""+b.getPerson_email());



        btn_decline.setOnClickListener(this);
        txt_email1.setOnClickListener(this);
        txt_contact.setOnClickListener(this);

         return rootView;
    }
    public static void popup(Context c)
    {


        View popupView = LayoutInflater.from(c).inflate(R.layout.decline_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);




        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        MyButton submit = (MyButton) popupView.findViewById(R.id.btn_submit);
        ImageView cross = (ImageView) popupView.findViewById(R.id.img_cross);

        // Set a click listener for the popup window close button
        submit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();


            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();


            }
        });



        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
    }



    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if(postObject.getString("success").equals("true"))
            {
                CustomSnackBar.toast(getActivity(), postObject.getString("message"));
                getActivity().onBackPressed();
            }
            else
            {
                CustomSnackBar.toast(getActivity(),postObject.getString("message"));
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_email1:
               email();
                break;
            case R.id.txt_contact:
              call();
                break;

            case R.id.btn_remove:
           //     popup(getActivity());
                DeclinePopup.popup(getActivity(), ProScheduledEventDetails.this, mRelativeLayout, b.getEvent_id(),view);

                break;
        }
    }

    void email()
    {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{""+txt_email1.getText()});
        i.putExtra(Intent.EXTRA_SUBJECT, "Socialide");
        i.putExtra(Intent.EXTRA_TEXT, "Sharing content.....");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    void call()
    {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+txt_contact.getText().toString()));

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }
}
