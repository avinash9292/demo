package com.socialide.Fragments;


import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.ImageView;
import android.widget.Toast;


import com.androidadvance.topsnackbar.TSnackbar;
import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.Validation;
import com.socialide.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ForgotPasswordFragment extends Fragment implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyEditTextView edt_email;
    ArrayList<NameValuePair> params;
    MyButton submit;
    View rootView;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        //  info = (ImageView)rootView.findViewById(R.id.btn_info);
        submit = (MyButton) rootView.findViewById(R.id.btn_submit);
        edt_email = (MyEditTextView) rootView.findViewById(R.id.et_email);
        submit.setOnClickListener(this);
        return rootView;
    }

    private boolean isValidate() {

        if (!Validation.isValidEmail("" + edt_email.getText().toString())) {

            CustomSnackBar.toast(getActivity(), "Please enter valid email");

            //TSnackbar.make(getActivity().findViewById(android.R.id.content), "Please enter email", TSnackbar.LENGTH_LONG).setTextColor(Color.WHITE).show();
        } else {
            edt_email.setError(null);
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_submit:
                if (isValidate()) {
                    params = getParams();
                    AsyncRequest getPosts = new AsyncRequest(this, getActivity(), "POST", params);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.FORGOT_PASSWORD);
                }

        }
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("email", "" + edt_email.getText().toString()));

        return params;
    }

    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.getString("success").equals("true")) {
                CustomSnackBar.toast(getActivity(), postObject.getString("message"));
                Toast.makeText(getActivity(), "" + postObject.getString("message"), Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            } else {
                CustomSnackBar.toast(getActivity(), postObject.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
