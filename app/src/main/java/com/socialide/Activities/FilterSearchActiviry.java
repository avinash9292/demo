package com.socialide.Activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.socialide.Adapters.RecyclerViewAdapterwithAlphabets;
import com.socialide.Adapters.RecyclerViewAdapterwithAlphabetsMulpleSelection;
import com.socialide.Fragments.SearchProfessional1;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.DatabaseHandlerOld;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Helper.getDropdownData;
import com.socialide.Model.BeanForCity;
import com.socialide.Model.BeanForCityList;
import com.socialide.Model.BeanForCountry;
import com.socialide.Model.BeanForProfessionalCategory;
import com.socialide.Model.BeanForState;
import com.socialide.R;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import java.util.ArrayList;
import java.util.List;

public class FilterSearchActiviry extends AppCompatActivity implements View.OnClickListener {
    CheckBox chk_availability, chk_verified;
    MyButton txt_country, txt_category, txt_exp, txt_score, btn_clear_all, btn_apply;
    MyTextView txt_selected_country, txt_state, txt_city, txt_selected_category, txt_subcategory, txt_scores, txt_experirnce;
    SeekBar seekbar_exp, seekbar_score;
    View line_view;
    RelativeLayout rl_names, rl_categories, rl_score, rl_exp;
    int city_count = 0, sub_cat_count = 0;
    String country_filter_text = "", city_filer_text = "", state_filer_text = "", cat_filer_text = "", subcat_filter_text = "";
    RecyclerView mRecyclerView_country, mRecyclerView_state, mRecyclerView_city, recycler_view_for_cat, recycler_view_for_subcat;
    ArrayList<String> al_subcat = new ArrayList<>();
    RecyclerViewFastScroller fastScroller, fast_scroller_for_cat, fast_scroller_for_subcat, fast_scroller_state, fast_scroller_city;
    private List<AlphabetItem> mAlphabetItems;
    DatabaseHandlerOld db;
    ArrayList<BeanForCountry> al_country = new ArrayList<>();
    ArrayList<String> al_country_names = new ArrayList<>();
    ArrayList<BeanForState> al_state = new ArrayList<>();
    ArrayList<BeanForCityList> al_city = new ArrayList<>();
    ArrayList<BeanForCityList> al_sub_cat_bean = new ArrayList<>();
    ArrayList<String> al_state_names = new ArrayList<>();
    ArrayList<String> al_city_names = new ArrayList<>();
    ArrayAdapter ad_cat, ad;
    ArrayList<BeanForProfessionalCategory> cat_saubcat_list = new ArrayList<>();
    ArrayList<String> cat_list = new ArrayList<>();
    RecyclerViewAdapterwithAlphabetsMulpleSelection ad_city, ad_sub_category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_layout);
        Log.v("akram", "filter activty");
        SearchProfessional1.is_filter_changed = false;
        chk_availability = (CheckBox) findViewById(R.id.chk_availability);
        chk_verified = (CheckBox) findViewById(R.id.chk_verified);
        txt_country = (MyButton) findViewById(R.id.txt_country);
        txt_category = (MyButton) findViewById(R.id.txt_category);
        txt_city = (MyTextView) findViewById(R.id.txt_city);
        txt_exp = (MyButton) findViewById(R.id.txt_exp);
        txt_score = (MyButton) findViewById(R.id.txt_score);

        btn_apply = (MyButton) findViewById(R.id.btn_apply);
        btn_clear_all = (MyButton) findViewById(R.id.btn_clear_all);
        rl_categories = (RelativeLayout) findViewById(R.id.rl_category);
        rl_names = (RelativeLayout) findViewById(R.id.rl_country);
        seekbar_exp = (SeekBar) findViewById(R.id.seekbar_exp);
        seekbar_score = (SeekBar) findViewById(R.id.seekbar_score);


        rl_score = (RelativeLayout) findViewById(R.id.rl_score);
        rl_exp = (RelativeLayout) findViewById(R.id.rl_exp);
        txt_selected_country = (MyTextView) findViewById(R.id.txt_selected_country);
        txt_state = (MyTextView) findViewById(R.id.txt_state);
        txt_selected_category = (MyTextView) findViewById(R.id.txt_selected_category);
        txt_subcategory = (MyTextView) findViewById(R.id.txt_subcategory);
        txt_experirnce = (MyTextView) findViewById(R.id.txt_experirnce);
        txt_scores = (MyTextView) findViewById(R.id.txt_scores);
        line_view = (View) findViewById(R.id.line_view);
        mRecyclerView_country = (RecyclerView) findViewById(R.id.recycler_view_for_country);
        mRecyclerView_state = (RecyclerView) findViewById(R.id.recycler_view_for_state);
        mRecyclerView_city = (RecyclerView) findViewById(R.id.recycler_view_for_city);
        recycler_view_for_cat = (RecyclerView) findViewById(R.id.recycler_view_for_cat);
        recycler_view_for_subcat = (RecyclerView) findViewById(R.id.recycler_view_for_subcat);
        fastScroller = (RecyclerViewFastScroller) findViewById(R.id.fast_scroller);
        fast_scroller_for_cat = (RecyclerViewFastScroller) findViewById(R.id.fast_scroller_for_cat);
        fast_scroller_for_subcat = (RecyclerViewFastScroller) findViewById(R.id.fast_scroller_for_subcat);
        fast_scroller_state = (RecyclerViewFastScroller) findViewById(R.id.fast_scroller_state);
        fast_scroller_city = (RecyclerViewFastScroller) findViewById(R.id.fast_scroller_city);
        txt_selected_country.setVisibility(View.GONE);
        txt_exp.setOnClickListener(this);
        txt_country.setOnClickListener(this);
        txt_category.setOnClickListener(this);
        txt_score.setOnClickListener(this);
        txt_selected_category.setOnClickListener(this);
        txt_selected_country.setOnClickListener(this);
        txt_state.setOnClickListener(this);
        btn_apply.setOnClickListener(this);
        btn_clear_all.setOnClickListener(this);
        chk_availability.setOnClickListener(this);
        chk_verified.setOnClickListener(this);
        initialiseDataForCountry();
        initialiseUIForCountry();
        initialiseDataForCat();
        initialiseUIForCat();
        txt_country.setBackgroundResource(android.R.color.transparent);


        seekbar_exp.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                seekBar.setProgress(i);
                txt_experirnce.setText(Html.fromHtml(getResources().getString(R.string.years)+" - <b>" + i + "+</b>"));
                SearchProfessional1.experience = "" + i;
                //   SearchProfessional1.is_filter_changed=true;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_score.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                seekBar.setProgress(i);
                txt_scores.setText("" + i + "+");
                SearchProfessional1.social_score = "" + i;
                //  SearchProfessional1.is_filter_changed=true;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mRecyclerView_country.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView_country, new ClickListener() {
            @Override
            public void onClick(View view, int position) {


                txt_selected_country.setText("" + al_country_names.get(position));
                SearchProfessional1.country = al_country_names.get(position);
                txt_selected_country.setVisibility(View.VISIBLE);
                SearchProfessional1.location = al_country_names.get(position);
                initialiseDataForState(position);
                initialiseUIForState();
                mRecyclerView_country.setVisibility(View.GONE);
                mRecyclerView_city.setVisibility(View.GONE);
                mRecyclerView_state.setVisibility(View.VISIBLE);
                fastScroller.setVisibility(View.GONE);
                fast_scroller_city.setVisibility(View.GONE);
                fast_scroller_state.setVisibility(View.VISIBLE);
                SearchProfessional1.is_filter_changed = true;
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));


        mRecyclerView_state.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView_state, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                city_count = 0;
                city_filer_text = "";
                SearchProfessional1.location = al_state_names.get(position);
                SearchProfessional1.state = al_state_names.get(position);
                txt_state.setText("" + al_state_names.get(position));
                txt_state.setVisibility(View.VISIBLE);
                initialiseDataForCity(position);
                initialiseUIForCIty();
                mRecyclerView_country.setVisibility(View.GONE);
                mRecyclerView_city.setVisibility(View.VISIBLE);
                mRecyclerView_state.setVisibility(View.GONE);
                fastScroller.setVisibility(View.GONE);
                fast_scroller_city.setVisibility(View.VISIBLE);
                fast_scroller_state.setVisibility(View.GONE);
                SearchProfessional1.is_filter_changed = true;

            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));


        mRecyclerView_city.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView_city, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BeanForCityList bean = null;
                if (al_city.get(position).isCheck()) {
                    bean = new BeanForCityList(al_city.get(position).getName(), false);
                } else {
                    bean = new BeanForCityList(al_city.get(position).getName(), true);

                }

                al_city.remove(position);
                al_city.add(position, bean);
                ad_city.notifyDataSetChanged();
                city_count = 0;
                city_filer_text = "";
                for (int i = 0; i < al_city.size(); i++) {
                    if (al_city.get(i).isCheck()) {
                        city_count = city_count + 1;
                        city_filer_text = city_filer_text + al_city.get(i).getName() + ",";
                    }
                }
                if (city_count != 0) {
                    txt_city.setVisibility(View.VISIBLE);
                    txt_city.setText(getResources().getString(R.string.hint_city)+" [ " + city_count + " ]");
                } else {
                    txt_city.setVisibility(View.GONE);
                }

                Log.e(city_filer_text + "  pre", city_filer_text + " post");
                if (city_filer_text.contains(",")) {
                    if (city_filer_text.charAt(city_filer_text.length() - 1) == ',') {

                        city_filer_text = city_filer_text.substring(0, city_filer_text.length() - 1);
                    }
                }
                Log.e(city_filer_text + "  pre", city_filer_text + " post");
                SearchProfessional1.location = city_filer_text;
                SearchProfessional1.city = city_filer_text;
                SearchProfessional1.is_filter_changed = true;
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));


        recycler_view_for_cat.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recycler_view_for_cat, new ClickListener() {
            @Override
            public void onClick(View view, int position) {


                txt_selected_category.setText("" + cat_list.get(position));
                txt_selected_category.setVisibility(View.VISIBLE);
                SearchProfessional1.category = cat_list.get(position);
                initialiseDataForsubCat(position);
                initialiseUIForsubCat();
                recycler_view_for_cat.setVisibility(View.GONE);
                recycler_view_for_subcat.setVisibility(View.VISIBLE);
                fast_scroller_for_cat.setVisibility(View.GONE);
                fast_scroller_for_subcat.setVisibility(View.VISIBLE);
                SearchProfessional1.is_filter_changed = true;
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));


        recycler_view_for_subcat.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recycler_view_for_subcat, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BeanForCityList bean = null;
                if (al_sub_cat_bean.get(position).isCheck()) {
                    bean = new BeanForCityList(al_sub_cat_bean.get(position).getName(), false);
                } else {
                    bean = new BeanForCityList(al_sub_cat_bean.get(position).getName(), true);

                }

                al_sub_cat_bean.remove(position);
                al_sub_cat_bean.add(position, bean);
                ad_sub_category.notifyDataSetChanged();
                sub_cat_count = 0;
                subcat_filter_text = "";
                for (int i = 0; i < al_sub_cat_bean.size(); i++) {
                    if (al_sub_cat_bean.get(i).isCheck()) {
                        sub_cat_count = sub_cat_count + 1;
                        subcat_filter_text = subcat_filter_text + al_sub_cat_bean.get(i).getName() + ",";
                    }
                }
                if (sub_cat_count != 0) {
                    txt_subcategory.setVisibility(View.VISIBLE);
                    txt_subcategory.setText(getResources().getString(R.string.hint_sub_category)+" [ " + sub_cat_count + " ]");
                } else {
                    txt_subcategory.setVisibility(View.GONE);

                }


                if (subcat_filter_text.contains(",")) {
                    if (subcat_filter_text.charAt(subcat_filter_text.length() - 1) == ',') {

                        subcat_filter_text = subcat_filter_text.substring(0, subcat_filter_text.length() - 1);
                    }
                }
                Log.e(subcat_filter_text + "  pre", subcat_filter_text + " post");
                SearchProfessional1.subcategory = subcat_filter_text;
                Log.e(subcat_filter_text + "  pre", subcat_filter_text + " post");
                SearchProfessional1.is_filter_changed = true;
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

        intializePreviousData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SearchProfessional1.checkBackFromFilter = false;
        overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_country:
                txt_country.setBackgroundResource(android.R.color.transparent);
                txt_category.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_exp.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_score.setBackgroundResource(R.drawable.no_top_order_rounded);
                line_view.setVisibility(View.GONE);


//                mRecyclerView_country.setVisibility(View.VISIBLE);
//                mRecyclerView_city.setVisibility(View.GONE);
//                mRecyclerView_state.setVisibility(View.GONE);
//                country_filter_text="";
//                state_filer_text="";
//                city_filer_text="";
//                txt_selected_country.setVisibility(View.GONE);
//                txt_city.setVisibility(View.GONE);
//                txt_state.setVisibility(View.GONE);

                rl_categories.setVisibility(View.GONE);
                rl_names.setVisibility(View.VISIBLE);
                rl_exp.setVisibility(View.GONE);
                rl_score.setVisibility(View.GONE);


                break;
            case R.id.txt_category:
                txt_country.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_category.setBackgroundResource(android.R.color.transparent);
                txt_exp.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_score.setBackgroundResource(R.drawable.no_top_order_rounded);
                line_view.setVisibility(View.GONE);
                rl_categories.setVisibility(View.VISIBLE);
                rl_names.setVisibility(View.GONE);
                rl_exp.setVisibility(View.GONE);
                rl_score.setVisibility(View.GONE);
                cat_filer_text = "";
                subcat_filter_text = "";

                break;
            case R.id.txt_exp:
                txt_country.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_category.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_exp.setBackgroundResource(android.R.color.transparent);
                txt_score.setBackgroundResource(R.drawable.no_top_order_rounded);
                line_view.setVisibility(View.GONE);
                rl_categories.setVisibility(View.GONE);
                rl_names.setVisibility(View.GONE);
                rl_exp.setVisibility(View.VISIBLE);
                rl_score.setVisibility(View.GONE);

                break;
            case R.id.txt_score:
                txt_country.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_category.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_exp.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_score.setBackgroundResource(android.R.color.transparent);
                line_view.setVisibility(View.VISIBLE);
                rl_categories.setVisibility(View.GONE);
                rl_names.setVisibility(View.GONE);
                rl_exp.setVisibility(View.GONE);
                rl_score.setVisibility(View.VISIBLE);
                break;


            case R.id.txt_selected_country:
                mRecyclerView_country.setVisibility(View.VISIBLE);
                mRecyclerView_city.setVisibility(View.GONE);
                mRecyclerView_state.setVisibility(View.GONE);
                country_filter_text = "";
                state_filer_text = "";
                city_filer_text = "";
                txt_selected_country.setVisibility(View.GONE);
                txt_city.setVisibility(View.GONE);
                txt_state.setVisibility(View.GONE);
                fastScroller.setVisibility(View.VISIBLE);
                fast_scroller_city.setVisibility(View.GONE);
                fast_scroller_state.setVisibility(View.GONE);

                break;

            case R.id.txt_state:

                mRecyclerView_city.setVisibility(View.GONE);
                mRecyclerView_state.setVisibility(View.VISIBLE);

                state_filer_text = "";
                city_filer_text = "";
                city_count = 0;
                txt_city.setVisibility(View.GONE);
                txt_state.setVisibility(View.GONE);
                fastScroller.setVisibility(View.GONE);
                fast_scroller_city.setVisibility(View.GONE);
                fast_scroller_state.setVisibility(View.VISIBLE);

                break;


            case R.id.txt_selected_category:
                recycler_view_for_cat.setVisibility(View.VISIBLE);
                recycler_view_for_subcat.setVisibility(View.GONE);
                subcat_filter_text = "";
                txt_subcategory.setVisibility(View.GONE);
                txt_selected_category.setVisibility(View.GONE);
                fast_scroller_for_cat.setVisibility(View.VISIBLE);
                fast_scroller_for_subcat.setVisibility(View.GONE);
                break;


            case R.id.btn_apply:

                SearchProfessional1.checkBackFromFilter = false;

                btn_clear_all.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));
                btn_apply.setBackground(new ColorDrawable(Color.parseColor("#3fb4b5")));

                btn_clear_all.setTextColor(Color.parseColor("#000000"));
                btn_apply.setTextColor(Color.parseColor("#ffffff"));


                if (chk_verified.isChecked()) {
                    SearchProfessional1.verified = "1";
                    Log.e("avinash", "" + chk_verified.isChecked());

                } else {
                    SearchProfessional1.verified = "0";
                }
                if (chk_availability.isChecked()) {
                    SearchProfessional1.availability = "1";
                    Log.e("avinash", "" + chk_availability.isChecked());

                } else {
                    SearchProfessional1.availability = "0";
                }


                finish();
                break;

            case R.id.chk_availability:
            case R.id.chk_verified:
                SearchProfessional1.is_filter_changed = true;
                break;

            case R.id.btn_clear_all:

                btn_clear_all.setBackground(new ColorDrawable(Color.parseColor("#3fb4b5")));
                btn_apply.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));

                btn_clear_all.setTextColor(Color.parseColor("#ffffff"));
                btn_apply.setTextColor(Color.parseColor("#000000"));
                mRecyclerView_country.setVisibility(View.VISIBLE);
                mRecyclerView_city.setVisibility(View.GONE);
                mRecyclerView_state.setVisibility(View.GONE);
                txt_selected_country.setVisibility(View.GONE);
                txt_city.setVisibility(View.GONE);
                txt_state.setVisibility(View.GONE);
                fastScroller.setVisibility(View.VISIBLE);
                fast_scroller_city.setVisibility(View.GONE);
                fast_scroller_state.setVisibility(View.GONE);


                txt_country.setBackgroundResource(android.R.color.transparent);
                txt_category.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_exp.setBackgroundResource(R.drawable.no_top_order_rounded);
                txt_score.setBackgroundResource(R.drawable.no_top_order_rounded);
                line_view.setVisibility(View.GONE);
                rl_categories.setVisibility(View.GONE);
                rl_names.setVisibility(View.VISIBLE);
                rl_exp.setVisibility(View.GONE);
                rl_score.setVisibility(View.GONE);

                cat_filer_text = "";
                subcat_filter_text = "";

                country_filter_text = "";
                state_filer_text = "";
                city_filer_text = "";
                seekbar_score.setProgress(0);
                seekbar_exp.setProgress(0);
                chk_availability.setChecked(false);
                chk_verified.setChecked(false);
                txt_experirnce.setText(Html.fromHtml(getResources().getString(R.string.years)+" - <b>" + 0 + "+</b>"));
                txt_scores.setText("" + 0 + "+");
                city_count = 0;
                sub_cat_count = 0;
                txt_selected_country.setVisibility(View.GONE);
                txt_subcategory.setVisibility(View.GONE);
                txt_state.setVisibility(View.GONE);
                txt_city.setVisibility(View.GONE);
                txt_selected_category.setVisibility(View.GONE);
                SearchProfessional1.category = "";
                SearchProfessional1.location = "";
                SearchProfessional1.subcategory = "";
                SearchProfessional1.social_score = "";
                SearchProfessional1.availability = "0";
                SearchProfessional1.experience = "";
                SearchProfessional1.verified = "0";

                break;

        }
    }


    protected void initialiseDataForCountry() {
        //Recycler view data
        db = new DatabaseHandlerOld(getApplicationContext());
        al_country = db.getCountry();
        for (int i = 0; i < al_country.size(); i++) {
            al_country_names.add(al_country.get(i).getName());
        }
        //Alphabet fast scroller data
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < al_country_names.size(); i++) {
            String name = al_country_names.get(i);
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
    }

    protected void initialiseUIForCountry() {
        mRecyclerView_country.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView_country.setAdapter(new RecyclerViewAdapterwithAlphabets(al_country_names));

        fastScroller.setRecyclerView(mRecyclerView_country);
        fastScroller.setUpAlphabet(mAlphabetItems);
    }


    protected void initialiseDataForState(int position) {
        //Recycler view data
        db = new DatabaseHandlerOld(getApplicationContext());
        al_country = db.getCountry();
        al_state_names.clear();
        al_state.clear();

        String id = "";
        for (int i = 0; i < al_country_names.size(); i++) {
            if (al_country_names.get(i).equals(("" + al_country_names.get(position)))) {
                id = al_country.get(i).getId();
                break;
            }
        }
        al_state = db.getState(id);
        for (int i = 0; i < al_state.size(); i++) {

            al_state_names.add(al_state.get(i).getName());

        }


        Log.e("size", al_state.size() + "c");


        //Alphabet fast scroller data
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < al_state_names.size(); i++) {
            String name = al_state_names.get(i);
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
    }


    protected void initialiseUIForState() {
        mRecyclerView_state.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView_state.setAdapter(new RecyclerViewAdapterwithAlphabets(al_state_names));

        fast_scroller_state.setRecyclerView(mRecyclerView_state);
        fast_scroller_state.setUpAlphabet(mAlphabetItems);
    }


    protected void initialiseDataForCity(int position) {
        //Recycler view data
        db = new DatabaseHandlerOld(getApplicationContext());

        String id = "";
        for (int i = 0; i < al_state_names.size(); i++) {
            if (al_state_names.get(i).equals(("" + al_state_names.get(position)))) {
                id = al_state.get(i).getId();
            }
        }


        Log.e("size", al_state.size() + "c    " + id);

        al_city_names = db.getCity(id);
        al_city.clear();
        Log.e("size", al_city_names.size() + "c");
        //Alphabet fast scroller data
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < al_city_names.size(); i++) {
            al_city.add(new BeanForCityList(al_city_names.get(i), false));
            String name = al_city_names.get(i);
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
    }


    protected void initialiseUIForCIty() {
        mRecyclerView_city.setLayoutManager(new LinearLayoutManager(this));
        ad_city = new RecyclerViewAdapterwithAlphabetsMulpleSelection(al_city);
        mRecyclerView_city.setAdapter(ad_city);

        fast_scroller_city.setRecyclerView(mRecyclerView_city);
        fast_scroller_city.setUpAlphabet(mAlphabetItems);

    }


    protected void initialiseDataForCat() {
        //Recycler view data

        cat_saubcat_list = getDropdownData.getStateList(FilterSearchActiviry.this);


        for (int i = 0; i < cat_saubcat_list.size(); i++) {
            if (!("" + cat_saubcat_list.get(i).getCategory()).equalsIgnoreCase("Other")) {
                cat_list.add(cat_saubcat_list.get(i).getCategory());
            }
        }


        //Alphabet fast scroller data
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < cat_list.size(); i++) {
            String name = cat_list.get(i);
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
    }

    protected void initialiseUIForCat() {
        recycler_view_for_cat.setLayoutManager(new LinearLayoutManager(this));
        recycler_view_for_cat.setAdapter(new RecyclerViewAdapterwithAlphabets(cat_list));

        fast_scroller_for_cat.setRecyclerView(recycler_view_for_cat);
        fast_scroller_for_cat.setUpAlphabet(mAlphabetItems);
    }


    protected void initialiseDataForsubCat(int position) {
        //Recycler view data


        al_subcat = new ArrayList<>();
        al_sub_cat_bean = new ArrayList<>();

        for (int i = 0; i < cat_list.size(); i++) {
            if (("" + cat_list.get(position)).equals("" + cat_list.get(i))) {

                al_subcat = cat_saubcat_list.get(i).getSub_category();

            }
        }


        //Alphabet fast scroller data
        mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < al_subcat.size(); i++) {
            al_sub_cat_bean.add(new BeanForCityList(al_subcat.get(i), false));
            String name = al_subcat.get(i);
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
    }

    protected void initialiseUIForsubCat() {
        recycler_view_for_subcat.setLayoutManager(new LinearLayoutManager(this));
        ad_sub_category = new RecyclerViewAdapterwithAlphabetsMulpleSelection(al_sub_cat_bean);
        recycler_view_for_subcat.setAdapter(ad_sub_category);

        fast_scroller_for_subcat.setRecyclerView(recycler_view_for_subcat);
        fast_scroller_for_subcat.setUpAlphabet(mAlphabetItems);
    }


    public void intializePreviousData() {

        city_count = 0;
        sub_cat_count = 0;

        if (SearchProfessional1.country != null && !SearchProfessional1.country.equalsIgnoreCase("")) {
            for (int i = 0; i < al_country_names.size(); i++) {

                if (SearchProfessional1.country.equalsIgnoreCase(al_country_names.get(i))) {
                    txt_selected_country.setVisibility(View.VISIBLE);
                    txt_selected_country.setText("" + al_country_names.get(i));
                    initialiseDataForState(i);
                    initialiseUIForState();
                    mRecyclerView_country.setVisibility(View.GONE);
                    mRecyclerView_city.setVisibility(View.GONE);
                    mRecyclerView_state.setVisibility(View.VISIBLE);
                    fastScroller.setVisibility(View.GONE);
                    fast_scroller_city.setVisibility(View.GONE);
                    fast_scroller_state.setVisibility(View.VISIBLE);
                    break;
                }
            }

            if (SearchProfessional1.state != null && !SearchProfessional1.state.equalsIgnoreCase("")) {
                for (int i = 0; i < al_state_names.size(); i++) {
                    if (SearchProfessional1.state.equalsIgnoreCase(al_state_names.get(i))) {
                        city_count = 0;
                        city_filer_text = "";

                        SearchProfessional1.state = al_state_names.get(i);
                        txt_state.setText("" + al_state_names.get(i));
                        txt_state.setVisibility(View.VISIBLE);
                        initialiseDataForCity(i);
                        initialiseUIForCIty();
                        mRecyclerView_country.setVisibility(View.GONE);
                        mRecyclerView_city.setVisibility(View.VISIBLE);
                        mRecyclerView_state.setVisibility(View.GONE);
                        fastScroller.setVisibility(View.GONE);
                        fast_scroller_city.setVisibility(View.VISIBLE);
                        fast_scroller_state.setVisibility(View.GONE);
                    }
                }
            }


            if (SearchProfessional1.city != null && !SearchProfessional1.city.equalsIgnoreCase("")) {

                String[] cities = SearchProfessional1.city.split(",");
                for (int j = 0; j < cities.length; j++) {

                    for (int i = 0; i < al_city_names.size(); i++) {
                        if (cities[j].equalsIgnoreCase(al_city_names.get(i))) {
                            city_count = city_count + 1;
                            BeanForCityList bean = null;
                            if (al_city.get(i).isCheck()) {
                                bean = new BeanForCityList(al_city.get(i).getName(), false);
                            } else {
                                bean = new BeanForCityList(al_city.get(i).getName(), true);

                            }

                            al_city.remove(i);
                            al_city.add(i, bean);
                            ad_city.notifyDataSetChanged();

                        }

                    }

                    if (city_count != 0) {
                        txt_city.setVisibility(View.VISIBLE);
                        txt_city.setText(getResources().getString(R.string.hint_city)+" [ " + city_count + " ]");
                    }


                }


            }


        }


        if (SearchProfessional1.category != null && !SearchProfessional1.category.equalsIgnoreCase("")) {
            for (int i = 0; i < cat_list.size(); i++) {

                if (SearchProfessional1.category.equalsIgnoreCase(cat_list.get(i))) {
                    txt_selected_category.setText("" + cat_list.get(i));
                    txt_selected_category.setVisibility(View.VISIBLE);
                    SearchProfessional1.category = cat_list.get(i);
                    initialiseDataForsubCat(i);
                    initialiseUIForsubCat();
                    recycler_view_for_cat.setVisibility(View.GONE);
                    recycler_view_for_subcat.setVisibility(View.VISIBLE);
                    fast_scroller_for_cat.setVisibility(View.GONE);
                    fast_scroller_for_subcat.setVisibility(View.VISIBLE);
                    break;
                }
            }

            if (SearchProfessional1.subcategory != null && !SearchProfessional1.subcategory.equalsIgnoreCase("")) {

                String[] subcategories = SearchProfessional1.subcategory.split(",");
                for (int j = 0; j < subcategories.length; j++) {
                    for (int i = 0; i < al_subcat.size(); i++) {


                        if (subcategories[j].equalsIgnoreCase(al_subcat.get(i))) {
                            sub_cat_count = sub_cat_count + 1;
                            BeanForCityList bean = null;
                            if (al_sub_cat_bean.get(i).isCheck()) {
                                bean = new BeanForCityList(al_sub_cat_bean.get(i).getName(), false);
                            } else {
                                bean = new BeanForCityList(al_sub_cat_bean.get(i).getName(), true);

                            }

                            al_sub_cat_bean.remove(i);
                            al_sub_cat_bean.add(i, bean);
                            ad_sub_category.notifyDataSetChanged();

                        }
                    }
                    if (sub_cat_count != 0) {
                        txt_subcategory.setVisibility(View.VISIBLE);
                        txt_subcategory.setText(getResources().getString(R.string.hint_sub_category)+" [ " + sub_cat_count + " ]");
                    }

                }
            }

        }


        if (SearchProfessional1.availability != null && !SearchProfessional1.availability.equalsIgnoreCase("")) {
            if (SearchProfessional1.availability.equalsIgnoreCase("1")) {

                chk_availability.setChecked(true);
            } else {
                chk_availability.setChecked(false);
            }
        }


        if (SearchProfessional1.verified != null && !SearchProfessional1.verified.equalsIgnoreCase("")) {
            if (SearchProfessional1.verified.equalsIgnoreCase("1")) {

                chk_verified.setChecked(true);
            } else {
                chk_verified.setChecked(false);
            }
        }


        if (SearchProfessional1.social_score != null && SearchProfessional1.social_score != "" && !"".equals(SearchProfessional1.social_score)) {

            seekbar_score.setProgress(Integer.parseInt(SearchProfessional1.social_score));
            txt_scores.setText("" + SearchProfessional1.social_score + "+");
        }


        if (SearchProfessional1.experience != null && SearchProfessional1.experience != "" && !SearchProfessional1.experience.equalsIgnoreCase("")) {
            seekbar_exp.setProgress(Integer.parseInt(SearchProfessional1.experience));
            txt_experirnce.setText(Html.fromHtml(getResources().getString(R.string.years)+" - <b>" + SearchProfessional1.experience + "+</b>"));
        }
    }

}


