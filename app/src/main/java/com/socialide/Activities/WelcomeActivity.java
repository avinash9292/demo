package com.socialide.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.crashlytics.android.Crashlytics;
import com.socialide.Adapters.WelcomeAdapter;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;

import io.fabric.sdk.android.Fabric;


public class WelcomeActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    WelcomeAdapter adapter;
    ViewPager welcomePager;
    MyButton bottomButton;
    RelativeLayout rl;
    private TextView[] dots;
    private LinearLayout dotsLayout;

    int pagerCurrentNo;
    public static int checkAnimation = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!Mypreferences.getString(Mypreferences.User_id, getApplicationContext()).equals("")) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.activity_welcome);

        rl = (RelativeLayout) findViewById(R.id.rl);

        rl.setBackgroundDrawable(getResources().getDrawable(R.drawable.first));


        // overridePendingTransition(R.anim.slide_to_left_activity,R.anim.slide_from_right_activity);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);

        adapter = new WelcomeAdapter(this, this);
        welcomePager = (ViewPager) findViewById(R.id.view_pager);
        bottomButton = (MyButton) findViewById(R.id.ll_bottom);
        bottomButton.setOnClickListener(this);
        welcomePager.setAdapter(adapter);
        welcomePager.setOnPageChangeListener(this);
        addBottomDots(0);

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[3];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < 3; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ll_bottom) {
            if (bottomButton.getText().toString().equals("Skip")) {
                continueToLoginScreen();
            } else if (pagerCurrentNo == 2) {
                continueToLoginScreen();
            } else {
                welcomePager.setCurrentItem(2, true);
            }
        }
    }

    public void continueToLoginScreen() {
        checkAnimation = 1;
        Intent intent;
        if (Mypreferences.getString(Mypreferences.User_id, getApplicationContext()).equals("")) {
            intent = new Intent(this, LoginActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);

        }

        Log.v("akram", "intent here");
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
        startActivity(intent, options.toBundle());
        // overridePendingTransition(R.anim.slide_to_left_activity,R.anim.slide_from_right_activity);
        finish();
    }

    public void changeBottomText() {
        if (pagerCurrentNo == 2) {
            bottomButton.setText(getResources().getString(R.string.letsGo));
        } else {
            bottomButton.setText(getResources().getString(R.string.Skip));
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {


        addBottomDots(position);

        this.pagerCurrentNo = position;
        changeBottomText();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
