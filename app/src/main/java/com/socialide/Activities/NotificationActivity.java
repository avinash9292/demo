package com.socialide.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.socialide.Adapters.SwipeRecyclerViewAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.DialogClass;
import com.socialide.Helper.DividerItemDecoration1;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Model.AllNotificationModel;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Akram on 11/10/2017.
 */

public class NotificationActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {

    /**
     * RecyclerView: The new recycler view replaces the list view. Its more modular and therefore we
     * must implement some of the functionality ourselves and attach it to our recyclerview.
     * <p/>
     * 1) Position items on the screen: This is done with LayoutManagers
     * 2) Animate & Decorate views: This is done with ItemAnimators & ItemDecorators
     * 3) Handle any touch events apart from scrolling: This is now done in our adapter's ViewHolder
     */

    private ArrayList<AllNotificationModel> notificationList;

    private TextView tvEmptyView;
    private RecyclerView mRecyclerView;
    public static AVLoadingIndicatorView avi;
    RelativeLayout relativeAllDelete;
    public static RelativeLayout relativeAllRead;
    MyTextView tvNotificationAllDelete;
    public static MyTextView tvNotificationAllRead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        tvEmptyView = (TextView) findViewById(R.id.tvNoNotification);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerNotification);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        relativeAllDelete = (RelativeLayout) findViewById(R.id.relativeAllDelete);
        relativeAllRead = (RelativeLayout) findViewById(R.id.relativeAllRead);
        tvNotificationAllDelete = (MyTextView) findViewById(R.id.tvNotificationAllDelete);
        tvNotificationAllRead = (MyTextView) findViewById(R.id.tvNotificationAllRead);

        // Layout Managers:
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        // Item Decorator:
        //mRecyclerView.addItemDecoration(new DividerItemDecoration1(getResources().getDrawable(R.drawable.divider)));
        // mRecyclerView.setItemAnimator(new FadeInLeftAnimator());


        Log.v("akram", "click ");
        ArrayList<NameValuePair> prama = new ArrayList<>();
        AsyncRequest getPosts = new AsyncRequest(this, this, "POST", prama, avi);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.getNotificationMessage);

        notificationList = new ArrayList<AllNotificationModel>();

        /* Scroll Listeners */
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("RecyclerView", "onScrollStateChanged");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        relativeAllDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogClass dialogClass = new DialogClass();
                dialogClass.exitDialog(NotificationActivity.this, NotificationActivity.this, "Are you sure you want to delete all notification.", "All Delete", "deleteNotification", NotificationActivity.this);

            }
        });

        relativeAllRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogClass dialogClass = new DialogClass();
                dialogClass.exitDialog(NotificationActivity.this, NotificationActivity.this, "Are you sure you want to mark all notification as read", "Mark as Read", "readNotification", NotificationActivity.this);

            }
        });
    }

    @Override
    public void asyncResponse(String response) {

        FunctionClass.logCatLong(response);
        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.getString("api_name").equals("readAllNotificationMessage")) {

                if (postObject.getString("success").equals("true")) {
                    Mypreferences.NotificationCount = 0;
                    ArrayList<NameValuePair> prama = new ArrayList<>();
                    AsyncRequest getPosts = new AsyncRequest(this, this, "POST", prama, avi);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.getNotificationMessage);

                } else {

                }
            } else if (postObject.getString("api_name").equals("deleteAllNotificationMessage")) {

                if (postObject.getString("success").equals("true")) {
                    Mypreferences.NotificationCount = 0;
                    mRecyclerView.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);
                    relativeAllDelete.setClickable(false);
                    relativeAllRead.setClickable(false);
                    tvNotificationAllDelete.setTextColor(Color.parseColor("#50000000"));
                    tvNotificationAllRead.setTextColor(Color.parseColor("#50000000"));
                }

            } else {
                if (postObject.getString("success").equals("true")) {
                    notificationList.clear();
                    boolean isRead = false;
                    JSONArray resultJsonArray = postObject.getJSONArray("result");
                    for (int i = 0; i < resultJsonArray.length(); i++) {

                        JSONObject jsonObject = resultJsonArray.getJSONObject(i);
                        if ("0".equals(jsonObject.getString("is_read") + "")) {
                            isRead = true;
                        }
                        notificationList.add(new AllNotificationModel(jsonObject.getString("profile_pic"),
                                jsonObject.getString("user_id"), jsonObject.getString("type"), jsonObject.getString("noti_id"),
                                jsonObject.getString("notification"), jsonObject.getString("is_read"), jsonObject.getString("created_on")));

                    }
                    Log.v("akram", "niti size = " + notificationList.size());

                    if (!isRead) {
                        relativeAllRead.setClickable(false);
                        tvNotificationAllRead.setTextColor(Color.parseColor("#50000000"));
                    }

                    // Creating Adapter object
                    SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(NotificationActivity.this, this, notificationList);
                    // Setting Mode to Single to reveal bottom View for one item in List
                    // Setting Mode to Mutliple to reveal bottom Views for multile items in List
                    ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);

                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    tvEmptyView.setVisibility(View.VISIBLE);
                    relativeAllDelete.setClickable(false);
                    relativeAllRead.setClickable(false);
                    tvNotificationAllDelete.setTextColor(Color.parseColor("#50000000"));
                    tvNotificationAllRead.setTextColor(Color.parseColor("#50000000"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void deleteNotification() {
        ArrayList<NameValuePair> prama = new ArrayList<>();
        AsyncRequest getPosts = new AsyncRequest(NotificationActivity.this, NotificationActivity.this, "POST", prama, avi);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.deleteAllNotificationMessage);

    }

    public void readNotification() {
        ArrayList<NameValuePair> prama = new ArrayList<>();
        AsyncRequest getPosts = new AsyncRequest(NotificationActivity.this, NotificationActivity.this, "POST", prama, avi);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.readAllNotificationMessage);
    }

}