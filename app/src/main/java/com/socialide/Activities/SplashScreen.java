package com.socialide.Activities;

import android.animation.ValueAnimator;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.PendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.location.LocationSettingsRequest;
//import com.google.android.gms.location.LocationSettingsResult;
//import com.google.android.gms.location.LocationSettingsStates;
//import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.crashlytics.android.Crashlytics;

import com.google.firebase.messaging.FirebaseMessaging;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.Config;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.LockableScrollView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.NotificationUtils;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;

public class SplashScreen extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {
    ArrayList<NameValuePair> params;
    AVLoadingIndicatorView view;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    Location location1;
    public static String device_token;
    Button btn_start;
    public static String offSet = "";
    //private BroadcastReceiver mRegistrationBroadcastReceiver;
    Double lat, lang;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String s = Locale.getDefault().getDisplayLanguage() + "    " + Locale.getDefault().getLanguage();
        Log.e("Avinash", "Lang    " + s);
        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_splash_screen);
        ScrollView QuranGalleryScrollView = (ScrollView) findViewById(R.id.QuranGalleryScrollView);
        HorizontalScrollView hori_sc = (HorizontalScrollView) findViewById(R.id.hori_sc);
        btn_start = (Button) findViewById(R.id.btn_start);
        QuranGalleryScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        hori_sc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);
        Fabric.with(this, new Crashlytics());

//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
//                Locale.getDefault());
//        Date currentLocalTime = calendar.getTime();
//        DateFormat date = new SimpleDateFormat("Z",Locale.getDefault());
//        String localTime = date.format(currentLocalTime);

        offSet = "" + TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60000;
        //TimeZone.getDefault().getRawOffset();

//        DateFormat date = new SimpleDateFormat("z",Locale.getDefault());
//        String localTime = date.format(currentLocalTime);
        //   Log.e("Avinash",localTime+" am"+i+"avi "+ (TimeZone.getDefault().getRawOffset()/60000));


//
//        locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
//
//        // Creating a criteria object to retrieve provider
//        Criteria criteria = new Criteria();
//
//        // Getting the name of the best provider
//        final String provider = locationManager.getBestProvider(criteria, true);
//
//        // Getting Current Location
//        location1 = locationManager.getLastKnownLocation(provider);
//
//        if (location1 != null) {
//
//            onLocationChanged(location1);
//            Mypreferences.Lat = location1.getLatitude();
//            Mypreferences.Lang = location1.getLongitude();
//
//        } else {
//            //This is what you need:
//            locationManager.requestLocationUpdates(provider, 1000, 0, this);
//        }


        //   cap
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
//
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
//
//                    txtMessage.setText(message);

                    CustomSnackBar.toast(SplashScreen.this, message);
                }
            }
        };

//        SlideImageView slideImageView = (SlideImageView) findViewById(R.id.img_vertical_slide);
//        slideImageView.setSource(R.drawable.og_faces);
//        slideImageView.setRate(0.9f);
//        slideImageView.setAxis(SlideImageView.Axis.HORIZONTAL);
        Animation animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.anima);

        final ImageView backgroundOne = (ImageView) findViewById(R.id.background_one);
        final ImageView backgroundTwo = (ImageView) findViewById(R.id.background_two);
        backgroundTwo.startAnimation(animFadeOut);
        backgroundOne.startAnimation(animFadeOut);

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(20000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = backgroundOne.getWidth();
                final float translationX = width * progress;
                backgroundOne.setTranslationX(translationX);
                backgroundTwo.setTranslationX(translationX - width);
            }
        });
        animator.start();
     /*   SlideImageView slideImageView = new SlideImageView(SplashScreen.this);
        slideImageView.setSource(R.drawable.og_faces);
        slideImageView.setRate(0.3f);
        slideImageView.setAxis(SlideImageView.Axis.HORIZONTAL);
*/


        displayFirebaseRegId();
        params = getParams();
        if (params.size() == 0) {

            // This method will be executed once the timer is over
            // Start your app main activity

            btn_start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(SplashScreen.this, WelcomeActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                    startActivity(i, options.toBundle());
                    finish();
                }
            });


        } else {
            //  view.show();
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            AsyncRequest getPosts = new AsyncRequest(this, "POST", params, view);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.LOGIN);
        }
    }


    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e("TAG", "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
            Log.e("TAG", "Firebaseinner reg id: " + regId);
            device_token = regId;
        }


        //    txtRegId.setText("Firebase Reg Id: " + regId);
        // else
        //   txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        if (!Mypreferences.getString(Mypreferences.Email, getApplicationContext()).equals("")) {
            params.add(new BasicNameValuePair("password", "" + Mypreferences.getString(Mypreferences.Pass, getApplicationContext())));
            params.add(new BasicNameValuePair("email", "" + Mypreferences.getString(Mypreferences.Email, getApplicationContext())));
            params.add(new BasicNameValuePair("device_type", "android"));
            params.add(new BasicNameValuePair("device_token", "" + SplashScreen.device_token));
            params.add(new BasicNameValuePair("time_offset", "" + TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 60000));
            // params.add(new BasicNameValuePair("time_offset", "+200"));
        }
        return params;
    }

    @Override
    public void asyncResponse(String response) {

        FunctionClass.logCatLong("" + response);

        try {
            JSONObject postObject = new JSONObject(response);

            view.hide();
            if (postObject.getString("success").equals("true")) {
                Log.e("success", "" + response);

                JSONObject result = postObject.getJSONObject("result");

                Mypreferences.User_id = result.getString("userid");
                if (postObject.getString("type").equals("Professional")) {

                    Mypreferences.User_Type = "Professional";

                    Mypreferences.FIRST_NAME = result.getString("first_name");
                    Mypreferences.LAST_NAME = result.getString("last_name");
                    Mypreferences.DOB = result.getString("dob");
                    Mypreferences.EXPERIENCE = result.getString("experience");
                    Mypreferences.MOBILE_NUMBER = result.getString("mobile_number");
                    Mypreferences.STATE = result.getString("state");
                    Mypreferences.CITY = result.getString("city");
                    Mypreferences.GENDER = result.getString("gender");
                    Mypreferences.COUNTRY = result.getString("country");
                    Mypreferences.CATEGORY = result.getString("category");
                    Mypreferences.CURRENT_COMPANY = result.getString("current_company");
                    Mypreferences.SUB_CATEGORY = result.getString("subcategory");
                    Mypreferences.ABOUT_YOU = result.getString("about_me");

                    Mypreferences.Maximum_Event_Request = result.getString("maximum_event_request");
                    Mypreferences.Join_Flow_Completed = result.getString("join_flow_completed");

                    Mypreferences.Social_Score = result.getString("social_score");
                    Mypreferences.Picture = result.getString("profile_pic");
                    Mypreferences.Availability = result.getString("availability");
                    Mypreferences.Profession = result.getString("profession");
                    Mypreferences.Access_token = result.getString("access_token");
                    Mypreferences.isAvailable = result.getString("is_available");
                    Mypreferences.NotificationCount = result.getInt("notification_count");

                }

                if (postObject.getString("type").equals("Organization")) {
                    Mypreferences.User_Type = "Organization";
                    Mypreferences.ORGANISATION_NAME = result.getString("name");
                    Mypreferences.LANDLINE_NUMBER = result.getString("landline_number");
                    Mypreferences.FIRST_NAME = result.getString("contact_firstname");
                    Mypreferences.LAST_NAME = result.getString("contact_lastname");
                    Mypreferences.MOBILE_NUMBER = result.getString("contact_number");
                    Mypreferences.STATE = result.getString("state");
                    Mypreferences.CITY = result.getString("city");
                    Mypreferences.REGISTRATION_NUMBER = result.getString("u_dise_number");
                    Mypreferences.COUNTRY = result.getString("country");
                    Mypreferences.Address = result.getString("address");
                    Mypreferences.DESIGNATION = result.getString("contact_position");
                    Mypreferences.STABLISHMENT_YEAR = result.getString("establishment_year");
                    Mypreferences.ABOUT_YOU = result.getString("about");
                    Mypreferences.Contact_position = result.getString("contact_position");

                    Mypreferences.Join_Flow_Completed = result.getString("join_flow_completed");

                    Mypreferences.Picture = result.getString("profile_pic");
                    Mypreferences.Access_token = result.getString("access_token");
                    Mypreferences.NotificationCount = result.getInt("notification_count");

                }

                // Start your app main activity

                if (Mypreferences.Join_Flow_Completed.equals("0")) {
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                    startActivity(i, options.toBundle());
                    finish();
                } else {
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                    startActivity(i, options.toBundle());
                    finish();
                }

            } else {

                Mypreferences.clear(SplashScreen.this);
                Intent i = new Intent(SplashScreen.this, WelcomeActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(i, options.toBundle());
                finish();
                CustomSnackBar.toast(SplashScreen.this, postObject.getString("message"));
            }
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//
//    }
//
//
//    public void location()
//    {
//
//        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            // location = locationManager.getLastKnownLocation(provider);
//
////                    if (location != null) {
////
////                        onLocationChanged(location);
////                        lat = location.getLatitude();
////                        lang = location.getLongitude();
////                    }
//            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//            Criteria criteria = new Criteria();
//
//
//            String provider = locationManager.getBestProvider(criteria, false);
//
//            if (provider != null & !provider.equals(""))
//
//            {
//
//
//
//                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                        && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//
//                    locationManager.requestLocationUpdates(provider, 1, 1, SplashScreen.this);
//                    Location location = locationManager.getLastKnownLocation(provider);
//
//                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1l, 1f, new LocationListener() {
//                        @Override
//                        public void onStatusChanged(String provider, int status, Bundle extras) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void onProviderEnabled(String provider) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void onProviderDisabled(String provider) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void onLocationChanged(Location location) {
//                            // TODO Auto-generated method stub
//                            location1 = location;
//                        }
//                    });
//
//                    if (location != null)
//
//                    {
//
//
//
//                        onLocationChanged(location);
//
//                    } else {
//
//                        //   Toast.makeText(getActivity(),"location not found",Toast.LENGTH_LONG ).show();
//
//                    }
//
//
//                    return;
//                }
//            } else
//
//            {
//
//                //  Toast.makeText(getApplicationContext(),"Provider is null",Toast.LENGTH_LONG).show();
//
//            }
//
//
//
//
//
//        } else {
//a();
//
//        }
//    }
//
//
//
//
//
//
//
//
//
//    public void a() {
//        GoogleApiClient googleApiClient = null;
//
//        if (googleApiClient == null) {
//            googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
//                    .addApi(LocationServices.API)
//                    .addConnectionCallbacks(SplashScreen.this)
//                    .addOnConnectionFailedListener(SplashScreen.this).build();
//            googleApiClient.connect();
//
//            LocationRequest locationRequest = LocationRequest.create();
//            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//            locationRequest.setInterval(30 * 1000);
//            locationRequest.setFastestInterval(5 * 1000);
//            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                    .addLocationRequest(locationRequest);
//
//            //**************************
//            builder.setAlwaysShow(true); //this is the key ingredient
//            //**************************
//
//            PendingResult<LocationSettingsResult> result =
//                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
//            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//                @Override
//                public void onResult(LocationSettingsResult result) {
//                    final Status status = result.getStatus();
//                    final LocationSettingsStates state = result.getLocationSettingsStates();
//                    switch (status.getStatusCode()) {
//                        case LocationSettingsStatusCodes.SUCCESS:
//                            // All location settings are satisfied. The client can initialize location
//                            // requests here.
//
//
//                            if (location1 != null) {
//
//                                onLocationChanged(location1);
//                                Mypreferences.Lat = location1.getLatitude();
//                                Mypreferences.Lang = location1.getLongitude();
//                            }
//
//                            break;
//                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            // Location settings are not satisfied. But could be fixed by showing the user
//                            // a dialog.
//                            try {
//                                // Show the dialog by calling startResolutionForResult(),
//                                // and check the result in onActivityResult().
//                                status.startResolutionForResult(
//                                        SplashScreen.this, 1000);
//                            } catch (IntentSender.SendIntentException e) {
//                                // Ignore the error.
//                            }
//                            break;
//                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            // Location settings are not satisfied. However, we have no way to fix the
//                            // settings so we won't show the dialog.
//                            break;
//                    }
//                }
//            });
//        }
//
//
//    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
