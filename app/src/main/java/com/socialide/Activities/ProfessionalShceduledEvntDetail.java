package com.socialide.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.EventsScheduled;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfessionalShceduledEvntDetail extends AppCompatActivity implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_decline;
    static RelativeLayout mRelativeLayout, relativeContactPerson;
    static PopupWindow mPopupWindow;
    MyTextView txt_email1, tv_event_name, tv_organisation_name, tv_event_guests, txt_date, txt_time, txt_email, txt_landline_number, edt_message, txt_person_name, txt_designation, txt_contact;
    CircleImageView iv_me_profile_pic;
    AVLoadingIndicatorView view;
    BeanForFetchEvents b;
    boolean check = true;
    CheckBox checkboxShareContact;
    String longStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_details_pro_scheduled);

        btn_decline = (MyButton) findViewById(R.id.btn_remove);
        txt_email1 = (MyTextView) findViewById(R.id.txt_email1);
        txt_contact = (MyTextView) findViewById(R.id.txt_contact);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);
        relativeContactPerson = (RelativeLayout) findViewById(R.id.relativeContactPerson);
        checkboxShareContact = (CheckBox) findViewById(R.id.checkboxShareContact);

        iv_me_profile_pic = (CircleImageView) findViewById(R.id.iv_me_profile_pic);

        longStr = getResources().getString(R.string.longlongDesc);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);


        tv_event_name = (MyTextView) findViewById(R.id.tv_name);
        tv_organisation_name = (MyTextView) findViewById(R.id.tv_organisation_name);
        tv_event_guests = (MyTextView) findViewById(R.id.tv_event_guests);
        txt_date = (MyTextView) findViewById(R.id.txt_date);
        txt_time = (MyTextView) findViewById(R.id.txt_time);
        txt_email = (MyTextView) findViewById(R.id.txt_email);
        txt_contact = (MyTextView) findViewById(R.id.txt_contact);
        edt_message = (MyTextView) findViewById(R.id.edt_message);
        txt_landline_number = (MyTextView) findViewById(R.id.txt_landline_number);
        txt_person_name = (MyTextView) findViewById(R.id.txt_person_name);
        txt_designation = (MyTextView) findViewById(R.id.txt_designation);


        if (EventFragment.position < EventsScheduled.activity_list1.size()) {

            b = EventsScheduled.activity_list1.get(EventFragment.position);

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.profilepic)
                    .showImageOnFail(R.drawable.ic_cross)
                    .resetViewBeforeLoading(true)
//.cacheInMemory(true)
                    .cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(new FadeInBitmapDisplayer(300))
                    .build();
            DonutProgress d = (DonutProgress) findViewById(R.id.loading);
            com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, ProfessionalShceduledEvntDetail.this);

            tv_event_name.setText(Html.fromHtml("<b>" + b.getEvent_title() + "</b>"));
            tv_organisation_name.setText("" + b.getEvent_by());
            tv_event_guests.setText("" + b.getEvent_location());

            String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

            String date = datetime.split("T")[0];
            String time = datetime.split("T")[1];
            txt_time.setText("" + time);
            txt_date.setText("" + date);
            // txt_landline_number.setText("" + b.getContact_number());

            SpannableString content = new SpannableString(b.getContact_number());
            content.setSpan(new UnderlineSpan(), 0, b.getContact_number().length(), 0);
            txt_landline_number.setText(content);

            SpannableString content1 = new SpannableString(b.getPerson_mobile_number());
            content1.setSpan(new UnderlineSpan(), 0, b.getPerson_mobile_number().length(), 0);
            txt_contact.setText(content1);

            SpannableString content2 = new SpannableString(b.getEmail());
            content2.setSpan(new UnderlineSpan(), 0, b.getEmail().length(), 0);
            txt_email.setText(content2);

            SpannableString content3 = new SpannableString(b.getPerson_email());
            content3.setSpan(new UnderlineSpan(), 0, b.getPerson_email().length(), 0);
            txt_email1.setText(content3);

            // txt_email.setText("" + b.getEmail());
            edt_message.setText("" + b.getMessage());
            // txt_contact.setText("" + b.getPerson_mobile_number());
            txt_person_name.setText("" + b.getPerson_name());
            txt_designation.setText("" + b.getPerson_designation());
            checkboxShareContact.setText("Share Contact details with " + b.getEvent_by());

            if (b.getLogin_user_sharecontact().equalsIgnoreCase("0")) {
                checkboxShareContact.setChecked(false);
            } else {
                checkboxShareContact.setChecked(true);
            }
            //txt_email1.setText("" + b.getPerson_email());

            if (b.getSharecontact().equalsIgnoreCase("1")) {
                relativeContactPerson.setVisibility(View.VISIBLE);
            } else {
                relativeContactPerson.setVisibility(View.GONE);
            }

            checkboxShareContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String shareContactStr = "";
                    if (isChecked) {
                        shareContactStr = "1";
                    } else {
                        shareContactStr = "0";
                    }
                    ArrayList<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                    params.add(new BasicNameValuePair("share_contact", shareContactStr));

                    AsyncRequest getPosts = new AsyncRequest(ProfessionalShceduledEvntDetail.this, "POST", params);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.updateShareContact);

                }
            });


            btn_decline.setOnClickListener(this);
            txt_email1.setOnClickListener(this);
            txt_contact.setOnClickListener(this);

        }
    }

    @Override
    public void asyncResponse(String response) {
        try {
            Log.v("akram", "reponse ; " + response);
            JSONObject postObject = new JSONObject(response);

            if (postObject.has("api_name")) {

                CustomSnackBar.toast(ProfessionalShceduledEvntDetail.this, postObject.getString("message"));

            } else {

                if (postObject.getString("success").equals("true")) {
                    CustomSnackBar.toast(ProfessionalShceduledEvntDetail.this, postObject.getString("message"));
                    Toast.makeText(this, ""+postObject.getString("message"), Toast.LENGTH_SHORT).show();
                    EventsScheduled.activity_list1.remove(EventFragment.position);
                    EventFragment.isBack = true;
                    Toast.makeText(this, ""+postObject.getString("message"), Toast.LENGTH_SHORT).show();
                    finish();
                    overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

                } else {
                    CustomSnackBar.toast(ProfessionalShceduledEvntDetail.this, postObject.getString("message"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_email1:
                email();
                break;
            case R.id.txt_contact:
                call();
                break;

            case R.id.btn_remove:
                //     popup(getActivity());
                popup(ProfessionalShceduledEvntDetail.this, ProfessionalShceduledEvntDetail.this, mRelativeLayout, b.getEvent_id(), view);

                break;
        }
    }

    void email() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"" + txt_email1.getText()});
        i.putExtra(Intent.EXTRA_SUBJECT, "Socialide");
        i.putExtra(Intent.EXTRA_TEXT, "Sharing content.....");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ProfessionalShceduledEvntDetail.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    void call() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + txt_contact.getText().toString()));

        if (ActivityCompat.checkSelfPermission(ProfessionalShceduledEvntDetail.this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }


    public static void popup(final Activity c, final AsyncRequest.OnAsyncRequestComplete d, RelativeLayout mRelativeLayout, final String eventid, final AVLoadingIndicatorView viw) {
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        View popupView = LayoutInflater.from(c).inflate(R.layout.decline_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );

        mPopupWindow.setFocusable(true);
        //    mPopupWindows.update();
        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));

        MyButton submit = (MyButton) popupView.findViewById(R.id.btn_submit);
        ImageView cross = (ImageView) popupView.findViewById(R.id.img_cross);
        final MyEditTextView editText = (MyEditTextView) popupView.findViewById(R.id.edt_reason);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        // Set a click listener for the popup window close button
        submit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {

                if (!("" + editText.getText().toString()).equals("")) {
                    // Dismiss the popup window

                    params.add(new BasicNameValuePair("eventId", "" + eventid));
                    params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                    params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                    params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
                    params.add(new BasicNameValuePair("message", "" +editText.getText().toString()));
                    mPopupWindow.dismiss();
                    AsyncRequest getPosts = new AsyncRequest(d, c, "POST", params, viw);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.DECLINE_AN_EVENT);


                    // s = editText.getText().toString();
                } else {
                    CustomSnackBar.toast(c, "Please insert to decline this event");
                }

            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();


            }
        });


        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }


}
