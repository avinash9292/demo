package com.socialide.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.socialide.Adapters.InformationAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.socialide.Activities.LoginActivity.back_handle;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    RecyclerView mRecyclerView;
    InformationAdapter adapter;
    ActivityOptions options;
    MyTextView txt_logout, txt_contact_type, txt_general_detail;
    RelativeLayout rl_general, rl_contact, rl3, rl3Noti;
    AVLoadingIndicatorView view;
    private RecyclerView.LayoutManager mLayoutManager;
    public static int profilePageCheck = 0;
    SettingActivity settingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Mypreferences.User_Type.equals("Organization")) {

            setContentView(R.layout.fragment_settings_pro);
            rl3 = (RelativeLayout) findViewById(R.id.rl3);
            rl3.setOnClickListener(this);

        } else {
            setContentView(R.layout.fragment_setting);
        }
        settingActivity = (SettingActivity) this;

        profilePageCheck = 0;
        rl3Noti = (RelativeLayout) findViewById(R.id.rl3Noti);

        rl3Noti.setOnClickListener(this);

        initRecyclerView();
        txt_logout = (MyTextView) findViewById(R.id.txt_logout);

        rl_general = (RelativeLayout) findViewById(R.id.rl_general);
        rl_contact = (RelativeLayout) findViewById(R.id.rl_contact);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);

        rl_contact.setOnClickListener(this);
        rl_general.setOnClickListener(this);
        txt_logout.setOnClickListener(this);
//First Name,Last Name,Designation,Mobile Number,Write about Organisation

    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        adapter = new InformationAdapter(getApplicationContext());
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_contact:

                profilePageCheck = 1;
                options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("set", 2));
                overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                finish();
                break;
            case R.id.rl_general:

                profilePageCheck = 1;
                options = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("set", 1));
                overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                finish();

                break;

            case R.id.txt_logout:

                //    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                CheckInternet mCheckInternet = new CheckInternet();
                if (!mCheckInternet.isConnectingToInternet(SettingActivity.this)) {


                    CustomSnackBar.toast(SettingActivity.this, "Please connect to internet!");

                } else {
                    exitDialog(SettingActivity.this, SettingActivity.this);
                }


                break;

            case R.id.rl3:
                profilePageCheck = 1;
                options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("set", 3));
                overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                finish();
                break;

            case R.id.rl3Noti:

                Intent intent = new Intent(SettingActivity.this, NotificationSetting.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                finish();
                break;
        }
    }

    @Override
    public void asyncResponse(String response) {
        txt_logout.setVisibility(View.VISIBLE);
        Log.v("akram", "response123 =" + response);

        try {
            JSONObject postObject = new JSONObject(response);


            if (postObject.getString("success").equals("true")) {
                CustomSnackBar.toast(SettingActivity.this, postObject.getString("message"));
                Mypreferences.clear(getApplicationContext());
                Mypreferences.Picture = "";
                Mypreferences.STABLISHMENT_YEAR = "STABLISHMENT_YEAR";
                Mypreferences.DOB = "DOB";
                Mypreferences.FIRST_NAME = "FIRST_NAME";
                Mypreferences.LAST_NAME = "LAST_NAME";
                Mypreferences.ORGANISATION_NAME = "ORGANISATION_NAME";
                Mypreferences.REGISTRATION_NUMBER = "REGISTRATION_NUMBER";
                Mypreferences.MOBILE_NUMBER = "MOBILE_NUMBER";
                Mypreferences.LANDLINE_NUMBER = "LANDLINE_NUMBER";
                Mypreferences.COUNTRY = "COUNTRY";
                Mypreferences.STATE = "STATE";
                Mypreferences.CITY = "CITY";
                Mypreferences.ABOUT_YOU = "ABOUT_YOU";
                Mypreferences.DESIGNATION = "DESIGNATION";
                Mypreferences.GENDER = "GENDER";
                Mypreferences.CATEGORY = "CATEGORY";
                Mypreferences.SUB_CATEGORY = "SUB_CATEGORY";
                Mypreferences.CURRENT_COMPANY = "CURRENT_COMPANY";
                Mypreferences.EXPERIENCE = "EXPERIENCE";
                Mypreferences.Access_token = "";
                Mypreferences.Maximum_Event_Request = "";
                Mypreferences.User_Type = "user_type";
                SettingActivity.profilePageCheck = 0;
                MainActivity.tabPosition = 0;
                options = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                back_handle = 0;
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i, options.toBundle());
                this.finish();

            } else {
                CustomSnackBar.toast(SettingActivity.this, postObject.getString("message"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Dialog exitDialog(final Context context, final Activity activity) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view1 = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();
        MyTextView txt_yes = (MyTextView) view1.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view1.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view1.findViewById(R.id.tvMsg);
        MyTextView tvTitleDialog = (MyTextView) view1.findViewById(R.id.tvTitleDialog);
        tvMsg.setText(getResources().getString(R.string.dialog_logoutDesc));
        tvTitleDialog.setText(getResources().getString(R.string.logout));
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_logout.setVisibility(View.GONE);
                AsyncRequest getPosts = new AsyncRequest(SettingActivity.this, "POST", new ArrayList<NameValuePair>(), view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.LOGOUT);
                alertDialoge.dismiss();
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view1);

        alertDialoge.show();

        return alertDialoge;

    }
}
