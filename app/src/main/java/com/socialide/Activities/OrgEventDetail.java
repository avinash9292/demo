package com.socialide.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.EventsDeclined;
import com.socialide.Fragments.EventsPending;
import com.socialide.Fragments.EventsScheduled;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DeclinePopup;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrgEventDetail extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {
    MyTextView txt_declined_static, txt_declined, txt_message, tv_event_name, tv_organisation_name, tv_event_guests, txt_date, txt_time, txt_email, txt_contact, edt_message;
    CircleImageView iv_me_profile_pic;
    BeanForFetchEvents b;
    MyButton btn_remove;
    static RelativeLayout mRelativeLayout;
    static PopupWindow mPopupWindow;
    AVLoadingIndicatorView view;
    MyTextView txt_email_static, txt_contact_static, txt_reason_static, txt_reason;
    RelativeLayout rl_contact, rl_email;
    LinearLayout relativeShare;
    CheckBox checkboxShareContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_details_org);

        txt_declined = (MyTextView) findViewById(R.id.txt_declined);
        txt_declined_static = (MyTextView) findViewById(R.id.txt_declined_static);
        txt_reason_static = (MyTextView) findViewById(R.id.txt_reason_static);
        txt_reason = (MyTextView) findViewById(R.id.txt_reason);

        txt_message = (MyTextView) findViewById(R.id.edt_message);
        relativeShare = (LinearLayout) findViewById(R.id.relativeShare);
        checkboxShareContact = (CheckBox) findViewById(R.id.checkboxShareContact);

        rl_email = (RelativeLayout) findViewById(R.id.rl_email);
        rl_contact = (RelativeLayout) findViewById(R.id.rl_contact);

        txt_contact_static = (MyTextView) findViewById(R.id.txt_contact_static);
        txt_email_static = (MyTextView) findViewById(R.id.txt_email_static);
        btn_remove = (MyButton) findViewById(R.id.btn_remove);
        iv_me_profile_pic = (CircleImageView) findViewById(R.id.iv_me_profile_pic);

        tv_event_name = (MyTextView) findViewById(R.id.tv_name);
        tv_organisation_name = (MyTextView) findViewById(R.id.tv_organisation_name);
        tv_event_guests = (MyTextView) findViewById(R.id.tv_event_guests);
        txt_date = (MyTextView) findViewById(R.id.txt_date);
        txt_time = (MyTextView) findViewById(R.id.txt_time);
        txt_email = (MyTextView) findViewById(R.id.txt_email);
        txt_contact = (MyTextView) findViewById(R.id.txt_contact);
        edt_message = (MyTextView) findViewById(R.id.edt_message);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);

        if (DetailsActivity.type.equals("p") || DetailsActivity.type.equals("s")) {

            if (DetailsActivity.type.equals("p")) {
                b = EventsPending.activity_list1.get(EventFragment.position);
                relativeShare.setVisibility(View.GONE);
            } else {
                b = EventsScheduled.activity_list1.get(EventFragment.position);
                if (b.getSharecontact().equalsIgnoreCase("0")) {
                    txt_contact_static.setVisibility(View.GONE);
                    txt_email_static.setVisibility(View.GONE);
                    rl_contact.setVisibility(View.GONE);
                    rl_email.setVisibility(View.GONE);
                }
            }
            tv_event_name.setText(Html.fromHtml("<b>" + b.getEvent_title() + "</b>"));
            tv_organisation_name.setText("" + b.getEvent_by());
            tv_event_guests.setText("" + b.getEvent_location());

            String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

            String date = datetime.split("T")[0];
            String time = datetime.split("T")[1];
            txt_time.setText("" + time);
            txt_date.setText("" + date);

            //txt_contact.setText("" + b.getContact_number());
            // txt_email.setText("" + b.getEmail());
            edt_message.setText("" + b.getMessage());

            SpannableString content = new SpannableString(b.getEmail());
            content.setSpan(new UnderlineSpan(), 0, b.getEmail().length(), 0);
            txt_email.setText(content);

            SpannableString content1 = new SpannableString(b.getContact_number());
            content1.setSpan(new UnderlineSpan(), 0, b.getContact_number().length(), 0);
            txt_contact.setText(content1);


            txt_declined.setVisibility(View.GONE);
            txt_declined_static.setVisibility(View.GONE);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            params.setMargins(0, 0, 0,44);
//
//            txt_message.setLayoutParams(params);
            btn_remove.setText(getResources().getString(R.string.cancelThisEvent));
        } else {
            relativeShare.setVisibility(View.GONE);
            b = EventsDeclined.activity_list1.get(EventFragment.position);

            txt_declined.setVisibility(View.GONE);
            txt_declined_static.setVisibility(View.GONE);

            txt_reason_static.setVisibility(View.VISIBLE);
            txt_reason.setVisibility(View.VISIBLE);
            txt_reason.setText(b.getReason() + "");

            tv_event_name.setText(Html.fromHtml("<b>" + b.getEvent_title() + "</b>"));
            tv_organisation_name.setText("" + b.getEvent_by());
            tv_event_guests.setText("" + b.getEvent_location());

            txt_contact.setText("" + b.getContact_number());
            txt_email.setText("" + b.getEmail());
            edt_message.setText("" + b.getMessage());
            txt_declined.setText("" + b.getDecline_by());

            SpannableString content = new SpannableString(b.getEmail());
            content.setSpan(new UnderlineSpan(), 0, b.getEmail().length(), 0);
            txt_email.setText(content);

            SpannableString content1 = new SpannableString(b.getContact_number());
            content1.setSpan(new UnderlineSpan(), 0, b.getContact_number().length(), 0);
            txt_contact.setText(content1);


//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            params.setMargins(0, 0, 0, 0);
//
//            txt_message.setLayoutParams(params);
            btn_remove.setText(getResources().getString(R.string.remove));
        }


        if (DetailsActivity.type.equals("s")) {
            relativeShare.setVisibility(View.VISIBLE);
            checkboxShareContact.setText("Share Contact details with " + b.getEvent_by());
            if (b.getLogin_user_sharecontact().equalsIgnoreCase("0")) {
                checkboxShareContact.setChecked(false);
            } else {
                checkboxShareContact.setChecked(true);
            }

        }

        checkboxShareContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String shareContactStr = "";
                if (isChecked) {
                    shareContactStr = "1";
                } else {
                    shareContactStr = "0";
                }
                ArrayList<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                params.add(new BasicNameValuePair("share_contact", "false"));

                AsyncRequest getPosts = new AsyncRequest(OrgEventDetail.this, "POST", params);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.updateShareContact);
            }
        });

        String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date = datetime.split("T")[0];
        String time = datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" + date);


        DonutProgress d = (DonutProgress) findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, OrgEventDetail.this);

        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("akram", "remove = " + btn_remove.getText().toString());
                if (btn_remove.getText().toString().contains("This")) {
                    DeclinePopup.popup(OrgEventDetail.this, OrgEventDetail.this, mRelativeLayout, b.getEvent_id(), view);

                } else {
                    ArrayList<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                    params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                    params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                    params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));


                    AsyncRequest getPosts = new AsyncRequest(OrgEventDetail.this, OrgEventDetail.this, "POST", params, view);
                    getPosts.execute(GloabalURI.baseURI + GloabalURI.REMOVE_AN_EVENT);


                }
            }
        });

    }

    @Override
    public void asyncResponse(String response) {
        FunctionClass.logCatLong(response);
        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.has("api_name")) {

                CustomSnackBar.toast(OrgEventDetail.this, postObject.getString("message"));

            } else {

                if (postObject.getString("success").equals("true")) {
                    CustomSnackBar.toast(OrgEventDetail.this, postObject.getString("message"));
                    //  CustomSnackBar.toast(OrgEventDetail.this, postObject.getString("message"));
                    Toast.makeText(this, "" + postObject.getString("message"), Toast.LENGTH_SHORT).show();
                    if (DetailsActivity.type.equals("p") || DetailsActivity.type.equals("s"))
                        if (DetailsActivity.type.equals("p")) {

                            EventsPending.activity_list1.remove(EventFragment.position);
                        }
                    if (DetailsActivity.type.equals("s")) {

                        EventsScheduled.activity_list1.remove(EventFragment.position);
                    }
                    if (DetailsActivity.type.equals("d")) {

                        EventsDeclined.activity_list1.remove(EventFragment.position);
                    }
                    EventFragment.isBack = true;
                    finish();
                    overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

                } else {
                    CustomSnackBar.toast(OrgEventDetail.this, postObject.getString("message"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventsDeclined.activity_list1.clear();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }
}
