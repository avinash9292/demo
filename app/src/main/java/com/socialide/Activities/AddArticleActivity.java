package com.socialide.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gun0912.tedpicker.Config;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Adapters.EventPhotosAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.BuildConfig;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.MyPastEvents;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DialogClass;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyAutoCompleteTextView;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForEvent;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.Model.BeanForUploadImage;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

import static com.socialide.Activities.MainActivity.tabPosition;
import static com.socialide.Adapters.EventPhotosAdapter.isClickable;

public class AddArticleActivity extends AppCompatActivity {
    MyButton btn_continue;
    Bitmap bitmap;
    String filePath;
    String profilePicPath = "";
    RelativeLayout rl_image;
    LinearLayout lin_img;
    MyEditTextView edt_title, edt_about_article;
    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    Uri uri;

    AVLoadingIndicatorView view;
    ProgressBar view_p_bar;
    ImageView img_cross, img_close;
    RoundedImageView imageView;
    MyTextView tvAddPicture;
    boolean isBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        edt_about_article = (MyEditTextView) findViewById(R.id.edt_about_article);
        edt_title = (MyEditTextView) findViewById(R.id.edt_title);
        imageView = (RoundedImageView) findViewById(R.id.img_pic);
        img_cross = (ImageView) findViewById(R.id.img_cross);
        img_close = (ImageView) findViewById(R.id.img_close);
        btn_continue = (MyButton) findViewById(R.id.btn_continue);
        view_p_bar = (ProgressBar) findViewById(R.id.view_p_bar);
        rl_image = (RelativeLayout) findViewById(R.id.rl_image);
        lin_img = (LinearLayout) findViewById(R.id.lin_img);
        tvAddPicture = (MyTextView) findViewById(R.id.tvAddPicture);

        view = (AVLoadingIndicatorView) findViewById(R.id.avi);

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CheckInternet mCheckInternet = new CheckInternet();

                    if (!mCheckInternet.isConnectingToInternet(AddArticleActivity.this)) {

                        CustomSnackBar.toast(AddArticleActivity.this, "Please connect to internet!");

                    } else {
                        if (edt_title.getText().toString().equalsIgnoreCase("")) {
                            CustomSnackBar.toast(AddArticleActivity.this, "Please enter title");
                        } else if (edt_about_article.getText().toString().equalsIgnoreCase("")) {
                            CustomSnackBar.toast(AddArticleActivity.this, "Please enter Description");
                        } else {
                            btn_continue.setVisibility(View.GONE);
                            view_p_bar.setVisibility(View.VISIBLE);
                            execMultipartPost();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        tvAddPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permission();
            }
        });

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = null;
                //lin_img.setVisibility(View.VISIBLE);
                //rl_image.setVisibility(View.GONE);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.profilepic));
                tvAddPicture.setText("Add Picture");
                img_cross.setVisibility(View.GONE);
            }
        });


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = null;
                onBackPressed();
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION

                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted

                    selectImage();

                } else {
                    // Permission Denied
                    // finish();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void permission() {

        ActivityCompat.requestPermissions(AddArticleActivity.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA},
                1);

    }

    public String image(Bitmap bm) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.socialide");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 70, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myDir + "/" + fname;
    }

    private void execMultipartPost() throws Exception {
        File file = new File(image(bitmap));
        String contentType = file.toURL().openConnection().getContentType();
        view.setVisibility(View.VISIBLE);
        view.show();

        RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), file);

        final String filename = "file_" + System.currentTimeMillis() / 1000L;
        RequestBody requestBody;

        if (bitmap != null) {

            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("article_name", "" + edt_title.getText().toString())
                    .addFormDataPart("description", "" + edt_about_article.getText().toString())
                    .addFormDataPart("article_image", filename + ".jpg", fileBody)
                    .addFormDataPart("access_token", Mypreferences.Access_token)
                    .build();

        } else {
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("article_name", "" + edt_title.getText().toString())
                    .addFormDataPart("description", "" + edt_about_article.getText().toString())
                    .addFormDataPart("access_token", Mypreferences.Access_token)
                    .build();
        }
        final Request request = new Request.Builder()
                .url(GloabalURI.baseURI + GloabalURI.addArticle)
                .addHeader("access_token", Mypreferences.Access_token)
                .post(requestBody)
                .build();
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(okhttp3.Call call, final IOException e) {
                AddArticleActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("akram","response : "+e);
                        isBack = true;
                        onBackPressed();
                        btn_continue.setVisibility(View.GONE);
                        view_p_bar.setVisibility(View.VISIBLE);
                        //Toast.makeText(AddArticleActivity.this, "nah", Toast.LENGTH_SHORT).show();
                        view.setVisibility(View.GONE);
                        view.hide();
                    }
                });
            }

            @Override
            public void onResponse(okhttp3.Call call, final Response response) throws IOException {
                AddArticleActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btn_continue.setVisibility(View.GONE);
                        view_p_bar.setVisibility(View.VISIBLE);
                        Log.v("akram","response : "+response);
                        try {

                            isBack = true;
                            JSONObject postObject = new JSONObject(response.body().string());
                            if (postObject.getString("success").equals("true")) {
                                CustomSnackBar.toast(AddArticleActivity.this, "Article added successfully");
                                onBackPressed();
                            } else {
                                String s = "" + postObject.getString("message");
                                CustomSnackBar.toast(AddArticleActivity.this, s);
                            }

                            view.setVisibility(View.GONE);
                            view.hide();

                        } catch (IOException e) {
                            e.printStackTrace();
                            view.setVisibility(View.GONE);
                            view.hide();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            view.setVisibility(View.GONE);
                            view.hide();
                        }
                    }
                });
            }
        });
    }


    public void ClickImageFromCamera() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String date = DateFormat.getDateTimeInstance().format(new Date());

        filePath = date + "temp.jpg";

        File f = new File(Environment.getExternalStorageDirectory(), filePath);

        Uri photoURI = FileProvider.getUriForFile(AddArticleActivity.this,
                BuildConfig.APPLICATION_ID +
                        ".provider", f);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        startActivityForResult(intent, 0);

    }

    public void GetImageFromGallery() {

        Intent GalIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(GalIntent, "Select Image From Gallery"), 2);

    }


    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddArticleActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    ClickImageFromCamera();
                    dialog.dismiss();

                } else if (options[item].equals("Choose from Gallery")) {

                    GetImageFromGallery();

                    dialog.dismiss();

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private String getPath(Uri uri) {

        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {

            File f = new File(Environment.getExternalStorageDirectory().toString());
            // Static_variable.uploaded_report_path_str=f.toString();
            for (File temp : f.listFiles()) {

                if (temp.getName().equals(filePath)) {
                    f = temp;
                    break;
                }
            }
            img_cross.setVisibility(View.VISIBLE);
            tvAddPicture.setText(getResources().getString(R.string.change_photos));
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

            profilePicPath = f.getAbsolutePath();
            // imageView.setImageBitmap(bitmap);

            Glide.with(AddArticleActivity.this)
                    .load(profilePicPath)
                    .into(imageView);


        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {

            if (data != null) {

                uri = data.getData();
                tvAddPicture.setText("Change Picture");
                img_cross.setVisibility(View.VISIBLE);
                Uri selectedImageUri = data.getData();
                String tempPath = getPath(selectedImageUri);

                //  Static_variable.usr_uploaded_img__str = tempPath;

                bitmap = BitmapFactory.decodeFile(tempPath);

                profilePicPath = tempPath;

                Glide.with(AddArticleActivity.this)
                        .load(profilePicPath)
                        .into(imageView);

                lin_img.setVisibility(View.GONE);
                rl_image.setVisibility(View.VISIBLE);


              /*  imageView.setImageBitmap(bitmap);
                img_back.setImageBitmap(bitmap);*/
                // ImageCropFunction();

            }

        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            if (data != null) {

                Bundle bundle = data.getExtras();

                bitmap = bundle.getParcelable("data");

                imageView.setImageBitmap(bitmap);
                lin_img.setVisibility(View.GONE);
                rl_image.setVisibility(View.VISIBLE);


            }
        }
    }

    @Override
    public void onBackPressed() {

        if (!isBack) {
            exitDialog();
        } else {
            Intent intent = new Intent(AddArticleActivity.this, MainActivity.class);

            startActivity(intent);
            overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
            finish();
        }

    }


    public Dialog exitDialog() {

        LayoutInflater factory = LayoutInflater.from(AddArticleActivity.this);
        final View view = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(AddArticleActivity.this).create();
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view.findViewById(R.id.tvMsg);
        tvMsg.setText(getResources().getString(R.string.dialog_backDesc));

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(AddArticleActivity.this, MainActivity.class);

                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
                finish();

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;

    }


}
