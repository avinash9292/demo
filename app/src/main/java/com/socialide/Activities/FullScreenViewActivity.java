package com.socialide.Activities;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.socialide.Adapters.FullScreenImageAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.AsyncTask.ImageConvertAsync;
import com.socialide.Fragments.ActivityFragment;
import com.socialide.Fragments.MyActivityFragment;
import com.socialide.Fragments.MyPastEvents;
import com.socialide.Fragments.OtherUserActivityFragment;
import com.socialide.Fragments.OtherUserPastEvents;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.SimpleGestureFilter;
import com.socialide.Model.BeanForActivities;
import com.socialide.Model.BeanForActivityImages;
import com.socialide.Model.BeanForUploadImage;
import com.socialide.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class FullScreenViewActivity extends Activity implements AsyncRequest.OnAsyncRequestComplete, SimpleGestureFilter.SimpleGestureListener {
    ArrayList<String> al;
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    CardView btnClose, btn_delete;
    MyTextView txt_position;
    BeanForActivities b;
    int positions;
    AVLoadingIndicatorView view;
    ArrayList<BeanForActivityImages> al1 = new ArrayList<>();
    int i;
    private SimpleGestureFilter detector;
    RelativeLayout mainLayout;
    ImageView imgArrowLeft, imgArrowRight, imgShare;
    String text, shareImage;
    int imagePosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_slide_with_full_screen);
        Log.v("akram", "onCreate = " + positions);
        viewPager = (ViewPager) findViewById(R.id.pager);
        btnClose = (CardView) findViewById(R.id.btnClose);
        btn_delete = (CardView) findViewById(R.id.btn_delete);
        txt_position = (MyTextView) findViewById(R.id.txt_position);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);

        imgArrowLeft = (ImageView) findViewById(R.id.imgArrowLeft);
        imgArrowRight = (ImageView) findViewById(R.id.imgArrowRight);
        imgShare = (ImageView) findViewById(R.id.imgShare);

        detector = new SimpleGestureFilter(this, this);
        i = getIntent().getIntExtra("pos", 0);

        if (getIntent().getIntExtra("type", 0) == 1) {

            b = MyActivityFragment.activity_list1.get(i);

        } else if (getIntent().getIntExtra("type", 0) == 2) {

            b = OtherUserActivityFragment.activity_list.get(i);

        } else if (getIntent().getIntExtra("type", 0) == 3) {
            positions = i;
            btn_delete.setVisibility(View.GONE);
            al = new ArrayList<>();
            for (int i1 = 0; i1 < AddEventPhotosActivity.beanForUploadImages.size(); i1++) {
                BeanForUploadImage beanForUploadImage = AddEventPhotosActivity.beanForUploadImages.get(i1);
                al.add(beanForUploadImage.getUrl());
            }
        } else {

            b = ActivityFragment.activity_list1.get(i);

        }
        /*viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Toast.makeText(FullScreenViewActivity.this, "Down", Toast.LENGTH_SHORT).show();
                        break;
                    case MotionEvent.ACTION_UP:
                        //  Toast.makeText(FullScreenViewActivity.this, "Up", Toast.LENGTH_SHORT).show();

                        break;

                }
                return false;
            }
        });
*/

        if (getIntent().getIntExtra("type", 0) != 3) {
            al = new ArrayList<>();
            if (b.getActivity_type().equals("article")) {

                al.add("" + b.getArticle_image());

            } else {
                al1 = b.getEvent_photos();

                if (al1.get(0).getType().equals(Mypreferences.User_Type) && al1.get(0).getUserid().equals(Mypreferences.User_id)) {
                    if (getIntent().getIntExtra("type", 0) != 2) {
                        btn_delete.setVisibility(View.VISIBLE);
                    } else {
                        btn_delete.setVisibility(View.GONE);

                    }
                } else {

                    btn_delete.setVisibility(View.GONE);

                }
                for (int k = 0; k < al1.size(); k++) {

                    al.add(al1.get(k).getImageUrl());

                }
            }
        }
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().getIntExtra("type", 0) != 3) {
                    if (b.getActivity_type().equals("article")) {
                        text = b.getDescription();
                        shareImage = b.getArticle_image();
                    } else {

                        text = b.getMessage();
                        al1 = b.getEvent_photos();
                        shareImage = al1.get(positions).getImageUrl();

                    }
                } else {

                    BeanForUploadImage beanForUploadImage = AddEventPhotosActivity.beanForUploadImages.get(positions);
                    shareImage = beanForUploadImage.getUrl();
                    text = getIntent().getStringExtra("disc");
                }
                ArrayList<String> strings = new ArrayList<String>();
                strings.add(shareImage);

                new ImageConvertAsync(strings, text, FullScreenViewActivity.this).execute();

            }
        });

        txt_position.setText(1 + " / " + al.size());
        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, al);

        viewPager.setAdapter(adapter);

        // displaying selected image first
        if (getIntent().getIntExtra("type", 0) != 3) {
            viewPager.setCurrentItem(0);
        } else {
            viewPager.setCurrentItem(i);
        }
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int pos = position + 1;
                imagePosition = position;

                Log.v("akram", "position selected = " + position);
                Log.v("akram", "al size selected = " + al.size());

                if (position == 0 && al.size() > 1) {
                    imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow_left));
                    imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow));
                } else if (position == 0 && al.size() == 1) {
                    imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow_left));
                    imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow));
                } else if ((position + 1) == al.size()) {
                    imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
                    imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow));
                } else {
                    imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
                    imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow));
                }

                txt_position.setText(position + 1 + " / " + al.size());
                positions = position;

                if (getIntent().getIntExtra("type", 0) != 3) {

                    if (al1.get(position).getType().equals(Mypreferences.User_Type) && al1.get(position).getUserid().equals(Mypreferences.User_id)) {
                        if (getIntent().getIntExtra("type", 0) != 2) {
                            btn_delete.setVisibility(View.VISIBLE);
                        } else {
                            btn_delete.setVisibility(View.GONE);
                        }
                    } else {
                        btn_delete.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        imgArrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (positions < al.size() - 1) {
                    viewPager.setCurrentItem(positions + 1);
                }
            }
        });

        imgArrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (al.size() > 0) {
                    if (viewPager.getCurrentItem() != 0) {
                        viewPager.setCurrentItem(positions - 1);
                    }
                }
            }
        });


        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog(FullScreenViewActivity.this, FullScreenViewActivity.this);
            }
        });

        if (getIntent().getIntExtra("type", 0) == 3) {

        }
        Log.v("akram", "i == " + i);
        int type = getIntent().getIntExtra("type", 0);

        Log.v("akram", "type == " + type);

        if (al.size() == 1) {
            imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow_left));
            imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow));
        } else if (type != 3) {
            imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow_left));
            imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow));
        } else if (al.size() > 1) {
            if (i == 0) {
                imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow_left));
                imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow));
            } else if ((i + 1) == al.size()) {
                imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow));
                imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
            } else {
                imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow));
                imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_left));
            }
        }
    }


    private ArrayList<NameValuePair> getParams(String postid, String postType) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//           params.add(new BasicNameValuePair("userid", ""+ Mypreferences.User_id));
//              params.add(new BasicNameValuePair("type", Mypreferences.User_Type));

        params.add(new BasicNameValuePair("eventId", "" + postid));
        params.add(new BasicNameValuePair("image_url", "" + postType));
        params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));
        //   params.add(new BasicNameValuePair("postType", ""+postType));
//		params.add(new BasicNameValuePair("type", Mypreferences.User_Type));

        return params;
    }


    public Dialog deleteDialog(final Context context, final Activity activity) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View view1 = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(context).create();
        MyTextView txt_yes = (MyTextView) view1.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view1.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view1.findViewById(R.id.tvMsg);
        MyTextView tvTitleDialog = (MyTextView) view1.findViewById(R.id.tvTitleDialog);
        tvMsg.setText(getResources().getString(R.string.dialog_deleteImageDesc));
        tvTitleDialog.setText(getResources().getString(R.string.dialog_deleteImageTitle));
        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("akram", "position on delete = " + positions);
                AsyncRequest getPosts = new AsyncRequest(FullScreenViewActivity.this, "POST", getParams(b.getEvent_id(), b.getEvent_photos().get(positions).getImageUrl()), view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.DELETE_EVENT_PHOTO);
                alertDialoge.dismiss();
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view1);
        alertDialoge.show();
        return alertDialoge;

    }

    @Override
    public void onBackPressed() {

        if (getIntent().getIntExtra("type", 0) != 2 && getIntent().getIntExtra("type", 0) != 3) {


            if (al1.size() != 0) {
                b.setEvent_photos(al1);

                if (getIntent().getIntExtra("type", 0) == 1) {

                    MyActivityFragment.activity_list1.remove(i);
                    MyActivityFragment.activity_list1.add(i, b);
                } else {
                    ActivityFragment.activity_list1.remove(i);
                    ActivityFragment.activity_list1.add(i, b);
                }
            } else {
                if (getIntent().getIntExtra("type", 0) == 1) {

                    MyActivityFragment.activity_list1.remove(i);
                } else {

                    ActivityFragment.activity_list1.remove(i);

                }
            }
        }
        Log.v("akram", "onfinish before " + positions);

        finish();
        Log.v("akram", "onfinish after = " + positions);
    }


    @Override
    public void asyncResponse(String response) {

        Log.v("akram", "resposen = " + response);
        JSONObject postObject = null;
        try {
            postObject = new JSONObject(response);

            if (postObject.getString("success").equals("true")) {

                if (positions < al.size()) {
                    al.remove(positions);
                    al1.remove(positions);

                    if (al.size() == 0) {
                        adapter.notifyDataSetChanged();
                        onBackPressed();
                    } else {

                        txt_position.setText(positions + 1 + " / " + al.size());
                        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
                                al);
                        if (al.size() == 1) {
                            imgArrowLeft.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow_left));
                            imgArrowRight.setImageDrawable(getResources().getDrawable(R.drawable.ic_inactive_arrow));
                            positions = 0;
                        }
                        viewPager.setAdapter(adapter);
                        viewPager.setCurrentItem(positions);
                        adapter.notifyDataSetChanged();
                    }
                }

            }
        } catch (JSONException e) {
            Log.v("akram", "sssss  " + postObject);
            e.printStackTrace();
        }
        Log.e("eeeeee", "sssss  " + postObject);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT:
                str = "Swipe Right";
                break;
            case SimpleGestureFilter.SWIPE_LEFT:
                str = "Swipe Left";
                break;
            case SimpleGestureFilter.SWIPE_DOWN:
                str = "Swipe Down";
                break;
            case SimpleGestureFilter.SWIPE_UP:
                str = "Swipe Up";
                break;

        }
        // Toast.makeText(this, str, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDoubleTap() {

    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


//
    //  {"success":"true","message":"Scheduled event details fetched successfully.","result":{"event_id":"15","profile_image":null,"event_name":"inform the authorities about illegal activities ","event_title":"telling someone you love them via the phone ","event_date":"21 May 2017","contact_details":{"professional_name":"AVINASH TIWARI","professional_profession":null,"professional_email":"aa@aa.aa","professional_mobile_no":null},"userid":"183","type":"Professional","sharecontact":"1"}}


}
