package com.socialide.Activities;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.socialide.Fragments.ActivityFragment;
import com.socialide.Fragments.EventDetails;
import com.socialide.Fragments.MyProfile;
import com.socialide.Fragments.ProPendingEventDetails;
import com.socialide.Fragments.SettingsFragment;
import com.socialide.R;

public class DetailsActivity extends FragmentActivity  implements ProPendingEventDetails.MyListener {

    public void onMyCancel() {
        finish();
    }
public static int i=0;
    public static String type="s";
   public static Fragment f=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        i=1;


        if (getIntent().getStringExtra("f").equals("f")){
            goToFragment(f);


        }

    }
    public void goToSettings(){
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new SettingsFragment(), "Settings")
                .addToBackStack(null)
                .commit();
    }


    public void gotoActivity(){
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new ActivityFragment(), "Settings")
                .addToBackStack(null)
                .commit();
    }

    public void goToMyprofile(){
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new MyProfile(), "MyProfile")
                .addToBackStack(null)
                .commit();
    }

    public void goToDetails(){
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment, new EventDetails(), "EventDetails")
                .addToBackStack(null)
                .commit();
    }



    public void goToFragment(Fragment fragment){


        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment,fragment);
        fragmentTransaction.commitNowAllowingStateLoss();

    }

    @Override
    public void onBackPressed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

            }
        }, 0);


       // this.finish();


    }



}
