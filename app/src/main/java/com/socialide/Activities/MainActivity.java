package com.socialide.Activities;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.socialide.Adapters.ViewPagerAdapter;
import com.socialide.Fragments.ActivityFragment;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.MeFragment;
import com.socialide.Fragments.SearchProfessional1;
import com.socialide.Fragments.TopUsersFragment;
import com.socialide.Helper.DialogClass;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Fragment currentFragment;
    private ViewPagerAdapter adapter;
    private AHBottomNavigationAdapter navigationAdapter;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
    private boolean useMenuResource = true;
    private int[] tabColors;
    MyTextView txt_header;
    public static int back_handle = 0;
    public static ImageView img_setting;
    private Handler handler = new Handler();
    RelativeLayout rl;
    CoordinatorLayout.LayoutParams linearParams, linearParams1, linearParams2;
    FrameLayout f;
    LoginActivity l;

    ImageView imgUploadPhotos;
    private AHBottomNavigationViewPager viewPager;
    private AHBottomNavigation bottomNavigation;
    private FloatingActionButton floatingActionButton;
    public static int tabPosition = 0;
    public static int checkClick = 0;
    public static RelativeLayout badge_layout1, relative_layout;
    public static Button button1;
    public static MyTextView tvNotificationCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rl = (RelativeLayout) findViewById(R.id.ll_fake_actionbar);
        initUI();
        l = new LoginActivity();
    }

    public void goToFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //  fragmentTransaction.setCustomAnimations(R.anim.slide_to_left, R.anim.slide_from_right);
        fragmentTransaction.add(R.id.id_holder, fragment, "Fragment");
        fragmentTransaction.commit();
    }

    private void initUI() {
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        relative_layout = (RelativeLayout) findViewById(R.id.relative_layout);
        button1 = (Button) findViewById(R.id.button1);
        tvNotificationCount = (MyTextView) findViewById(R.id.tvNotificationCount);
        img_setting = (ImageView) findViewById(R.id.img_setting);
        txt_header = (MyTextView) findViewById(R.id.txt_header);
        imgUploadPhotos = (ImageView) findViewById(R.id.imgUploadPhotos);
        badge_layout1 = (RelativeLayout) findViewById(R.id.badge_layout1);
        relative_layout = (RelativeLayout) findViewById(R.id.relative_layout);

        button1 = (Button) findViewById(R.id.button1);

        imgUploadPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        if (Mypreferences.User_Type.equalsIgnoreCase("Professional")) {

                            Intent intent = new Intent(MainActivity.this, AddArticleActivity.class);
                            intent.putExtra("position", EventFragment.position);
                            intent.putExtra("from", "tab");

                   /* ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                 */
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                            finish();
                        } else {
                            selectImage();
                        }
                    }
                }, 1);

            }
        });


        txt_header.setText(getResources().getString(R.string.header_activity));
        viewPager = (AHBottomNavigationViewPager) findViewById(R.id.view_pager);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating_action_button);
        f = (FrameLayout) findViewById(R.id.id_holder);
        linearParams = new CoordinatorLayout.LayoutParams(
                new CoordinatorLayout.LayoutParams(
                        CoordinatorLayout.LayoutParams.MATCH_PARENT,
                        CoordinatorLayout.LayoutParams.WRAP_CONTENT));

        linearParams2 = new CoordinatorLayout.LayoutParams(
                new CoordinatorLayout.LayoutParams(
                        CoordinatorLayout.LayoutParams.MATCH_PARENT,
                        CoordinatorLayout.LayoutParams.WRAP_CONTENT));

        linearParams1 = new CoordinatorLayout.LayoutParams(
                new CoordinatorLayout.LayoutParams(
                        CoordinatorLayout.LayoutParams.MATCH_PARENT,
                        CoordinatorLayout.LayoutParams.WRAP_CONTENT));
        if (useMenuResource) {
            tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);

            if (Mypreferences.User_Type.equals("Organization")) {
                navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu_organization);

            } else {
                navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu);

            }
            navigationAdapter.setupWithBottomNavigation(bottomNavigation, tabColors);
        }

        bottomNavigation.setAccentColor(Color.parseColor("#3fb4b5"));
        bottomNavigation.manageFloatingActionButtonBehavior(floatingActionButton);
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setAnimation(null);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setColoredModeColors(getResources().getColor(R.color.primary), getResources().getColor(R.color.primary));

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                Log.v("akram", "checkClick = " + checkClick);

                if (position == 0) {
                    EventFragment.checkTabPosition = 0;
                    MeFragment.checkTabPosition = 0;
                    badge_layout1.setVisibility(View.GONE);
                    tabPosition = 0;
                    imgUploadPhotos.setVisibility(View.VISIBLE);
                    img_setting.setVisibility(View.GONE);
                    txt_header.setText(getResources().getString(R.string.header_activity));
                    goToFragment(new ActivityFragment());

                    linearParams.setMargins(0, 0, 0, 0);
                    rl.setLayoutParams(linearParams);
                    rl.requestLayout();


                } else if (position == 1) {
                    MeFragment.checkTabPosition = 0;
                    badge_layout1.setVisibility(View.GONE);
                    tabPosition = 1;
                    img_setting.setVisibility(View.GONE);
                    imgUploadPhotos.setVisibility(View.GONE);

                    linearParams.setMargins(0, 0, 0, 0);
                    rl.setLayoutParams(linearParams);
                    rl.requestLayout();


                    txt_header.setText(getResources().getString(R.string.header_events));

                    goToFragment(new EventFragment());

                } else if (position == 2) {
                    MeFragment.checkTabPosition = 0;
                    imgUploadPhotos.setVisibility(View.GONE);
                    badge_layout1.setVisibility(View.GONE);
                    img_setting.setVisibility(View.GONE);
                    EventFragment.checkTabPosition = 0;
                    if (!Mypreferences.User_Type.equals("Organization")) {
                        Intent intent = new Intent(MainActivity.this, AddEventPhotosActivity.class);
                        intent.putExtra("position", EventFragment.position);
                        intent.putExtra("from", "tab");
                        startActivity(intent);

                        overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                        finish();
                    } else {
                        tabPosition = 2;
                        linearParams.setMargins(0, 0, 0, 0);
                        rl.setLayoutParams(linearParams);
                        rl.requestLayout();
                        txt_header.setText(getResources().getString(R.string.header_search));
                        Log.v("akram", "is fil " + SearchProfessional1.is_filter_changed);
                        goToFragment(new SearchProfessional1());

                    }


                    /*img_setting.setVisibility(View.GONE);

                    txt_header.setText("Add Event Photos");
                    linearParams.setMargins(0, 0, 0, 0);
                    rl.setLayoutParams(linearParams);
                    rl.requestLayout();


//
//                    linearParams1.setMargins(0, 96, 0, 0);
//                    viewPager.setLayoutParams(linearParams1);
//                    viewPager.requestLayout();
//
//                    linearParams2.setMargins(0, 96, 0, 0);
//                    f.setLayoutParams(linearParams2);
//                    f.requestLayout();


                    goToFragment(new AddEventPhotos())*/
                    ;

                } else if (position == 3) {
                    MeFragment.checkTabPosition = 0;
                    imgUploadPhotos.setVisibility(View.GONE);
                    badge_layout1.setVisibility(View.GONE);
                    EventFragment.checkTabPosition = 0;

                    tabPosition = 3;

                    img_setting.setVisibility(View.GONE);

                    txt_header.setText(getResources().getString(R.string.header_top_user));
                    linearParams.setMargins(0, 0, 0, 0);
                    rl.setLayoutParams(linearParams);
                    rl.requestLayout();

//
//                    linearParams1.setMargins(0, 96, 0, 0);
//                    viewPager.setLayoutParams(linearParams1);
//                    viewPager.requestLayout();
//
//                    linearParams2.setMargins(0, 96, 0, 0);
//                    f.setLayoutParams(linearParams2);
//                    f.requestLayout();


                    goToFragment(new TopUsersFragment());

                } else if (position == 4) {
                    imgUploadPhotos.setVisibility(View.GONE);
                    badge_layout1.setVisibility(View.VISIBLE);

                    EventFragment.checkTabPosition = 0;

                    tabPosition = 4;

                    linearParams.setMargins(0, 0, 0, 0);
                    rl.setLayoutParams(linearParams);
                    rl.requestLayout();
                    txt_header.setText(getResources().getString(R.string.header_profile));
                    //img_setting.setVisibility(View.VISIBLE);

//                    linearParams1.setMargins(0, 106, 0, 0);
//                    viewPager.setLayoutParams(linearParams1);
//                    viewPager.requestLayout();
//
//                    linearParams2.setMargins(0, 106, 0, 0);
//                    f.setLayoutParams(linearParams2);
//                    f.requestLayout();
                    goToFragment(new MeFragment());

                }
                return true;
            }
        });


        img_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(MainActivity.this,SettingActivity.class));

                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(new Intent(MainActivity.this, SettingActivity.class), options.toBundle());
            }
        });

		/*
        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
			@Override public void onPositionChange(int y) {
				Log.d("DemoActivity", "BottomNavigation Position: " + y);
			}
		});
		*/

        //  viewPager.setOffscreenPageLimit(0);
        viewPager.setOffscreenPageLimit(4);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        currentFragment = adapter.getCurrentFragment();

//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // Setting custom colors for notification
//                AHNotification notification = new AHNotification.Builder()
//                        .setText(":)")
//                        .setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.btn_background))
//                        .setTextColor(ContextCompat.getColor(MainActivity.this, R.color.btn_text_color))
//                        .build();
//                bottomNavigation.setNotification(notification, 1);
//                Snackbar.make(bottomNavigation, "Snackbar with bottom navigation",
//                        Snackbar.LENGTH_SHORT).show();
//
//            }
//        }, 3000);

        //bottomNavigation.setDefaultBackgroundResource(R.drawable.bottom_navigation_background);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigation.setCurrentItem(tabPosition);
    }

    @Override
    public void onBackPressed() {

        DialogClass dialogClass = new DialogClass();
        dialogClass.exitDialog(MainActivity.this, MainActivity.this, "Do you want to exit?", "Exit", "exit", null);

    }


    public static void popup1(Context c) {

        final PopupWindow mPopupWindow;

        final View popupView = LayoutInflater.from(c).inflate(R.layout.dialog_for_reapeat_availability, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mPopupWindow = new PopupWindow(
                popupView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );


        mPopupWindow.setBackgroundDrawable(new ColorDrawable(
                Color.TRANSPARENT));

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }


        // Get a reference for the custom view close button
        MyButton done = (MyButton) popupView.findViewById(R.id.btn_apply);
        ImageView img_cross = (ImageView) popupView.findViewById(R.id.img_cross);

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();

            }
        });

        mPopupWindow.setAnimationStyle(R.style.animationName);
        //   mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);

    }


    private void selectImage() {

        final CharSequence[] options = {"Write an Article", "Add Event Photos", "Cancel"};

        LayoutInflater factory = LayoutInflater.from(this);
        final View view = factory.inflate(R.layout.dialog_article_event, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(this).create();

        LinearLayout btnWriteArticle = (LinearLayout) view.findViewById(R.id.btnWriteArticle);
        LinearLayout btnEvent = (LinearLayout) view.findViewById(R.id.btnEvent);

        btnWriteArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddArticleActivity.class);
                intent.putExtra("position", EventFragment.position);
                intent.putExtra("from", "tab");

                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                finish();
                alertDialoge.dismiss();
            }
        });

        btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, AddEventPhotosActivity.class);
                intent.putExtra("position", EventFragment.position);
                intent.putExtra("from", "tab");


                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                finish();


                alertDialoge.dismiss();

            }
        });
        alertDialoge.setView(view);
        alertDialoge.show();
        /*

        final Dialog custom_pd;
        custom_pd = new Dialog(MainActivity.this);
        custom_pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        custom_pd.setContentView(R.layout.dialog_article_event);
        custom_pd.setCancelable(true);
        custom_pd.setCanceledOnTouchOutside(false);

        LinearLayout btnWriteArticle = custom_pd.findViewById(R.id.btnWriteArticle);
        LinearLayout btnEvent = custom_pd.findViewById(R.id.btnEvent);

        btnWriteArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddArticleActivity.class);
                intent.putExtra("position", EventFragment.position);
                intent.putExtra("from", "tab");

                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                finish();
                custom_pd.dismiss();
            }
        });

        btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, AddEventPhotosActivity.class);
                intent.putExtra("position", EventFragment.position);
                intent.putExtra("from", "tab");


                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);

                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                finish();


                custom_pd.dismiss();

            }
        });

        custom_pd.show();*/

      /*  if (con != null) {
            try {
                custom_pd.show();
            } catch (Exception e) {
                Log.e("Avinash", "" + e.getMessage());
            }
        }*/

/*
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Post");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Write an Article")) {

                    Intent intent = new Intent(MainActivity.this, AddArticleActivity.class);
                    intent.putExtra("position", EventFragment.position);
                    intent.putExtra("from", "tab");

                   *//* ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                 *//*
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                    finish();
                    dialog.dismiss();

                } else if (options[item].equals("Add Event Photos")) {

                    Intent intent = new Intent(MainActivity.this, AddEventPhotosActivity.class);
                    intent.putExtra("position", EventFragment.position);
                    intent.putExtra("from", "tab");

                   *//* ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                 *//*
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                    finish();


                    dialog.dismiss();

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();*/
    }

}
