package com.socialide.Activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Adapters.EventPhotosAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.EventsDeclined;
import com.socialide.Fragments.MyPastEvents;
import com.socialide.Fragments.OtherUserPastEvents;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.Model.BeanForUploadImage;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyEventDetalils extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {
    MyTextView txt_caption, tv_event_name, tvNoPhotos, tv_organisation_name, txt_reason_static, txt_declined_static, tv_event_guests, txt_date, txt_time, txt_email, txt_landline_number, edt_message, txt_reason, txt_declined;
    CircleImageView iv_me_profile_pic;
    Button btn_remove;
    BeanForFetchEvents b;
    AVLoadingIndicatorView view;
    RelativeLayout rl_email, rl_landline, rl_caption;
    RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    EventPhotosAdapter adapter;
    LinearLayout layoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_details_pro_declined);

        iv_me_profile_pic = (CircleImageView) findViewById(R.id.iv_me_profile_pic);
        btn_remove = (Button) findViewById(R.id.btn_remove);

        tv_event_name = (MyTextView) findViewById(R.id.tv_name);
        tv_organisation_name = (MyTextView) findViewById(R.id.tv_organisation_name);
        tv_event_guests = (MyTextView) findViewById(R.id.tv_event_guests);
        txt_date = (MyTextView) findViewById(R.id.txt_date);
        txt_time = (MyTextView) findViewById(R.id.txt_time);
        txt_email = (MyTextView) findViewById(R.id.txt_email);
        tvNoPhotos = (MyTextView) findViewById(R.id.tvNoPhotos);
        txt_landline_number = (MyTextView) findViewById(R.id.txt_landline_number);
        edt_message = (MyTextView) findViewById(R.id.edt_message);
        txt_reason = (MyTextView) findViewById(R.id.txt_reason);
        txt_reason_static = (MyTextView) findViewById(R.id.txt_reason_static);
        txt_declined = (MyTextView) findViewById(R.id.txt_declined);
        txt_declined_static = (MyTextView) findViewById(R.id.txt_declined_static);
        txt_caption = (MyTextView) findViewById(R.id.txt_caption);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutBtn = (LinearLayout) findViewById(R.id.layoutBtn);
        mRecyclerView.setVisibility(View.VISIBLE);

        view = (AVLoadingIndicatorView) findViewById(R.id.avi);
        rl_email = (RelativeLayout) findViewById(R.id.rl_email);
        rl_landline = (RelativeLayout) findViewById(R.id.rl_landline);
        rl_caption = (RelativeLayout) findViewById(R.id.rl_caption);

        rl_landline.setVisibility(View.GONE);
        rl_email.setVisibility(View.GONE);
        txt_landline_number.setVisibility(View.GONE);
        txt_email.setVisibility(View.GONE);

        AddEventPhotosActivity.beanForUploadImages.clear();
        Log.v("akram", "size 2 = " + MyPastEvents.eventImageUrlJson.size());
        JSONArray jsonArray1 = MyPastEvents.eventImageUrlJson.get(EventFragment.position);
        Log.v("akram", "jsonArray  = " + jsonArray1.length());
        for (int i = 0; i < MyPastEvents.eventImageUrlJson.get(EventFragment.position).length(); i++) {
            try {
                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);

                AddEventPhotosActivity.beanForUploadImages.add(new BeanForUploadImage(jsonObject1.getString("image_url"), true));
                //AddEventPhotosActivity.beanForUploadImages.add();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (AddEventPhotosActivity.beanForUploadImages.size() == 0) {
            tvNoPhotos.setVisibility(View.VISIBLE);
        }

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        adapter = new EventPhotosAdapter(this, this, 1);
        mRecyclerView.setAdapter(adapter);

        if (OtherUserPastEvents.checkfragment == 1) {
            layoutBtn.setVisibility(View.GONE);
            b = OtherUserPastEvents.activity_list.get(EventFragment.position);
            txt_caption.setVisibility(View.GONE);
            rl_caption.setVisibility(View.GONE);
        } else {
            txt_caption.setVisibility(View.VISIBLE);
            rl_caption.setVisibility(View.VISIBLE);
            b = MyPastEvents.activity_list1.get(EventFragment.position);
        }


        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(MyEventDetalils.this, FullScreenViewActivity.class);
                i.putExtra("pos", position);

                i.putExtra("type", 3);
                i.putExtra("disc", b.getMessage());
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(MyEventDetalils.this, R.anim.slide_to_right, R.anim.slide_from_left);
                MyEventDetalils.this.startActivity(i, options.toBundle());

            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));
        if (b.getEvent_description().equalsIgnoreCase("")) {
            txt_caption.setText(getResources().getString(R.string.notAvailable));
        } else {
            txt_caption.setText(b.getEvent_description());
        }

        tv_event_name.setText(Html.fromHtml("<b>" + b.getEvent_title() + "</b>"));
        tv_organisation_name.setText("" + b.getEvent_by());
        tv_event_guests.setText("" + b.getEvent_location());

        String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date = datetime.split("T")[0];
        String time = datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" + date);
        txt_landline_number.setText("" + b.getContact_number());
        txt_email.setText("" + b.getEmail());
        edt_message.setText("" + b.getMessage());
        txt_declined.setText("" + b.getDecline_by());
        txt_reason.setText("" + b.getReason());


        txt_declined.setVisibility(View.GONE);
        txt_declined_static.setVisibility(View.GONE);
        txt_reason.setVisibility(View.GONE);
        txt_reason_static.setVisibility(View.GONE);


        DonutProgress d = (DonutProgress) findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, MyEventDetalils.this);

        btn_remove.setText(getResources().getString(R.string.header_addEventPhoto));
        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyEventDetalils.this, AddEventPhotosActivity.class);
                intent.putExtra("eventId", b.getEvent_id());
                intent.putExtra("from", "profile");

                   /* ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                 */
                startActivity(intent);
                overridePendingTransition(R.anim.slide_to_top, R.anim.slide_from_bottom);
                finish();

               /* Intent intent = new Intent(MyEventDetalils.this, AddEventPhotosActivity.class);
                intent.putExtra("eventId", b.getEvent_id());
                intent.putExtra("from", "profile");
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(intent, options.toBundle());*/

            }
        });
    }


    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if (postObject.getString("success").equals("true")) {
                CustomSnackBar.toast(MyEventDetalils.this, postObject.getString("message"));
                EventsDeclined.activity_list1.remove(EventFragment.position);
                EventFragment.isBack = true;

                finish();
                overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

            } else {
                CustomSnackBar.toast(MyEventDetalils.this, postObject.getString("message"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }
}
