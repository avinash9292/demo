package com.socialide.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.EventsPending;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.DeclinePopup;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfessionalPendingEventDetail extends Activity implements AsyncRequest.OnAsyncRequestComplete {
    MyButton btn_decline, btn_accept;

    MyTextView tv_event_name, tv_organisation_name, tv_event_guests, txt_date, txt_time, txt_email, txt_contact, edt_message;
    CircleImageView iv_me_profile_pic;
    AVLoadingIndicatorView view;
    AsyncRequest getPosts;
    static RelativeLayout mRelativeLayout;
    static PopupWindow mPopupWindow;
    RelativeLayout rl_landline;
    boolean shareContact = false;
    BeanForFetchEvents b = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_details_pro_pending);

        btn_decline = (MyButton) findViewById(R.id.btn_decline);
        btn_accept = (MyButton) findViewById(R.id.btn_accept);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);

        iv_me_profile_pic = (CircleImageView) findViewById(R.id.iv_me_profile_pic);

        tv_event_name = (MyTextView) findViewById(R.id.tv_name);


        rl_landline = (RelativeLayout) findViewById(R.id.rl_landline);

        tv_organisation_name = (MyTextView) findViewById(R.id.tv_organisation_name);
        tv_event_guests = (MyTextView) findViewById(R.id.tv_event_guests);
        txt_date = (MyTextView) findViewById(R.id.txt_date);
        txt_time = (MyTextView) findViewById(R.id.txt_time);
        txt_email = (MyTextView) findViewById(R.id.txt_email);
        txt_contact = (MyTextView) findViewById(R.id.txt_contact);
        edt_message = (MyTextView) findViewById(R.id.edt_message);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);


        b = EventsPending.activity_list1.get(EventFragment.position);
        tv_event_name.setText(Html.fromHtml("<b>" + b.getEvent_title() + "</b>"));

        DonutProgress d = (DonutProgress) findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, ProfessionalPendingEventDetail.this);
        tv_organisation_name.setText("" + b.getEvent_by());
        tv_event_guests.setText("" + b.getEvent_location());

        txt_contact.setText("" + b.getContact_number());
        txt_email.setText("" + b.getEmail());

        SpannableString content = new SpannableString(b.getContact_number());
        content.setSpan(new UnderlineSpan(), 0, b.getContact_number().length(), 0);
        txt_contact.setText(content);

        SpannableString content1 = new SpannableString(b.getEmail());
        content1.setSpan(new UnderlineSpan(), 0, b.getEmail().length(), 0);
        txt_email.setText(content1);

        edt_message.setText("" + b.getMessage());


        String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date = datetime.split("T")[0];
        String time = datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" + date);

        btn_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeclinePopup.popup(ProfessionalPendingEventDetail.this, ProfessionalPendingEventDetail.this, mRelativeLayout, b.getEvent_id(), view);
            }
        });


        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));

                getPosts = new AsyncRequest(ProfessionalPendingEventDetail.this, "POST", params, view);

                shareContact();
            }
        });

    }

    @Override
    public void asyncResponse(String response) {
        Log.v("akram", "response = " + response);
        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.getString("success").equals("true")) {

                if (postObject.getString("message").equalsIgnoreCase("Event accept successfully.")) {
                    String shareContactStr = "";
                    if (shareContact) {
                        ArrayList<NameValuePair> params = new ArrayList<>();
                        params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                        params.add(new BasicNameValuePair("share_contact", shareContactStr));

                        AsyncRequest getPosts = new AsyncRequest(ProfessionalPendingEventDetail.this, "POST", params, view);
                        getPosts.execute(GloabalURI.baseURI + GloabalURI.updateShareContact);

                    } else {
                        CustomSnackBar.toast(ProfessionalPendingEventDetail.this, postObject.getString("message"));
                        EventsPending.activity_list1.remove(EventFragment.position);

                        EventFragment.isBack = true;

                        finish();
                        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
                    }

                } else /*if (postObject.getString("api_name").equalsIgnoreCase("professionalShareContact"))*/ {
                    CustomSnackBar.toast(ProfessionalPendingEventDetail.this, postObject.getString("message"));
                    Toast.makeText(this, "" + postObject.getString("message"), Toast.LENGTH_SHORT).show();
                    EventsPending.activity_list1.remove(EventFragment.position);

                    EventFragment.isBack = true;

                    finish();
                    overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
                }
            } else {
                CustomSnackBar.toast(ProfessionalPendingEventDetail.this, postObject.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Dialog shareContact() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View view = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(this).create();
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view.findViewById(R.id.tvMsg);
        MyTextView tvTitleDialog = (MyTextView) view.findViewById(R.id.tvTitleDialog);
        tvMsg.setText("Do you want to share contact details with " + b.getEvent_by() + " for event " + b.getEvent_title() + "?");
        tvTitleDialog.setText("Share Contact");

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // NotificationActivity notificationActivity = new NotificationActivity();
                shareContact = true;

                getPosts.execute(GloabalURI.baseURI + GloabalURI.ACCEPT_AN_EVENT);
                alertDialoge.dismiss();
            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareContact = false;
                getPosts.execute(GloabalURI.baseURI + GloabalURI.ACCEPT_AN_EVENT);
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }
}
