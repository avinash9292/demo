package com.socialide.Activities;

import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.AsyncTask.ImageConvertAsync;
import com.socialide.Fragments.MyActivityFragment;
import com.socialide.Fragments.OtherUserActivityFragment;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForActivities;
import com.socialide.Model.BeanForActivityImages;
import com.socialide.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PostDetails1 extends AppCompatActivity implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete {
    BeanForActivities b;
    MyActivityFragment f;
    int count = 0;
    public ImageView img_share, img_like, iv_activity_event, img_arrow;
    public MyTextView tv_event_name;
    public RoundedImageView iv_event_profile_pic;
    DonutProgress d, d1;
    public MyTextView tv_event_time;
    public MyTextView tv_event_description;
    public MyTextView txt_share, txt_count;
    ProgressBar view_like;
    RelativeLayout rl;
    Intent sharingIntent;
    public MyTextView txt_likes, txt_like, txt_loading;
    private DisplayImageOptions options;
    int type = 0;
    ArrayList<Uri> filesUri;
    Uri uri;
    String url = "";
    ArrayList<String> imgArr;
    String text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_details);

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profilepic)
                .showImageOnFail(R.drawable.ic_cross)
                .resetViewBeforeLoading(true)
//.cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        iv_event_profile_pic = (RoundedImageView) findViewById(R.id.iv_event_profile_pic);
        tv_event_time = (MyTextView) findViewById(R.id.tv_event_time);
        tv_event_description = (MyTextView) findViewById(R.id.tv_event_description);
        tv_event_name = (MyTextView) findViewById(R.id.tv_name);
        txt_likes = (MyTextView) findViewById(R.id.txt_likes);
        txt_like = (MyTextView) findViewById(R.id.txt_like);
        txt_loading = (MyTextView) findViewById(R.id.txt_loading);
        img_share = (ImageView) findViewById(R.id.img_share);
        img_like = (ImageView) findViewById(R.id.img_like);
        img_arrow = (ImageView) findViewById(R.id.img_arrow);
        iv_activity_event = (ImageView) findViewById(R.id.iv_activity_event);
        txt_share = (MyTextView) findViewById(R.id.txt_share);
        txt_count = (MyTextView) findViewById(R.id.txt_count);
        view_like = (ProgressBar) findViewById(R.id.view_like);
        d = (DonutProgress) findViewById(R.id.loading);
        d1 = (DonutProgress) findViewById(R.id.loading1);

        txt_share.setOnClickListener(this);
        img_share.setOnClickListener(this);

        img_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txt_like.setEnabled(false);

                view_like.setVisibility(View.VISIBLE);
                img_like.setVisibility(View.GONE);

                AsyncRequest getPosts = new AsyncRequest(PostDetails1.this, "POST", getParams(b.getActivity_id(), b.getActivity_type()));
                getPosts.execute(GloabalURI.baseURI + GloabalURI.LIKE_DISLIKE);

            }
        });
        img_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                report_popup();

            }
        });


        int i = getIntent().getIntExtra("pos", 0);
        type = getIntent().getIntExtra("type", 2);

        if (type == 2) {
            b = OtherUserActivityFragment.activity_list.get(i);
        } else {
            b = MyActivityFragment.activity_list1.get(i);
        }


        txt_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (txt_like.getText().toString().equals("Like")) {
//                    txt_like.setText("Liked");
//                    img_like.setImageResource(R.drawable.like_green_icon);
//                    txt_like.setTextColor(Color.parseColor("#3fb4b5"));
//                } else {
//
//                    txt_like.setText("Like");
//                    img_like.setImageResource(R.drawable.like_gray_icon);
//                    txt_like.setTextColor(Color.parseColor("#727272"));
//
//
//                }
                txt_like.setEnabled(false);

                view_like.setVisibility(View.VISIBLE);
                img_like.setVisibility(View.GONE);

                AsyncRequest getPosts = new AsyncRequest(PostDetails1.this, "POST", getParams(b.getActivity_id(), b.getActivity_type()));
                getPosts.execute(GloabalURI.baseURI + GloabalURI.LIKE_DISLIKE);


            }
        });


        if (!b.getIsUserLikedPost().equals("0")) {
            txt_like.setText(getResources().getString(R.string.liked));
            img_like.setImageResource(R.drawable.like_green_icon);
            txt_like.setTextColor(Color.parseColor("#3fb4b5"));
        } else {

            txt_like.setText(getResources().getString(R.string.like));
            img_like.setImageResource(R.drawable.like_gray_icon);
            txt_like.setTextColor(Color.parseColor("#727272"));


        }

        String[] dater = b.getActivity_date_time().trim().split(" ");


        if (TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]).equals("false")) {
            tv_event_time.setText(getResources().getString(R.string.added)+" " + dater[0] + " " + dater[1] + " " + dater[2]);

        } else {
            tv_event_time.setText(getResources().getString(R.string.added)+" " + TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]));
        }


        if (b.getActivity_type().equals("article")) {

            if (!b.getArticle_image().equals("")) {
                iv_activity_event.setVisibility(View.VISIBLE);
                com.socialide.Helper.ImageLoader.image(b.getArticle_image(), iv_activity_event, d1, PostDetails1.this);

            } else {
                iv_activity_event.setVisibility(View.GONE);

            }
            com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, iv_event_profile_pic, d, PostDetails1.this);

            txt_count.setVisibility(View.GONE);

            // tv_event_time.setText("Added " +  TimeConveter.agotime(b.getArticle_date(), b.getTime()));


            tv_event_description.setText("" + b.getDescription());

            //  tv_event_name.setText("" + b.getArticle_name());


            tv_event_name.setText(Html.fromHtml("<b>" + b.getArticle_name() + "</b> "+getResources().getString(R.string.articleAddedBy)+" <b>" + b.getArticle_by() + "</b>"));

            if (Integer.parseInt(b.getNo_of_like()) == 1) {
                txt_likes.setText("" + b.getNo_of_like() + " "+getResources().getString(R.string.like));

            } else {
                txt_likes.setText("" + b.getNo_of_like() + " "+getResources().getString(R.string.likes));

            }


            txt_count.setVisibility(View.GONE);

        } else {

            // tv_event_time.setText("Added " +  TimeConveter.agotime(b.getEvent_date(), b.getEvent_time()));


            tv_event_description.setText("" + b.getDescription());


            if (!Mypreferences.User_Type.equals("Organization")) {
                tv_event_name.setText(Html.fromHtml("<b>" + b.getProfessional_name() + "</b> "+getResources().getString(R.string.added)+" " + b.getEvent_photos().size() + " "+getResources().getString(R.string.photosForEvent)+" <b>" + b.getEvent_title() + "</b>"));
            } else {
                tv_event_name.setText(Html.fromHtml("<b>" + b.getOrganisation_name() + "</b> "+getResources().getString(R.string.added)+" " + b.getEvent_photos().size() + " "+getResources().getString(R.string.photosForEvent)+" <b>" + b.getEvent_title() + "</b>"));

            }

            //     tv_event_name.setText("" + b.getEvent_title());
            if (Integer.parseInt(b.getNo_of_like()) == 1) {
                txt_likes.setText("" + b.getNo_of_like() + " "+getResources().getString(R.string.like));

            } else {
                txt_likes.setText("" + b.getNo_of_like() + " "+getResources().getString(R.string.likes));
            }

            ArrayList<BeanForActivityImages> al1 = b.getEvent_photos();
            if (al1.size() >= 1) {
                com.socialide.Helper.ImageLoader.image(al1.get(0).getImageUrl(), iv_activity_event, d1, PostDetails1.this);
                com.socialide.Helper.ImageLoader.image(Mypreferences.Picture, iv_event_profile_pic, d, PostDetails1.this);

                if (b.getEvent_photos().size() > 1) {
                    int j = b.getEvent_photos().size() - 1;
                    txt_count.setText("+ " + j);
                    txt_count.setVisibility(View.VISIBLE);

                } else {

                    txt_count.setVisibility(View.GONE);
                }
            }
        }

        iv_activity_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), FullScreenViewActivity.class);
                i.putExtra("pos", getIntent().getIntExtra("pos", 0));
                if (type == 1) {
                    i.putExtra("type", 1);
                } else {
                    i.putExtra("type", 2);
                }
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
                startActivity(i, options.toBundle());
            }
        });
    }

    private void report_popup() {

        final CharSequence[] options = {"Report", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(PostDetails1.this);
        builder.setTitle("Report the Post");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Report")) {

                    dialog.dismiss();

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private ArrayList<NameValuePair> getParams(String postid, String postType) {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//           params.add(new BasicNameValuePair("userid", ""+ Mypreferences.User_id));
//              params.add(new BasicNameValuePair("type", Mypreferences.User_Type));

        params.add(new BasicNameValuePair("eventId", "" + postid));
        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
        //   params.add(new BasicNameValuePair("postType", ""+postType));
        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));

        return params;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_share:
            case R.id.img_share:

                imgArr = new ArrayList<>();

                ArrayList<BeanForActivityImages> al1;

                if (b.getActivity_type().equals("article")) {
                    text = b.getDescription();
                    imgArr.add("" + b.getArticle_image());

                } else {
                    text = b.getMessage();
                    al1 = b.getEvent_photos();

                    for (int k = 0; k < al1.size(); k++) {

                        imgArr.add(al1.get(k).getImageUrl());

                    }
                }

                new ImageConvertAsync(imgArr, text, PostDetails1.this).execute();

                break;


        }
    }


    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);
            Log.e("eeeeee", "sssss  " + postObject);


//
            //  {"success":"true","message":"Scheduled event details fetched successfully.","result":{"event_id":"15","profile_image":null,"event_name":"inform the authorities about illegal activities ","event_title":"telling someone you love them via the phone ","event_date":"21 May 2017","contact_details":{"professional_name":"AVINASH TIWARI","professional_profession":null,"professional_email":"aa@aa.aa","professional_mobile_no":null},"userid":"183","type":"Professional","sharecontact":"1"}}

            if (postObject.getString("success").equals("true")) {

                txt_like.setEnabled(true);

                view_like.setVisibility(View.GONE);
                img_like.setVisibility(View.VISIBLE);
                BeanForActivities a = null;
                if (type == 2) {
                    a = OtherUserActivityFragment.activity_list.get(getIntent().getIntExtra("pos", 0));
                } else {
                    a = MyActivityFragment.activity_list1.get(getIntent().getIntExtra("pos", 0));
                }
                int like = Integer.parseInt(a.getNo_of_like());
                if (!postObject.getString("message").contains("disliked")) {

                    txt_like.setText(getResources().getString(R.string.liked));
                    img_like.setImageResource(R.drawable.like_green_icon);
                    txt_like.setTextColor(Color.parseColor("#3fb4b5"));
                    like++;

                    a.setNo_of_like("" + like);
                    if (like == 1) {
                        txt_likes.setText("" + like + " "+getResources().getString(R.string.like));
                    } else {
                        txt_likes.setText("" + like + " "+getResources().getString(R.string.likes));

                    }
                    a.setIsUserLikedPost("1");

                } else {


                    txt_like.setText("Like");
                    img_like.setImageResource(R.drawable.like_gray_icon);
                    txt_like.setTextColor(Color.parseColor("#727272"));
                    like = like - 1;

                    a.setNo_of_like("" + like);
                    txt_likes.setText("" + like);

                    if (like == 1) {
                        txt_likes.setText("" + like + " "+getResources().getString(R.string.like));
                    } else {
                        txt_likes.setText("" + like + " "+getResources().getString(R.string.likes));

                    }

                    a.setIsUserLikedPost("0");

                }
                if (type == 2) {
                    OtherUserActivityFragment.activity_list.remove(getIntent().getIntExtra("pos", 0));
                    OtherUserActivityFragment.activity_list.add(getIntent().getIntExtra("pos", 0), a);

                } else {
                    MyActivityFragment.activity_list1.remove(getIntent().getIntExtra("pos", 0));
                    MyActivityFragment.activity_list1.add(getIntent().getIntExtra("pos", 0), a);

                }


            } else {
                txt_like.setEnabled(true);

                view_like.setVisibility(View.GONE);
                img_like.setVisibility(View.VISIBLE);
            }
        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }

    }


    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
