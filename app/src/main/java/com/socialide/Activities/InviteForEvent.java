package com.socialide.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.CircleProgress;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Adapters.AvailabilityAdapterForMyProfile;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.AsyncTask.AsyncRequestForFavorite;
import com.socialide.Fragments.OtherUserProfile;
import com.socialide.Fragments.SearchProfessional1;
import com.socialide.Helper.Alert;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.Config;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.NonScrollExpandableListView;
import com.socialide.Helper.TimeConveter;
import com.socialide.Helper.Validation;
import com.socialide.Model.BeanForProfessionalSearch;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class InviteForEvent extends AppCompatActivity implements View.OnClickListener, AsyncRequest.OnAsyncRequestComplete, AsyncRequestForFavorite.OnAsyncRequestComplete {

    MyTextView tv_profession, tv_name, tv_city, txt_show, tvNoAvailability;
    AVLoadingIndicatorView view;
    MyEditTextView edt_title, edt_date, edt_time, edt_message;
    CircleImageView iv_event_profile_pic;
    DonutProgress loading;
    ImageView img_fav, img_verify;
    CheckBox chk_share_contact;
    MyButton btn_invite;
    String date, time;
    int day, month, years;
    ProgressBar view_like, view_p_bar;
    int pos;
    String dayname = "";
    boolean isShow = false;
    AvailabilityAdapterForMyProfile listAdapter;
    static List<String> listDataHeader = new ArrayList<>();
    NonScrollExpandableListView expandable_list;
    BeanForProfessionalSearch bean;
    static HashMap<String, List<String>> listDataChild = new HashMap<>();
    String strForConvertUTC = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_for_event);
        tv_profession = (MyTextView) findViewById(R.id.tv_profession);
        tv_name = (MyTextView) findViewById(R.id.tv_name);
        tv_city = (MyTextView) findViewById(R.id.tv_city);
        txt_show = (MyTextView) findViewById(R.id.txt_show);
        tvNoAvailability = (MyTextView) findViewById(R.id.tvNoAvailability);
        edt_title = (MyEditTextView) findViewById(R.id.edt_title);
        edt_date = (MyEditTextView) findViewById(R.id.edt_date);
        edt_time = (MyEditTextView) findViewById(R.id.edt_time);
        edt_message = (MyEditTextView) findViewById(R.id.edt_message);
        iv_event_profile_pic = (CircleImageView) findViewById(R.id.iv_event_profile_pic);
        img_fav = (ImageView) findViewById(R.id.img_fav);
        img_verify = (ImageView) findViewById(R.id.img_verify);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);
        view_like = (ProgressBar) findViewById(R.id.view_like);
        view_p_bar = (ProgressBar) findViewById(R.id.view_p_bar);
        loading = (DonutProgress) findViewById(R.id.loading);
        chk_share_contact = (CheckBox) findViewById(R.id.chk_share_contact);
        btn_invite = (MyButton) findViewById(R.id.btn_invite);
        expandable_list = (NonScrollExpandableListView) findViewById(R.id.expandable_list);
        edt_date.setOnClickListener(this);

        if (Mypreferences.Request_an_event_from.equals("profile")) {
            bean = OtherUserProfile.bean;
        } else {
            pos = getIntent().getIntExtra("pos", 0);
            bean = SearchProfessional1.professionla_list1.get(getIntent().getIntExtra("pos", 0));
        }

        com.socialide.Helper.ImageLoader.image(bean.getProfile_pic(), iv_event_profile_pic, loading, InviteForEvent.this);

        tv_profession.setText("" + bean.getProfession());
        tv_name.setText("" + bean.getFirst_name() + " " + bean.getLast_name());
        tv_city.setText("" + bean.getCity());


        if (("" + bean.getIs_verified()).equalsIgnoreCase("0")) {
            img_verify.setImageResource(R.drawable.notverifiedicon);
        } else {
            img_verify.setImageResource(R.drawable.verifiedicon);
        }

        if (("" + bean.getIs_favorite()).equalsIgnoreCase("0")) {
            img_fav.setImageResource(R.drawable.favoriteheart);
        } else {
            img_fav.setImageResource(R.drawable.favoriteheartfull);
        }


        if (Mypreferences.Request_an_event_from.equals("profile")) {
            if (("" + OtherUserProfile.is_favorite).equalsIgnoreCase("0")) {
                img_fav.setImageResource(R.drawable.favoriteheart);
            } else {
                img_fav.setImageResource(R.drawable.favoriteheartfull);
            }
        }

        expandable_list.setGroupIndicator(null);
        expandable_list.setChildIndicator(null);
        String[] reverse = ("" + bean.getAvailability()).split("@@");
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        for (int i = 0; i < reverse.length; i++) {

            String[] reverse2 = reverse[i].split("_");
            ArrayList<String> a = new ArrayList();
            if (reverse2.length > 1) {

                listDataHeader.add(reverse2[0]);

                if (reverse2.length > 1) {
                    Log.e("reserve" + i, "" + reverse2[0]);

                    String reverse3[] = reverse2[1].split(",");

                    for (int j = 0; j < reverse3.length; j++) {

                        Log.v("reserve1" + i, "" + reverse3[j]);
                        a.add("" + reverse3[j]);

                    }
                }

                if (a.size() == 0) {
                    a.add("");
                }
            }
            listDataChild.put(reverse2[0], a);
        }



        listAdapter = new AvailabilityAdapterForMyProfile(InviteForEvent.this, listDataHeader, listDataChild);

        // setting list adapter
        expandable_list.setAdapter(listAdapter);

        for (int l = 0; l < listDataHeader.size(); l++) {

            expandable_list.expandGroup(l);
        }

        expandable_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                return true;
            }
        });
        edt_date.setOnClickListener(this);
        edt_time.setOnClickListener(this);
        btn_invite.setOnClickListener(this);
        img_fav.setOnClickListener(this);
        txt_show.setOnClickListener(this);


        edt_title.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (("" + edt_date.getText().toString()).equals("")) {
                        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        setDateTimeField();

                    }
                    return true;
                }
                return false;
            }
        });


        edt_date.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press

                    return true;
                }
                return false;
            }
        });
    }


    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            Calendar newDate = Calendar.getInstance();

            public void onDateSet(DatePicker view, int year, int monthOfYear, final int dayOfMonth) {
                day = dayOfMonth;
                years = year;
                month = monthOfYear;
                newDate.set(year, monthOfYear, dayOfMonth);
                edt_date.setText(dateFormatter.format(newDate.getTime()));


                SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                Date date = new Date(year, monthOfYear, dayOfMonth - 1);
                dayname = simpledateformat.format(date);

                Log.e("Avinash", "Day" + dayname);

                if (("" + edt_date.getText().toString()).length() > 0) {
                    final Calendar mcurrentTime = Calendar.getInstance();
                    final Calendar c = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(InviteForEvent.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        if (selectedHour<9){
//                            selectedHour=Integer.parseInt("0"+selectedHour);
//                        }
                            mcurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                            mcurrentTime.set(Calendar.MINUTE, selectedMinute);
                            mcurrentTime.set(Calendar.YEAR, years);
                            mcurrentTime.set(Calendar.MONTH, month);
                            mcurrentTime.set(Calendar.DAY_OF_MONTH, day);


//                            if (selectedMinute < 9) {
//                                selectedMinute = Integer.parseInt("0" + selectedMinute);
//                            }
//                            if (selectedMinute == 0) {
//                                selectedMinute = Integer.parseInt("00");
//                            }

                            if (mcurrentTime.getTimeInMillis() > c.getTimeInMillis()) {
                                //it's after current
                                String curTime = String.format("%02d:%02d", selectedHour, selectedMinute);
                                //  edt_time.setText(selectedHour + ":" + selectedMinute);
                                // edt_time.setText(curTime);
                                strForConvertUTC = selectedHour + ":" + selectedMinute;
                                FunctionClass functionClass = new FunctionClass(InviteForEvent.this, InviteForEvent.this);
                                String finalTime = functionClass.updateTime(selectedHour, selectedMinute);
                                edt_time.setText(finalTime);
                            } else {
                                //it's before current'
                                CustomSnackBar.toast(InviteForEvent.this, "You can not select past time for an event.");
                            }
                        }
                    }, hour, minute, false);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");

                    mTimePicker.show();
                } else {

                    CustomSnackBar.toast(InviteForEvent.this, "Please select date first");
                }
            }


        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        fromDatePickerDialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.edt_date:
                dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                setDateTimeField();
                break;

            case R.id.txt_show:
                if (!isShow) {
                    isShow = !isShow;
                    txt_show.setText(getResources().getString(R.string.hide_availability));
                    expandable_list.setVisibility(View.VISIBLE);

                    if (listDataHeader.size() == 0) {
                        tvNoAvailability.setVisibility(View.VISIBLE);
                    } else {
                        tvNoAvailability.setVisibility(View.GONE);
                    }

                } else {
                    isShow = !isShow;
                    txt_show.setText(getResources().getString(R.string.show_availability));
                    expandable_list.setVisibility(View.GONE);
                    tvNoAvailability.setVisibility(View.GONE);
                }
                break;

            case R.id.img_fav:

                CheckInternet mCheckInternet = new CheckInternet();

                if (!mCheckInternet.isConnectingToInternet(InviteForEvent.this)) {

                    CustomSnackBar.toast(InviteForEvent.this, "Please connect to internet!");

                } else {
                    view_like.setVisibility(View.VISIBLE);
                    img_fav.setVisibility(View.INVISIBLE);

                    AsyncRequestForFavorite getPosts1 = new AsyncRequestForFavorite(InviteForEvent.this, "POST", getParams1(), view, view_like);
                    getPosts1.execute(GloabalURI.baseURI + GloabalURI.favoriteUnfavoriteUser);
                }
                break;

            case R.id.btn_invite:
                if (isValidate()) {
                    CheckInternet mCheckInternet1 = new CheckInternet();

                    if (!mCheckInternet1.isConnectingToInternet(InviteForEvent.this)) {
                        CustomSnackBar.toast(InviteForEvent.this, "Please connect to internet!");
                    } else {
                        Log.e("strForConvertUTCm", "strForConvertUTC  " + strForConvertUTC);
                        if (checkAvailability("" + strForConvertUTC, dayname)) {
                            btn_invite.setVisibility(View.GONE);
                            view_p_bar.setVisibility(View.VISIBLE);
                            //  Log.e("avinash", TimeConveter.convertToUTC1(edt_date.getText().toString(), edt_time.getText().toString()));
                            String Datetime = TimeConveter.convertToUTC1(edt_date.getText().toString(), edt_time.getText().toString());

                            date = Datetime.split("T")[0];
                            time = Datetime.split("T")[1];

                            AsyncRequest getPosts = new AsyncRequest(InviteForEvent.this, "POST", getParams(), view);
                            getPosts.execute(GloabalURI.baseURI + GloabalURI.requestForEvent);
                        } else {
                            CustomSnackBar.toast(InviteForEvent.this, "" + bean.getFirst_name() + " is not availbale on that time");
                        }
                    }
                }
                break;

            case R.id.edt_time:

                if (("" + edt_date.getText().toString()).length() > 0) {
                    final Calendar mcurrentTime = Calendar.getInstance();
                    final Calendar c = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR);
                    int minute = mcurrentTime.get(Calendar.MINUTE);

                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(InviteForEvent.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        if (selectedHour<9){
//                            selectedHour=Integer.parseInt("0"+selectedHour);
//                        }
                            strForConvertUTC = selectedHour + ":" + selectedMinute;
                            Log.e("strForConvertUTC", "strForConvertUTC  " + strForConvertUTC);
                            mcurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour);
                            mcurrentTime.set(Calendar.MINUTE, selectedMinute);
                            mcurrentTime.set(Calendar.YEAR, years);
                            mcurrentTime.set(Calendar.MONTH, month);
                            mcurrentTime.set(Calendar.DAY_OF_MONTH, day);
//                            if (selectedMinute < 9) {
//                                selectedMinute = Integer.parseInt("0" + selectedMinute);
//                            }
//                            if (selectedMinute == 0) {
//                                selectedMinute = Integer.parseInt("00");
//                            }

                            if (mcurrentTime.getTimeInMillis() > c.getTimeInMillis()) {
                                //it's after current
                                String curTime = String.format("%02d:%02d", selectedHour, selectedMinute);
                                //  edt_time.setText(selectedHour + ":" + selectedMinute);
                                FunctionClass functionClass = new FunctionClass(InviteForEvent.this, InviteForEvent.this);
                                String finalTime = functionClass.updateTime(selectedHour, selectedMinute);
                                edt_time.setText(finalTime);
                            } else {
                                //it's before current'
                                CustomSnackBar.toast(InviteForEvent.this, "You can not select past time for an event.");
                            }


                        }
                    }, hour, minute, false);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");


                    mTimePicker.show();
                } else {

                    CustomSnackBar.toast(InviteForEvent.this, "Please select date first");
                }
                break;
        }

    }


    @Override
    public void onBackPressed() {
        exitDialog();
    }


    private boolean isValidate() {
        if (!Validation.isValidName("" + edt_title.getText().toString())) {
            // edt_firstname.setError("Please enter your first name");
            CustomSnackBar.toast(InviteForEvent.this, "Please enter event title");

        } else if (!Validation.isValidName("" + edt_date.getText().toString())) {
            //  edt_lastname.setError("Please enter your last name");
            CustomSnackBar.toast(InviteForEvent.this, "Please enter date of event");

        } else if (!Validation.isValidName("" + edt_time.getText().toString())) {
            //  edt_contact.setError("Please enter mobile number");
            CustomSnackBar.toast(InviteForEvent.this, "Please enter time of event");

        } else if (!Validation.isValidName("" + edt_message.getText().toString())) {
            //   edt_email.setError("Please enter email");
            CustomSnackBar.toast(InviteForEvent.this, "Please enter message related to event invitation");


        } else {
            return true;
        }
        return false;
    }


    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("professional_id", "" + bean.getUserid()));
        params.add(new BasicNameValuePair("event_time", "" + time));
        params.add(new BasicNameValuePair("event_date", "" + date));
        params.add(new BasicNameValuePair("messasge", "" + edt_message.getText().toString()));
        params.add(new BasicNameValuePair("event_title", "" + edt_title.getText().toString()));
        if (chk_share_contact.isChecked()) {
            params.add(new BasicNameValuePair("share_contact", "1"));

        } else {
            params.add(new BasicNameValuePair("share_contact", "0"));

        }
        // params.add(new BasicNameValuePair("device_token", ""+ SplashScreen.device_token));
        return params;
    }

    @Override
    public void asyncResponse(String response) {
        btn_invite.setVisibility(View.VISIBLE);
        view_p_bar.setVisibility(View.GONE);
        Log.e("Response", "" + response);
        try {
            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {
                String s = "" + postObject.getString("message");
                Toast.makeText(this, "" + s, Toast.LENGTH_SHORT).show();
                finish();
            } else {
                String s = "" + postObject.getString("message");
                CustomSnackBar.toast(InviteForEvent.this, s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<NameValuePair> getParams1() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("favorite_user_id", "" + bean.getUserid()));
        return params;
    }


    @Override
    public void asyncResponsefavorite(String response) {

        Log.e("response", "" + response);
        try {
            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {


                if (Mypreferences.Request_an_event_from.equals("profile")) {
                    SearchProfessional1.is_filter_changed = true;
                    SearchProfessional1.favCheckApi = false;
                    if (!postObject.getString("message").contains("removed")) {
                        bean.setIs_favorite("1");
                        OtherUserProfile.is_favorite = "1";
                        img_fav.setImageResource(R.drawable.favoriteheartfull);
                    } else {

                        bean.setIs_favorite("0");
                        OtherUserProfile.is_favorite = "0";
                        img_fav.setImageResource(R.drawable.favoriteheart);
                    }
                    OtherUserProfile.bean = bean;

                } else {
                    if (!postObject.getString("message").contains("removed")) {
                        bean.setIs_favorite("1");
                        img_fav.setImageResource(R.drawable.favoriteheartfull);
                        SearchProfessional1.listForfavProfessionals.add(bean);
                    } else {
                        bean.setIs_favorite("0");
                        img_fav.setImageResource(R.drawable.favoriteheart);
                        for (int i = 0; i < SearchProfessional1.listForfavProfessionals.size(); i++) {
                            if (bean.getUserid().equals(SearchProfessional1.listForfavProfessionals.get(i).getUserid())) {
                                SearchProfessional1.listForfavProfessionals.remove(SearchProfessional1.listForfavProfessionals.get(i));
                            }
                        }
                    }
                    SearchProfessional1.professionla_list1.remove(pos);
                    SearchProfessional1.professionla_list1.add(pos, bean);
                }

                view_like.setVisibility(View.GONE);
                img_fav.setVisibility(View.VISIBLE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public Dialog exitDialog() {

        LayoutInflater factory = LayoutInflater.from(InviteForEvent.this);
        final View view = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(InviteForEvent.this).create();
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view.findViewById(R.id.tvMsg);
        tvMsg.setText(getResources().getString(R.string.dialog_backDesc));

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;

    }


    public boolean checkAvailability(String s1, String day) {


        boolean checkAvailable = false;
        boolean x = true;
        int group = 0;

        for (int i = 0; i < listDataHeader.size(); i++) {
            if (listDataHeader.get(i).equalsIgnoreCase("" + day)) {
                group = i;
            }
        }
        String s = listDataHeader.get(group);
        List<String> al = listDataChild.get(s);

        if (al == null) {
            return false;
        }

        for (int i = 0; i < al.size(); i++) {

            if (al.get(i) != null || !al.get(i).equalsIgnoreCase("Not Available") || !al.get(i).equalsIgnoreCase("")) {


                String time[] = al.get(i).split("-");


                String hhmm1[] = time[0].split(":");
                int h1 = Integer.parseInt(hhmm1[0].trim());
                int m1 = Integer.parseInt(hhmm1[1].trim());


                String hhmm2[] = time[1].split(":");
                int h2 = Integer.parseInt(hhmm2[0].trim());
                int m2 = Integer.parseInt(hhmm2[1].trim());


                String chh1[] = s1.split(":");


                int ch1 = Integer.parseInt(chh1[0]);

                int mh1 = Integer.parseInt(chh1[1]);


/*--------------------------for checking "from hour" to previous "from hours " of same day */

                        /*--------------------------for checking "from hour" to previous "from_hours and to_hour" of same day */

                if (h1 <= ch1 && ch1 <= h2) {
                    if (ch1 == h1 && m1 >= mh1) {
                        //   return false;
// return false;
                    } else if (ch1 == h2 && m2 <= mh1) {
                        // return false;
// return false;
                    } else {
                        return true;

                    }


                }

            }


        }

        return false;


    }


}

