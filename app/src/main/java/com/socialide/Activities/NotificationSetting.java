package com.socialide.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ScrollView;
import android.widget.Toast;

import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by info1010 on 8/22/2017.
 */

public class NotificationSetting extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete, CompoundButton.OnCheckedChangeListener {

    SwitchCompat SCNotification1, SCNotification2, SCNotification3, SCNotification4, SCNotification5,
            SCNotification6, SCNotification7, SCNotification8, SCNotification9, SCEmail1, SCEmail2,
            SCEmail3, SCEmail4, SCEmail5, SCEmail6, SCEmail7, SCEmail8, SCEmail9, SCSms1, SCSms2, SCSms3, SCSms4,
            SCSms5, SCSms6, SCSms7, SCSms8, SCSms9;

    AVLoadingIndicatorView avi;
    ArrayList<NameValuePair> params;

    MyTextView tvFetching;
    ScrollView scrollView;
    int apiCheck = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_setting);

        findIds();
        setCheckedListener();
        params = getParams(0, "", "");
        AsyncRequest getPosts = new AsyncRequest(this, this, "POST", params, avi);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.getSetting);
    }

    private void findIds() {
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);

        tvFetching = (MyTextView) findViewById(R.id.tvFetching);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        SCNotification1 = (SwitchCompat) findViewById(R.id.SCNotification1);
        SCNotification2 = (SwitchCompat) findViewById(R.id.SCNotification2);
        SCNotification3 = (SwitchCompat) findViewById(R.id.SCNotification3);
        SCNotification4 = (SwitchCompat) findViewById(R.id.SCNotification4);
        SCNotification5 = (SwitchCompat) findViewById(R.id.SCNotification5);
        SCNotification6 = (SwitchCompat) findViewById(R.id.SCNotification6);
        SCNotification7 = (SwitchCompat) findViewById(R.id.SCNotification7);
        SCNotification8 = (SwitchCompat) findViewById(R.id.SCNotification8);
        SCNotification9 = (SwitchCompat) findViewById(R.id.SCNotification9);

        SCEmail1 = (SwitchCompat) findViewById(R.id.SCEmail1);
        SCEmail2 = (SwitchCompat) findViewById(R.id.SCEmail2);
        SCEmail3 = (SwitchCompat) findViewById(R.id.SCEmail3);
        SCEmail4 = (SwitchCompat) findViewById(R.id.SCEmail4);
        SCEmail5 = (SwitchCompat) findViewById(R.id.SCEmail5);
        SCEmail6 = (SwitchCompat) findViewById(R.id.SCEmail6);
        SCEmail7 = (SwitchCompat) findViewById(R.id.SCEmail7);
        SCEmail8 = (SwitchCompat) findViewById(R.id.SCEmail8);
        SCEmail9 = (SwitchCompat) findViewById(R.id.SCEmail9);

        SCSms1 = (SwitchCompat) findViewById(R.id.SCSms1);
        SCSms2 = (SwitchCompat) findViewById(R.id.SCSms2);
        SCSms3 = (SwitchCompat) findViewById(R.id.SCSms3);
        SCSms4 = (SwitchCompat) findViewById(R.id.SCSms4);
        SCSms5 = (SwitchCompat) findViewById(R.id.SCSms5);
        SCSms6 = (SwitchCompat) findViewById(R.id.SCSms6);
        SCSms7 = (SwitchCompat) findViewById(R.id.SCSms7);
        SCSms8 = (SwitchCompat) findViewById(R.id.SCSms8);
        SCSms9 = (SwitchCompat) findViewById(R.id.SCSms9);
    }

    private void setCheckedListener() {

        SCNotification1.setOnCheckedChangeListener(this);
        SCNotification2.setOnCheckedChangeListener(this);
        SCNotification3.setOnCheckedChangeListener(this);
        SCNotification4.setOnCheckedChangeListener(this);
        SCNotification5.setOnCheckedChangeListener(this);
        SCNotification6.setOnCheckedChangeListener(this);
        SCNotification7.setOnCheckedChangeListener(this);
        SCNotification8.setOnCheckedChangeListener(this);
        SCNotification9.setOnCheckedChangeListener(this);

        SCEmail1.setOnCheckedChangeListener(this);
        SCEmail2.setOnCheckedChangeListener(this);
        SCEmail3.setOnCheckedChangeListener(this);
        SCEmail4.setOnCheckedChangeListener(this);
        SCEmail5.setOnCheckedChangeListener(this);
        SCEmail6.setOnCheckedChangeListener(this);
        SCEmail7.setOnCheckedChangeListener(this);
        SCEmail8.setOnCheckedChangeListener(this);
        SCEmail9.setOnCheckedChangeListener(this);

        SCSms1.setOnCheckedChangeListener(this);
        SCSms2.setOnCheckedChangeListener(this);
        SCSms3.setOnCheckedChangeListener(this);
        SCSms4.setOnCheckedChangeListener(this);
        SCSms5.setOnCheckedChangeListener(this);
        SCSms6.setOnCheckedChangeListener(this);
        SCSms7.setOnCheckedChangeListener(this);
        SCSms8.setOnCheckedChangeListener(this);
        SCSms9.setOnCheckedChangeListener(this);
    }

    @Override
    public void asyncResponse(String response) {

        Log.v("akram", "response = " + response);

        if (apiCheck == 0) {
            tvFetching.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);

            try {
                JSONObject postObject = new JSONObject(response);

                if (postObject.getString("success").equalsIgnoreCase("true")) {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = postObject.getJSONObject("settings");
                    setData(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    private ArrayList<NameValuePair> getParams(int checkApi, String key, String value) {

        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));
        if (checkApi == 0) {
            apiCheck = 0;
            tvFetching.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        } else {
            params.add(new BasicNameValuePair(key, value));
        }

        return params;
    }

    private void setData(JSONObject jsonObject) {

        try {
            if (jsonObject.getString("notification_likes").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification1, true);
            } else checkedMethod(SCNotification1, false);

            if (jsonObject.getString("notification_new_article").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification2, true);
            } else checkedMethod(SCNotification2, false);

            if (jsonObject.getString("notification_someone_added_event_photo").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification3, true);
            } else checkedMethod(SCNotification3, false);

            if (jsonObject.getString("notification_someone_added_my_event_photo").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification4, true);
            } else checkedMethod(SCNotification4, false);

            if (jsonObject.getString("notification_someone_invite_for_event").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification5, true);
            } else checkedMethod(SCNotification5, false);

            if (jsonObject.getString("notification_someone_cancelled_for_event").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification6, true);
            } else checkedMethod(SCNotification6, false);

            if (jsonObject.getString("notification_someone_accept_event").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification7, true);
            } else checkedMethod(SCNotification7, false);

            if (jsonObject.getString("notification_event_reminder").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification8, true);
            } else checkedMethod(SCNotification8, false);

            if (jsonObject.getString("notification_someone_cancelled_for_scheduled_event").equalsIgnoreCase("1")) {
                checkedMethod(SCNotification9, true);
            } else checkedMethod(SCNotification9, false);


            /*set Email data*/
            if (jsonObject.getString("email_likes").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail1, true);
            } else checkedMethod(SCEmail1, false);

            if (jsonObject.getString("email_new_article").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail2, true);
            } else checkedMethod(SCEmail2, false);

            if (jsonObject.getString("email_someone_added_event_photo").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail3, true);
            } else checkedMethod(SCEmail3, false);

            if (jsonObject.getString("email_someone_added_my_event_photo").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail4, true);
            } else checkedMethod(SCEmail4, false);

            if (jsonObject.getString("email_someone_invite_for_event").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail5, true);
            } else checkedMethod(SCEmail5, false);

            if (jsonObject.getString("email_someone_cancelled_event").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail6, true);
            } else checkedMethod(SCEmail6, false);

            if (jsonObject.getString("email_someone_accept_event").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail7, true);
            } else checkedMethod(SCEmail7, false);

            if (jsonObject.getString("email_event_reminder").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail8, true);
            } else checkedMethod(SCEmail8, false);

            if (jsonObject.getString("email_someone_cancelled_scheduled_event").equalsIgnoreCase("1")) {
                checkedMethod(SCEmail9, true);
            } else checkedMethod(SCEmail9, false);

          /*set SMS data*/
            if (jsonObject.getString("sms_likes").equalsIgnoreCase("1")) {
                checkedMethod(SCSms1, true);
            } else checkedMethod(SCSms1, false);

            if (jsonObject.getString("sms_new_article").equalsIgnoreCase("1")) {
                checkedMethod(SCSms2, true);
            } else checkedMethod(SCSms2, false);

            if (jsonObject.getString("sms_someone_added_event_photo").equalsIgnoreCase("1")) {
                checkedMethod(SCSms3, true);
            } else checkedMethod(SCSms3, false);

            if (jsonObject.getString("sms_someone_added_my_event_photo").equalsIgnoreCase("1")) {
                checkedMethod(SCSms4, true);
            } else checkedMethod(SCSms4, false);

            if (jsonObject.getString("sms_someone_invite_for_event").equalsIgnoreCase("1")) {
                checkedMethod(SCSms5, true);
            } else checkedMethod(SCSms5, false);

            if (jsonObject.getString("sms_someone_cancelled_event").equalsIgnoreCase("1")) {
                checkedMethod(SCSms6, true);
            } else checkedMethod(SCSms6, false);

            if (jsonObject.getString("sms_someone_accept_event").equalsIgnoreCase("1")) {
                checkedMethod(SCSms7, true);
            } else checkedMethod(SCSms7, false);

            if (jsonObject.getString("sms_event_reminder").equalsIgnoreCase("1")) {
                checkedMethod(SCSms8, true);
            } else checkedMethod(SCSms8, false);

            if (jsonObject.getString("sms_someone_cancelled_scheduled_event").equalsIgnoreCase("1")) {
                checkedMethod(SCSms9, true);
            } else checkedMethod(SCSms9, false);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkedMethod(SwitchCompat switchCompat, boolean condition) {
        switchCompat.setChecked(condition);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        switch (compoundButton.getId()) {

            case R.id.SCNotification1:

                settingApi("notification_likes", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification2:

                settingApi("notification_new_article", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification3:

                settingApi("notification_someone_added_event_photo", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification4:

                settingApi("notification_someone_added_my_event_photo", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification5:

                settingApi("notification_someone_invite_for_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification6:

                settingApi("notification_someone_cancelled_for_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification7:

                settingApi("notification_someone_accept_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification8:

                settingApi("notification_event_reminder", (b == true) ? "1" : "0");
                break;

            case R.id.SCNotification9:

                settingApi("notification_someone_cancelled_for_scheduled_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail1:

                settingApi("email_likes", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail2:

                settingApi("email_new_article", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail3:

                settingApi("email_someone_added_event_photo", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail4:

                settingApi("email_someone_added_my_event_photo", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail5:

                settingApi("email_someone_invite_for_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail6:

                settingApi("email_someone_cancelled_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail7:

                settingApi("email_someone_accept_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail8:

                settingApi("email_event_reminder", (b == true) ? "1" : "0");
                break;

            case R.id.SCEmail9:

                settingApi("email_someone_cancelled_scheduled_event", (b == true) ? "1" : "0");
                break;


            case R.id.SCSms1:

                settingApi("sms_likes", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms2:

                settingApi("sms_new_article", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms3:

                settingApi("sms_someone_added_event_photo", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms4:

                settingApi("sms_someone_added_my_event_photo", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms5:

                settingApi("sms_someone_invite_for_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms6:

                settingApi("sms_someone_cancelled_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms7:

                settingApi("sms_someone_accept_event", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms8:

                settingApi("sms_event_reminder", (b == true) ? "1" : "0");
                break;

            case R.id.SCSms9:

                settingApi("sms_someone_cancelled_scheduled_event", (b == true) ? "1" : "0");
                break;

        }
    }

    private void settingApi(String key, String value) {
        apiCheck = 1;
        params = getParams(1, key, value);
        AsyncRequest getPosts = new AsyncRequest(this, this, "POST", params, avi);
        getPosts.execute(GloabalURI.baseURI + GloabalURI.uploadSetting);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NotificationSetting.this, SettingActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
        finish();
    }
}

