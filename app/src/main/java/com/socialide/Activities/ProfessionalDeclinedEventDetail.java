package com.socialide.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.EventsDeclined;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfessionalDeclinedEventDetail extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete {
    MyTextView tv_event_name, tv_organisation_name, tv_event_guests, txt_date, txt_time, txt_email, txt_landline_number, edt_message, txt_reason, txt_declined;
    CircleImageView iv_me_profile_pic;
    Button btn_remove;
    BeanForFetchEvents b;
    AVLoadingIndicatorView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_details_pro_declined);

        iv_me_profile_pic = (CircleImageView) findViewById(R.id.iv_me_profile_pic);
        btn_remove = (Button) findViewById(R.id.btn_remove);

        RelativeLayout rl_caption = (RelativeLayout) findViewById(R.id.rl_caption);

        rl_caption.setVisibility(View.GONE);

        tv_event_name = (MyTextView) findViewById(R.id.tv_name);
        tv_organisation_name = (MyTextView) findViewById(R.id.tv_organisation_name);
        tv_event_guests = (MyTextView) findViewById(R.id.tv_event_guests);
        txt_date = (MyTextView) findViewById(R.id.txt_date);
        txt_time = (MyTextView) findViewById(R.id.txt_time);
        txt_email = (MyTextView) findViewById(R.id.txt_email);
        txt_landline_number = (MyTextView) findViewById(R.id.txt_landline_number);
        edt_message = (MyTextView) findViewById(R.id.edt_message);
        txt_reason = (MyTextView) findViewById(R.id.txt_reason);
        txt_declined = (MyTextView) findViewById(R.id.txt_declined);
        MyTextView  txt_caption = (MyTextView) findViewById(R.id.txt_caption);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);

        txt_caption.setVisibility(View.GONE);
        b = EventsDeclined.activity_list1.get(EventFragment.position);
        tv_event_name.setText(Html.fromHtml("<b>" + b.getEvent_title() + "</b>"));
        tv_organisation_name.setText("" + b.getEvent_by());
        tv_event_guests.setText("" + b.getEvent_location());

        String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

        String date = datetime.split("T")[0];
        String time = datetime.split("T")[1];
        txt_time.setText("" + time);
        txt_date.setText("" + date);
        txt_landline_number.setText("" + b.getContact_number());
        txt_email.setText("" + b.getEmail());

        SpannableString content = new SpannableString(b.getContact_number());
        content.setSpan(new UnderlineSpan(), 0, b.getContact_number().length(), 0);
        txt_landline_number.setText(content);

        SpannableString content1 = new SpannableString(b.getEmail());
        content1.setSpan(new UnderlineSpan(), 0, b.getEmail().length(), 0);
        txt_email.setText(content1);


        edt_message.setText("" + b.getMessage());
        txt_declined.setText("" + b.getDecline_by());
        txt_reason.setText("" + b.getReason());

        DonutProgress d = (DonutProgress) findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(b.getProfile_image(), iv_me_profile_pic, d, ProfessionalDeclinedEventDetail.this);
        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("eventId", "" + b.getEvent_id()));
                params.add(new BasicNameValuePair("type", "" + Mypreferences.User_Type));
                params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
                params.add(new BasicNameValuePair("access_token", "" + Mypreferences.Access_token));

                AsyncRequest getPosts = new AsyncRequest(ProfessionalDeclinedEventDetail.this, ProfessionalDeclinedEventDetail.this, "POST", params, view);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.REMOVE_AN_EVENT);

            }
        });


    }


    @Override
    public void asyncResponse(String response) {
        try {
            JSONObject postObject = new JSONObject(response);


            if (postObject.getString("success").equals("true")) {
                CustomSnackBar.toast(ProfessionalDeclinedEventDetail.this, postObject.getString("message"));
                EventsDeclined.activity_list1.remove(EventFragment.position);
                EventFragment.isBack = true;

                finish();
                overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);

            } else {
                CustomSnackBar.toast(ProfessionalDeclinedEventDetail.this, postObject.getString("message"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }
}
