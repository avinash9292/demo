package com.socialide.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Adapters.InformationAdapter;
import com.socialide.Adapters.ViewPagerAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.AsyncTask.AsyncRequestForFavorite;
import com.socialide.Fragments.MeFragment;
import com.socialide.Fragments.MyActivityFragment;
import com.socialide.Fragments.MyPastEvents;
import com.socialide.Fragments.MyProfile;
import com.socialide.Fragments.OtherUserActivityFragment;
import com.socialide.Fragments.OtherUserPastEvents;
import com.socialide.Fragments.OtherUserProfile;
import com.socialide.Fragments.SearchProfessional1;
import com.socialide.Helper.CheckInternet;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by info1010 on 9/21/2017.
 */

public class OtherUserDetailsActivity extends AppCompatActivity implements AsyncRequestForFavorite.OnAsyncRequestComplete {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static ImageView img_back;
    public static CircleImageView profilepic, iv_event_profile_pic1;
    RelativeLayout relative_bg;
    public static DonutProgress d;
    AVLoadingIndicatorView avi;
    RecyclerView mRecyclerView;
    InformationAdapter adapter;
    ProgressBar view_fav;
    private RecyclerView.LayoutManager mLayoutManager;
    public static MyTextView txt_name, txt_profession, txt_loaction;
    ImageButton backMyButton;
    public static int checkTabPosition = 0;
    ArrayList<NameValuePair> params;
    public static String userId, type;
    public static MyTextView tvLoading;
    public static LinearLayout layoutMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user);
        findIds();
        checkTabPosition = 0;
        OtherUserPastEvents.activity_list.clear();
        tvLoading.setVisibility(View.VISIBLE);

        userId = getIntent().getStringExtra("userId");
        type = getIntent().getStringExtra("type");
        Log.v("akram", "type in activity = " + type);


        if (type.equalsIgnoreCase("Organization")) {
            iv_event_profile_pic1.setVisibility(View.INVISIBLE);
            profilepic.setBorderColor(Color.parseColor("#3fb4b5"));
            profilepic.setBorderWidth(2);
        } else {
            iv_event_profile_pic1.setVisibility(View.VISIBLE);
            profilepic.setBorderWidth(0);
            profilepic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CheckInternet mCheckInternet = new CheckInternet();

                    if (!mCheckInternet.isConnectingToInternet(OtherUserDetailsActivity.this)) {

                        CustomSnackBar.toast(OtherUserDetailsActivity.this, "Please connect to internet!");

                    } else {
                        profilepic.setVisibility(View.GONE);
                        iv_event_profile_pic1.setVisibility(View.GONE);
                        view_fav.setVisibility(View.VISIBLE);

                        AsyncRequestForFavorite getPosts1 = new AsyncRequestForFavorite(OtherUserDetailsActivity.this, "POST", getParams1(), avi, view_fav);
                        getPosts1.execute(GloabalURI.baseURI + GloabalURI.favoriteUnfavoriteUser);
                    }
                }
            });

            iv_event_profile_pic1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profilepic.performClick();
                }
            });
        }
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(R.color.primary);
        viewPager.setCurrentItem(checkTabPosition);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                checkTabPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void findIds() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        txt_name = (MyTextView) findViewById(R.id.txt_name);
        txt_profession = (MyTextView) findViewById(R.id.txt_profession);
        txt_loaction = (MyTextView) findViewById(R.id.txt_loaction);
        tvLoading = (MyTextView) findViewById(R.id.tvLoading);
        profilepic = (CircleImageView) findViewById(R.id.iv_event_profile_pic);
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        iv_event_profile_pic1 = (CircleImageView) findViewById(R.id.iv_event_profile_pic1);
        img_back = (ImageView) findViewById(R.id.img_back);
        relative_bg = (RelativeLayout) findViewById(R.id.relative_bg);
        d = (DonutProgress) findViewById(R.id.loading);
        view_fav = (ProgressBar) findViewById(R.id.view_fav);
        layoutMain = (LinearLayout) findViewById(R.id.layoutMain);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OtherUserProfile(), "Profile");
        adapter.addFragment(new OtherUserActivityFragment(), "Posts");
        adapter.addFragment(new OtherUserPastEvents(), "Events");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
    }


    private ArrayList<NameValuePair> getParams1() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("favorite_user_id", "" + userId));
        return params;
    }


    @Override
    public void asyncResponsefavorite(String response) {
        profilepic.setVisibility(View.VISIBLE);
        iv_event_profile_pic1.setVisibility(View.VISIBLE);
        view_fav.setVisibility(View.GONE);
        profilepic.setEnabled(true);
        iv_event_profile_pic1.setEnabled(true);
        Log.e("response  4234", "" + response);
        try {
            JSONObject postObject = new JSONObject(response);
            if (postObject.getString("success").equals("true")) {
                SearchProfessional1.is_filter_changed = true;
                SearchProfessional1.favCheckApi = false;
                Log.v("akram", "is filter = " + SearchProfessional1.is_filter_changed);
                if (!postObject.getString("message").contains("removed")) {
                    OtherUserProfile.is_favorite = "1";
                    iv_event_profile_pic1.setBackgroundResource(R.drawable.favoriteborder);
                } else {

                    OtherUserProfile.is_favorite = "0";
                    iv_event_profile_pic1.setBackgroundResource(R.drawable.favoritebordergray);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
