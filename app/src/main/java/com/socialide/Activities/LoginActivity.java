package com.socialide.Activities;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.LoginFragment;
import com.socialide.Fragments.OrgainizationOtherDetails;
import com.socialide.Fragments.OrganisationSignupSecond;
import com.socialide.Fragments.ProffressionalAvailability;
import com.socialide.Fragments.ProffressionalOtherDetails;
import com.socialide.Fragments.ProffressionalSignupSecond;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;

import io.fabric.sdk.android.Fabric;

import static com.socialide.R.anim.*;

public class LoginActivity extends FragmentActivity implements LocationListener {

    public static int i = 0, back_handle = 0;
    int signupCheck = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        if (Mypreferences.User_Type.equals("Professional")) {


            if (getIntent().getIntExtra("set", 0) == 2) {
                goToFragment1(new ProffressionalOtherDetails());
            } else if (getIntent().getIntExtra("set", 0) == 1) {
                goToFragment1(new ProffressionalSignupSecond());
            } else if (getIntent().getIntExtra("set", 0) == 3) {

                goToFragment1(new ProffressionalAvailability());
            } else if (Mypreferences.getString(Mypreferences.Email, getApplicationContext()).equals("")) {
                goToFragment(new LoginFragment());

            } else if (Mypreferences.DOB.equals("")) {
                goToFragment(new ProffressionalSignupSecond());

            } else if (Mypreferences.CURRENT_COMPANY.equals("")) {
                goToFragment(new ProffressionalOtherDetails());

            } else if (Mypreferences.Maximum_Event_Request.equals("")) {

                goToFragment1(new ProffressionalAvailability());
            } else {
                goToFragment(new LoginFragment());
            }


        } else if (Mypreferences.User_Type.equals("Organization")) {

            if (getIntent().getIntExtra("set", 0) == 2) {
                goToFragment1(new OrgainizationOtherDetails());
            } else if (getIntent().getIntExtra("set", 0) == 1) {
                Log.v("akram", "if satttfdgdfg if");
                goToFragment1(new OrganisationSignupSecond());
            } else if (Mypreferences.getString(Mypreferences.Email, getApplicationContext()).equals("")) {
                goToFragment(new LoginFragment());

            } else if (Mypreferences.STABLISHMENT_YEAR.equals("")) {
                Log.v("akram", "if satttata");
                if (signupCheck == 0) {
                    LoginActivity.i = 1;
                }
                goToFragment(new OrganisationSignupSecond());

            } else if (Mypreferences.DESIGNATION.equals("")) {
                if (signupCheck == 0) {
                    LoginActivity.i = 1;
                }
                goToFragment(new OrgainizationOtherDetails());
            } else {
                goToFragment(new LoginFragment());
            }
        } else
            goToFragment(new LoginFragment());
    }


    public void goToFragment(Fragment fragment) {

        if (LoginActivity.i == 0) {
            Log.v("akram", "add to back statck null");
            //  fragmentTransaction.setCustomAnimations(0,slide_from_right);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

            fragmentTransaction.setCustomAnimations(slide_to_right, slide_from_left, slide_to_left, slide_from_right);

            fragmentTransaction.replace(R.id.fragment, fragment, "Fragment").addToBackStack(null);

            fragmentTransaction.commitAllowingStateLoss();
        } else {
            Log.v("akram", "add to back statck null not");
            //  fragmentTransaction.setCustomAnimations(slide_to_left,slide_from_right);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Log.v("akram", "value = " + WelcomeActivity.checkAnimation);
            if (WelcomeActivity.checkAnimation == 1) {
                WelcomeActivity.checkAnimation = 0;
            } else {
                fragmentTransaction.setCustomAnimations(slide_to_right, slide_from_left, slide_to_left, slide_from_right);
            }
            fragmentTransaction.replace(R.id.fragment, fragment, "Fragment");

            fragmentTransaction.commitAllowingStateLoss();

            LoginActivity.i = 0;
        }
    }

    public void goToFragment1(Fragment fragment) {

        //  fragmentTransaction.setCustomAnimations(0,slide_from_right);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

//            fragmentTransaction.setCustomAnimations(slide_to_right,slide_from_left,slide_to_left, slide_from_right);

        fragmentTransaction.replace(R.id.fragment, fragment, "Fragment");

        fragmentTransaction.commitAllowingStateLoss();

    }

    public void continueToSecondScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_to_right, R.anim.slide_from_left);
        startActivity(intent, options.toBundle());
        finish();
    }


    @Override
    public void onBackPressed() {

        if ((getIntent().getIntExtra("set", 0) != 0)) {
            //super.onBackPressed();
            Intent intent = new Intent(LoginActivity.this, SettingActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
            finish();

        }

        if (back_handle == 0) {
            SettingActivity.profilePageCheck = 0;
            super.onBackPressed();

        } else {
            finish();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
