package com.socialide.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gun0912.tedpicker.Config;
import com.gun0912.tedpicker.ImagePickerActivity;
import com.socialide.Adapters.EventPhotosAdapter;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.MyPastEvents;
import com.socialide.Helper.ClickListener;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyAutoCompleteTextView;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.MyEditTextView;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.RecyclerTouchListener;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.BeanForEvent;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.Model.BeanForUploadImage;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

import static com.socialide.Activities.MainActivity.tabPosition;
import static com.socialide.Adapters.EventPhotosAdapter.isClickable;

public class AddEventPhotosActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete, View.OnClickListener {

    RelativeLayout ll_fake_actionbar;
    View rootView;
    RecyclerView mRecyclerView;
    EventPhotosAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final int INTENT_REQUEST_GET_IMAGES = 13;

    final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    List<String> permissionsNeeded;
    List<String> permissionsList;
    // public static ArrayList<String> image_uris = new ArrayList<>();
    ArrayList<String> imageUploadArr = new ArrayList<>();
    ArrayList<NameValuePair> params;
    BeanForEvent beanForEvent;

    AVLoadingIndicatorView view;
    MyButton btn_continue;
    MyAutoCompleteTextView edt_Event, edt_conducted_by;
    public static ArrayList<String> eventArr = new ArrayList<>();
    ArrayList<BeanForEvent> beanForEvents = new ArrayList<>();
    MyEditTextView edt_date, edt_time, edt_about_you;
    HttpEntity resEntity;
    OkHttpClient okHttpClient;
    ProgressDialog dialog;
    AlertDialog alert11;
    OnAsyncRequestComplete caller;
    private int imageLimit = 0;
    String successMsg;
    String eventId = "";
    boolean checkProfile = false;
    BeanForFetchEvents b;
    MyTextView tvNoEvent;
    LinearLayout layoutMain;

    public static ArrayList<BeanForUploadImage> beanForUploadImages = new ArrayList<>();
    BeanForUploadImage beanForUploadImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_events_photo_upload);
        initRecyclerView();
        layoutMain.setVisibility(View.GONE);
        tvNoEvent.setVisibility(View.GONE);
        if (getIntent().getStringExtra("from").equalsIgnoreCase("profile")) {
            checkProfile = false;
        } else {
            checkProfile = true;
        }
        beanForUploadImages.clear();
        //image_uris.clear();
        imageUploadArr.clear();
        eventArr.clear();
        // mRecyclerView_country.setEnabled(false);
        dialogProgress();
        if (checkProfile) {
            isClickable = false;
            btn_continue.setClickable(false);
            params = getParams();
            AsyncRequest getPosts = new AsyncRequest(this, this, "POST", params, view);
            getPosts.execute(GloabalURI.baseURI + GloabalURI.getEvent);
        } else {
            layoutMain.setVisibility(View.VISIBLE);
            isClickable = true;
            btn_continue.setClickable(true);
            beanForUploadImages.clear();
            //image_uris.clear();
            imageUploadArr.clear();
            b = MyPastEvents.activity_list1.get(EventFragment.position);
            edt_Event.setText(b.getEvent_title());

            Log.v("akram", "date = " + b.getEvent_date() + " time = " + b.getEvent_time());
            String datetime = TimeConveter.getdatetime(b.getEvent_date(), b.getEvent_time());

            String date = datetime.split("T")[0];
            String time = datetime.split("T")[1];

            String dateToUTC = TimeConveter.convertToUTC(date, time);
            Log.v("akram", "date to UTC = " + dateToUTC);
            edt_date.setText(date);
            edt_time.setText(time);
            edt_conducted_by.setText(b.getEvent_by());
            eventId = b.getEvent_id();

            if (b.getEvent_description().equalsIgnoreCase("") || b.getEvent_description().equalsIgnoreCase(null)) {
                edt_about_you.setTextColor(Color.parseColor("#000000"));
                edt_about_you.setText("");
                edt_about_you.setFocusable(true);
            } else {
                edt_about_you.setTextColor(Color.parseColor("#88000000"));
                edt_about_you.setFocusable(false);
                edt_about_you.setText(b.getEvent_description());
            }

            JSONArray jsonArray1 = MyPastEvents.eventImageUrlJson.get(EventFragment.position);
            for (int i = 0; i < MyPastEvents.eventImageUrlJson.get(EventFragment.position).length(); i++) {
                try {
                    JSONObject jsonObject1 = jsonArray1.getJSONObject(i);

                    // image_uris.add(jsonObject1.getString("image_url"));
                    beanForUploadImages.add(new BeanForUploadImage(jsonObject1.getString("image_url"), true));

                    if (jsonObject1.getString("type").equalsIgnoreCase(Mypreferences.User_Type)) {
                        imageLimit++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


          /*  for (int i = 0; i < MyPastEvents.eventImageUrlArr.get(EventFragment.position).size(); i++) {
                image_uris.add(MyPastEvents.eventImageUrlArr.get(EventFragment.position).get(i));
                if (MyPastEvents.eventTypeUrlArr.get(EventFragment.position).get(i).equalsIgnoreCase(Mypreferences.User_Type)) {
                    imageLimit++;
                }
            }*/
            adapter.notifyDataSetChanged();


            /*eventArr.add(jsonObject.getString("event_title"));
            BeanForEvent beanForEvent = new BeanForEvent(jsonObject.getString("event_title"),
                    jsonObject.getString("event_description"), jsonObject.getString("event_date"),
                    jsonObject.getString("event_time"), jsonObject.getString("event_by"), jsonObject.getString("event_id"), eventPhotoses);

            beanForEvents.add(beanForEvent);

        ArrayAdapter a = new ArrayAdapter(AddEventPhotosActivity.this, R.layout.spinner_item, R.id.txt, eventArr);
        edt_Event.setAdapter(a);*/

        }

        ArrayAdapter a = new ArrayAdapter(AddEventPhotosActivity.this, R.layout.spinner_item, R.id.txt, eventArr);
        edt_Event.setAdapter(a);

        edt_Event.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                beanForEvent = beanForEvents.get(i);
                beanForUploadImages.clear();
                // image_uris.clear();
                imageUploadArr.clear();
                edt_time.setText(beanForEvent.getEvent_time());
                edt_date.setText(beanForEvent.getEvent_date());
                edt_conducted_by.setText(beanForEvent.getEvent_by());
                eventId = beanForEvent.getEvent_id();
                if (beanForEvent.getEvent_description().equalsIgnoreCase("") || beanForEvent.getEvent_description().equalsIgnoreCase(null)) {
                    edt_about_you.setTextColor(Color.parseColor("#000000"));
                    edt_about_you.setText("");
                    edt_about_you.setFocusable(true);
                } else {
                    edt_about_you.setTextColor(Color.parseColor("#88000000"));
                    edt_about_you.setFocusable(false);
                    edt_about_you.setText(beanForEvent.getEvent_description());
                }
                imageLimit = 0;

                for (int j = 0; j < beanForEvent.getEventPhotoses().size(); j++) {

                    BeanForEvent.EventPhotos eventPhotos = beanForEvent.getEventPhotoses().get(j);

                    beanForUploadImages.add(new BeanForUploadImage(eventPhotos.getImage_url(), true));

                    //image_uris.add(eventPhotos.getImage_url());

                    if (eventPhotos.getType().equalsIgnoreCase(Mypreferences.User_Type)) {
                        imageLimit++;
                    }

                    File file = new File(eventPhotos.getImage_url());

                }
                adapter.notifyDataSetChanged();
            }
        });

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (isClickable != false) {

                    if (beanForUploadImages.size() == position && !edt_Event.getText().toString().equalsIgnoreCase("")) {

                        if (imageUploadArr.size() != 5&&beanForUploadImages.size()!=5) {
                            permission();
                        }else{
                          //  Toast.makeText(AddEventPhotosActivity.this, "Already set 5 Images.", Toast.LENGTH_SHORT).show();
                            CustomSnackBar.toast(AddEventPhotosActivity.this,  "Already set 5 images for this event.");

                        }
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //  popup();
            }
        }));

        if (checkProfile) {
            edt_Event.setOnClickListener(this);
        }
        btn_continue.setOnClickListener(this);
    }

    private void getImages() {

        Config config = new Config();
        config.setSelectionLimit(5 - beanForUploadImages.size());
        ImagePickerActivity.setConfig(config);
        Intent intent = new Intent(AddEventPhotosActivity.this, ImagePickerActivity.class);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
    }


    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ll_fake_actionbar = (RelativeLayout) findViewById(R.id.ll_fake_actionbar);
        view = (AVLoadingIndicatorView) findViewById(R.id.avi);
        btn_continue = (MyButton) findViewById(R.id.btn_continue);
        edt_Event = (MyAutoCompleteTextView) findViewById(R.id.edt_Event);

        edt_date = (MyEditTextView) findViewById(R.id.edt_date);
        edt_time = (MyEditTextView) findViewById(R.id.edt_time);
        edt_about_you = (MyEditTextView) findViewById(R.id.edt_about_you);
        tvNoEvent = (MyTextView) findViewById(R.id.tvNoEvent);
        layoutMain = (LinearLayout) findViewById(R.id.layoutMain);
        edt_conducted_by = (MyAutoCompleteTextView) findViewById(R.id.edt_conducted_by);

        ll_fake_actionbar.setVisibility(View.VISIBLE);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(AddEventPhotosActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        adapter = new EventPhotosAdapter(AddEventPhotosActivity.this, AddEventPhotosActivity.this, 0);
        mRecyclerView.setAdapter(adapter);
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("userid", "" + Mypreferences.User_id));
        params.add(new BasicNameValuePair("type", Mypreferences.User_Type));
        params.add(new BasicNameValuePair("pagecount", "20"));
        params.add(new BasicNameValuePair("pageindex", "1"));
        params.add(new BasicNameValuePair("access_token", Mypreferences.Access_token));

        return params;
    }

    @Override
    public void onActivityResult(int requestCode, int resuleCode, Intent intent) {
        super.onActivityResult(requestCode, resuleCode, intent);

        if (requestCode == INTENT_REQUEST_GET_IMAGES && resuleCode == Activity.RESULT_OK) {

            ArrayList<Uri> uris = new ArrayList<>();

            uris = intent.getParcelableArrayListExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

            for (int i = 0; i < uris.size(); i++) {
                beanForUploadImages.add(new BeanForUploadImage(getPath(uris.get(i)), false));

                //image_uris.add(getPath(uris.get(i)));
                imageUploadArr.add(getPath(uris.get(i)));

            }

            adapter.notifyDataSetChanged();
            //do something
        }
    }

    public void permission() {

        ActivityCompat.requestPermissions(AddEventPhotosActivity.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA},
                1);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case 1: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial

                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION

                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted

                    getImages();
                    // selection_page();
                } else {
                    // Permission Denied
                    // finish();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void asyncResponse(String response) {

        isClickable = true;
        btn_continue.setClickable(true);

        FunctionClass.logCatLong(response);


        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.getString("success").equalsIgnoreCase("true")) {
                tvNoEvent.setVisibility(View.GONE);
                layoutMain.setVisibility(View.VISIBLE);
                JSONArray jsonArray = postObject.getJSONArray("result");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    JSONArray jsonArray1 = jsonObject.getJSONArray("event_photos");

                    ArrayList<BeanForEvent.EventPhotos> eventPhotoses = new ArrayList<>();

                    for (int j = 0; j < jsonArray1.length(); j++) {

                        try {
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(j);

                            if (jsonObject1.getString("type").equalsIgnoreCase("" + Mypreferences.User_Type)) {
                                BeanForEvent.EventPhotos eventPhotos = new BeanForEvent.EventPhotos(jsonObject1.getString("type"),
                                        jsonObject1.getString("userid"), jsonObject1.getString("image_id"), jsonObject1.getString("image_url"));
                                eventPhotoses.add(eventPhotos);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    eventArr.add(jsonObject.getString("event_title"));
                    BeanForEvent beanForEvent = new BeanForEvent(jsonObject.getString("event_title"),
                            jsonObject.getString("event_description"), jsonObject.getString("event_date"),
                            jsonObject.getString("event_time"), jsonObject.getString("event_by"), jsonObject.getString("event_id"), eventPhotoses);

                    beanForEvents.add(beanForEvent);
                }
                ArrayAdapter a = new ArrayAdapter(AddEventPhotosActivity.this, R.layout.spinner_item, R.id.txt, eventArr);
                edt_Event.setAdapter(a);

            } else {

                tvNoEvent.setVisibility(View.VISIBLE);
                layoutMain.setVisibility(View.GONE);

            }

        } catch (JSONException e) {

            e.printStackTrace();
        }
    }

    private String getPath(Uri uri) {

        if (uri == null) {
            return null;
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.edt_Event:

                edt_Event.showDropDown();

                break;

            case R.id.btn_continue:
                Log.v("akram", "image upload sixe = " + imageUploadArr.size());
                try {

                    if (edt_Event.getText().toString().equalsIgnoreCase("")) {
                        CustomSnackBar.toast(AddEventPhotosActivity.this, "Please select event.");
                    } else {

                        if (beanForUploadImages.size()!=5) {
                            if (imageUploadArr.size() != 0 && imageUploadArr != null) {
                                UploadEventPhotot uploadEventPhotot = new UploadEventPhotot();
                                uploadEventPhotot.execute();
                            } else {
                                CustomSnackBar.toast(AddEventPhotosActivity.this, "Please select photos.");
                            }
                        }else{
                            CustomSnackBar.toast(AddEventPhotosActivity.this,  "Already set 5 images for this event.");
                        }
                    }
                    //execMultipartPost();
                    // executeMultipartPost();
                    // execMultipartPost();
                } catch (Exception e) {
                    Log.v("akram", "catchhhh =" + e);
                    e.printStackTrace();
                }
                break;
        }
    }

    public String image(Bitmap bm) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.socialide");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myDir + "/" + fname;
    }


    Bitmap bitmap = null;

    public void dialogProgress() {

        dialog = new ProgressDialog(AddEventPhotosActivity.this);

        dialog.setMessage("Photos uploading may take a while, please be patient.");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
       /* dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialogSecond();

            }
        });*/
    }

    public void dialogSecond() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(AddEventPhotosActivity.this);
        builder1.setMessage("Photos uploading is in progress, do you want to cancel it?");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog1, int id) {
                        //  okHttpClient.connectTimeoutMillis();
                        // UploadImageextends uploadImageextends
                        dialog1.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog1, int id) {
                        dialogProgress();
                    }
                });

        alert11 = builder1.create();
        alert11.show();

    }

    private String execMultipartPost() throws Exception {

        okHttpClient = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).build();

        try {

            ArrayList<RequestBody> requestBodies = new ArrayList<>();
            RequestBody requestBody3;

            RequestBody requestBody = null, requestBody11 = null, requestBody22 = null, requestBody33 = null, requestBody44 = null, requestBody55 = null;
            File file1 = null, file2 = null, file3 = null, file4 = null, file5 = null;
            for (int i = 0; i < imageUploadArr.size(); i++) {

                switch (i) {

                    case 0:

                        bitmap = BitmapFactory.decodeFile(imageUploadArr.get(i));
                        file1 = new File(image(bitmap));
                        String contentType = file1.toURL().openConnection().getContentType();
                        requestBody11 = RequestBody.create(MediaType.parse(contentType), file1);

                        break;

                    case 1:

                        file2 = new File(imageUploadArr.get(i));
                        String contentType1 = file2.toURL().openConnection().getContentType();
                        requestBody22 = RequestBody.create(MediaType.parse(contentType1), file2);

                        break;
                    case 2:

                        file3 = new File(imageUploadArr.get(i));
                        String contentType2 = file3.toURL().openConnection().getContentType();
                        requestBody33 = RequestBody.create(MediaType.parse(contentType2), file3);


                        break;
                    case 3:

                        file4 = new File(imageUploadArr.get(i));
                        String contentType3 = file4.toURL().openConnection().getContentType();
                        requestBody44 = RequestBody.create(MediaType.parse(contentType3), file4);

                        break;
                    case 4:

                        file5 = new File(imageUploadArr.get(i));
                        String contentType4 = file5.toURL().openConnection().getContentType();
                        requestBody55 = RequestBody.create(MediaType.parse(contentType4), file5);

                        break;
                }
            }

            if (imageUploadArr.size() == 1) {

                requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("eventImage1", file1.getName(), requestBody11)
                        .addFormDataPart("eventImageCount", "" + imageUploadArr.size())
                        .addFormDataPart("userid", "" + Mypreferences.User_id)
                        .addFormDataPart("type", "" + Mypreferences.User_Type)
                        .addFormDataPart("eventId", eventId)
                        .addFormDataPart("event_description", edt_about_you.getText().toString())
                        .addFormDataPart("access_token", Mypreferences.Access_token)
                        .build();

            } else if (imageUploadArr.size() == 2) {

                requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("eventImage1", file1.getName(), requestBody11)
                        .addFormDataPart("eventImage2", file2.getName(), requestBody22)
                        .addFormDataPart("eventImageCount", "" + imageUploadArr.size())
                        .addFormDataPart("userid", "" + Mypreferences.User_id)
                        .addFormDataPart("type", "" + Mypreferences.User_Type)
                        .addFormDataPart("eventId", eventId)
                        .addFormDataPart("event_description", edt_about_you.getText().toString())
                        .addFormDataPart("access_token", Mypreferences.Access_token)
                        .build();

            } else if (imageUploadArr.size() == 3) {

                requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("eventImage1", file1.getName(), requestBody11)
                        .addFormDataPart("eventImage2", file2.getName(), requestBody22)
                        .addFormDataPart("eventImage3", file3.getName(), requestBody33)
                        .addFormDataPart("eventImageCount", "" + imageUploadArr.size())
                        .addFormDataPart("userid", "" + Mypreferences.User_id)
                        .addFormDataPart("type", "" + Mypreferences.User_Type)
                        .addFormDataPart("event_description", edt_about_you.getText().toString())
                        .addFormDataPart("access_token", Mypreferences.Access_token)
                        .addFormDataPart("eventId", eventId)
                        .build();

            } else if (imageUploadArr.size() == 4) {

                requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("eventImage1", file1.getName(), requestBody11)
                        .addFormDataPart("eventImage2", file2.getName(), requestBody22)
                        .addFormDataPart("eventImage3", file3.getName(), requestBody33)
                        .addFormDataPart("eventImage4", file4.getName(), requestBody44)
                        .addFormDataPart("eventImageCount", "" + imageUploadArr.size())
                        .addFormDataPart("userid", "" + Mypreferences.User_id)
                        .addFormDataPart("type", "" + Mypreferences.User_Type)
                        .addFormDataPart("eventId", eventId)
                        .addFormDataPart("event_description", edt_about_you.getText().toString())
                        .addFormDataPart("access_token", Mypreferences.Access_token)
                        .build();

            } else if (imageUploadArr.size() == 5) {

                requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("eventImage1", file1.getName(), requestBody11)
                        .addFormDataPart("eventImage2", file2.getName(), requestBody22)
                        .addFormDataPart("eventImage3", file3.getName(), requestBody33)
                        .addFormDataPart("eventImage4", file4.getName(), requestBody44)
                        .addFormDataPart("eventImage5", file5.getName(), requestBody55)
                        .addFormDataPart("eventImageCount", "" + imageUploadArr.size())
                        .addFormDataPart("userid", "" + Mypreferences.User_id)
                        .addFormDataPart("type", "" + Mypreferences.User_Type)
                        .addFormDataPart("eventId", eventId)
                        .addFormDataPart("event_description", edt_about_you.getText().toString())
                        .addFormDataPart("access_token", Mypreferences.Access_token)
                        .build();
            } else {
                CustomSnackBar.toast(AddEventPhotosActivity.this, "Limit Crossed");
            }

            if (requestBody != null) {
                Request request = new Request.Builder()
                        .url(GloabalURI.baseURI + GloabalURI.uploadEventPhotos)
                        .addHeader("access_token", Mypreferences.Access_token)
                        .post(requestBody)
                        .build();


                final Buffer buffer = new Buffer();
                request.body().writeTo(buffer);

               /* httpClient.getConnectionManager().shutdown();*/
                Response responseMain = okHttpClient.newCall(request).execute();
                Log.v("akram", "respomnse " + responseMain.body().string());
                String responseJson = responseMain.body().string();
                JSONObject postObject = new JSONObject(responseJson);
                successMsg = postObject.getString("success");


                return responseMain.body().string();
            }

        } catch (Exception e) {

            if (checkProfile) {
                tabPosition = 0;
            } else {
                tabPosition = 4;
            }
            Intent intent = new Intent(AddEventPhotosActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
            finishAffinity();
        }
        return null;
    }

    public class UploadEventPhotot extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog.show();
        }

        @Override
        protected void onPostExecute(String response) {

            Log.v("akram", "success = " + successMsg);
            if (successMsg != null) {

                try {

                    if (successMsg.equals("true")) {
                        // JSONObject result= postObject.getJSONObject("result");
                        if (checkProfile) {
                            tabPosition = 0;
                        } else {
                            tabPosition = 4;
                        }
                        Intent intent = new Intent(AddEventPhotosActivity.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
                        finishAffinity();

                    } else {

                        JSONObject jsonObject = new JSONObject();
                        String s = "" + jsonObject.getString("message");

                        CustomSnackBar.toast(AddEventPhotosActivity.this, s);
                    }
                } catch (JSONException e) {
                    Log.v("akram", "catch = " + e);
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }

        @Override
        protected String doInBackground(String... urls) {

            try {
                return execMultipartPost();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public void onBackPressed() {

        exitDialog();

    }

    public interface OnAsyncRequestComplete {
        public void asyncResponse(String response);
    }


    public Dialog exitDialog() {

        LayoutInflater factory = LayoutInflater.from(AddEventPhotosActivity.this);
        final View view = factory.inflate(R.layout.dialog_exit, null);
        final AlertDialog alertDialoge = new AlertDialog.Builder(AddEventPhotosActivity.this).create();
        MyTextView txt_yes = (MyTextView) view.findViewById(R.id.txt_yes);
        MyTextView txt_no = (MyTextView) view.findViewById(R.id.txt_no);
        MyTextView tvMsg = (MyTextView) view.findViewById(R.id.tvMsg);
        tvMsg.setText(getResources().getString(R.string.dialog_backDesc));

        txt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkProfile == true) {
                    Intent intent = new Intent(AddEventPhotosActivity.this, MainActivity.class);

                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
                    finish();
                } else {
                    Intent intent = new Intent(AddEventPhotosActivity.this, MyEventDetalils.class);

                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_to_bottom, R.anim.slide_from_top);
                    finish();
                }

            }
        });

        txt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialoge.dismiss();
            }
        });
        alertDialoge.setCancelable(true);
        alertDialoge.setCanceledOnTouchOutside(true);
        alertDialoge.setView(view);

        alertDialoge.show();

        return alertDialoge;

    }

}
