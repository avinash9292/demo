package com.socialide.Adapters;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.socialide.Helper.MyImageView;

import com.socialide.Helper.TouchImageView;
import com.socialide.R;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Avinash on 5/4/2017.
 */
public class FullScreenImageAdapter extends PagerAdapter {
    private DisplayImageOptions options;
    private Activity _activity;
    private ArrayList<String> _imagePaths;
    private LayoutInflater inflater;

    // constructor
    public FullScreenImageAdapter(Activity activity,
                                  ArrayList<String> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.profilepic)
                .showImageOnFail(R.drawable.ic_cross)
                .resetViewBeforeLoading(true)
//.cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_view, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);

        final DonutProgress spinner = (DonutProgress) viewLayout.findViewById(R.id.loading);
        com.socialide.Helper.ImageLoader.image(_imagePaths.get(position), imgDisplay, spinner, _activity);

//
//        ImageLoader imageLoader=ImageLoader.getInstance();
//        imageLoader.init(ImageLoaderConfiguration.createDefault(_activity));
//
//        imageLoader.displayImage(_imagePaths.get(position), imgDisplay, options, new SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingStarted(String imageUri, View view) {
//                int count;
//                URL url = null;
//                spinner.setVisibility(View.VISIBLE);
//
//                spinner.setMax(100);
//
//
//                spinner.setProgress(0);
//
//            }
//
//            @Override
//            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                String message = null;
//                switch (failReason.getType()) {
//                    case IO_ERROR:
//                        message = "Input/Output error";
//                        break;
//                    case DECODING_ERROR:
//                        message = "Image can't be decoded";
//                        break;
//                    case NETWORK_DENIED:
//                        message = "Downloads are denied";
//                        break;
//                    case OUT_OF_MEMORY:
//                        message = "Out Of Memory error";
//                        break;
//                    case UNKNOWN:
//                        message = "Unknown error";
//                        break;
//                }
//                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
//
//                spinner.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                spinner.setVisibility(View.GONE);
//            }
//
//
//        }, new ImageLoadingProgressListener() {
//            @Override
//            public void onProgressUpdate(String imageUri, View view, int current, int total) {
//
//                Log.e("current","aa ;   "+current);
//                Log.e("total","total ;   "+total);
//                    spinner.setProgress(Math.round(100.0f * current / total));
//            }
//        });

        container.addView(viewLayout, 0);


//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//        Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
//        imgDisplay.setImageBitmap(bitmap);

        //   new DownloadImageTask(imgDisplay).execute(_imagePaths.get(position));

        // close button click event

        //    ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        TouchImageView bmImage;
        ProgressDialog pg = new ProgressDialog(_activity);
        Bitmap mIcon11;

        public DownloadImageTask(TouchImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            pg.setMessage("wait");
            // pg.show();


            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                pg.dismiss();
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(
                Bitmap result) {
            pg.dismiss();
            bmImage.setImageBitmap(result);
        }

    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}