package com.socialide.Adapters;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.socialide.Fragments.ActivityFragment;
import com.socialide.Fragments.AddEventPhotos;
import com.socialide.Fragments.EventDetails;
import com.socialide.Fragments.EventFragment;
import com.socialide.Fragments.MeFragment;
import com.socialide.Fragments.SearchProfessional1;
import com.socialide.Fragments.SearchProfessionals;
import com.socialide.Fragments.TopUsersFragment;
import com.socialide.Helper.Mypreferences;

import java.util.ArrayList;

/**
 *
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

	private ArrayList<Fragment> fragments = new ArrayList<>();
	private Fragment currentFragment;

	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);

		fragments.clear();
		fragments.add(new ActivityFragment());
		fragments.add(new EventFragment());
		if (Mypreferences.User_Type.equals("Organization")) {
			fragments.add(new SearchProfessional1());

		}
		else {
			fragments.add(new AddEventPhotos());

		}
		fragments.add(new TopUsersFragment());
		fragments.add(new MeFragment());

	}

	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return 1;

	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		if (getCurrentFragment() != object) {
			currentFragment = ((Fragment) object);
		}
		super.setPrimaryItem(container, position, object);
	}

	/**
	 * Get the current fragment
	 */
	public Fragment getCurrentFragment() {
		return currentFragment;
	}
}