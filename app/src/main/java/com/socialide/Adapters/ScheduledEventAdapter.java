package com.socialide.Adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.socialide.Activities.DetailsActivity;
import com.socialide.Fragments.EventDetails;
import com.socialide.Fragments.EventsPending;
import com.socialide.Fragments.ProDeclinedEventDetails;
import com.socialide.Fragments.ProPendingEventDetails;
import com.socialide.Fragments.ProScheduledEventDetails;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Victor on 7/2/15.
 */
public class ScheduledEventAdapter extends RecyclerView.Adapter<ScheduledEventAdapter.EventsViewHolder> {
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<BeanForFetchEvents> event_list= new ArrayList<>();

    Context mContext;

    RecyclerView mRecyclerView;

    public ScheduledEventAdapter(Context context,RecyclerView mRecyclerView,ArrayList<BeanForFetchEvents> event_list){
        this.mContext = context;
        this.event_list=event_list;
        this.mRecyclerView=mRecyclerView;


        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)  this.mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();


                Log.e("totalItemCount",""+totalItemCount);
                Log.e("lastVisibleItem",""+lastVisibleItem+visibleThreshold);

                if (!EventsPending.isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    EventsPending.isLoading = true;
                }
            }
        });
    }
    @Override
    public int getItemViewType(int position) {
        return event_list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_event_item,parent,false);

        EventsViewHolder viewHolder = new EventsViewHolder(view);
        viewHolder.iv_event_profile_pic = (ImageView)view.findViewById(R.id.iv_event_profile_pic);
        viewHolder.tv_event_organisaton = (MyTextView)view.findViewById(R.id.tv_profession);
        viewHolder.tv_event_name = (MyTextView)view.findViewById(R.id.tv_event_name);
        viewHolder.tv_city = (MyTextView)view.findViewById(R.id.tv_city);
        viewHolder.tv_detail = (MyTextView)view.findViewById(R.id.tv_detail);
        viewHolder.txt_date = (MyTextView)view.findViewById(R.id.txt_date);
        viewHolder.txt_time = (MyTextView)view.findViewById(R.id.txt_time);
        viewHolder.rl_item = (RelativeLayout)view.findViewById(R.id.rl_item);






       view.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i=new Intent(mContext,DetailsActivity.class);
               ActivityOptions options =
                       ActivityOptions.makeCustomAnimation(mContext,R.anim.slide_to_right, R.anim.slide_from_left);

               if(Mypreferences.getString(Mypreferences.User_Type,mContext).equals("o")) {
                   DetailsActivity.f = new EventDetails();

                   if (DetailsActivity.type.equals("s")) {
                       i.putExtra("f", "f");
                       mContext.startActivity(i,options.toBundle());
                   }

                   if (DetailsActivity.type.equals("p")) {

                       i.putExtra("f", "f");

                       mContext.startActivity(i,options.toBundle());
                   }

                   if (DetailsActivity.type.equals("d")) {
                       i.putExtra("f", "f");

                       mContext.startActivity(i,options.toBundle());
                   }

               }
               else {

                   if (DetailsActivity.type.equals("s")) {
                       i.putExtra("f", "f");
                       DetailsActivity.f = new ProScheduledEventDetails();

                       mContext.startActivity(i,options.toBundle());
                   }

                   if (DetailsActivity.type.equals("p")) {
                       DetailsActivity.f = new ProPendingEventDetails();

                       i.putExtra("f", "f");

                       mContext.startActivity(i,options.toBundle());
                   }

                   if (DetailsActivity.type.equals("d")) {
                       i.putExtra("f", "f");
                       DetailsActivity.f = new ProDeclinedEventDetails();


                       mContext.startActivity(i,options.toBundle());
                   }

               }

           }
       });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {


        BeanForFetchEvents bean= event_list.get(position);
        holder.txt_time.setText(""+bean.getEvent_time());
        holder.txt_date.setText(""+bean.getEvent_date());
        holder.tv_event_name.setText(""+bean.getEvent_title());
        holder.tv_event_organisaton.setText(""+bean.getEvent_by());
//        holder.tv_city.setText(""+bean.getOrganization_name());
        holder.tv_detail.setText(""+bean.getEvent_description());


        holder.iv_event_profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        Log.e("alalala","a "+event_list.size());
        return event_list.size();
    }

    static class EventsViewHolder extends RecyclerView.ViewHolder {


        public ImageView iv_event_profile_pic,right_gray_arrow;
        public MyTextView  tv_event_name,tv_event_organisaton,tv_city,tv_detail,txt_date,txt_time;



        public RelativeLayout rl_item;


        public EventsViewHolder(View v){
            super(v);
        }

    }
     OnLoadMoreListener mOnLoadMoreListener;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }
    public void setLoaded() {
        EventsPending.isLoading = false;
    }
}
