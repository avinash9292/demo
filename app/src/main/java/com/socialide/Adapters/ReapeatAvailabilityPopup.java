package com.socialide.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import android.widget.ToggleButton;

import com.socialide.Fragments.ProffressionalAvailability;
import com.socialide.Helper.MyTextView;
import com.socialide.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Victor on 6/30/15.
 */
public class ReapeatAvailabilityPopup extends RecyclerView.Adapter<ReapeatAvailabilityPopup.SettingsViewHolderItem>{


static public ArrayList<Integer> selection=new ArrayList<>();
    ArrayList<String> info;
    Context mContext;

    public ReapeatAvailabilityPopup(Context context){
        this.mContext = context;
        info = new ArrayList<>();

        info.add(context.getString(R.string.txt_weekday1));
        info.add(context.getString(R.string.txt_weekday2));
        info.add(context.getString(R.string.txt_weekday3));
        info.add(context.getString(R.string.txt_weekday4));
        info.add(context.getString(R.string.txt_weekday5));
        info.add(context.getString(R.string.txt_weekday6));
        info.add(context.getString(R.string.txt_weekday7));


    }

    @Override
    public SettingsViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_week_days_selection,parent,false);
        SettingsViewHolderItem settingsViewHolderItem = new SettingsViewHolderItem(view);
        settingsViewHolderItem.infoMyTextView = (MyTextView)view.findViewById(R.id.txt_week_days);
        settingsViewHolderItem.chk = (CheckBox)view.findViewById(R.id.chk_week_days);
        return settingsViewHolderItem;
    }

    @Override
    public void onBindViewHolder(final SettingsViewHolderItem holder, final int position) {
        holder.infoMyTextView.setText(""+info.get(position));
        if(ProffressionalAvailability.group==position)
        {
            holder.chk.setChecked(true);
        }
        else {
            holder.chk.setChecked(false);
        }
        holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    selection.add(position);
                }
                else {

                    selection.removeAll(Arrays.asList(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return 7;
    }

    static class SettingsViewHolderItem extends RecyclerView.ViewHolder{
        public MyTextView infoMyTextView;
        public View view;
        public CheckBox chk;

        public SettingsViewHolderItem(View v){
            super(v);
            this.view = v;
        }
    }


}
