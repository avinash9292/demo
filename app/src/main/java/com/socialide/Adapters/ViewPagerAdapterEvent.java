package com.socialide.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.socialide.Fragments.EventsDeclined;
import com.socialide.Fragments.EventsPending;
import com.socialide.Fragments.EventsScheduled;

import java.util.ArrayList;

/**
 *
 */
public class ViewPagerAdapterEvent extends FragmentStatePagerAdapter {

	//integer to count number of tabs
	int tabCount;

	//Constructor to the class
	public ViewPagerAdapterEvent(FragmentManager fm, int tabCount) {
		super(fm);
		//Initializing tab count
		this.tabCount= tabCount;
	}

	//Overriding method getItem
	@Override
	public Fragment getItem(int position) {
		//Returning the current tabs
		switch (position) {
			case 0:
				EventsScheduled tab1 = new EventsScheduled();
				return tab1;
			case 1:
				EventsPending tab2 = new EventsPending();
				return tab2;
			case 2:
				EventsDeclined tab3 = new EventsDeclined();
				return tab3;
			default:
				return null;
		}
	}

	//Overriden method getCount to get the number of tabs
	@Override
	public int getCount() {
		return tabCount;
	}
}