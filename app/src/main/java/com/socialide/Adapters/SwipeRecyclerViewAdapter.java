package com.socialide.Adapters;

/**
 * Created by Akram on 11/10/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Activities.NotificationActivity;
import com.socialide.AsyncTask.AsyncRequest;
import com.socialide.Fragments.LoginFragment;
import com.socialide.Helper.CircularImageView;
import com.socialide.Helper.Config;
import com.socialide.Helper.FunctionClass;
import com.socialide.Helper.GloabalURI;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.TimeConveter;
import com.socialide.Model.AllNotificationModel;
import com.socialide.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SimpleViewHolder> implements AsyncRequest.OnAsyncRequestComplete {


    private Context mContext;
    private ArrayList<AllNotificationModel> studentList;
    Activity activity;
    int deletePosition = 0;
    SwipeLayout swipeLayoutGlobal;


    public SwipeRecyclerViewAdapter(Activity activity, Context context, ArrayList<AllNotificationModel> objects) {
        this.mContext = context;
        this.activity = activity;
        this.studentList = objects;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_row_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final AllNotificationModel item = studentList.get(position);

        com.socialide.Helper.ImageLoader.image(item.getProfile_pic(), viewHolder.circleImage, viewHolder.loading, activity);
        viewHolder.tvNotification.setText(item.getNotification());

        String[] dater = item.getCreated_on().trim().split(" ");

        if (TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]).equals("false")) {
            viewHolder.tvTime.setText("" + dater[0] + " " + dater[1] + " " + dater[2]);

        } else {
            viewHolder.tvTime.setText("" + TimeConveter.agotime(dater[0] + " " + dater[1] + " " + dater[2], dater[3] + " " + dater[4]));
        }

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
        // viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper1));

        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));

        viewHolder.swipeLayout.setTag(position);
        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });


        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(mContext, " onClick : " + item.getName() + " \n" + item.getEmailId(), Toast.LENGTH_SHORT).show();
            }
        });

       /* viewHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                studentList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, studentList.size());
                mItemManger.closeAllItems();
                // Toast.makeText(view.getContext(), "Deleted " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });*/
        viewHolder.linearDelete.setTag(position);
        viewHolder.linearRead.setTag(position);

        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

        viewHolder.linearDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int post = (Integer) v.getTag();

                AllNotificationModel item = studentList.get(post);

                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("noti_id", "" + item.getNoti_id()));
                swipeLayoutGlobal = viewHolder.swipeLayout;
                deletePosition = post;
                AsyncRequest getPosts = new AsyncRequest(SwipeRecyclerViewAdapter.this, activity, "POST", params, NotificationActivity.avi);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.deleteNotificationMessage);

            }
        });

        viewHolder.linearRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int post = (Integer) v.getTag();
                //AllNotificationModel item = new AllNotificationModel();
                viewHolder.circleRead.setVisibility(View.INVISIBLE);
                viewHolder.itemRow.setBackgroundColor(Color.parseColor("#ffffff"));
                viewHolder.swipeLayout.close();
                viewHolder.linearRead.setVisibility(View.GONE);

                AllNotificationModel item = studentList.get(post);
                item.setIs_read("1");
                studentList.set(post, item);

                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("noti_id", "" + item.getNoti_id()));
                swipeLayoutGlobal = viewHolder.swipeLayout;
                AsyncRequest getPosts = new AsyncRequest(SwipeRecyclerViewAdapter.this, activity, "POST", params);
                getPosts.execute(GloabalURI.baseURI + GloabalURI.readNotificationMessage);

            }
        });

        if (item.getIs_read().equalsIgnoreCase("0")) {

            viewHolder.circleRead.setVisibility(View.VISIBLE);
            viewHolder.itemRow.setBackgroundColor(Color.parseColor("#303fb4b5"));
            viewHolder.linearRead.setVisibility(View.VISIBLE);
        } else {
            viewHolder.circleRead.setVisibility(View.INVISIBLE);
            viewHolder.itemRow.setBackgroundColor(Color.parseColor("#ffffff"));
            viewHolder.linearRead.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        RelativeLayout itemRow;
        CircleImageView circleRead, circleImage;
        DonutProgress loading;
        MyTextView tvNotification, tvTime;
        LinearLayout linearDelete, linearRead;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            tvNotification = (MyTextView) itemView.findViewById(R.id.tvNotification);
            tvTime = (MyTextView) itemView.findViewById(R.id.tvTime);

            itemRow = (RelativeLayout) itemView.findViewById(R.id.itemRow);
            circleRead = (CircleImageView) itemView.findViewById(R.id.circleRead);
            circleImage = (CircleImageView) itemView.findViewById(R.id.circleImage);
            loading = (DonutProgress) itemView.findViewById(R.id.loading);
            linearDelete = (LinearLayout) itemView.findViewById(R.id.linearDelete);
            linearRead = (LinearLayout) itemView.findViewById(R.id.linearRead);

        }
    }

    @Override
    public void asyncResponse(String response) {
        FunctionClass.logCatLong(response);
        try {
            JSONObject postObject = new JSONObject(response);

            if (postObject.getString("api_name").equals("deleteNotificationMessage")) {

                if (postObject.getString("success").equals("true")) {

                    AllNotificationModel item = studentList.get(deletePosition);
                    if (item.getIs_read().equalsIgnoreCase("0")) {
                        Mypreferences.NotificationCount--;
                        if (Mypreferences.NotificationCount < 0) {
                            Mypreferences.NotificationCount = 0;
                        }
                    }
                    mItemManger.removeShownLayouts(swipeLayoutGlobal);
                    studentList.remove(deletePosition);
                    notifyItemRemoved(deletePosition);
                    notifyItemRangeChanged(deletePosition, studentList.size());
                    mItemManger.closeAllItems();
                }
            } else if (postObject.getString("api_name").equals("readNotificationMessage")) {
                if (postObject.getString("success").equals("true")) {
                    Mypreferences.NotificationCount--;
                    if (Mypreferences.NotificationCount < 0) {
                        Mypreferences.NotificationCount = 0;
                    }
                }
            }
        } catch (Exception e) {
        }
    }

}
