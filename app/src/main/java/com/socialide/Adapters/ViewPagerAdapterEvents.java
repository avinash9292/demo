package com.socialide.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.socialide.Fragments.ActivityFragment;
import com.socialide.Fragments.EventDetails;
import com.socialide.Fragments.EventsDeclined;
import com.socialide.Fragments.EventsPending;
import com.socialide.Fragments.EventsScheduled;
import com.socialide.Fragments.MeFragment;
import com.socialide.Fragments.TopUsersFragment;

import java.util.ArrayList;

/**
 *
 */
public class ViewPagerAdapterEvents extends FragmentStatePagerAdapter {

	private ArrayList<Fragment> fragments = new ArrayList<>();
	private Fragment currentFragment;


	public ViewPagerAdapterEvents(FragmentManager fm) {
		super(fm);

		fragments.clear();
		fragments.add(new EventsScheduled());
		fragments.add(new EventsPending());
		fragments.add(new EventsDeclined());


	}

	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		if (getCurrentFragment() != object) {
			currentFragment = ((Fragment) object);


		}
		super.setPrimaryItem(container, position, object);
	}



	/**
	 * Get the current fragment
	 */
	public Fragment getCurrentFragment() {
		return currentFragment;
	}
}