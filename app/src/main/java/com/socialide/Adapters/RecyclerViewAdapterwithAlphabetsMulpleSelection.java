package com.socialide.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.socialide.Helper.MyTextView;
import com.socialide.Model.BeanForCityList;
import com.socialide.R;
import com.viethoa.RecyclerViewFastScroller;

import java.util.List;

/**
 * Created by VietHoa on 07/10/15.
 */
public class RecyclerViewAdapterwithAlphabetsMulpleSelection extends RecyclerView.Adapter<RecyclerViewAdapterwithAlphabetsMulpleSelection.ViewHolder>
    implements RecyclerViewFastScroller.BubbleTextGetter {

    private List<BeanForCityList> mDataArray;

    public RecyclerViewAdapterwithAlphabetsMulpleSelection(List<BeanForCityList> dataset) {
        mDataArray = dataset;
    }

    @Override
    public int getItemCount() {
        if (mDataArray == null)
            return 0;
        return mDataArray.size();
    }

    @Override
    public RecyclerViewAdapterwithAlphabetsMulpleSelection.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item_multi_selection, parent, false);
        ViewHolder vh = new ViewHolder(v);
        vh.mTextView=(MyTextView) v.findViewById(R.id.txt);
        vh.img_check=(ImageView) v.findViewById(R.id.img_check);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mTextView.setText(mDataArray.get(position).getName());
        if (mDataArray.get(position).isCheck()){
           holder.img_check.setVisibility(View.VISIBLE);
        }
        else {
            holder.img_check.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        if (pos < 0 || pos >= mDataArray.size())
            return null;

        String name = mDataArray.get(pos).getName();
        if (name == null || name.length() < 1)
            return null;

        return mDataArray.get(pos).getName().substring(0, 1);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        MyTextView mTextView;
        ImageView img_check;

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }

}
