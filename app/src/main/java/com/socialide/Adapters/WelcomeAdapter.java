package com.socialide.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.socialide.Activities.LoginActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Activities.WelcomeActivity;
import com.socialide.Helper.MyButton;
import com.socialide.Helper.Mypreferences;
import com.socialide.R;

/**
 * Created by Victor on 6/28/15.
 */
public class WelcomeAdapter extends PagerAdapter {
    MyButton bottomButton;
    RelativeLayout rl;
    Context context;
    WelcomeActivity wel;
    LayoutInflater mLayoutInflater;
    Activity activity;

    public WelcomeAdapter(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        wel = new WelcomeActivity();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView;

        itemView = mLayoutInflater.inflate(R.layout.single_image_view, container, false);

        bottomButton = (MyButton) itemView.findViewById(R.id.ll_bottom);
        rl = (RelativeLayout) itemView.findViewById(R.id.rl);


        if (position == 0) {
            rl.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.first));

        }
        if (position == 1) {
            rl.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.second));

        }

        if (position == 2) {
            rl.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.third));

        }
        if (position == 2) {
            bottomButton.setText(context.getResources().getString(R.string.letsGo));
        } else {
            bottomButton.setText(context.getResources().getString(R.string.Skip));
        }


        bottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (Mypreferences.getString(Mypreferences.User_id, context).equals("")) {
                    WelcomeActivity.checkAnimation = 1;
                    intent = new Intent(context, LoginActivity.class);
                } else {
                    intent = new Intent(context, MainActivity.class);

                }
                Log.v("akram", "intent new ");
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(context, R.anim.slide_to_right, R.anim.slide_from_left);
                context.startActivity(intent, options.toBundle());
                // overridePendingTransition(R.anim.slide_to_left_activity,R.anim.slide_from_right_activity);
                activity.finish();
            }
        });

        container.addView(itemView);
        return itemView;
    }

}
