package com.socialide.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.ToggleButton;

import com.socialide.Helper.MyTextView;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Victor on 6/30/15.
 */
public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsViewHolderItem>{



    ArrayList<String> info;
    Context mContext;

    public SettingsAdapter(Context context){
        this.mContext = context;
        info = new ArrayList<>();
        info.add("General");
        info.add("Maximum Event request (weekly)");
        info.add("Availability");
        info.add("Click here to change password");
        info.add("Push Notification");
        info.add("Someone invited you for an event");
        info.add("Someone cancelled an event");
        info.add("Event reminder");
        info.add("Likes");
        info.add("SMS Notification");
        info.add("Someone invited you for an event");
        info.add("Event reminder");
        info.add("Email Notifications");
        info.add("Someone invited you for an event");
        info.add("Someone cancelled an event");
        info.add("Event reminder");
        info.add("New article");

    }

    @Override
    public SettingsViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_settings_item,parent,false);
        SettingsViewHolderItem settingsViewHolderItem = new SettingsViewHolderItem(view);
        settingsViewHolderItem.infoMyTextView = (MyTextView)view.findViewById(R.id.tv_settings_title);
        settingsViewHolderItem.tgbMyButton = (ToggleButton)view.findViewById(R.id.tgb_on_off_settings);
        return settingsViewHolderItem;
    }

    @Override
    public void onBindViewHolder(SettingsViewHolderItem holder, int position) {

//        holder.infoMyTextView.setText(info.get(position));
//        if(isSettingsTitle(position)){
//            holder.view.setBackgroundColor(mContext.getResources().getColor(R.color.divider));
//            holder.tgbMyButton.setVisibility(View.GONE);
//        }else{
//            holder.view.setBackgroundColor(mContext.getResources().getColor(R.color.icons));
//            holder.tgbMyButton.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public int getItemCount() {
        return info.size();
    }

    static class SettingsViewHolderItem extends RecyclerView.ViewHolder{
        public MyTextView infoMyTextView;
        public View view;
        public ToggleButton tgbMyButton;

        public SettingsViewHolderItem(View v){
            super(v);
            this.view = v;
        }
    }

    public boolean isSettingsTitle(int position){
        if(position == 0 || position == 4 || position == 9 || position == 13 )
            return true;
        return false;
    }
}
