package com.socialide.Adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;


import com.github.lzyzsd.circleprogress.DonutProgress;
import com.socialide.Fragments.SearchProfessional1;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Model.BeanForProfessionalSearch;
import com.socialide.R;
import com.wang.avi.AVLoadingIndicatorView;


import java.util.List;



/**
 * Created by info1010 on 8/9/2017.
 */

public class FavPeofessionalforSearchAdapter extends RecyclerView.Adapter<FavPeofessionalforSearchAdapter.HomeViewHolderItem> {
    Context mContext;
    Activity activity;
    LinearLayoutManager linearLayoutManager;
    int totalItemCount, lastVisibleItem;
    List<BeanForProfessionalSearch> beanForReferralFanses;

    public FavPeofessionalforSearchAdapter(Context context, RecyclerView mRecyclerView, List<BeanForProfessionalSearch> beanForReferralFanses, final LinearLayoutManager linearLayoutManager,Activity activity) {

        this.mContext = context;
        this.beanForReferralFanses = beanForReferralFanses;
        this.linearLayoutManager = linearLayoutManager;
        this.activity=activity;
        this.linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                Log.e("zzzzzzzzzzzzzz", "zz>>>>>>>> " + totalItemCount + " " + lastVisibleItem);
                if (!SearchProfessional1.isLoading1 && totalItemCount <= (lastVisibleItem + 2)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    SearchProfessional1.isLoading1 = true;
                }
            }
        });
    }

    @Override
    public HomeViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_onsearch_screen_item, parent, false);
        HomeViewHolderItem settingsViewHolderItem = new HomeViewHolderItem(view);

        settingsViewHolderItem.tvName = (TextView) view.findViewById(R.id.txt_name);
        settingsViewHolderItem.iv_event_profile_pic = (ImageView) view.findViewById(R.id.iv_event_profile_pic);
        settingsViewHolderItem.loading = (DonutProgress) view.findViewById(R.id.loading);
        return settingsViewHolderItem;
    }

    @Override
    public void onBindViewHolder(HomeViewHolderItem holder, int position) {

        BeanForProfessionalSearch referralFans = beanForReferralFanses.get(position);

        holder.tvName.setText(""+referralFans.getFirst_name());
        com.socialide.Helper.ImageLoader.image(referralFans.getProfile_pic(), holder.iv_event_profile_pic, holder.loading, activity);



    }

    OnLoadMoreListener mOnLoadMoreListener;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        SearchProfessional1.isLoading1 = false;
    }

    @Override
    public int getItemCount() {
        return beanForReferralFanses.size();
    }

    static class HomeViewHolderItem extends RecyclerView.ViewHolder {

        public View view;

        TextView tvName;
        public ImageView iv_event_profile_pic;
        public DonutProgress loading ;
        public HomeViewHolderItem(View v) {
            super(v);
            this.view = v;
        }
    }





}

