package com.socialide.Adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Activities.AddEventPhotosActivity;
import com.socialide.Helper.RoundedCornersTransformation;
import com.socialide.Model.BeanForUploadImage;
import com.socialide.R;

import java.io.File;
import java.util.ArrayList;



/**
 * Created by Victor on 7/1/15.
 */
public class EventPhotosAdapter extends RecyclerView.Adapter<EventPhotosAdapter.EventPhotosViewHolder> {

    ArrayList<String> eventImagesUrl;
    Context mContext;
    String eventUrl = "https://www.dkit.ie/sites/default/files/event.jpg";
    public static boolean isClickable = true;
    Activity activity;
    int checkCondition = 0;

    public EventPhotosAdapter(Context context, Activity activity, int checkCondition) {
        this.mContext = context;
        this.activity = activity;
        eventImagesUrl = new ArrayList<>();
        this.checkCondition = checkCondition;
    }

    @Override
    public EventPhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event_photo, parent, false);
        EventPhotosViewHolder viewHolder = new EventPhotosViewHolder(view);
        viewHolder.imgEvent = (RoundedImageView) view.findViewById(R.id.imgEvent);
        viewHolder.cvAddPhotos = (CardView) view.findViewById(R.id.cvAddPhotos);
        viewHolder.loading1 = (DonutProgress) view.findViewById(R.id.loading1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventPhotosViewHolder holder, int position) {
        if (getItemCount() - 1 == position && checkCondition == 0) {

            holder.imgEvent.setVisibility(View.GONE);
            holder.cvAddPhotos.setVisibility(View.VISIBLE);

        } else {
            holder.imgEvent.setVisibility(View.VISIBLE);
            holder.cvAddPhotos.setVisibility(View.GONE);
           /* Glide.with(mContext)
                    .load(image_uris.get(position))
                    .centerCrop()
                    .into(holder.imgEvent);*/

            BeanForUploadImage beanForUploadImage = AddEventPhotosActivity.beanForUploadImages.get(position);

            if (beanForUploadImage.isGlobal()) {
                com.socialide.Helper.ImageLoader.image(beanForUploadImage.getUrl(), holder.imgEvent, holder.loading1, activity);

            } else {
                Glide.with(mContext)
                        .load(beanForUploadImage.getUrl())
                        .bitmapTransform(new RoundedCornersTransformation(mContext, 15, 10))
                        .into(holder.imgEvent);
            }



           /* Glide.with(mContext)
                    .load(image_uris.get(position))
                    .bitmapTransform(new RoundedCornersTransformation(mContext, 15, 10))
                    .into(holder.imgEvent);*/
        }
    }

    @Override
    public int getItemCount() {

        if (checkCondition == 1) {
            return AddEventPhotosActivity.beanForUploadImages.size();
        } else {
            return AddEventPhotosActivity.beanForUploadImages.size() + 1;
        }

    }

    static class EventPhotosViewHolder extends RecyclerView.ViewHolder {

        public RoundedImageView imgEvent;
        public CardView cvAddPhotos;
        public DonutProgress loading1;

        public EventPhotosViewHolder(View v) {
            super(v);

        }
    }
}
