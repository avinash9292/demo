package com.socialide.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.socialide.Fragments.EventsPending;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.OnLoadMoreListener;
import com.socialide.Model.BeanForFetchEvents;
import com.socialide.Model.BeanForFilterData;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Victor on 7/3/15.
 */
public class FilterDataAdapter extends RecyclerView.Adapter<FilterDataAdapter.EventsViewHolder> {



    ArrayList<BeanForFilterData> event_list= new ArrayList<>();

    Context mContext;

    RecyclerView mRecyclerView;

    public FilterDataAdapter(Context context, ArrayList<BeanForFilterData> event_list){
        this.mContext = context;
        this.event_list=event_list;





    }


    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_data_search_screen_item,parent,false);

        EventsViewHolder viewHolder = new EventsViewHolder(view);
        viewHolder.img_delete = (ImageView)view.findViewById(R.id.img_delete);
        viewHolder.filter_text = (MyTextView)view.findViewById(R.id.filter_text);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {


        BeanForFilterData bean= event_list.get(position);
        holder.filter_text.setText(""+bean.getData());

//        holder.iv_event_profile_pic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        Log.e("alalala","a "+event_list.size());
        return event_list.size();
    }

    static class EventsViewHolder extends RecyclerView.ViewHolder {


        public ImageView img_delete;
        public MyTextView  filter_text;
        public EventsViewHolder(View v){
            super(v);
        }

    }


}
