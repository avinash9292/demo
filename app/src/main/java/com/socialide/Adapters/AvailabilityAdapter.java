package com.socialide.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;

import com.socialide.Fragments.ProffressionalAvailability;
import com.socialide.Helper.CustomSnackBar;
import com.socialide.Helper.MyTextView;
import com.socialide.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Avinash on 3/30/2017.
 */
public class AvailabilityAdapter extends BaseExpandableListAdapter {
    MyTextView item;
    private Activity context;
    private Map<String, List<String>> laptopCollections;
    private List<String> laptops;

    public AvailabilityAdapter(Activity context, List<String> laptops,
                               Map<String, List<String>> laptopCollections) {
        this.context = context;
        this.laptopCollections = laptopCollections;
        this.laptops = laptops;


    }

    public Object getChild(int groupPosition, int childPosition) {
        return laptopCollections.get(laptops.get(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String laptop = (String) getChild(groupPosition, childPosition);


        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_availability_child_items, null);
        }


        item = (MyTextView) convertView.findViewById(R.id.txt_time);

        ImageView delete = (ImageView) convertView.findViewById(R.id.img_delete);
        delete.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Do you want to remove?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                List<String> child =
                                        laptopCollections.get(laptops.get(groupPosition));
                                child.remove(childPosition);
                                notifyDataSetChanged();
                            }
                        });
                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });


//        if(hourOfDay>12) {
//            datehid.setText(String.valueOf(hourOfDay-12)+ ":"+(String.valueOf(minute)+"pm"));
//        } else if(hourOfDay==12) {
//            datehid.setText("12"+ ":"+(String.valueOf(minute)+"pm"));
//        } else if(hourOfDay<12) {
//            if(hourOfDay!=0) {
//                datehid.setText(String.valueOf(hourOfDay) + ":" + (String.valueOf(minute) + "am"));
//            } else {
//                datehid.setText("12" + ":" + (String.valueOf(minute) + "am"));
//            }
//        }
        Log.v("akram", "time " + laptop);
        item.setText(getAMPM(laptop));
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return laptopCollections.get(laptops.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return laptops.get(groupPosition);
    }

    public int getGroupCount() {
        return laptops.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String laptopName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.fragment_availability_group_item,
                    null);
        }
        ImageView item = (ImageView) convertView.findViewById(R.id.img_pick_date);
        MyTextView text = (MyTextView) convertView.findViewById(R.id.txt_time);


        text.setText("" + laptops.get(groupPosition));
        Log.e("day" + groupPosition, "sss  " + laptops.get(groupPosition));

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                ProffressionalAvailability.group = groupPosition;
                ProffressionalAvailability.popup(context);


            }
        });


        return convertView;
    }

    public String getAMPM(String two_time) {

        if (("" + two_time).contains("-")) {
            String time[] = two_time.split("-");


            String h = "";

            String m = "";
            String time1[] = new String[2];
            for (int i = 0; i < time.length; i++) {


                if (Integer.parseInt((time[i].split(":"))[1].trim()) < 61) {
                    //  m=String.format("%02d", (time[i].split(":"))[1].trim());

                    DecimalFormat formatter = new DecimalFormat("00");
                    m = formatter.format(Integer.parseInt((time[i].split(":"))[1].trim()));
                    //  h = formatter.format(Integer.parseInt((time[i].split(":"))[0].trim()));

                }
                if (Integer.parseInt((time[i].split(":"))[0].trim()) > 12) {
                    String hour = String.valueOf(Integer.parseInt((time[i].split(":"))[0].trim()) - 12);
                    if(hour.length()==1)
                        hour = "0"+hour;

                    time1[i] = hour + " : " + (String.valueOf(m) + " pm");
                } else if (Integer.parseInt((time[i].split(":"))[0].trim()) == 12) {
                    time1[i] = "12" + " : " + (String.valueOf(m) + " pm");
                } else if (Integer.parseInt((time[i].split(":"))[0].trim()) < 12) {
                    if (Integer.parseInt((time[i].split(":"))[0].trim()) != 0) {
                        String hour1 = (String.valueOf(Integer.parseInt((time[i].split(":"))[0].trim())));
                        if(hour1.length()==1)
                            hour1 = "0"+hour1;
                        time1[i] = hour1 + " : " + (String.valueOf(m) + " am");
                    } else {
                        time1[i] = ("12" + " : " + (String.valueOf(m) + " am"));
                    }
                }
            }
            if (time1.length==1) {
                time1[0] = "0"+time1[0];
            }


            String final_result = time1[0] + "  -  " + time1[1];

            return final_result;
        }
        return "Not Available";
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
