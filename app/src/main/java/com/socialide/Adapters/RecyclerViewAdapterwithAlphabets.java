package com.socialide.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.socialide.Helper.MyTextView;
import com.socialide.R;
import com.viethoa.RecyclerViewFastScroller;

import java.util.List;

/**
 * Created by VietHoa on 07/10/15.
 */
public class RecyclerViewAdapterwithAlphabets extends RecyclerView.Adapter<RecyclerViewAdapterwithAlphabets.ViewHolder>
    implements RecyclerViewFastScroller.BubbleTextGetter {

    private List<String> mDataArray;

    public RecyclerViewAdapterwithAlphabets(List<String> dataset) {
        mDataArray = dataset;
    }

    @Override
    public int getItemCount() {
        if (mDataArray == null)
            return 0;
        return mDataArray.size();
    }

    @Override
    public RecyclerViewAdapterwithAlphabets.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        vh.mTextView=(MyTextView) v.findViewById(R.id.txt);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mTextView.setText(mDataArray.get(position));
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        if (pos < 0 || pos >= mDataArray.size())
            return null;

        String name = mDataArray.get(pos);
        if (name == null || name.length() < 1)
            return null;

        return mDataArray.get(pos).substring(0, 1);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        MyTextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }

}
