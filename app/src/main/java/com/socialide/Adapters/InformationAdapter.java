package com.socialide.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.socialide.Activities.DetailsActivity;
import com.socialide.Activities.MainActivity;
import com.socialide.Fragments.MeFragment;
import com.socialide.Fragments.MyProfile;
import com.socialide.Fragments.SettingsFragment;
import com.socialide.Helper.MyTextView;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Victor on 6/28/15.
 */
public class InformationAdapter extends RecyclerView.Adapter<InformationAdapter.ViewHolderItem>{

    ArrayList<String> info;
    Context mContext;
    int type = 0;

    static class ViewHolderItem extends RecyclerView.ViewHolder{
        public MyTextView infoMyTextView;
        public View view;
        public ImageView arrow;

        public ViewHolderItem(View v){
            super(v);
            this.view = v;
        }

    }

    public InformationAdapter(Context context){
        this.mContext = context;
        info = new ArrayList<>();
        info.add("How it works?");
        info.add("Vision");
        info.add("About us");
        info.add("Contact us");
        info.add("Terms & condition");
        info.add("Privacy policy");
    }

    MeFragment fragment;

    public InformationAdapter(Context context,int type,Fragment fragment){
        this.mContext = context;
        this.fragment = (MeFragment)fragment;
        info = new ArrayList<>();
        this.type = type;

        info.add("How it works?");
        info.add("Vision");
        info.add("About us");
        info.add("Contact us");
        info.add("Terms & condition");
        info.add("Privacy policy");
    }

    public void setData(ArrayList<String> info){
        this.info = info;
    }

    @Override
    public InformationAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int resource) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_settings,parent,false);
        ViewHolderItem viewHolderItem = new ViewHolderItem(view);
        viewHolderItem.infoMyTextView = (MyTextView)view.findViewById(R.id.tv_settings_title);
        viewHolderItem.arrow = (ImageView) view.findViewById(R.id.iv_arrow);
        return viewHolderItem;
    }

    @Override
    public void onBindViewHolder(InformationAdapter.ViewHolderItem holder, int position) {
        holder.infoMyTextView.setText(info.get(position));
        holder.view.setBackgroundColor(Color.WHITE);
        holder.arrow.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return info.size();
    }
}
