package com.socialide.Adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.socialide.Activities.FullScreenViewActivity;
import com.socialide.Activities.PostDetails;
import com.socialide.Fragments.ActivityFragment;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.RoundRectCornerImageView;
import com.socialide.Model.BeanForActivities;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Victor on 7/3/15.
 */
public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityViewHolder> implements View.OnClickListener{
String text;
   static Context c;
    Intent sharingIntent;
    ArrayList<BeanForActivities> activity_list;
    public ActivityAdapter(Context context,ArrayList<BeanForActivities> activity_list){
this.c=context;
        this.activity_list=activity_list;
    }

    @Override
    public ActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_activity_item,parent,false);

        ActivityViewHolder viewHolder = new ActivityViewHolder(view);
        viewHolder.iv_activity_profilePic= (RoundRectCornerImageView) view.findViewById(R.id.iv_event_profile_pic);
        viewHolder.tv_event_time= (MyTextView) view.findViewById(R.id.tv_event_time);
        viewHolder.tv_event_description= (MyTextView) view.findViewById(R.id.tv_event_description);
        viewHolder.tv_event_name= (MyTextView) view.findViewById(R.id.tv_event_name);
        viewHolder.txt_likes= (MyTextView) view.findViewById(R.id.txt_likes);
        viewHolder.img_share= (ImageView) view.findViewById(R.id.img_share);
        viewHolder.img_like= (ImageView) view.findViewById(R.id.img_like);
        viewHolder.img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
        viewHolder.iv_activity_event= (ImageView) view.findViewById(R.id.iv_activity_event);
        viewHolder.txt_share= (MyTextView) view.findViewById(R.id.txt_share);
        viewHolder.rl= (RelativeLayout) view.findViewById(R.id.rl);


        Bitmap dummy = BitmapFactory.decodeResource(view.getResources(), R.drawable.profilepic);
//
//        dummy= ImageHelper.getRoundedCornerBitmap(dummy,10);
//        viewHolder.iv_activity_profilePic.setImageBitmap(dummy);




        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ActivityViewHolder holder, int position) {

        BeanForActivities b= activity_list.get(position);


        holder.tv_event_time.setText(""+b.getTime());


        holder.tv_event_description.setText(""+b.getDescription());

        holder.tv_event_name.setText(""+b.getArticle_name());

        holder.txt_likes.setText(""+b.getNo_of_like());

        holder.txt_share.setOnClickListener(this);
        holder.img_share.setOnClickListener(this);
        text= holder.tv_event_description.getText().toString();
        holder.iv_activity_profilePic.setImageResource(R.drawable.profilepic);




        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.iv_activity_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(c,FullScreenViewActivity.class);
                ActivityOptions options =
                        ActivityOptions.makeCustomAnimation(c,R.anim.slide_to_right, R.anim.slide_from_left);
                c.startActivity(i, options.toBundle());
            }
        });

        holder.img_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityFragment.popup(c);
            }
        });

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                makeTextViewResizable(holder, 2, "Read More", true);
            }
        }, 10);


    }


    static class ActivityViewHolder extends RecyclerView.ViewHolder{

        public ImageView img_share,img_like,iv_activity_event,img_arrow;
        public MyTextView  tv_event_name;
        public RoundRectCornerImageView iv_activity_profilePic;

        public MyTextView  tv_event_time;
        public MyTextView  tv_event_description;
        public MyTextView  txt_share;
        RelativeLayout rl;
        public MyTextView  txt_likes,txt_like;
        public ActivityViewHolder(View itemView) {
            super(itemView);
        }
    }



    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_share:
                 sharingIntent = new Intent(Intent.ACTION_SEND);



                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Socialide");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
                c.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                break;
            case R.id.txt_share:
                sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Socialide");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
                c.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                break;

        }
    }







    public static void makeTextViewResizable(final ActivityViewHolder tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.tv_event_description.getTag() == null) {
            tv.tv_event_description.setTag(tv.tv_event_description.getText());
        }
        ViewTreeObserver vto = tv.tv_event_description.getViewTreeObserver();

        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.tv_event_description.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

                Layout l = tv.tv_event_description.getLayout();

                if(l!=null) {

                    if (maxLine == 0) {
                        int lineEndIndex = tv.tv_event_description.getLayout().getLineEnd(0);
                        String text = tv.tv_event_description.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                        tv.tv_event_description.setText(text);
                        tv.tv_event_description.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.tv_event_description.setText(
                                addClickablePartTextViewResizable(Html.fromHtml(tv.tv_event_description.getText().toString()), tv.tv_event_description, maxLine, expandText,
                                        viewMore), TextView.BufferType.SPANNABLE);
                    } else if (maxLine > 0 && tv.tv_event_description.getLineCount() >= maxLine) {
                        int lineEndIndex = tv.tv_event_description.getLayout().getLineEnd(maxLine - 1);
                        String text = tv.tv_event_description.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                        tv.tv_event_description.setText(text);
                        tv.tv_event_description.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.tv_event_description.setText(
                                addClickablePartTextViewResizable(Html.fromHtml(tv.tv_event_description.getText().toString()), tv.tv_event_description, maxLine, expandText,
                                        viewMore), TextView.BufferType.SPANNABLE);
                    } else {
                        int lineEndIndex = tv.tv_event_description.getLayout().getLineEnd(tv.tv_event_description.getLayout().getLineCount() - 1);
                        String text = tv.tv_event_description.getText().subSequence(0, lineEndIndex) + " " + expandText;
                        tv.tv_event_description.setText(text);
                        tv.tv_event_description.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.tv_event_description.setText(
                                addClickablePartTextViewResizable(Html.fromHtml(tv.tv_event_description.getText().toString()), tv.tv_event_description, lineEndIndex, expandText,
                                        viewMore), TextView.BufferType.SPANNABLE);
                    }
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {


                    Intent i=new Intent(c,PostDetails.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(c,R.anim.slide_to_right, R.anim.slide_from_left);
                    c.startActivity(i,options.toBundle());
//


                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }


}
