package com.socialide.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.makeramen.roundedimageview.RoundedImageView;
import com.socialide.Activities.OtherUserDetailsActivity;
import com.socialide.Helper.MyTextView;
import com.socialide.Helper.Mypreferences;
import com.socialide.Helper.RoundRectCornerImageView;
import com.socialide.Model.BeanForTopUser;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Victor on 6/29/15.
 */
public class TopUsersAdapter extends RecyclerView.Adapter<TopUsersAdapter.TopUsersViewHolder> {
    ArrayList<BeanForTopUser> user_list;
    private Context mContext;
    Activity _activity;
    private final int VIEW_TYPE_ITEM = 0;
    private final int BUTTON_TYPE = 1;
    BeanForTopUser b;

    public TopUsersAdapter(Context context, ArrayList<BeanForTopUser> user_list, Activity activty) {
        this.mContext = context;
        this.user_list = user_list;
        this._activity = activty;
    }

    private static final String profilePicURL =
            "https://scontent-vie1-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/10462325_10207001233652223_2525710094052420409_n.jpg?oh=736e59d97de29728e056be007dc8e620&oe=5631E70E";

    static class TopUsersViewHolder extends RecyclerView.ViewHolder {
        public MyTextView userProfileName, tv_user_location1, tv_user_location;
        public RoundedImageView userProfilePic;
        public DonutProgress d;
        public MyTextView userSocialScore;
        public MyTextView userEventsNumber;

        public TopUsersViewHolder(View v) {
            super(v);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == user_list.size()) {
            return BUTTON_TYPE;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public TopUsersAdapter.TopUsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TopUsersViewHolder viewHolderItem;
        if (viewType == VIEW_TYPE_ITEM) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_top_users_item, parent, false);
            viewHolderItem = new TopUsersViewHolder(view);
            viewHolderItem.userProfileName = (MyTextView) view.findViewById(R.id.tv_user_name);
            viewHolderItem.userProfilePic = (RoundedImageView) view.findViewById(R.id.iv_profile_pic);
            viewHolderItem.tv_user_location = (MyTextView) view.findViewById(R.id.tv_user_location);
            viewHolderItem.userSocialScore = (MyTextView) view.findViewById(R.id.tv_social_score);
            viewHolderItem.userEventsNumber = (MyTextView) view.findViewById(R.id.tv_events_week);
            viewHolderItem.tv_user_location1 = (MyTextView) view.findViewById(R.id.tv_user_location1);
            viewHolderItem.d = (DonutProgress) view.findViewById(R.id.loading);
            return viewHolderItem;
        } else if (viewType == BUTTON_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contain_view, parent, false);
            viewHolderItem = new TopUsersViewHolder(view);
            return viewHolderItem;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final TopUsersAdapter.TopUsersViewHolder holder, int position) {

        if (position == user_list.size()) {
            return;
        }

        b = user_list.get(position);

        com.socialide.Helper.ImageLoader.image(b.getProfile_pic(), holder.userProfilePic, holder.d, _activity);


        holder.userEventsNumber.setText(mContext.getResources().getString(R.string.eventThisWeek)+": " + b.getThis_week_event_count());
        holder.userSocialScore.setText(mContext.getResources().getString(R.string.socialScore)+": " + b.getAll_event_count());
        holder.tv_user_location.setText("" + b.getCity());
        holder.tv_user_location1.setText("" + b.getCountry());


        if (b.getType().equals("Organization")) {
            holder.userProfileName.setText(Html.fromHtml("<b>" + b.getName() + "</b>"));
        } else {
            holder.userProfileName.setText(Html.fromHtml("<b>" + b.getFirst_name() + " " + b.getLast_name() + "</b>"));
        }
        holder.userProfileName.setTag(position);
        holder.userProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = (Integer) view.getTag();
                BeanForTopUser b = user_list.get(pos);

                Intent intent = new Intent(mContext, OtherUserDetailsActivity.class);
                intent.putExtra("userId", b.getUserid());
                intent.putExtra("type", b.getType());

                if (b.getUserid().equalsIgnoreCase(Mypreferences.User_id)) {

                    if (!b.getType().equalsIgnoreCase(Mypreferences.User_Type)) {
                        mContext.startActivity(intent);
                        _activity.overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);
                    }

                } else {

                    mContext.startActivity(intent);
                    _activity.overridePendingTransition(R.anim.slide_to_right, R.anim.slide_from_left);

                }
            }
        });
        holder.userProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.userProfileName.performClick();
            }
        });

    }

    @Override
    public int getItemCount() {
        if (user_list.size() != 0) {
            return user_list.size() + 1;
        } else {
            return 0;
        }
    }
}
