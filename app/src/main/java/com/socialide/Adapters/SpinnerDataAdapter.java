package com.socialide.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.socialide.Helper.MyTextView;
import com.socialide.R;

import java.util.ArrayList;

/**
 * Created by Akram on 11/7/2017.
 */

public class SpinnerDataAdapter extends BaseAdapter {

    ArrayList<String> arrayList;
    Activity activity;
    Context context;
    int position;

    public SpinnerDataAdapter(ArrayList<String> arrayList, Activity activity, Context context, int position) {
        this.arrayList = arrayList;
        this.activity = activity;
        this.context = context;
        this.position = position;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = activity.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.adapter_spinner, null);
        MyTextView txt = (MyTextView) view.findViewById(R.id.txt);
        ImageView btnRadio = (ImageView) view.findViewById(R.id.btnRadio);

        if (this.position == position) {
            btnRadio.setVisibility(View.VISIBLE);
        } else {
            btnRadio.setVisibility(View.INVISIBLE);
        }


        txt.setText(arrayList.get(position));
        return view;
    }
}
