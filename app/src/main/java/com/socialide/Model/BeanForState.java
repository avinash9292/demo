package com.socialide.Model;

/**
 * Created by Avinash on 10/6/2017.
 */

public class BeanForState {

    String id,name,county_id;

    public BeanForState(String id, String name, String county_id) {
        this.id = id;
        this.name = name;
        this.county_id = county_id;
    }

    public String getCounty_id() {
        return county_id;
    }

    public void setCounty_id(String county_id) {
        this.county_id = county_id;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
