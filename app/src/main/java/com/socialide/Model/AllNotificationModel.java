package com.socialide.Model;

/**
 * Created by Akram on 11/10/2017.
 */
import java.io.Serializable;

public class AllNotificationModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String profile_pic,user_id,type,noti_id,notification,is_read,created_on;

    public AllNotificationModel() {

    }

    public AllNotificationModel(String profile_pic, String user_id, String type, String noti_id, String notification, String is_read, String created_on) {
        this.profile_pic = profile_pic;
        this.user_id = user_id;
        this.type = type;
        this.noti_id = noti_id;
        this.notification = notification;
        this.is_read = is_read;
        this.created_on = created_on;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getType() {
        return type;
    }

    public String getNoti_id() {
        return noti_id;
    }

    public String getNotification() {
        return notification;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getCreated_on() {
        return created_on;
    }
}