package com.socialide.Model;

/**
 * Created by Avinash on 5/20/2017.
 */
public class BeanForActivityImages {

    String id,imageUrl;
    String userid, type;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BeanForActivityImages(String id, String imageUrl,String userid,String type) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.userid=userid;
        this.type=type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
