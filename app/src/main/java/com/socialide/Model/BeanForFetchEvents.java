package com.socialide.Model;

/**
 * Created by Avinash on 5/10/2017.
 */
public class BeanForFetchEvents {


    String event_id,profile_image,event_by,event_title,event_description,event_location,event_time,event_date,contact_number,email,message,person_name,person_designation,person_email,person_mobile_number,decline_by,reason,userid,type,sharecontact;
    String professional_id,login_user_sharecontact;

    public String getProfessional_id() {
        return professional_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getEvent_by() {
        return event_by;
    }

    public void setEvent_by(String event_by) {
        this.event_by = event_by;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_description() {
        return event_description;
    }

    public void setEvent_description(String event_description) {
        this.event_description = event_description;
    }

    public String getEvent_location() {
        return event_location;
    }

    public void setEvent_location(String event_location) {
        this.event_location = event_location;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getPerson_designation() {
        return person_designation;
    }

    public void setPerson_designation(String person_designation) {
        this.person_designation = person_designation;
    }

    public String getPerson_email() {
        return person_email;
    }

    public void setPerson_email(String person_email) {
        this.person_email = person_email;
    }

    public String getPerson_mobile_number() {
        return person_mobile_number;
    }

    public void setPerson_mobile_number(String person_mobile_number) {
        this.person_mobile_number = person_mobile_number;
    }

    public String getDecline_by() {
        return decline_by;
    }

    public void setDecline_by(String decline_by) {
        this.decline_by = decline_by;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSharecontact() {
        return sharecontact;
    }

    public String getLogin_user_sharecontact() {
        return login_user_sharecontact;
    }

    public void setSharecontact(String sharecontact) {
        this.sharecontact = sharecontact;
    }

    public BeanForFetchEvents(String event_id, String profile_image, String event_by, String event_title, String event_description, String event_location, String event_time, String event_date, String contact_number, String email, String message, String person_name, String person_designation, String person_email, String person_mobile_number, String decline_by, String reason, String userid, String type, String sharecontact,String professional_id) {
        this.event_id = event_id;
        this.profile_image = profile_image;
        this.event_by = event_by;
        this.event_title = event_title;
        this.event_description = event_description;
        this.event_location = event_location;
        this.event_time = event_time;
        this.event_date = event_date;
        this.contact_number = contact_number;
        this.email = email;
        this.message = message;
        this.person_name = person_name;
        this.person_designation = person_designation;
        this.person_email = person_email;
        this.person_mobile_number = person_mobile_number;
        this.decline_by = decline_by;
        this.reason = reason;
        this.userid = userid;
        this.type = type;
        this.sharecontact = sharecontact;
        this.professional_id = professional_id;
    }


    public BeanForFetchEvents(String event_id, String profile_image, String event_by, String event_title, String event_description, String event_location, String event_time, String event_date, String contact_number, String email, String message, String person_name, String person_designation, String person_email, String person_mobile_number, String decline_by, String reason, String userid, String type, String sharecontact,String professional_id,String login_user_sharecontact) {
        this.event_id = event_id;
        this.profile_image = profile_image;
        this.event_by = event_by;
        this.event_title = event_title;
        this.event_description = event_description;
        this.event_location = event_location;
        this.event_time = event_time;
        this.event_date = event_date;
        this.contact_number = contact_number;
        this.email = email;
        this.message = message;
        this.person_name = person_name;
        this.person_designation = person_designation;
        this.person_email = person_email;
        this.person_mobile_number = person_mobile_number;
        this.decline_by = decline_by;
        this.reason = reason;
        this.userid = userid;
        this.type = type;
        this.sharecontact = sharecontact;
        this.professional_id = professional_id;
        this.login_user_sharecontact = login_user_sharecontact;
    }


}




