package com.socialide.Model;

/**
 * Created by Avinash on 10/23/2017.
 */

public class BeanForFilterData {

    String data,type;

    public BeanForFilterData(String data, String type) {
        this.data = data;
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
