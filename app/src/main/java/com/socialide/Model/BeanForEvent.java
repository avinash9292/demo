package com.socialide.Model;

import java.util.ArrayList;

/**
 * Created by info1010 on 8/18/2017.
 */

public class BeanForEvent {

    String event_title,event_description,event_date,event_time,event_by,event_id;
    ArrayList<EventPhotos> eventPhotoses = new ArrayList<>();


    public BeanForEvent(String event_title, String event_description, String event_date, String event_time, String event_by, String event_id, ArrayList<EventPhotos> eventPhotoses) {
        this.event_title = event_title;
        this.event_description = event_description;
        this.event_date = event_date;
        this.event_time = event_time;
        this.event_by = event_by;
        this.event_id = event_id;
        this.eventPhotoses = eventPhotoses;
    }

    public String getEvent_title() {
        return event_title;
    }

    public String getEvent_description() {
        return event_description;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getEvent_time() {
        return event_time;
    }

    public String getEvent_by() {
        return event_by;
    }

    public String getEvent_id() {
        return event_id;
    }

    public ArrayList<EventPhotos> getEventPhotoses() {
        return eventPhotoses;
    }

    public static class EventPhotos{

        String type,userid,image_id,image_url;

        public String getType() {
            return type;
        }

        public String getUserid() {
            return userid;
        }

        public String getImage_id() {
            return image_id;
        }

        public String getImage_url() {
            return image_url;
        }

        public EventPhotos(String type, String userid, String image_id, String image_url) {
            this.type = type;
            this.userid = userid;
            this.image_id = image_id;
            this.image_url = image_url;
        }
    }

}
