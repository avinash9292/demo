package com.socialide.Model;

/**
 * Created by Avinash on 10/17/2017.
 */

public class BeanForCityList {

    String name;
    boolean check;

    public BeanForCityList(String name, boolean check) {
        this.name = name;
        this.check = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
