package com.socialide.Model;

/**
 * Created by Avinash on 10/6/2017.
 */

public class BeanForCountry {

    String id,name;

    public BeanForCountry(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
