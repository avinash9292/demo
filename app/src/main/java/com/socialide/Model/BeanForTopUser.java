package com.socialide.Model;

/**
 * Created by Avinash on 6/1/2017.
 */
public class BeanForTopUser {

    String  name, contact_firstname, contact_lastname, contact_number, contact_position,u_dise_number,  address;

    String  userid,
            first_name,
            last_name,
            profession,
            experience,
            mobile_number,
            state,
            city,
            profile_pic,
            country,
            current_company,
            category,
            subcategory,
            all_event_count,
            this_week_event_count,
            type;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getCurrent_company() {
        return current_company;
    }

    public void setCurrent_company(String current_company) {
        this.current_company = current_company;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public BeanForTopUser(String userid, String first_name, String last_name, String profession, String experience, String mobile_number, String state, String city, String profile_pic, String country, String current_company, String category, String subcategory, String all_event_count, String this_week_event_count, String type) {

        this.userid = userid;
        this.first_name = first_name;
        this.last_name = last_name;
        this.profession = profession;
        this.experience = experience;
        this.mobile_number = mobile_number;
        this.state = state;
        this.city = city;
        this.profile_pic = profile_pic;
        this.country = country;
        this.current_company = current_company;
        this.category = category;
        this.subcategory = subcategory;
        this.all_event_count = all_event_count;
        this.this_week_event_count = this_week_event_count;
        this.type = type;
    }

    public BeanForTopUser(String userid, String name, String contact_firstname, String contact_lastname, String contact_number, String contact_position, String state, String city, String profile_pic, String u_dise_number, String country, String address, String all_event_count, String this_week_event_count, String type) {
        this.userid = userid;
        this.name = name;
        this.contact_firstname = contact_firstname;
        this.contact_lastname = contact_lastname;
        this.contact_number = contact_number;
        this.contact_position = contact_position;
        this.state = state;
        this.city = city;
        this.profile_pic = profile_pic;
        this.u_dise_number = u_dise_number;
        this.country = country;
        this.address = address;
        this.all_event_count = all_event_count;
        this.this_week_event_count = this_week_event_count;
        this.type = type;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact_firstname() {
        return contact_firstname;
    }

    public void setContact_firstname(String contact_firstname) {
        this.contact_firstname = contact_firstname;
    }

    public String getContact_lastname() {
        return contact_lastname;
    }

    public void setContact_lastname(String contact_lastname) {
        this.contact_lastname = contact_lastname;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getContact_position() {
        return contact_position;
    }

    public void setContact_position(String contact_position) {
        this.contact_position = contact_position;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getU_dise_number() {
        return u_dise_number;
    }

    public void setU_dise_number(String u_dise_number) {
        this.u_dise_number = u_dise_number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAll_event_count() {
        return all_event_count;
    }

    public void setAll_event_count(String all_event_count) {
        this.all_event_count = all_event_count;
    }

    public String getThis_week_event_count() {
        return this_week_event_count;
    }

    public void setThis_week_event_count(String this_week_event_count) {
        this.this_week_event_count = this_week_event_count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
