package com.socialide.Model;

/**
 * Created by Avinash on 10/6/2017.
 */

public class BeanForCity {

    String id,name,stateid;

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public BeanForCity(String id, String name, String stateid) {

        this.id = id;
        this.name = name;
        this.stateid = stateid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
