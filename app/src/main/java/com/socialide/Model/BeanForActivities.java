package com.socialide.Model;

import java.util.ArrayList;

/**
 * Created by Avinash on 5/20/2017.
 */
public class BeanForActivities {


    String article_name, article_date, time, article_by, description, article_image, activity_by;
    boolean isliked;
    String activity_date_time,event_id;

    public String getActivity_date_time() {
        return activity_date_time;
    }

    public void setActivity_date_time(String activity_date_time) {
        this.activity_date_time = activity_date_time;
    }

    public BeanForActivities(String activity_type, String activity_id, String activity_user_id, String article_name, String article_date, String time, String article_by, String description, String profile_pic, String no_of_like, String article_image, String activity_by, String isUserLikedPost, String activity_date_time, boolean isliked) {

        this.isUserLikedPost = isUserLikedPost;
        this.activity_type = activity_type;
        this.activity_id = activity_id;
        this.activity_user_id = activity_user_id;
        this.article_name = article_name;
        this.article_date = article_date;
        this.time = time;
        this.activity_date_time = activity_date_time;
        this.isliked = isliked;
        this.article_by = article_by;
        this.description = description;
        this.profile_pic = profile_pic;
        this.no_of_like = no_of_like;
        this.article_image = article_image;
        this.activity_by = activity_by;
    }

    public BeanForActivities(String activity_type, String activity_id, String activity_user_id, String article_name, String article_date, String time, String article_by, String description, String profile_pic, String no_of_like, String article_image, String activity_by, String isUserLikedPost, String activity_date_time, boolean isliked,String isHiddenByReporting) {

        this.isUserLikedPost = isUserLikedPost;
        this.activity_type = activity_type;
        this.activity_id = activity_id;
        this.activity_user_id = activity_user_id;
        this.article_name = article_name;
        this.article_date = article_date;
        this.time = time;
        this.activity_date_time = activity_date_time;
        this.isliked = isliked;
        this.article_by = article_by;
        this.description = description;
        this.profile_pic = profile_pic;
        this.no_of_like = no_of_like;
        this.article_image = article_image;
        this.activity_by = activity_by;
        this.isHiddenByReporting = isHiddenByReporting;
    }

    public String getIsHiddenByReporting() {
        return isHiddenByReporting;
    }

    public String getArticle_name() {
        return article_name;
    }

    public void setArticle_name(String article_name) {
        this.article_name = article_name;
    }

    public String getArticle_date() {
        return article_date;
    }

    public void setArticle_date(String article_date) {
        this.article_date = article_date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getArticle_by() {
        return article_by;
    }

    public void setArticle_by(String article_by) {
        this.article_by = article_by;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getArticle_image() {
        return article_image;
    }

    public void setArticle_image(String article_image) {
        this.article_image = article_image;
    }

    public String getActivity_by() {
        return activity_by;
    }

    public void setActivity_by(String activity_by) {
        this.activity_by = activity_by;
    }


    String activity_type, activity_id, activity_user_id, event_title, event_date, event_time, message, event_location, professional_name, organisation_name, profile_pic, no_of_like;
    ArrayList<BeanForActivityImages> event_photos = new ArrayList<>();


    public String getActivity_type() {
        return activity_type;
    }

    public void setActivity_type(String activity_type) {
        this.activity_type = activity_type;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getActivity_user_id() {
        return activity_user_id;
    }

    public void setActivity_user_id(String activity_user_id) {
        this.activity_user_id = activity_user_id;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEvent_location() {
        return event_location;
    }

    public void setEvent_location(String event_location) {
        this.event_location = event_location;
    }

    public String getProfessional_name() {
        return professional_name;
    }

    public void setProfessional_name(String professional_name) {
        this.professional_name = professional_name;
    }

    public String getOrganisation_name() {
        return organisation_name;
    }

    public void setOrganisation_name(String organisation_name) {
        this.organisation_name = organisation_name;
    }

    public String getEvent_id() {
        return event_id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getNo_of_like() {
        return no_of_like;
    }

    public void setNo_of_like(String no_of_like) {
        this.no_of_like = no_of_like;
    }

    public ArrayList<BeanForActivityImages> getEvent_photos() {
        return event_photos;
    }

    public void setEvent_photos(ArrayList<BeanForActivityImages> event_photos) {
        this.event_photos = event_photos;
    }

    String isUserLikedPost;

    public String getIsUserLikedPost() {
        return isUserLikedPost;
    }

    public boolean isliked() {
        return isliked;
    }

    public void setIsliked(boolean isliked) {
        this.isliked = isliked;
    }

    public void setIsUserLikedPost(String isUserLikedPost) {
        this.isUserLikedPost = isUserLikedPost;

    }

    public String getUserIdOfOrgPost() {
        return userIdOfOrgPost;
    }

    public String getUserIdOfProPost() {
        return userIdOfProPost;
    }

    String userIdOfOrgPost, userIdOfProPost,isHiddenByReporting;

    public BeanForActivities(String activity_type, String activity_id, String activity_user_id, String event_title, String event_date, String event_time, String message, String event_location, String professional_name, String organisation_name, String profile_pic, String no_of_like, ArrayList<BeanForActivityImages> event_photos, String isUserLikedPost, String activity_date_time, boolean isliked, String userIdOfOrgPost, String userIdOfProPost, String activity_by,String description,String event_id) {
        this.activity_type = activity_type;
        this.activity_id = activity_id;
        this.activity_user_id = activity_user_id;
        this.event_title = event_title;
        this.event_date = event_date;
        this.event_time = event_time;
        this.isUserLikedPost = isUserLikedPost;
        this.message = message;
        this.isliked = isliked;
        this.activity_date_time = activity_date_time;
        this.event_location = event_location;
        this.professional_name = professional_name;
        this.organisation_name = organisation_name;
        this.profile_pic = profile_pic;
        this.no_of_like = no_of_like;
        this.event_photos = event_photos;
        this.userIdOfOrgPost = userIdOfOrgPost;
        this.userIdOfProPost = userIdOfProPost;
        this.activity_by = activity_by;
        this.description = description;
        this.event_id = event_id;

    }


    public BeanForActivities(String activity_type, String activity_id, String activity_user_id, String event_title, String event_date, String event_time, String message, String event_location, String professional_name, String organisation_name, String profile_pic, String no_of_like, ArrayList<BeanForActivityImages> event_photos, String isUserLikedPost, String activity_date_time, boolean isliked, String userIdOfOrgPost, String userIdOfProPost, String activity_by,String description,String event_id,String isHiddenByReporting) {
        this.activity_type = activity_type;
        this.activity_id = activity_id;
        this.activity_user_id = activity_user_id;
        this.event_title = event_title;
        this.event_date = event_date;
        this.event_time = event_time;
        this.isUserLikedPost = isUserLikedPost;
        this.message = message;
        this.isliked = isliked;
        this.activity_date_time = activity_date_time;
        this.event_location = event_location;
        this.professional_name = professional_name;
        this.organisation_name = organisation_name;
        this.profile_pic = profile_pic;
        this.no_of_like = no_of_like;
        this.event_photos = event_photos;
        this.userIdOfOrgPost = userIdOfOrgPost;
        this.userIdOfProPost = userIdOfProPost;
        this.activity_by = activity_by;
        this.description = description;
        this.event_id = event_id;
        this.isHiddenByReporting = isHiddenByReporting;

    }


}
