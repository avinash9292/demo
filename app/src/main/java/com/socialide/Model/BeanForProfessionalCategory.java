package com.socialide.Model;

import java.util.ArrayList;

/**
 * Created by Avinash on 10/12/2017.
 */

public class BeanForProfessionalCategory {

    String category;
    ArrayList<String> sub_category = new ArrayList<>();

    public BeanForProfessionalCategory() {
    }

    public BeanForProfessionalCategory(String category, ArrayList<String> sub_category) {
        this.category = category;
        this.sub_category = sub_category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<String> getSub_category() {
        return sub_category;
    }

    public void setSub_category(ArrayList<String> sub_category) {
        this.sub_category = sub_category;
    }
}
