package com.socialide.Model;

/**
 * Created by info1010 on 9/29/2017.
 */

public class BeanForUploadImage {

    String url;
    boolean isGlobal;

    public BeanForUploadImage(String url, boolean isGlobal) {
        this.url = url;
        this.isGlobal = isGlobal;

    }

    public String getUrl() {
        return url;
    }

    public boolean isGlobal() {
        return isGlobal;
    }
}
