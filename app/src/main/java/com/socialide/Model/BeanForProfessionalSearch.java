package com.socialide.Model;

/**
 * Created by Avinash on 10/13/2017.
 */

public class BeanForProfessionalSearch {
    String userid,first_name,last_name,dob,experience,password,email,mobile_number,state,city,profile_pic,gender,country,current_company,category,subcategory,profession,about_me,maximum_event_request,availability,social_score,join_flow_completed;
String is_available,is_verified,is_favorite;

    public String getIs_available() {
        return is_available;
    }

    public void setIs_available(String is_available) {
        this.is_available = is_available;
    }

    public String getIs_verified() {
        return is_verified;
    }

    public BeanForProfessionalSearch(String userid, String first_name, String last_name, String dob, String experience, String password, String email, String mobile_number, String state, String city, String profile_pic, String gender, String country, String current_company, String category, String subcategory, String profession, String about_me, String maximum_event_request, String availability, String social_score, String join_flow_completed, String is_available, String is_verified, String is_favorite) {
        this.userid = userid;
        this.first_name = first_name;
        this.last_name = last_name;
        this.dob = dob;
        this.experience = experience;
        this.password = password;
        this.email = email;
        this.mobile_number = mobile_number;
        this.state = state;
        this.city = city;
        this.profile_pic = profile_pic;
        this.gender = gender;
        this.country = country;
        this.current_company = current_company;
        this.category = category;
        this.subcategory = subcategory;
        this.profession = profession;
        this.about_me = about_me;
        this.maximum_event_request = maximum_event_request;
        this.availability = availability;
        this.social_score = social_score;
        this.join_flow_completed = join_flow_completed;
        this.is_available = is_available;
        this.is_verified = is_verified;
        this.is_favorite = is_favorite;
    }

    public void setIs_verified(String is_verified) {
        this.is_verified = is_verified;
    }

    public String getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(String is_favorite) {
        this.is_favorite = is_favorite;
    }

    public BeanForProfessionalSearch(String userid, String first_name, String last_name, String dob, String experience, String password, String email, String mobile_number, String state, String city, String profile_pic, String gender, String country, String current_company, String category, String subcategory, String profession, String about_me, String maximum_event_request, String availability, String social_score, String join_flow_completed) {
        this.userid = userid;
        this.first_name = first_name;
        this.last_name = last_name;
        this.dob = dob;
        this.experience = experience;
        this.password = password;
        this.email = email;
        this.mobile_number = mobile_number;
        this.state = state;
        this.city = city;
        this.profile_pic = profile_pic;
        this.gender = gender;
        this.country = country;
        this.current_company = current_company;
        this.category = category;
        this.subcategory = subcategory;
        this.profession = profession;
        this.about_me = about_me;
        this.maximum_event_request = maximum_event_request;
        this.availability = availability;
        this.social_score = social_score;
        this.join_flow_completed = join_flow_completed;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrent_company() {
        return current_company;
    }

    public void setCurrent_company(String current_company) {
        this.current_company = current_company;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getMaximum_event_request() {
        return maximum_event_request;
    }

    public void setMaximum_event_request(String maximum_event_request) {
        this.maximum_event_request = maximum_event_request;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getSocial_score() {
        return social_score;
    }

    public void setSocial_score(String social_score) {
        this.social_score = social_score;
    }

    public String getJoin_flow_completed() {
        return join_flow_completed;
    }

    public void setJoin_flow_completed(String join_flow_completed) {
        this.join_flow_completed = join_flow_completed;
    }
}
