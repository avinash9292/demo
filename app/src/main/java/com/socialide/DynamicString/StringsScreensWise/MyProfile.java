package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Akram on 11/14/2017.
 */

public class MyProfile {

    public String notAvailable= "notAvailable";
    public String socialScore= "Social Score";
    public String verificationStatus= "Verification status";
    public String establishYear = "Establish year ";
    public String verifyEmail = "Verify Email";

    public String available= "Available";
    public String acceptEventsOrWeek= "Accept Events/Week";
    public String verifyMobile= "Verify Mobile";

}
