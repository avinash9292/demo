package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/15/2017.
 */

public class DialogStrings {

    public String exitDesc="Do you want to exit?";
    public String logoutDesc="Are you sure, you want to logout?";
    public String btnYes="YES";
    public String btnNo="NO";
    public String btnlogout="Logout";
    public String btnOk="OK";
    public String backDesc="Are you sure, you want to go back?";
    public String reportTitle="Report the post";
    public String report="Report";
    public String cancel="Cancel";
    public String reasonTitle="Give a reason for Decline this Event";
    public String timeTitle="Select Time";
    public String dateTitle="Select Date";
    public String emailVerifyTitle="Verification Email";
    public String mobileVerifyTitle="Please Enter Verification Code";
    public String mobileVerifyHint="Type here";
    public String galleryPickTitle="Select Image From Gallery";
    public String chooseImageTitle="Add Photo!";
    public String chooseImageCamera="Take Photo";
    public String chooseImageGallery="Choose from Gallery";


}
