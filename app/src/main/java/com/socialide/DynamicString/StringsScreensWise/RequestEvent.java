package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/15/2017.
 */

public class RequestEvent {
    public String showAvailability="Show Availability";
    public String eventTitle="EventTitle";
    public String eventTitleHint="Title";
    public String message="Message";
    public String messageHint="Write personal message to professional";
    public String btnInvite="Invite";
    public String shareContact="Share Contact Details.";
    public String shareContactDesc="(By checking this box, you can share contact person's email and mobile with professional)";
    public String pasttimeError="You can not select past time for an event.";
    public String dateSelectError="Please select date first";
    public String titleError="Please enter event title";
    public String dateError="Please enter date of event";
    public String timeError="Please enter time of event";
    public String messageError="Please enter message related to event invitation";
}
