package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Akram on 11/14/2017.
 */

public class SchedulePendingEvent {

        String date = "Date";
        String time = "Time";
        String message = "Message";
        String declinedBy = "Declined By";
        String remove = "Remove";

        String landline = "LandLine Number";
        String contactWith = "Contact With";
        String cancelThisEvent = "Cancel This Event";
        String personName = "Person Name";
        String designation = "Designation";
        String accept = "Accept";
        String decline = "Decline";

        String caption = "Caption";
        String reason = "Reason";

}
