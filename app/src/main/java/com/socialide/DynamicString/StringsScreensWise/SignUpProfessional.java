package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpProfessional {
    public String firstNameHint="First Name";
    public String lastNameHint="Last Name";
    public String MobileNameHint="Mobile Number";
    public String emailNameHint="Email";
    public String passwordNameHint="Password";
     public String firstNameError="Please enter your first name";
    public String lastNameError="Please enter your last name";
    public String MobileError="Please enter mobile number";
    public String emailNullError="Please enter email";
    public String emailInvalidError="Please enter correct email";
    public String passwordError="Please enter password";
    public String btnSignUp="Sign Up";
}
