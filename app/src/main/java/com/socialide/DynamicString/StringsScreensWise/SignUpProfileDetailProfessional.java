package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpProfileDetailProfessional {

    public String uploadPIc = "Upload your pic";
    public String dobHint = "Date of Birth";
    public String genderHint = "Gender";
    public String cityHint = "City";
    public String countryHint = "Country";
    public String stateHint = "State";
    public String dobError = "Please select your date of birth";
    public String genderError = "Please select gender";
    public String cityError = "Please select city";
    public String countryError = "Please select country";
    public String stateError = "Please select state";
    public String btnSaveContinue = "Save and Continue >";
    public String btnDoLater = "Do this later";

}
