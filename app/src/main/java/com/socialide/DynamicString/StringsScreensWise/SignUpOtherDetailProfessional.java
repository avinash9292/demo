package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpOtherDetailProfessional {
    public String currentCompanyHint="Current Company";
    public String experienceHint="Total Experience(In years)";
    public String categoryHint="Category";
    public String subCategoryHint="Sub Category";
    public String aboutHint="Write about yourself (Optional)";

    public String currentCompanyError="Please enter your current company name";
    public String experienceError="Please enter experience";
    public String categoryError="Please enter category";
    public String subCategoryError="Please enter subcategory";



}
