package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Akram on 11/14/2017.
 */

public class NotificationSetting {

    public String pushNotification = "Push Notification Settings";
    public String emailSetting = "E-mail Settings";
    public String smsSetting = "SMS Settings";

    public String notificationSetting1 = "When someone likes your post.";
    public String notificationSetting2 = "When someone adds new article.";
    public String notificationSetting3 = "When someone adds events photos.";
    public String notificationSetting4 = "When Organisation adds photos related to my event.";
    public String notificationSetting5 = "When Professional adds photos related to my event.";
    public String notificationSetting6 = "When Organisation sends request for an event.";
    public String notificationSetting7 = "When Professional sends request for an event.";

    public String notificationSetting8 = "When Organisation sends request for an event.";
    public String notificationSetting9 = "When Professional sends request for an event.";
    public String notificationSetting10 = "When Organisation cancel an event request.";

    public String notificationSetting11 = "When Professional cancel an event request";
    public String notificationSetting12 = "When en event request accepted by Organisation.";
    public String notificationSetting13 = "When en event request accepted by Professional.";
    public String notificationSetting14 = "When Organisation declined an event request.";

    public String notificationSetting15 = "When Professional declined an event request.";
    public String notificationSetting16 = "When Organisation cancels schedule event.";
    public String notificationSetting17 = "When Professional cancels schedule event.";
}
