package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpOtherDetailOrganization {
    public String firstNameHint="First Name";
    public String lastNameHint="Last Name";
    public String designationHint="Designation";
    public String mobileHint="Mobile Number";
    public String aboutHint="Write about organisation (Optional)";
    public String firstNameError="Please enter first name";
    public String lastNameError="Please enter last name";
    public String designationError="Please enter designation in organisation";
    public String mobileError="Please enter mobile number";
    public String btnSubmit="Submit";


}
