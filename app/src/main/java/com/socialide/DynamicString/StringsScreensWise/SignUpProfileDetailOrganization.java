package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpProfileDetailOrganization {

public String uploadPIc="Upload Organisation Picture";
public String establishmentYearHint ="Establishment Year";
public String cityHint ="City";
public String countryHint ="Country";
public String stateHint ="State";
public String establishmentYearError ="Please select year of establishment";
public String cityError ="Please select city";
public String countryError ="Please select country";
public String stateError ="Please select state";
public String btnSaveContinue ="Save and Continue >";

}
