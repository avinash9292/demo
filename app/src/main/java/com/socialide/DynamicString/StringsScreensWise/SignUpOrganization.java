package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpOrganization {
    public String NameHint="Organisation Name";
    public String registrationNumberHint="Registration Number";
    public String landLineHint="Landline Number";
    public String EmailHint="Email";
    public String passwordHint="Password";
    public String NameError="Please enter organisation name";
    public String registrationNumberError="Please enter registration number";
    public String landLineError="Please enter landlines number";
    public String emailNullError="Please enter email";
    public String emailInvalidError="Please enter correct email";
    public String passwordError="Please enter password";
    public String btnSignUp="Sign Up";
}
