package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class Headers {

    public String login="Login";
    public String userType="User Type";
    public String forgotPassword="Forgot Password";
    public String SignUp="SignUp";
    public String profileDetail="";
    public String otherDetail="Other detail";
    public String otherDetailOrganization="Contact detail";
    public String availability_and_event="Availability";
    public String activity="Activity";
    public String events="Event";
    public String top_user="Top User";
    public String Profile="Profile";
    public String other_profile="Other Profile";
    public String settings="Settings";
    public String notification_settings="Notification Settings";
    public String eventDetails="Event Details";
    public String Search="Search";
    public String Filter="Filter";
    public String addArticle="Add Article";
    public String addEventPhoto="Add Event Photos";
}
