package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Avinash on 11/14/2017.
 */

public class SignUpAvailabilityProfessional {

    public String active="Active";
    public String activeDescription="(When you’re out of station then set it to off so Organization won’t request you when you’re not available. Make sure you turn it on when you’re available again.)";
    public String maximumEventRequest="Maximum event request";
    public String inWeek="(in a week)";
    public String availability="Availability";
    public String availabilityDescription="You can repeat added time for other days of the week, select days below to repeat time:";
    public String timeOverLapError="Time you entered overlapping with existing time for same day, please select any other time";
    public String timeIntervalError="Time interval should be more than 2 hours.";
    public String monDay="Monday";
    public String tuesday="Tuesday";
    public String wednesday="Wednesday";
    public String thursday="Thursday";
    public String friday="Friday";
    public String saturday="Saturday";
    public String sunday="Sunday";


}
