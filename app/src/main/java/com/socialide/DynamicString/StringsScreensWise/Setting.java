package com.socialide.DynamicString.StringsScreensWise;

/**
 * Created by Akram on 11/14/2017.
 */

public class Setting {
    public String editProfile = "Edit Profile";
    public String generalDetails = "General Details";
    public String generalDetailsBottom = "Date of Birth,Gender,Country,State,City";
    public String personalDetails = "Personal Details";
    public String personalDetailsBottom = "Current company,Total Experience,Work Category,Work Subcategory,About you";
    public String Information = "information";
    public String contactDetails = "Contact Details";
    public String contactDetailsBottom = "First Name, Last Name, Designation, Mobile Number, Write about Organization";
    public String notificationSetting = "Notification Setting";

    public String howItWorks = "How it works?";
    public String vision = "Vision";
    public String aboutUs = "About us";
    public String contactUs = "Contact us";
    public String TermsAndCondition = "Terms & Condition";
    public String privacyPolicy = "Privacy policy";

    public String logout = "Logout";

}
